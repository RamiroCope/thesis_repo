# README #

This repository contains relevant code and articles for the M.Sc. thesis project Bayesian Deep Learning.

### Owners:

* Peter Holler Langhorn
* Ramiro Mata

## Requirements:

- Any working Python environment (We have been using python ver. 3.6.0)

- Pytorch v0.4

        - to install do: conda install pytorch torchvision -c pytorch

- BayesianOptimization

        - to install do: pip install bayesian-optimization

- Torchvision

         Should be installed by default when installing pyTorch. If not do: pip install torchvision

### All relevant code can be found in Main.

We place relevant papers to the thesis and also for inspiration in the Papers directory.


The following contains a description of the contents in the Main directory.

#### Benchmarks:

The benchmark data such as Wine and MNIST can be found here as well as scripts for testing different optimizers/models on these data sets.

#### Datasets:

Contains classes pertaining to UCI Repository Datasets that make loading and batching compatible with Pytorch. 

#### HyperparamOpt:

Contains script jobs and results for hyperparameter optimization using Bayesian Optimization for the models in the thesis.

#### Models:

Contains the black-box models using priors such as gaussian, students-t, etc, used in the thesis.

#### Optimizers:

Contains all code relevant to the Optimizers developed in the thesis: Noisy Adam, Noisy KFAC, Vprop. 

#### Simulations:

Contains the scripts used for creating the Simulated Experiments in the thesis. 
