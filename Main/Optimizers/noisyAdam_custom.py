import torch
from torch.optim import Optimizer
import numpy as np

class noisyAdam(Optimizer):
    """Implements Noisy Adam algorithm.

    Proposed by Guodong Zhang et al.
    `<https://arxiv.org/pdf/1712.02390.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        num_examples (integer): number of training examples
        lr (float, optional): learning rate (default: 1e-3)
        betas (float, optinal): decay rates for mean and Fisher updates
            (default: 0.9, 0.999)
        kl_weight (float, optional): weight term added to KL divergence (default: 1)
        prior_var (float, optional): variance of prior distribution (default: 1)
        gamma_ex (float, optional): extrinsic damping term (default: 0)
        return_params (bool, optional): return posterior parameters? (default: False)
    """
    
    def __init__(self,
                 params,
                 num_examples,
                 lr=1e-3,
                 betas=(0.9, 0.999),
                 kl_weight = 1,
                 prior_var = 1,
                 gamma_ex = 0):
        
        defaults = dict(lr=lr, betas=betas, kl_weight = kl_weight,
                        prior_var = prior_var, gamma_ex = gamma_ex,
                        num_examples = num_examples)
        
        super(noisyAdam, self).__init__(params, defaults)
        
        # to store the posterior parameters, mu and sigma
        # used only for plotting KL divergence
        self.posterior_params = {}
        # lambda in pseudo code - the KL weighting term
        self.kl_weight = kl_weight
        # total number of training examples (e.g. for MNIST, num_examples = 60.000)
        self.num_examples = num_examples
        # intrinsic damping term
        self.gamma_in = kl_weight/(num_examples*prior_var)
        # adds the two damping terms. gamma_ex may be zero.                
        self.gamma = self.gamma_in + gamma_ex
        

    def sample(self, training = True, return_params = False):
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    # m in pseudo code
                    state['exp_avg'] = torch.zeros_like(p)
                    # Exponential moving average of squared gradient values
                    # f in pseudo code
                    state['exp_avg_sq'] = torch.ones_like(p.data) * (1.0/0.001 - 1.0/group['prior_var']) / self.num_examples#torch.zeros_like(p)
                    
                    # value initialization for mu parameter
                    if len(p.size()) > 1:
                        stdv = 1.0/np.sqrt(p.size(1))
                    else:                        
                        stdv = 1.0/np.sqrt(p.size(0))
                        
                    # mu - the mean of the variational posterior
                    state['mu'] = torch.Tensor(p.size()).uniform_(-stdv, stdv)
                    #state['mu'] = torch.Tensor(p.size()).uniform_(-0.1, 0.1)
                
                # mean parameter of variational posterior
                mu = state['mu']
                
                # when testing, weights are the mean of the variational posterior
                if not training:
                    p.data = mu
                    return
                
                # Fisher approximation, i.e. variance
                f = state['exp_avg_sq']
        
                # noise term
                e = torch.randn(p.size())
                
                # damped precision                
                f_damped = 1/(group['num_examples']/group['kl_weight']*f + 1/group['prior_var'])
                
                # pseudo code line 1
                # sample from variational posterior
                p.data = mu.add(e*f_damped.sqrt())
                
                if return_params:
                    
                    # standard deviation of variational posterior
                    self.posterior_params["mu{0}".format(j)] = mu
                    self.posterior_params["sigma{0}".format(j)] = f_damped.sqrt()
                    self.posterior_params["f{0}".format(j)] = f
                
        if return_params:
            return self.posterior_params
        
        return

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue

                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, \
                                       please consider SparseAdam instead')

                state = self.state[p]

                # pseduo code line 2 - Get modified gradient
                # Note the difference in sign from pseudo code.
                # This is because we minimize instead of maximize ELBO.
                v = grad + self.gamma_in*state['mu']
                
                # get m, f, beta_1, beta_2 as in pseudo code
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1, beta2 = group['betas']
                mu = state['mu']

                # update step
                state['step'] += 1

                # pseudo code line 3: Updated biased first moment estimate
                # decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(1 - beta1, v)
                
                # pseudo code line 4: Update biased second raw moment estimate
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)

                # pseudo code line 5 & 6
                bias_correction1 = 1 - beta1 ** state['step']
                bias_correction2 = 1 - beta2 ** state['step']
                
                m_hat = exp_avg.div(bias_correction1)
                f_hat = exp_avg_sq.div(bias_correction2)

                # pseudo code line 7
                # update parameter
                mu.data.addcdiv_(-group['lr'], m_hat, f_hat.sqrt() + self.gamma)

        return loss
    
    
