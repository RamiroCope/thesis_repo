import numpy as np
import pandas as pd
import torch
import os, inspect
from sklearn.model_selection import train_test_split


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))


class data(object):  
    
    
    
    def __init__(self, X_train, X_test, y_train, y_test, train = True):
        
        #data = pd.read_csv(currentdir + "/winequality-red.csv", sep=";")
        #data = pd.read_csv("D:\\DTU MMC\\Github Repos\\thesis_repo\\Main\\Datasets\\Regression\\winequality-red.csv", sep=";")
        
        self.train = train
        
        # we drop target col
        #X = data.drop(['quality'], axis=1).as_matrix()
      
        
        # normalize data
        #X -= np.mean(X,axis=0)
        #X /= np.var(X, axis=0)
        
        # we set quality as the target
        #y = data['quality'].as_matrix()
        
        #X_train, X_test, y_train, y_test = train_test_split(x, y, test_size = test_size, random_state = 0)
        
        # normalization constants
        X_train_mean = np.mean(X_train,axis=0)
        X_train_std  = np.std(X_train, axis=0)
        y_train_mean = np.mean(y_train,axis=0)
        y_train_std  = np.std(y_train, axis=0)
        
        # normalize data
        X_train = (X_train - X_train_mean) / X_train_std
        X_test  = (X_test - X_train_mean)  / X_train_std
        y_train = (y_train - y_train_mean) / y_train_std
        y_test  = (y_test - y_train_mean)  / y_train_std

        self.y_train_mean = y_train_mean
        self.y_train_std  = y_train_std
        
        if train:
        
            # Convert to proper tensors
            if X_train.shape[1] > 1:
                self.train_data = torch.from_numpy(X_train).float()
                self.train_labels = torch.from_numpy(y_train.reshape(-1,1)).float()
            else:
                self.train_data = torch.from_numpy(X_train.reshape(-1,1)).float()
                self.train_labels = torch.from_numpy(y_train.reshape(-1,1)).float()
            
            #print('Number of features: ', X.shape[1])
            #print('Number of instances: ', X.shape[0])

        else:
            
            if X_test.shape[1] > 1:
                self.test_data = torch.from_numpy(X_test).float()
                self.test_labels = torch.from_numpy(y_test.reshape(-1,1)).float()
            else:
                self.test_data = torch.from_numpy(X_test.reshape(-1,1)).float()
                self.test_labels = torch.from_numpy(y_test.reshape(-1,1)).float()
            
    def get_target_normalization_constants(self):
        return self.y_train_mean, self.y_train_std
    
    
    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        if self.train:
            data, target = self.train_data[index], self.train_labels[index]
        else:
            data, target = self.test_data[index], self.test_labels[index]
            
        return data, target

            
    def __len__(self):
        if self.train:
            return len(self.train_data)
        else:
            return len(self.test_data)

