import numpy as np
import pandas as pd
import torch
import os, inspect
from sklearn.model_selection import train_test_split


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

class year(object):  
    
     
    def __init__(self, train = True, test_size = 0.1):
        
        data = pd.read_csv(currentdir +"/YearPredictionMSD.txt", header = None)
        
        self.train = train
        
        # we drop target col
        X = data.drop(0, axis=1).astype(dtype='float32').as_matrix()
        
        # we set year as the target
        y = data[0].astype(dtype='float32').as_matrix()
        
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = test_size, random_state = 0)

        
        # we take the 1st 463,715 rows as train
        # as specified by the UCI repository
        #X_train = X.iloc[0:463715,:].as_matrix()
        #X_test  = X.iloc[463715:,:].as_matrix()
        #y_train = y.iloc[0:463715].as_matrix().astype(dtype='float32')
        #y_test  = y.iloc[463715:].as_matrix().astype(dtype='float32')
        
       
        # normalization constants
        X_train_mean = np.mean(X_train,axis=0)
        X_train_std  = np.std(X_train, axis=0)
        y_train_mean = np.mean(y_train,axis=0)
        y_train_std  = np.std(y_train, axis=0)
        
        # normalize data
        X_train = (X_train - X_train_mean) / X_train_std
        X_test  = (X_test - X_train_mean)  / X_train_std
        y_train = (y_train - y_train_mean) / y_train_std
        y_test  = (y_test - y_train_mean)  / y_train_std

        self.y_train_mean = y_train_mean
        self.y_train_std  = y_train_std
        
        
        if train:
        
            # Convert to proper tensors
            self.train_data = torch.from_numpy(X_train).float()
            self.train_labels = torch.from_numpy(y_train.reshape(-1,1)).float()
            print('Number of features: ', X.shape[1])
            print('Number of instances: ', X.shape[0])

        else:

            self.test_data = torch.from_numpy(X_test).float()
            self.test_labels = torch.from_numpy(y_test.reshape(-1,1)).float()
    
    def get_target_normalization_constants(self):
        return self.y_train_mean, self.y_train_std
            
    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        if self.train:
            data, target = self.train_data[index], self.train_labels[index]
        else:
            data, target = self.test_data[index], self.test_labels[index]
            
        return data, target

            
    def __len__(self):
        if self.train:
            return len(self.train_data)
        else:
            return len(self.test_data)