import numpy as np
from urllib.request import urlopen
from sklearn.datasets import load_svmlight_file
import torch
from sklearn.model_selection import train_test_split

class ausScale(object):  
     
    def __init__(self, train = True, test_size = 0.5):

        self.train = train
        
        australian_scale = urlopen("https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/australian_scale")
        
        X, y = load_svmlight_file(australian_scale)
        X = X.toarray() 
        
         # normalize data
        X -= np.mean(X,axis=0)
        X /= np.var(X, axis=0)
        
        for i in range(0,len(y)):
            if y[i] < 0:
                y[i] = 0
        
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state = 0)
        
         
        if train:
        
            # Convert to proper tensors
            self.train_data = torch.from_numpy(X_train).float()
            self.train_labels = torch.from_numpy(y_train).long()

        else:

            self.test_data = torch.from_numpy(X_test).float()
            self.test_labels = torch.from_numpy(y_test).long()
            
            
    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        if self.train:
            data, target = self.train_data[index], self.train_labels[index]
        else:
            data, target = self.test_data[index], self.test_labels[index]
            
        return data, target

            
    def __len__(self):
        if self.train:
            return len(self.train_data)
        else:
            return len(self.test_data)