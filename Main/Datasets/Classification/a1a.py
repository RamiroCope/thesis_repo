from urllib.request import urlopen
from sklearn.datasets import load_svmlight_file
import numpy as np
import torch

class a1a(object):
    
    """
        This class loads and processes the a1a data to be used in torchvisions
        Dataloader class
    """
    
    def __init__(self, train = True):

        self.train = train
        
        # below could and maybe should be replaced with text files since
        # opening url and downloading each time takes too long
        # get train and test data
        a1a_train = urlopen("https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/a1a")
        a1a_test = urlopen("https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/a1a.t")
        
        # load data, convert to array and replace -1 labels with 0
        X_train, y_train = load_svmlight_file(a1a_train)
        X_train = X_train.toarray()
        
        for i in range(0,len(y_train)):
            if y_train[i] < 0:
                y_train[i] = 0
        
        # is below really necessary?
        # add missing features in sparse matrix
        missing_features = 4
        X_train = np.c_[X_train, np.zeros((len(X_train),missing_features))]

        # load data, convert to array and replace -1 labels with 0
        X_test, y_test = load_svmlight_file(a1a_test)
        X_test = X_test.toarray()
        
        for i in range(0,len(y_test)):
            if y_test[i] < 0:
                y_test[i] = 0
                
                
        if train:
        
            # Convert to proper tensors
            self.train_data = torch.from_numpy(X_train).float()
            self.train_labels = torch.from_numpy(y_train).long()

        else:
            # Convert to proper tensors
            self.test_data = torch.from_numpy(X_test).float()
            self.test_labels = torch.from_numpy(y_test).long()
            
            
    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        if self.train:
            data, target = self.train_data[index], self.train_labels[index]
        else:
            data, target = self.test_data[index], self.test_labels[index]
            
        return data, target

            
    def __len__(self):
        if self.train:
            return len(self.train_data)
        else:
            return len(self.test_data)