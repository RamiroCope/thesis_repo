
# TWO GAUSSIANS PLOT
# EUCLIDEAN DISTANCE BETWEEN THEM INAPPROPRIATE POINT TO BE MADE

import numpy as np
import matplotlib.pyplot as plt
import matplotlib 
from scipy.stats import norm

# set font size
matplotlib.rcParams.update({'font.size':14})

# Plot between -10 and 10 with .001 steps.
x_axis = np.arange(-10, 10, 0.001)

fig = plt.figure(figsize=(8, 6), facecolor='w', edgecolor='k')
#fig, ax = plt.subplots(nrows=2, ncols=1)

plt.subplot(2,1,1)
plt.plot(x_axis, norm.pdf(x_axis,-2,1))
plt.plot(x_axis, norm.pdf(x_axis,2,1))
plt.axvline(x=-2, linestyle='dashed')
plt.axvline(x=2, linestyle='dashed')
plt.ylabel('p(x)')

plt.subplot(2,1,2)
plt.plot(x_axis, norm.pdf(x_axis,2,3))
plt.plot(x_axis, norm.pdf(x_axis,-2,3))

plt.axvline(x=-2, linestyle='dashed')
plt.axvline(x=2, linestyle='dashed')
plt.xlabel('x')
plt.ylabel('p(x)')
plt.show()