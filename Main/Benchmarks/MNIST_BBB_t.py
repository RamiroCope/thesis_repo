"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
from torch import optim
from Main.Models.BBB_t import BNN


"""
    Load data
"""


batch_size = 100


# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=True, download=False, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=False, download=False, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)


"""
    Define the neural net
"""

# Hierarchical setup collapses to studen t
# df = 2*alpha, scale = sqrt(alpha * beta)
#
prior_prec = 3.0
prec_init = 500.0

df_prior = 6.0
df_init = 6.0

# initialize network    
net = BNN(input_size = 784,
              hidden_sizes = [200,200],
              output_size = 10,
              act_func = "Relu",
              df_prior = df_prior,
              df_init = df_init,
              prior_prec = prior_prec,
              prec_init = prec_init)


"""
    Define loss and optimizer
"""


# loss
criterion = nn.CrossEntropyLoss()

# N as in pseudo code for noisy Adam is the total number of observations
N = trainset.train_data.shape[0]

# optimizer
optimizer = optim.Adam(net.parameters(),
                          lr = 1e-3,
                          betas = (0.9, 0.999))

# setup adaptive learning rate
scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[19,39,59], gamma=0.1)


"""
    Training the network
"""


max_epochs = 80

num_batches = N/batch_size

train_loss = []
test_loss = []
KL_div = []
ELBO_updates = []
updates = []
lr_plt = []


for epoch in range(max_epochs):
    running_loss = 0.0

    for batch, data in enumerate(trainLoader, 0):
        net.train(True)
        inputs, labels = data
        
        optimizer.zero_grad()
        outputs = net(inputs)   
        NLL = criterion(outputs, labels)
        pi = 2**(num_batches - batch + 1) / (2**(num_batches) - 1)
        kl = net.kl_divergence()/N
        ELBO = NLL + kl
        ELBO.backward()
        
        optimizer.step()
        
        # Print statistics
        running_loss += NLL.item()
        
        if batch % 100 == 99:
            
            print('[%d, %5d] ========> train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss / 100))
            KL_div += [kl]
            ELBO_updates += [ELBO.item()]
            updates += [batch + (epoch*num_batches)]
            train_loss += [running_loss/100]
            
            for data in testLoader:
                net.train(False)
                images, labels = data
                outputs = net(images, training = False)
                test_loss_val = criterion(outputs, labels)
                test_loss += [test_loss_val.item()]
            print('[%d, %5d] =========> test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val.item()))
            
            print("[%d, %5d] =====> KL divergence: %.3f" %
                  (epoch + 1, batch + 1, kl))
            
            running_loss = 0.0
      
    scheduler.step()
    
print('Finished training')    


"""
    Plotting
"""


updates = [x / num_batches for x in updates]

fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(9,8))
fig.tight_layout(h_pad = 5, w_pad = 3,pad=4)

plt.subplot(2,2,1) 
plt.plot(updates, train_loss)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("train loss", fontsize=12)
plt.title("train loss")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)

plt.subplot(2,2,2)
plt.plot(updates, test_loss)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("test loss", fontsize=12)
plt.title("test loss")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)

plt.subplot(2,2,3)
plt.plot(updates, KL_div)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("KL Divergence", fontsize=12)
plt.title("KL Divergence")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)

plt.subplot(2,2,4)
plt.plot(updates, ELBO_updates)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("ELBO", fontsize=12)
plt.title("ELBO")

plt.subplots_adjust(top=0.85)
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
plt.savefig("t_dist_statistics.eps")
plt.show()


"""
    Accuracy of fitted model
"""

# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    net.train(False)
    images, labels = data
    outputs = net(images, training = False)
    _, predicted = torch.max(outputs, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %.3f %%' % (
    100 * correct.item() / total))