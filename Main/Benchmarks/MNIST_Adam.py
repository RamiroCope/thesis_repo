"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from Main.Optimizers.noisyKFAC_closure import noisyKFAC
from Main.Optimizers.noisyAdam_closure_custom import noisyAdam
from Main.Optimizers.Vprop_closure import Vprop
import torch.optim as optim
from Main.Models.BBB import BNN
import numpy as np
import time


"""
Load data
"""


batch_size = 100

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=True, download=False, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=False, download=False, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)
    
N = trainset.train_data.shape[0]

"""
    Define neural networks
"""


class FNN(nn.Module):
    
    def __init__(self,H1,bias = True):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1, bias = bias)
        self.NN2 = nn.Linear(H1,10, bias = bias)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x

# Initialize neural nets
net_Adam = FNN(100, bias = False)




"""
    Loss and optimizers
"""


criterion = nn.CrossEntropyLoss()

# Note that KFAC typically needs a warmup time with a very low learning rate
# for a number of epochs, before turning up the learning rate.


optimizer_Adam = optim.Adam(net_Adam.parameters(), lr = 1e-3)


"""
    Training the network
"""

# choose how many epochs the noisy KFAC network should run warm_up
max_epochs = 50
num_batches = N/batch_size

# indices for accessing weight matrices of the different layers

train_loss_Adam = []
test_loss_Adam = []


KL_div = []
ELBO = []
updates = []
lr_plt = []


t0 = time.time()

for epoch in range(max_epochs):
    
    running_loss_Adam      = 0.0

    for batch, data in enumerate(trainLoader, 0):
        
        inputs, labels = data
#        inputs = inputs.cuda()
#        labels = labels.cuda()

        
        # For Adam
        optimizer_Adam.zero_grad()
        output_Adam = net_Adam(inputs)
        loss_Adam = criterion(output_Adam, labels)
        loss_Adam.backward()
        optimizer_Adam.step()

        
        # Print statistics
        running_loss_Adam      += loss_Adam.item()
        
        if batch % 100 == 99:
            
            print('[%d, %5d] ===================> Adam train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_Adam / 100))
            
            
            
            updates += [batch + (epoch*num_batches)]
            
            train_loss_Adam += [running_loss_Adam/100]
            
            for data in testLoader:
                images, labels = data
#                images = images.cuda()
#                labels = labels.cuda()
                outputs_Adam      = net_Adam(images)
                
                test_loss_val_Adam      = criterion(outputs_Adam, labels)
                
                test_loss_Adam      += [test_loss_val_Adam.item()]
                
            print('[%d, %5d] ===================> Adam test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_Adam.item()))
    

            running_loss_Adam = 0.0
    
print('Finished training')    

t1 = time.time()

total = t1-t0

print("training Adam on MNIST took %.2f seconds" % total)
