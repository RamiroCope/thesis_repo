"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from Main.Optimizers.noisyAdam_closure_custom import noisyAdam
import numpy as np
from torch.utils.data import DataLoader
import time

'REGRESSION UCI DATASETS'
from Main.Datasets.Regression.wine import wine as dataset

"""
Load data
"""


batch_size=32

# train data
trainset = dataset()
y_train_mean, y_train_std = trainset.get_target_normalization_constants()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = dataset(train = False)
testLoader = DataLoader(testset, batch_size=160, shuffle=False)

N = trainset.train_data.shape[0]
num_features = trainset.train_data.shape[1]

"""
    Define neural networks
"""


class FNN(nn.Module):
    
    def __init__(self,num_features, H1, bias = False):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=bias)
        self.NN2 = nn.Linear(H1,1, bias=bias)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x

# Initialize neural nets
net_NoisyAdam = FNN(num_features, 50, bias = False)


# For black box
# initialize network    
prior_var = 0.79#0.5305362661717282
prior_prec = 1 /prior_var



"""
    Loss and optimizers
"""


criterion = nn.MSELoss()



prior_var = 0.1
var_init = 0.001

optimizer_NoisyAdam = noisyAdam(net_NoisyAdam.parameters(), num_examples = N,
                          gamma_ex = 0,
                          lr = 1e-3,
                          prior_var = 0.812,#,0.3091957884008956,
                          var_init = var_init,
                          kl_weight = 0.275,#0.7248238502772228,
                          betas = (0.9, 0.999))



"""
    Training the network
"""

# choose how many epochs the noisy KFAC network should run warm_up
warm_up = 1
max_epochs = 50

num_batches = N/batch_size

# indices for accessing weight matrices of the different layers
indices = [0,1,2]

train_loss_NoisyAdam = []
test_loss_NoisyAdam = []



KL_div = []
ELBO = []
updates = []
lr_plt = []


t0 = time.time()

for epoch in range(max_epochs):
    

    running_loss_NoisyAdam = 0.0

    for batch, data in enumerate(trainLoader, 0):
        
        inputs, targets = data
#        inputs = inputs.cuda()
#        labels = labels.cuda()
        
       
        
        # For noisyAdam
        def closure():
            optimizer_NoisyAdam.zero_grad()
            outputs = net_NoisyAdam(inputs) 
            loss = criterion(outputs, targets)
            loss.backward()
            return loss
        
        LL_loss_NoisyAdam, _ = optimizer_NoisyAdam.step(closure, return_params = False)
        
       
        running_loss_NoisyAdam += LL_loss_NoisyAdam.item()
        
        if batch % 10 == 9:

            
            print('[%d, %5d] ========> NoisyAdam train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_NoisyAdam / 10))
            

            
            
            updates += [batch + (epoch*num_batches)]
            
            train_loss_NoisyAdam += [running_loss_NoisyAdam/10]
            
            for data in testLoader:
                images, labels = data
#                images = images.cuda()
#                labels = labels.cuda()
                outputs_NoisyAdam = net_NoisyAdam(images)
                
                test_loss_val_NoisyAdam = criterion(outputs_NoisyAdam, labels)
                
                test_loss_NoisyAdam += [test_loss_val_NoisyAdam.item()]
                
            print('[%d, %5d] ========> NoisyAdam test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_NoisyAdam.item()))
            running_loss_NoisyAdam = 0.0
    
print('Finished training')    


t1 = time.time()

total = t1-t0

print("total runtime for noisy Adam on Wine: %.2f seconds" % total)
