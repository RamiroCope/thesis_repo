"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from Main.Optimizers.noisyKFAC_closure import noisyKFAC
from Main.Optimizers.noisyAdam_closure_custom import noisyAdam
from Main.Optimizers.Vprop_closure import Vprop
import torch.optim as optim
from Main.Models.BBB import BNN
import numpy as np
from torch.utils.data import DataLoader

'REGRESSION UCI DATASETS'
from Main.Datasets.Regression.wine import wine as dataset

"""
Load data
"""


batch_size=32

# train data
trainset = dataset()
y_train_mean, y_train_std = trainset.get_target_normalization_constants()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = dataset(train = False)
testLoader = DataLoader(testset, batch_size=160, shuffle=False)

N = trainset.train_data.shape[0]
num_features = trainset.train_data.shape[1]

"""
    Define neural networks
"""


class FNN(nn.Module):
    
    def __init__(self,num_features, H1, bias = False):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=bias)
        self.NN2 = nn.Linear(H1,1, bias=bias)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x

# Initialize neural nets
net_KFAC = FNN(num_features, 50, bias = False)
net_NoisyAdam = FNN(num_features, 50, bias = False)
net_Vprop = FNN(num_features, 50, bias = False)
net_Adam = FNN(num_features, 50, bias = False)

# For black box
# initialize network    
prior_var = 0.79#0.5305362661717282
prior_prec = 1 /prior_var
net_BBB = BNN(input_size = num_features,
                  hidden_sizes = [50],
                  output_size = 1,
                  act_func = "Relu",
                  prior_prec = prior_prec,
                  prec_init = 500)


"""
    Loss and optimizers
"""


criterion = nn.MSELoss()

# Note that KFAC typically needs a warmup time with a very low learning rate
# for a number of epochs, before turning up the learning rate.
optimizer_KFAC = noisyKFAC(net_KFAC.parameters(),
                      net_KFAC.modules(),
                      num_examples = N,
                      t_inv = 100,
                      t_stats = 10,
                      lr = 1e-6,
                      beta = 0.99,
                      prior_var =  0.37,#0.6410636415821905,
                      kl_weight = 1.0)#0.10778217913428191)

prior_var = 0.1
var_init = 0.001

optimizer_NoisyAdam = noisyAdam(net_NoisyAdam.parameters(), num_examples = N,
                          gamma_ex = 0,
                          lr = 1e-3,
                          prior_var = 0.812,#,0.3091957884008956,
                          var_init = var_init,
                          kl_weight = 0.275,#0.7248238502772228,
                          betas = (0.9, 0.999))

optimizer_Vprop = Vprop(net_Vprop.parameters(), num_examples = N,
                          lr = 1e-3,
                          alpha = 0.99,
                          prior_var = 0.088,#0.04388867098517026,                          
                          var_init = var_init)

optimizer_Adam = optim.Adam(net_Adam.parameters(), lr = 1e-3)

optimizer_BBB = optim.Adam(net_BBB.parameters(), lr = 1e-3)

"""
    Training the network
"""

# choose how many epochs the noisy KFAC network should run warm_up
warm_up = 1
max_epochs = 50

num_batches = N/batch_size

# indices for accessing weight matrices of the different layers
indices = [0,1,2]

train_loss_KFAC = []
test_loss_KFAC = []
train_loss_NoisyAdam = []
test_loss_NoisyAdam = []
train_loss_Vprop = []
test_loss_Vprop = []
train_loss_Adam = []
test_loss_Adam = []
train_loss_BBB = []
test_loss_BBB = []


KL_div = []
ELBO = []
updates = []
lr_plt = []

for epoch in range(max_epochs):
    
    if epoch == warm_up:
        for param_group in optimizer_KFAC.param_groups:
            param_group['lr'] = 1e-3

    running_loss_KFAC      = 0.0
    running_loss_NoisyAdam = 0.0
    running_loss_Vprop     = 0.0
    running_loss_Adam      = 0.0
    running_loss_BBB       = 0.0

    for batch, data in enumerate(trainLoader, 0):
        
        inputs, targets = data
#        inputs = inputs.cuda()
#        labels = labels.cuda()
        
        # For KFAC
        def closure():
            optimizer_KFAC.zero_grad()
            outputs = net_KFAC(inputs) 
            loss = criterion(outputs, targets)
            loss.backward()
            return loss
        
        LL_loss_KFAC, _ = optimizer_KFAC.step(closure, return_params = False)
        
        # For noisyAdam
        def closure():
            optimizer_NoisyAdam.zero_grad()
            outputs = net_NoisyAdam(inputs) 
            loss = criterion(outputs, targets)
            loss.backward()
            return loss
        
        LL_loss_NoisyAdam, _ = optimizer_NoisyAdam.step(closure, return_params = False)
        
        # For Vprop
        def closure():
            optimizer_Vprop.zero_grad()
            outputs = net_Vprop(inputs) 
            loss = criterion(outputs, targets)
            loss.backward()
            return loss
        
        LL_loss_Vprop, _ = optimizer_Vprop.step(closure, return_params = False)
        
        # For Adam
        optimizer_Adam.zero_grad()
        output_Adam = net_Adam(inputs)
        loss_Adam = criterion(output_Adam, targets)
        loss_Adam.backward()
        optimizer_Adam.step()
        
        # For BBB
        optimizer_BBB.zero_grad()
        output_BBB = net_BBB(inputs)
        LL_loss_BBB = criterion(output_BBB, targets)
        KL_loss = net_BBB.kl_divergence()/N#num_batches
        loss_BBB = LL_loss_BBB + KL_loss
        loss_BBB.backward()
        optimizer_BBB.step()
        
        # Print statistics
        running_loss_KFAC      += LL_loss_KFAC.item()
        running_loss_NoisyAdam += LL_loss_NoisyAdam.item()
        running_loss_Vprop     += LL_loss_Vprop.item()
        running_loss_Adam      += loss_Adam.item()
        running_loss_BBB       += LL_loss_BBB.item()#loss_BBB.item()
        
        if batch % 10 == 9:
            
            print('[%d, %5d] =====> NoisyKFAC train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_KFAC / 10))
            
            print('[%d, %5d] ========> NoisyAdam train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_NoisyAdam / 10))
            
            print('[%d, %5d] ===============> Vprop train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_Vprop / 10))
            
            print('[%d, %5d] ===================> Adam train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_Adam / 10))
            
            print('[%d, %5d] =======================> BBB train loss: %.3f \n' %
                  (epoch + 1, batch + 1, running_loss_BBB / 10))
            
            
            updates += [batch + (epoch*num_batches)]
            
            train_loss_KFAC += [running_loss_KFAC/10]
            train_loss_NoisyAdam += [running_loss_NoisyAdam/10]
            train_loss_Vprop += [running_loss_Vprop/10]
            train_loss_Adam += [running_loss_Adam/10]
            train_loss_BBB += [running_loss_BBB/10]
            
            for data in testLoader:
                images, labels = data
#                images = images.cuda()
#                labels = labels.cuda()
                outputs_KFAC = net_KFAC(images)
                outputs_NoisyAdam = net_NoisyAdam(images)
                outputs_Vprop = net_Vprop(images)
                outputs_Adam = net_Adam(images)
                outputs_BBB = net_BBB(images, training = False)
                
                test_loss_val_KFAC = criterion(outputs_KFAC, labels)
                test_loss_val_NoisyAdam = criterion(outputs_NoisyAdam, labels)
                test_loss_val_Vprop = criterion(outputs_Vprop, labels)
                test_loss_val_Adam = criterion(outputs_Adam, labels)
                test_loss_val_BBB = criterion(outputs_BBB, labels)
                
                test_loss_KFAC += [test_loss_val_KFAC.item()]
                test_loss_NoisyAdam += [test_loss_val_NoisyAdam.item()]
                test_loss_Vprop += [test_loss_val_Vprop.item()]
                test_loss_Adam += [test_loss_val_Adam.item()]
                test_loss_BBB += [test_loss_val_BBB.item()]
                
            print('[%d, %5d] =====> NoisyKFAC test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_KFAC.item()))
            print('[%d, %5d] ========> NoisyAdam test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_NoisyAdam.item()))
            print('[%d, %5d] ===============> Vprop test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_Vprop.item()))
            print('[%d, %5d] ===================> Adam test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_Adam.item()))
            print('[%d, %5d] =======================> BBB test loss: %.3f \n' %
                  (epoch + 1, batch + 1, test_loss_val_BBB.item()))
    
            running_loss_KFAC = 0.0
            running_loss_NoisyAdam = 0.0
            running_loss_Vprop = 0.0
            running_loss_Adam = 0.0
            running_loss_BBB = 0.0
    
print('Finished training')    


"""
    Plotting
"""


# Convert updates to epochs
updates = [x / num_batches for x in updates]


fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(7,6))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,1,1)
 
kfac,  = plt.plot(updates[2:], train_loss_KFAC[2:], '-.', label = 'Noisy KFAC')
nadam, = plt.plot(updates, train_loss_NoisyAdam, '--', label = 'Noisy Adam')
vprop, = plt.plot(updates, train_loss_Vprop, ':', label = 'Vprop')
adam,  = plt.plot(updates, train_loss_Adam, label = 'Adam')
bbb,   = plt.plot(updates, train_loss_BBB, '-.', label = 'BBB')

plt.legend(handles=[kfac, nadam, vprop, adam, bbb])
plt.xlabel("Epochs",fontsize=14), plt.ylabel("MSE train loss", fontsize=14)
plt.title("Mean Squared Error Training loss on Wine dataset", fontsize = 14)


plt.subplot(2,1,2)

kfac_test,  = plt.plot(updates, test_loss_KFAC, '-.', label = 'Noisy KFAC')
nadam_test, = plt.plot(updates, test_loss_NoisyAdam, '--', label = 'Noisy Adam')
vprop_test, = plt.plot(updates, test_loss_Vprop, ':', label = 'Vprop')
adam_test,  = plt.plot(updates, test_loss_Adam, label = 'Adam')
bbb_test,   = plt.plot(updates, test_loss_BBB, '-.', label = 'BBB')

plt.legend(handles = [kfac_test, nadam_test, vprop_test, adam_test, bbb_test])
plt.xlabel("Epochs", fontsize = 14), plt.ylabel("MSE test loss", fontsize = 14)
plt.title("Mean Squared Error Test loss on Wine dataset", fontsize = 14)

#
#plt.subplot(2,2,3)
#plt.plot(updates, KL_div)
#plt.xlabel("Updates"), plt.ylabel("KL Divergence")
#plt.title("Noisy Adam KL Divergence")
#
#
#plt.subplot(2,2,4)
#plt.plot(updates, ELBO)
#plt.xlabel("Updates"), plt.ylabel("ELBO")
#plt.title("Noisy Adam ELBO")
plt.savefig('..\\Results_and_figs\\WINE_traintest_losses.eps', dpi=300)
plt.show()

"""
    Accuracy of fitted model
"""


# Test on entire test set
error_KFAC = []
error_NoisyAdam = []
error_Vprop = []
error_Adam = []
error_BBB = []
total = 0

for data in testLoader:
    x, y = data
#    images = images.cuda()
#    labels = labels.cuda()
    outputs_KFAC = net_KFAC(x)
    outputs_NoisyAdam = net_NoisyAdam(x)
    outputs_Vprop = net_Vprop(x)
    outputs_Adam = net_Adam(x)
    outputs_BBB = net_BBB(x)
    
    error_KFAC      += [(outputs_KFAC.data - y.data).numpy()]
    error_NoisyAdam += [(outputs_NoisyAdam.data - y.data).numpy()]
    error_Vprop     += [(outputs_Vprop.data - y.data).numpy()]
    error_Adam      += [(outputs_Adam.data - y.data).numpy()]
    error_BBB       += [(outputs_BBB.data - y.data).numpy()]

error_KFAC = np.array([x for ls in error_KFAC for x in ls])
error_NoisyAdam = np.array([x for ls in error_NoisyAdam for x in ls])
error_Vprop = np.array([x for ls in error_Vprop for x in ls])
error_Adam = np.array([x for ls in error_Adam for x in ls])
error_BBB = np.array([x for ls in error_BBB for x in ls])

normalized_RMSE_KFAC  = np.mean(error_KFAC**2)**0.5
normalized_RMSE_NoisyAdam  = np.mean(error_NoisyAdam**2)**0.5
normalized_RMSE_Vprop  = np.mean(error_Vprop**2)**0.5
normalized_RMSE_Adam  = np.mean(error_Adam**2)**0.5
normalized_RMSE_BBB  = np.mean(error_BBB**2)**0.5

print('Normalized RMSE with Noisy KFAC on the Wine dataset: %.3f ' % (
    normalized_RMSE_KFAC.item()))
print('Normalized RMSE with Noisy Adam on the Wine dataset: %.3f ' % (
    normalized_RMSE_NoisyAdam.item()))
print('Normalized RMSE with Vprop on the Wine dataset: %.3f ' % (
    normalized_RMSE_Vprop.item()))
print('Normalized RMSE with Adam on the Wine dataset: %.3f ' % (
    normalized_RMSE_Adam.item()))
print('Normalized RMSE with BBB on the Wine dataset: %.3f ' % (
    normalized_RMSE_BBB.item()))




KFAC_l += [normalized_RMSE_KFAC.item()]
nadam_l += [normalized_RMSE_NoisyAdam.item()]
vprop_l += [normalized_RMSE_Vprop.item()]
adam_l += [normalized_RMSE_Adam.item()]
bbb_l += [normalized_RMSE_BBB.item()]





KFAC_l = []
nadam_l = []
vprop_l = []
adam_l = []
bbb_l = []

np.mean(KFAC_l)
np.std(KFAC_l)
np.mean(nadam_l)
np.std(nadam_l)
np.mean(vprop_l)
np.std(vprop_l)
np.mean(bbb_l)
np.std(bbb_l)
np.mean(adam_l)
np.std(adam_l)
