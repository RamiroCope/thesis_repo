
import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

from bayes_opt import BayesianOptimization
from Main.Optimizers.noisyKFAC_closure import noisyKFAC as noisyAdam

import time
import torch
import torch.nn.functional as F
import torch.nn as nn

import torch.nn.init as init
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.distributions import Normal


import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output
import matplotlib.mlab as mlab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from sklearn import preprocessing
from sklearn.model_selection import train_test_split


'REGRESSION UCI DATASETS'
from Main.Datasets.Regression.wine import wine as dataset



"""Our Model"""
class FNN(nn.Module):
    
    def __init__(self,num_features, H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=False)
        self.NN2 = nn.Linear(H1,1, bias=False)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x
    
    



"""Data Preparation"""

batch_size=32

# train data
trainset = dataset()
y_train_mean, y_train_std = trainset.get_target_normalization_constants()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = dataset(train = False)
testLoader = DataLoader(testset, batch_size=batch_size, shuffle=False)

N = trainset.train_data.shape[0]
num_features = trainset.train_data.shape[1]

"""Hyper-parameters to Optimize"""

net = FNN(num_features, 50)

criterion = nn.MSELoss()
optimizer = noisyAdam(net.parameters(),
                      net.modules(),
                      num_examples=N,
                      t_inv= 100,
                      t_stats=20,
                      lr = 1e-3,
                      beta = 0.99,
                      prior_var = 0.1,
                      kl_weight = 1.0)

t0 = time.time()

for epoch in range(50):

    for j, data in enumerate(trainLoader, 0):

        inputs, labels = data
        #inputs, labels = Variable(inputs), Variable(labels)

#        mus = []
#        As_inv = []
#        Ss_inv = []
#        weights = []
#        Covs = []

        def closure():
            optimizer.zero_grad()
            outputs = net(inputs) 
            loss = criterion(outputs, labels)
            loss.backward()
            return loss
        
        LL_loss, params = optimizer.step(closure, return_params = False)
#
#        for i, p in enumerate(net.parameters()):
#            sample, A_inv, S_inv = params["sample{0}".format(i)],\
#                        params["A_inv{0}".format(i)],\
#                        params["S_inv{0}".format(i)]
                        
#            mus += [p.data.cpu().view(-1).detach().numpy()]
#            if i == 0:
#                As_inv = A_inv
#                Ss_inv = S_inv
#            weights += [sample.cpu().view(-1).detach().numpy()]  

t1 = time.time()

total = t1-t0

print("noisy KFAC runtime on Wine: %.2f seconds" % total)
        
