"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
from torch.utils.data import DataLoader
import time

'REGRESSION UCI DATASETS'
from Main.Datasets.Regression.wine import wine as dataset

"""
Load data
"""


batch_size=32

# train data
trainset = dataset()
y_train_mean, y_train_std = trainset.get_target_normalization_constants()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = dataset(train = False)
testLoader = DataLoader(testset, batch_size=160, shuffle=False)

N = trainset.train_data.shape[0]
num_features = trainset.train_data.shape[1]

"""
    Define neural networks
"""


class FNN(nn.Module):
    
    def __init__(self,num_features, H1, bias = False):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=bias)
        self.NN2 = nn.Linear(H1,1, bias=bias)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x

# Initialize neural nets
net_Adam = FNN(num_features, 50, bias = False)




"""
    Loss and optimizers
"""


criterion = nn.MSELoss()

# Note that KFAC typically needs a warmup time with a very low learning rate
# for a number of epochs, before turning up the learning rate.


optimizer_Adam = optim.Adam(net_Adam.parameters(), lr = 1e-3)


"""
    Training the network
"""

# choose how many epochs the noisy KFAC network should run warm_up
warm_up = 1
max_epochs = 50

num_batches = N/batch_size

# indices for accessing weight matrices of the different layers
indices = [0,1,2]


train_loss_Adam = []
test_loss_Adam = []



KL_div = []
ELBO = []
updates = []
lr_plt = []

t0 = time.time()

for epoch in range(max_epochs):
    

    running_loss_Adam      = 0.0


    for batch, data in enumerate(trainLoader, 0):
        
        inputs, targets = data
#        inputs = inputs.cuda()
#        labels = labels.cuda()

        
        # For Adam
        optimizer_Adam.zero_grad()
        output_Adam = net_Adam(inputs)
        loss_Adam = criterion(output_Adam, targets)
        loss_Adam.backward()
        optimizer_Adam.step()
        

        
        # Print statistics
        running_loss_Adam      += loss_Adam.item()
        
        
        if batch % 10 == 9:
            
            
            print('[%d, %5d] ===================> Adam train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_Adam / 10))
            
            
            
            updates += [batch + (epoch*num_batches)]
            
            
            train_loss_Adam += [running_loss_Adam/10]
            
            
            for data in testLoader:
                images, labels = data
#                images = images.cuda()
#                labels = labels.cuda(
                outputs_Adam = net_Adam(images)
                test_loss_val_Adam = criterion(outputs_Adam, labels)
                
                test_loss_Adam += [test_loss_val_Adam.item()]
            print('[%d, %5d] ===================> Adam test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_Adam.item()))
            
            running_loss_Adam = 0.0
            
    
print('Finished training')    


t1 = time.time()

total = t1-t0

print("total runtime for adam on Wine: %.2f seconds" % total)