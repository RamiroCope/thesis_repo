"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from Main.Optimizers.Vprop_closure import Vprop
from torch.distributions import Normal
import time

"""
    Load data
"""

batch_size = 100

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=True, download=False, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=False, download=False, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)


"""
    Define the neural net
"""


class FNN(nn.Module):
    
    def __init__(self,H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1)
        self.NN2 = nn.Linear(H1,10)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x

# initialize network    
net = FNN(100)
#net.cuda()


"""
    Define loss and optimizer
"""


# loss
criterion = nn.CrossEntropyLoss()

# N as in pseudo code for noisy Adam is the total number of observations
N = trainset.train_data.shape[0]

prior_var = 0.03
var_init = 0.01

# optimizer
optimizer = Vprop(net.parameters(), num_examples = N,
                          lr = 1e-3,
                          alpha = 0.99,
                          prior_var = prior_var,                          
                          var_init = var_init)

# setup adaptive learning rate
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[30,40], gamma=0.1)
#scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.85)


"""
    Training the network
"""


max_epochs = 50

num_batches = N/batch_size

# define prior distribution for KL divergence computation
p_dist = Normal(0, np.sqrt(prior_var))

# indices for accessing weight matrices of the different layers
indices = [0,2]

train_loss = []
test_loss = []
KL_div = []
ELBO = []
updates = []

t0 = time.time()

for epoch in range(max_epochs):

    running_loss = 0.0
    
    for batch, data in enumerate(trainLoader, 0):
        
        samples = []
        sigmas = []
        mus = []
        fs = []
        
        inputs, labels = data
#        inputs = inputs.cuda()
#        labels = labels.cuda()

        def closure():
            optimizer.zero_grad()
            outputs = net(inputs) 
            loss = criterion(outputs, labels)
            loss.backward()
            return loss
        
        LL_loss, params = optimizer.step(closure, return_params = True)
        
        # extract diagnostics
        lqw, lpw = 0.0, 0.0

        for i, p in enumerate(net.parameters()):
            sample, sigma = params["sample{0}".format(i)],\
                            params["sigma{0}".format(i)]
            
            samples += [sample.cpu().view(-1).detach().numpy()]
            mus += [p.cpu().view(-1).detach().numpy()]
            sigmas += [sigma.cpu().view(-1).detach().numpy()]            
        
            # KL Divergence terms    
            q_dist = Normal(p.data, sigma)
            lqw += q_dist.log_prob(p).sum()
            lpw += p_dist.log_prob(p.cpu()).sum()
    
    
    
        KL_loss = (lqw.data.cpu().numpy() - lpw.data.cpu().numpy())/N
        loss = LL_loss + KL_loss
        
        # Print statistics
        running_loss += LL_loss.item()
        if batch % 100 == 99:
            print('[%d, %5d] ====> train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss / 100))
            updates += [batch + (epoch*num_batches)]
            train_loss += [running_loss/100]
            KL_div += [KL_loss]
            ELBO += [loss.item()]
            
            for data in testLoader:
                images, labels = data
#                images = images.cuda()
#                labels = labels.cuda()
                outputs = net(images)
                test_loss_val = criterion(outputs, labels)
                test_loss += [test_loss_val.item()]
                
            print('[%d, %5d] ====> test loss:  %.3f' %
                  (epoch + 1, batch + 1, test_loss_val.item()))
            
#            fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
#            fig.tight_layout(h_pad = 5, w_pad = 5)
#
#            plt.subplot(2,2,1)
#            #plt.plot(updates, train_loss) # plot something
#            plt.hist([mus[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("mu")
#            plt.title("mu histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2'])
#            
#            plt.subplot(2,2,2)
#            
#            plt.hist([sigmas[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("standard deviation")
#            plt.title("standard deviation histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2'])
#            
#            plt.subplot(2,2,3)
#            
#            plt.hist([samples[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("sampled weights")
#            plt.title("histogram of sampled weights in BNN")
#            plt.legend(['Layer 1','Layer 2'])
#    
#            # update canvas immediately
#            plt.show()
            running_loss = 0.0
#    lr_plt += [scheduler.get_lr()[0]]        
#    scheduler.step()
#    print("=======> lr is: %.10f" % scheduler.get_lr()[0])
    
print('Finished training')    


t1 = time.time()

total = t1-t0

print("training Vprop on MNIST took %.2f seconds" % total)


"""
    Plotting
"""


updates = [x / num_batches for x in updates]

fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(7,6))
fig.tight_layout(h_pad = 5, w_pad = 3)
plt.suptitle("Train and Test statistics for Vprop", fontsize = 14)

plt.subplot(2,2,1) 
plt.plot(updates, train_loss)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("train loss", fontsize=12)
plt.title("Vprop training loss")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
#plt.show()

plt.subplot(2,2,2)
plt.plot(updates, test_loss)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("test loss", fontsize=12)
plt.title("Vprop test loss")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
#plt.show()

plt.subplot(2,2,3)
plt.plot(updates, KL_div)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("KL Divergence", fontsize=12)
plt.title("Vprop KL Divergence")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
#plt.show()

plt.subplot(2,2,4)
plt.plot(updates, ELBO)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("ELBO", fontsize=12)
plt.title("Vprop ELBO")

plt.subplots_adjust(top=0.85)
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
plt.show()


"""
    Accuracy of fitted model
"""


# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
#    images = images.cuda()
#    labels = labels.cuda()
    outputs = net(images)
    _, predicted = torch.max(outputs, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %.3f %%' % (
    100 * correct.item() / total))