"""
    Set working directory
"""
import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.distributions import Normal
import scipy.stats as stats
from torch import optim
from Main.Models.BBB import BNN
import time

#import matplotlib
#matplotlib.use('TkAgg')
#from torch.nn.parameter import Parameter
#from torch.optim import Optimizer
#from torch.distributions import Normal
#from IPython.display import Image, display, clear_output
#import numpy as np


"""
    Load data
"""


    
batch_size = 100

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=True, download=False, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=False, download=False, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)


"""
    Define the neural net
"""


prior_prec = 10.0
prec_init = 500.0


# initialize network    
net = BNN(input_size = 784,
              hidden_sizes = [100],
              output_size = 10,
              act_func = "Relu",
              prior_prec = prior_prec,
              prec_init = prec_init)


def avneg_loglik_categorical(logits, y):
    logloss = F.cross_entropy(logits, y)
    return logloss

def stoch_loglik_categorical(logits, y, train_set_size):
    loglik = - train_set_size * avneg_loglik_categorical(logits, y)
    return loglik

def elbo_categorical(logits, y, train_set_size, kl):
    logliks = stoch_loglik_categorical(logits, y, train_set_size)
#    mc_loglik = mc_loss(pred_list=logits_list, y=y, loss_fn=stoch_loglik_categorical, train_set_size=train_set_size)
    elbo = logliks - kl
    return elbo

def avneg_elbo_categorical(logits, y, train_set_size, kl):
    avneg_elbo = - elbo_categorical(logits, y, train_set_size, kl) / train_set_size
    return avneg_elbo


    

"""
    Define loss and optimizer
"""

# loss
criterion = nn.CrossEntropyLoss(size_average=True)



# N as in pseudo code for noisy Adam is the total number of observations
N = trainset.train_data.shape[0]

# optimizer
optimizer = optim.Adam(net.parameters(), lr = 1e-3)

# setup adaptive learning rate
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[1,29], gamma=0.1)
#scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.90)


"""
    Training the network
"""

t0 = time.time()

max_epochs = 50

num_batches = N/batch_size

# indices for accessing weight matrices of the different layers
indices = [0,2,4]

train_loss = []
test_loss = []
KL_div = []
ELBO_updates = []
updates = []
lr_plt = []


for epoch in range(max_epochs):
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for batch, data in enumerate(trainLoader, 0):
#        net.train(True)
        inputs, labels = data
        
        optimizer.zero_grad()
        outputs = net(inputs)   
        NLL = criterion(outputs, labels)
#        pi = 2**(num_batches - batch + 1) / ( 2**(num_batches) - 1 )
        kl = net.kl_divergence()/N
#        ELBO = NLL + kl/num_batches
#        ELBO = ELBO_loss(outputs, labels, kl)
        ELBO = NLL + kl
        ELBO.backward()
        
        optimizer.step()
        
        # Print statistics
        running_loss += NLL.item()
        
        if batch % 100 == 99:
            
#            mus = []
#            sigmas = []
#            weights = []
#            
#            mu, sigma, weight = net.return_params()
#            
#            mus += [mu]
#            sigmas += [sigma]
#            weights += [weight]
            
            print('[%d, %5d] ====> train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss / 100))
            KL_div += [kl]
            ELBO_updates += [ELBO.item()]
            updates += [batch + (epoch*num_batches)]
            train_loss += [running_loss/100]
            
            for data in testLoader:
#                net.train(False)
                images, labels = data
                outputs = net(images, training = False)
                test_loss_val = criterion(outputs, labels)
#                test_loss_val = avneg_elbo_categorical(outputs, labels, N, net.kl_divergence())
                test_loss += [test_loss_val.item()]
                
            print('[%d, %5d] =====> test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val.item()))
            
            print('============> KL Divergence: %.3f' % (kl))
            
#            mus_flat = [item for sublist in mus for item in sublist]
#            sigmas_flat = [item for sublist in sigmas for item in sublist]
#            weights_flat = [item for sublist in weights for item in sublist]
#            
#            
#            fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
#            fig.tight_layout(h_pad = 5, w_pad = 5)
#            
#            plt.subplot(2,2,1)
#            plt.hist(mus_flat, bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("mu")
#            plt.title("mu histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
##            plt.subplot(2,2,2)
##            plt.hist(dfs_flat, bins = 30, edgecolor='black', normed = True)
##            plt.xlabel("df")
##            plt.title("df histogram of variational posterior")
##            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.subplot(2,2,3)
#            plt.hist(sigmas_flat, bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("sigma")
#            plt.title("sigma histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.subplot(2,2,4)
#            plt.hist(weights_flat, bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("weights")
#            plt.title("weight histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.show()
#            
#            print("=======================================================")
            
            running_loss = 0.0
#    lr_plt += [scheduler.get_lr()[0]]        
#    scheduler.step()
#    print("=======> lr is: %.10f" % scheduler.get_lr()[0])
    
print('Finished training')    
t1 = time.time()

total = t1-t0

print("Training BBB on MNIST took %.2f seconds" % total)

"""
    Plotting
"""


updates = [x / num_batches for x in updates]

fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(7,6))
fig.tight_layout(h_pad = 5, w_pad = 3)
plt.suptitle("Train and Test statistics for BBB", fontsize = 12)

plt.subplot(2,2,1) 
plt.plot(updates, train_loss)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("train loss", fontsize=12)
plt.title("BBB training loss")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
#plt.show()

plt.subplot(2,2,2)
plt.plot(updates, test_loss)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("test loss", fontsize=12)
plt.title("BBB test loss")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
#plt.show()

plt.subplot(2,2,3)
plt.plot(updates, KL_div)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("KL Divergence", fontsize=12)
plt.title("BBB KL Divergence")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
#plt.show()

plt.subplot(2,2,4)
plt.plot(updates, ELBO_updates)
plt.xlabel("Epochs", fontsize=12), plt.ylabel("ELBO", fontsize=12)
plt.title("BBB ELBO")

plt.subplots_adjust(top=0.85)
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
plt.show()


"""
    Accuracy of fitted model
"""



# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    net.train(False)
    images, labels = data
    outputs = net(images, training = False)
    _, predicted = torch.max(outputs, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %.3f %%' % (
    100 * correct.item() / total))