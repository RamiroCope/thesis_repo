"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from Main.Optimizers.noisyKFAC_closure import noisyKFAC
import numpy as np
import time

"""
Load data
"""


batch_size = 128

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=True, download=False, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root=parentdir+'\\Main\\Datasets\\Classification\\MNIST', train=False, download=False, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)
    
N = trainset.train_data.shape[0]
    

"""
    Define neural net
"""


class FNN(nn.Module):
    
    def __init__(self,H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1, bias = False)
        self.NN2 = nn.Linear(H1,10, bias = False)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x
    
net = FNN(100)

#net.cuda()


"""
    Loss and optimizer
"""


criterion = nn.CrossEntropyLoss()

# Note that KFAC typically needs a warmup time with a very low learning rate
# for a number of epochs, before turning up the learning rate.
optimizer = noisyKFAC(net.parameters(),
                      net.modules(),
                      num_examples=N,
                      t_inv= 100,
                      t_stats=10,
                      lr = 1e-6,
                      beta = 0.99,
                      prior_var = 0.36,
                      kl_weight = 0.89)



"""
    Training the network
"""

# choose how many epochs the network should run warm_up
warm_up = 1
max_epochs = 50

num_batches = N/batch_size

#p_dist = torch.distributions

# indices for accessing weight matrices of the different layers
indices = [0,1,2]

train_loss = []
test_loss = []
KL_div = []
ELBO = []
updates = []
lr_plt = []

t0 = time.time()

for epoch in range(max_epochs):
    
    if epoch == warm_up:
        for param_group in optimizer.param_groups:
            param_group['lr'] = 1e-3
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for batch, data in enumerate(trainLoader, 0):        
        
        mus = []
        As_inv = []
        Ss_inv = []
        weights = []
        Covs = []
        
        inputs, labels = data
#        inputs = inputs.cuda()
#        labels = labels.cuda()
        
        def closure():
            optimizer.zero_grad()
            outputs = net(inputs) 
            loss = criterion(outputs, labels)
            loss.backward()
            return loss
        
        LL_loss, params = optimizer.step(closure, return_params = True)
        
#        params_dict = optimizer.sample(return_params = True)
        
#        outputs = net(inputs)        
        
        # extract diagnostics
#        lqw, lpw = 0.0, 0.0
#
            
#        
#            # KL Divergence terms    
#            q_dist = Normal(mu, sigma)
#            lqw += q_dist.log_prob(p).sum()
#            lpw += p_dist.log_prob(p).sum()
    
#        mus = np.concatenate(mus)
#        sigmas = np.concatenate(sigmas)
#        weights = np.concatenate(weights)
#        fs = np.concatenate(fs)
    
#        KL_loss = (lqw - lpw)/N
        
#        LL_loss = criterion(outputs, labels)
    
        loss = LL_loss #+ KL_loss
        
        # Print statistics
        running_loss += LL_loss.item()
        if batch % 100 == 99:
            
#            for i, p in enumerate(net.parameters()):
#                sample, A_inv, S_inv = params["sample{0}".format(i)],\
#                            params["A_inv{0}".format(i)],\
#                            params["S_inv{0}".format(i)]
#                            
#                mus += [p.data.cpu().view(-1).detach().numpy()]
#                As_inv += [A_inv.cpu().contiguous().view(-1).detach().numpy()]
#                Ss_inv += [S_inv.cpu().contiguous().view(-1).detach().numpy()]
#                weights += [sample.cpu().view(-1).detach().numpy()]
            
            print('[%d, %5d] ====> train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss / 100))
            updates += [batch + (epoch*num_batches)]
            train_loss += [running_loss/100]
#            KL_div += [KL_loss]
#            ELBO += [loss.item()]
            
            for data in testLoader:
                images, labels = data
#                images = images.cuda()
#                labels = labels.cuda()
                outputs = net(images)
                test_loss_val = criterion(outputs, labels)
                test_loss += [test_loss_val.item()]
                
            print('[%d, %5d] ====> test loss:  %.3f' %
                  (epoch + 1, batch + 1, test_loss_val.item()))
            
#            fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
#            fig.tight_layout(h_pad = 5, w_pad = 5)
#
#            plt.subplot(2,2,1)
#            #plt.plot(updates, train_loss) # plot something
#            plt.hist([mus[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("mu")
#            plt.title("mu histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.subplot(2,2,2)
#            
#            plt.hist([As_inv[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("Covariance matrix A")
#            plt.title("A matrix histogram")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.subplot(2,2,3)
#            
#            plt.hist([weights[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("weights")
#            plt.title("histogram of all weights in BNN")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#    
#            plt.subplot(2,2,4)
#            
#            plt.hist([Ss_inv[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("Covariance matrix S")
#            plt.title("S matrix histogram")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#    
#            # update canvas immediately
#            plt.show()
            running_loss = 0.0
#    lr_plt += [scheduler.get_lr()[0]]        
#    scheduler.step()
#    print("=======> lr is: %.10f" % scheduler.get_lr()[0])
    
print('Finished training')    

t1 = time.time()

total = t1-t0

print("Finished training noisy KFAC on MNIST in %.2f seconds" % total)

"""
    Plotting
"""


fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(7,6))
fig.tight_layout(h_pad = 5, w_pad = 3)
plt.suptitle("Train and Test statistics for Noisy KFAC", fontsize = 14)

plt.subplot(2,2,1) 
plt.plot(updates, train_loss)
plt.xlabel("Updates"), plt.ylabel("train loss")
plt.title("Noisy KFAC training loss")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
#plt.show()

plt.subplot(2,2,2)
plt.plot(updates, test_loss)
plt.xlabel("Updates"), plt.ylabel("test loss")
plt.title("Noisy KFAC test loss")
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
#plt.show()

#plt.subplot(2,2,3)
#plt.plot(updates, KL_div)
#plt.xlabel("Updates"), plt.ylabel("KL Divergence")
#plt.title("Noisy KFAC KL Divergence")
#plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
##plt.show()
#
#plt.subplot(2,2,4)
#plt.plot(updates, ELBO)
#plt.xlabel("Updates"), plt.ylabel("ELBO")
#plt.title("Noisy KFAC ELBO")

plt.subplots_adjust(top=0.85)
plt.grid(color='grey', linestyle='-', linewidth=1, alpha = 0.5)
plt.show()



""" 
    Correlation Matrix
"""


#A_inv = params['A_inv1']
#D = 1.0/np.sqrt(np.diag(A_inv))
#D_inv = np.diag(D)
#A_corr = np.matmul(D_inv, np.matmul(A_inv, D_inv))
#plt.matshow(A_corr)


"""
    Accuracy of fitted model
"""


# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
#    images = images.cuda()
#    labels = labels.cuda()
    outputs = net(images)
    _, predicted = torch.max(outputs, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %.3f %%' % (
    100 * correct.item() / total))