"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from Main.Optimizers.Vprop_closure import Vprop
import numpy as np
from torch.utils.data import DataLoader
import time

'REGRESSION UCI DATASETS'
from Main.Datasets.Regression.wine import wine as dataset

"""
Load data
"""


batch_size=32

# train data
trainset = dataset()
y_train_mean, y_train_std = trainset.get_target_normalization_constants()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = dataset(train = False)
testLoader = DataLoader(testset, batch_size=160, shuffle=False)

N = trainset.train_data.shape[0]
num_features = trainset.train_data.shape[1]

"""
    Define neural networks
"""


class FNN(nn.Module):
    
    def __init__(self,num_features, H1, bias = False):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=bias)
        self.NN2 = nn.Linear(H1,1, bias=bias)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x

# Initialize neural nets
net_Vprop = FNN(num_features, 50, bias = False)




"""
    Loss and optimizers
"""


criterion = nn.MSELoss()



prior_var = 0.1
var_init = 0.001


optimizer_Vprop = Vprop(net_Vprop.parameters(), num_examples = N,
                          lr = 1e-3,
                          alpha = 0.99,
                          prior_var = 0.088,#0.04388867098517026,                          
                          var_init = var_init)


"""
    Training the network
"""

# choose how many epochs the noisy KFAC network should run warm_up
warm_up = 1
max_epochs = 50

num_batches = N/batch_size

# indices for accessing weight matrices of the different layers
indices = [0,1,2]


train_loss_Vprop = []
test_loss_Vprop = []



KL_div = []
ELBO = []
updates = []
lr_plt = []

t0 = time.time()

for epoch in range(max_epochs):
    

    running_loss_Vprop     = 0.0


    for batch, data in enumerate(trainLoader, 0):
        
        inputs, targets = data
#        inputs = inputs.cuda()
#        labels = labels.cuda()

        
        # For Vprop
        def closure():
            optimizer_Vprop.zero_grad()
            outputs = net_Vprop(inputs) 
            loss = criterion(outputs, targets)
            loss.backward()
            return loss
        
        LL_loss_Vprop, _ = optimizer_Vprop.step(closure, return_params = False)
        

        
        # Print statistics
        running_loss_Vprop     += LL_loss_Vprop.item()
        
        if batch % 10 == 9:
            
            print('[%d, %5d] ===============> Vprop train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_Vprop / 10))

            
            
            updates += [batch + (epoch*num_batches)]
            
            train_loss_Vprop += [running_loss_Vprop/10]
            
            for data in testLoader:
                images, labels = data
#                images = images.cuda()
#                labels = labels.cuda()
                outputs_Vprop = net_Vprop(images)
                
                test_loss_val_Vprop = criterion(outputs_Vprop, labels)
                
                test_loss_Vprop += [test_loss_val_Vprop.item()]
                
            print('[%d, %5d] ===============> Vprop test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_Vprop.item()))

    
            running_loss_Vprop = 0.0
    
print('Finished training')    


t1 = time.time()

total = t1-t0


print("Total runtime for Vprop on Wine: %.2f seconds" % total)