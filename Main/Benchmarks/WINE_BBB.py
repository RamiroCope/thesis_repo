"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from Main.Models.BBB import BNN
import numpy as np
from torch.utils.data import DataLoader
import time

'REGRESSION UCI DATASETS'
from Main.Datasets.Regression.wine import wine as dataset

"""
Load data
"""


batch_size=32

# train data
trainset = dataset()
y_train_mean, y_train_std = trainset.get_target_normalization_constants()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = dataset(train = False)
testLoader = DataLoader(testset, batch_size=160, shuffle=False)

N = trainset.train_data.shape[0]
num_features = trainset.train_data.shape[1]

"""
    Define neural networks
"""


# For black box
# initialize network    
prior_var = 0.79#0.5305362661717282
prior_prec = 1 /prior_var
net_BBB = BNN(input_size = num_features,
                  hidden_sizes = [50],
                  output_size = 1,
                  act_func = "Relu",
                  prior_prec = prior_prec,
                  prec_init = 500)


"""
    Loss and optimizers
"""


criterion = nn.MSELoss()

optimizer_BBB = optim.Adam(net_BBB.parameters(), lr = 1e-3)

"""
    Training the network
"""

# choose how many epochs the noisy KFAC network should run warm_up
warm_up = 1
max_epochs = 50

num_batches = N/batch_size

# indices for accessing weight matrices of the different layers
indices = [0,1,2]


train_loss_BBB = []
test_loss_BBB = []


KL_div = []
ELBO = []
updates = []
lr_plt = []

t0 = time.time()

for epoch in range(max_epochs):
    

    running_loss_BBB       = 0.0

    for batch, data in enumerate(trainLoader, 0):
        
        inputs, targets = data
#        inputs = inputs.cuda()
#        labels = labels.cuda()
        
       
        
        # For BBB
        optimizer_BBB.zero_grad()
        output_BBB = net_BBB(inputs)
        LL_loss_BBB = criterion(output_BBB, targets)
        KL_loss = net_BBB.kl_divergence()/N#num_batches
        loss_BBB = LL_loss_BBB + KL_loss
        loss_BBB.backward()
        optimizer_BBB.step()
        

        running_loss_BBB       += LL_loss_BBB.item()#loss_BBB.item()
        
        if batch % 10 == 9:
            

            
            print('[%d, %5d] =======================> BBB train loss: %.3f \n' %
                  (epoch + 1, batch + 1, running_loss_BBB / 10))
            
            
            updates += [batch + (epoch*num_batches)]
            

            train_loss_BBB += [running_loss_BBB/10]
            
            for data in testLoader:
                images, labels = data
#                images = images.cuda()
#                labels = labels.cuda()

                outputs_BBB = net_BBB(images, training = False)
                

                test_loss_val_BBB = criterion(outputs_BBB, labels)
                

                test_loss_BBB += [test_loss_val_BBB.item()]
                

            print('[%d, %5d] =======================> BBB test loss: %.3f \n' %
                  (epoch + 1, batch + 1, test_loss_val_BBB.item()))
    

            running_loss_BBB = 0.0
    
print('Finished training')    

t1 = time.time()

total = t1-t0

print("runtime for BBB on Wine: %.2f seconds" % total)
