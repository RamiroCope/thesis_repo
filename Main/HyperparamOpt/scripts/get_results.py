import pandas as pd

import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

print("parentdir is ", parentdir)

res_vprop = pd.read_csv('UCI/results/bayesopt_WINE_Vprop.csv')
vprop_max = res_vprop.max()

res_nadam = pd.read_csv("UCI/results/bayesopt_WINE_noisyAdam_closure_custom")
nadam_max = res_nadam[res_nadam['# prior_var'] > 0.05]
nadam_max = nadam_max[nadam_max[' kl_weight'] >= 0.2]
nadam_max = nadam_max.min()

nadam_max


res_nkfac = pd.read_csv('UCI/results/bayesopt_WINE_noisyKFAC_closure')

res_bbb = pd.read_csv('UCI/results/bayesopt_BBB_kl_divided_by_N')

