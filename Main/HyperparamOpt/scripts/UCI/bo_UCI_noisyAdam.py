
import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

from bayes_opt import BayesianOptimization
from Optimizers.noisyAdam_closure_custom import noisyAdam

import torch
import torch.nn.functional as F
import torch.nn as nn

import torch.nn.init as init
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.distributions import Normal


import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output
import matplotlib.mlab as mlab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from sklearn import preprocessing
from sklearn.model_selection import train_test_split




'REGRESSION UCI DATASETS'
from Datasets.Regression.wine import wine as dataset



"""Our Model"""
class FNN(nn.Module):
    
    def __init__(self,num_features, H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=False)
        self.NN2 = nn.Linear(H1,1, bias=False)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x
    
    
"""Function to Evaluate"""   
def noisyAdam_evaluate(prior_var, kl_weight):

    
    """Data Preparation"""

    batch_size=10
    
    # train data
    trainset = dataset()
    y_train_mean, y_train_std = trainset.get_target_normalization_constants()
    trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)
    
    # test data
    testset = dataset(train = False)
    testLoader = DataLoader(testset, batch_size=batch_size, shuffle=False)
    
    N = trainset.train_data.shape[0]
    num_features = trainset.train_data.shape[1]
    
    """Hyper-parameters to Optimize"""
    
    model = FNN(num_features, 50)
    criterion = nn.MSELoss()
    optimizer = noisyAdam(model.parameters(), 
                          num_examples=N, 
                          gamma_ex=0.01,
                          var_init = 0.001,
                          kl_weight=kl_weight,
                          lr=1e-3, 
                          #betas = (beta1, beta2), 
                          prior_var=prior_var)
    
    
    
    for epoch in range(50):

        for i, data in enumerate(trainLoader, 0):

            inputs, labels = data

            def closure():
                optimizer.zero_grad()
                outputs = model(inputs) 
                loss = criterion(outputs, labels)
                loss.backward()
                return loss
            
            optimizer.step(closure)
    
    # Test on entire test set
    error=[]
    for data in testLoader:
        x, y = data
        #optimizer.sample(training = False)
        outputs = model(x)
        error += [(outputs.data - y.data).numpy()]
    #flattening list of lists
    error = np.array([x for ls in error for x in ls])
    normalized_RMSE  = np.mean(error**2)**0.5
    #unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5 
    
    return normalized_RMSE




"""Do Bayesian Opt"""

num_iter = 1
init_points = 0

bo = BayesianOptimization(noisyAdam_evaluate, 
                               {
                               #'gamma_ex' :(0.0,1e-1), 
                               'prior_var' : (1e-2,1),
                               'kl_weight' : (0.01,1),
                               #'beta1'     : (0.9,0.999),
                               #'beta2'     : (0.95,0.9999)
                              })


bo.explore({
                 #'gamma_ex' : [1e-2, 1e-3],
                 'prior_var': [0.1, 1.0],
                 'kl_weight': [0.2, 0.5],
                 #'beta1'    : [0.9, 0.99],
                 #'beta2'    : [0.99, 0.999]
                })

bo.maximize(init_points=init_points, n_iter=num_iter, acq = 'ei', xi = 0.1)

bo.points_to_csv("results\\bayesopt_noisyAdam_closure_custom")
print(bo.res['max'])