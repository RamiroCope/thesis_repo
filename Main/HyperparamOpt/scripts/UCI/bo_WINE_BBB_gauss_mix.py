import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

from bayes_opt import BayesianOptimization
#from Optimizers.noisyAdam_closure_orig import noisyAdam


import torch.nn.init as init
from torch.autograd import Variable
from torch.utils.data import DataLoader
#import torchvision
#import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.distributions import Normal
import scipy.stats as stats
from torch import optim
from Testing.Peter.Pytorch04.Models.BBB_gaussian_mix import BNN

import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output
import matplotlib.mlab as mlab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from sklearn import preprocessing
from sklearn.model_selection import train_test_split



'REGRESSION UCI DATASETS'
from Datasets.Regression.wine import wine as dataset


    
    
"""Function to Evaluate"""   
def evaluate(prior_var):

    prior_prec = 1/ (prior_var)
    
    """Data Preparation"""

    batch_size=32
    
    # train data
    trainset = dataset()
    y_train_mean, y_train_std = trainset.get_target_normalization_constants()
    trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)
    
    # test data
    testset = dataset(train = False)
    testLoader = DataLoader(testset, batch_size=batch_size, shuffle=False)
    
    N = trainset.train_data.shape[0]
    num_features = trainset.train_data.shape[1]
    
    
    """Hyper-parameters to Optimize"""
    
    # initialize model  
    model = BNN(input_size = num_features,
              hidden_sizes = [50],
              output_size = 1,
              act_func = "Relu",
              prior_prec = prior_prec,
              prec_init = 500)
    criterion = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr = 1e-1)
    scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.90)
    num_batches = N/batch_size
    
    
    # Train Model
    for epoch in range(50):

        for batch, data in enumerate(trainLoader, 0):
            model.train(True)
            inputs, labels = data
            
            optimizer.zero_grad()
            outputs = model(inputs)   
            NLL = criterion(outputs, labels)
            ELBO = NLL + model.kl_divergence()/N
            ELBO.backward()
            
            optimizer.step()
            
        scheduler.step()
    
    # Test on entire test set
    error=[]
    for data in testLoader:
        x, y = data
        #optimizer.sample(training = False)
        outputs = model(x)
        error += [(outputs.data - y.data).numpy()]
    #flattening list of lists
    error = np.array([x for ls in error for x in ls])
    normalized_RMSE  = np.mean(error**2)**0.5
    #unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5 
    
    return -normalized_RMSE




"""Do Bayesian Opt"""

num_iter = 100
init_points = 10

bo = BayesianOptimization(evaluate, 
                               {
                               #'gamma_ex' :(0.0,1e-1), 
                               'prior_var' : (1e-2,1),
                               #'prec_init' : (0.01,1),
                               #'beta1'     : (0.9,0.999),
                               #'beta2'     : (0.95,0.9999)
                              })


bo.explore({
                 #'gamma_ex' : [1e-2, 1e-3],
                 'prior_var': [1, 0.1],
                 #'prec_init': [0.2, 0.5],
                 #'beta1'    : [0.9, 0.99],
                 #'beta2'    : [0.99, 0.999]
                })

bo.maximize(init_points=init_points, n_iter=num_iter, acq = 'ei', xi = 0.1)

bo.points_to_csv("results\\bayesopt_BBB_gauss_mix_kl_divided_by_N")
print(bo.res['max'])