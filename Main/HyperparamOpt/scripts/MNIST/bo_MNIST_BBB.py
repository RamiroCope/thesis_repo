import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 


from bayes_opt import BayesianOptimization
import torchvision
import torchvision.transforms as transforms
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from Models.BBB import BNN



"""Our Model"""
class FNN(nn.Module):
    
    def __init__(self,H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1,bias=False)
        self.NN2 = nn.Linear(H1,10, bias=False)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x
    

    
    
"""Function to Evaluate"""   
def evaluate(prior_prec):

    batch_size=100
    
    """Data Preparation"""
    # transformer used to transform images to tensors
    transform = transforms.Compose([transforms.ToTensor()])

    # train data
    trainset = torchvision.datasets.MNIST(root= parentdir + '/Datasets/Classification/MNIST/', train=True,  download=False, transform=transform)
    trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

    # test data
    testset = torchvision.datasets.MNIST(root= parentdir +'/Datasets/Classification/MNIST/', train=False, download=False, transform=transform)
    testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)
    N = trainset.train_data.shape[0]
    #num_cols = trainset.train_data.shape[1]
    
    """Hyper-parameters to Optimize"""
    
    prior_prec = 1/prior_prec
    
    # initialize model  
    model = BNN(input_size = 784,
              hidden_sizes = [100],
              output_size = 10,
              act_func = "Relu",
              prior_prec = prior_prec,
              prec_init = 500)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr = 1e-3)
#    scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.90)
#    num_batches = N/batch_size
    
    
    # Train Moel
    for epoch in range(50):

        for batch, data in enumerate(trainLoader, 0):
            model.train(True)
            inputs, labels = data
            
            optimizer.zero_grad()
            outputs = model(inputs)   
            NLL = criterion(outputs, labels)
            ELBO = NLL + model.kl_divergence()/N
            ELBO.backward()
            
            optimizer.step()
            
#        scheduler.step()
    
    # Test on entire test set
    correct = 0
    total = 0

    for data in testLoader:
        model.train(False)
        images, labels = data
        outputs = model(images, training=False)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum()
    accuracy = 100 * correct.item() / total
    
    return accuracy




"""Do Bayesian Opt"""

num_iter = 100
init_points = 10

bo = BayesianOptimization(evaluate, 
                               {
                               #'gamma_ex' :(0.0,1e-1), 
                               'prior_prec' : (0.018,1)
#                               'prec_init' : (0.01,1),
                               #'beta1'     : (0.9,0.999),
                               #'beta2'     : (0.95,0.9999)
                              })


bo.explore({
                 #'gamma_ex' : [1e-2, 1e-3],
                 'prior_prec': [0.1, 1.0]
#                 'prec_init': [0.2, 0.5],
                 #'beta1'    : [0.9, 0.99],
                 #'beta2'    : [0.99, 0.999]
                })

bo.maximize(init_points=init_points, n_iter=num_iter, acq = 'ei', xi = 0.1)

bo.points_to_csv("results\\bayesopt_BBB_PROPER.csv")
print(bo.res['max'])