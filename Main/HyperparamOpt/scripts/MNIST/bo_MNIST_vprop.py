import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

from bayes_opt import BayesianOptimization
from Optimizers.Vprop_closure import Vprop
import torch
import torch.nn.functional as F
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

"""Our Model"""
class FNN(nn.Module):
    
    def __init__(self,H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1,bias=False)
        self.NN2 = nn.Linear(H1,10, bias=False)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x
    
    
"""Function to Evaluate"""   
def vprop_evaluate(prior_var):

    
    """Data Preparation"""
    # transformer used to transform images to tensors
    transform = transforms.Compose([transforms.ToTensor()])

    # train data
    trainset = torchvision.datasets.MNIST(root= parentdir + '/Datasets/Classification/MNIST/', train=True,  download=False, transform=transform)
    trainLoader = torch.utils.data.DataLoader(trainset, batch_size=100, shuffle=True)

    # test data
    testset = torchvision.datasets.MNIST(root= parentdir +'/Datasets/Classification/MNIST/', train=False, download=False, transform=transform)
    testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)
    N = trainset.train_data.shape[0]
    #num_cols = trainset.train_data.shape[1]
    
    """Hyper-parameters to Optimize"""
    
    model = FNN(100)
    criterion = nn.CrossEntropyLoss()
    optimizer = Vprop(model.parameters(), 
                          num_examples = N,
                          lr = 1e-3,
                          alpha=0.99,
                          prior_var = prior_var,
                          var_init = 0.001,
                          )
    
    
    
    
    for epoch in range(50):

        for i, data in enumerate(trainLoader, 0):

            inputs, labels = data
            #inputs, labels = Variable(inputs), Variable(labels)

            
            def closure():
                optimizer.zero_grad()
                outputs = model(inputs) 
                loss = criterion(outputs, labels)
                loss.backward()
                return loss
            
            optimizer.step(closure)
    
    # Test on entire test set
    correct = 0
    total = 0

    for data in testLoader:
        images, labels = data
        #optimizer.sample(training = False)
        outputs = model(images)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum()
    accuracy = 100 * correct.item() / total
    val_result = accuracy
    
    return val_result




"""Do Bayesian Opt"""

num_iter = 100
init_points = 10

bo = BayesianOptimization(vprop_evaluate, 
                               {
                               #'gamma_ex' :(0.0,1e-1), 
                               'prior_var' : (0.018,1)
                               #'beta1'     : (0.9,0.999),
                               #'beta2'     : (0.95,0.9999)
                              })


bo.explore({
                 #'gamma_ex' : [1e-2, 1e-3],
                 'prior_var': [0.1, 1.0]
                 #'beta1'    : [0.9, 0.99],
                 #'beta2'    : [0.99, 0.999]
                })

bo.maximize(init_points=init_points, n_iter=num_iter, acq = 'ei', xi = 0.1)

bo.points_to_csv("results\\bayesopt_vprop_PROPER.csv")
print(bo.res['max'])