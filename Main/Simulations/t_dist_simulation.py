"""
    This script investigates the flexibility of a student's t distribution
    when compared against a Normal distribution
"""


"""
    Setup working environment
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from Main.Models.BBB_t_linear import BNN as BNN_t
from Main.Models.BBB_linear import BNN
from torch import optim
import torch.distributions as distributions


"""
    Simulate targets
"""

### Construct Design matrix ###

# number of observations
num_obs = 1000
# number of weights
num_weights = 1000

X_mu = 2.0
X_std = 1.0

X_dist = distributions.Normal(X_mu, X_std)

X_train = X_dist.sample((num_obs, num_weights))
X_test = X_dist.sample((num_obs, num_weights))


# Noise
eps_stdv = 1.0
d_eps = distributions.Normal(0.0, eps_stdv)
eps = d_eps.sample((num_obs,1))

### Generate true weights ###
w_zeros = torch.zeros((1,990))
w_vals = torch.Tensor(1,10).uniform_(1,2)*30
w_sample = torch.cat((w_zeros.t(),w_vals.t())).t()

# Generate targets
y_train = F.linear(X_train, w_sample) + eps
y_test = F.linear(X_test, w_sample) + eps


"""
    Linear regression in a neural network style class
"""


linear_bayes_t = BNN_t(input_size = num_weights, hidden_sizes = [],
                       prior_prec = 1.0, prec_init=500, df_prior = 2.0, df_init = 3.0)

linear_bayes   = BNN(input_size = num_weights, hidden_sizes = [],
                       prior_prec = 1.0, prec_init=500)


# loss
criterion = nn.MSELoss()

# train set size
N = X_train.shape[0]

optimizer = optim.Adam(linear_bayes.parameters(), lr = 1e-3)
optimizer_t = optim.Adam(linear_bayes_t.parameters(), lr = 1e-2)


"""
    Train linear regression
"""


epochs = 20000

train_loss = []
train_loss_t = []
test_loss = []
test_loss_t = []
updates = []
    
for epoch in range(epochs):

    running_loss_bayes_t = 0.0
    running_loss_bayes = 0.0

    mus = []
    sigmas = []
    fs = []
    weights = []
    
    inputs  = X_train
    targets = y_train.squeeze()   
    
    outputs_bayes = linear_bayes(inputs)
    outputs_bayes_t = linear_bayes_t(inputs)
    
    optimizer.zero_grad()
    optimizer_t.zero_grad()
    
    loss_bayes = criterion(outputs_bayes, targets)
    loss_bayes_t = criterion(outputs_bayes_t, targets)
    
    kl_t = linear_bayes_t.kl_divergence()
    kl = linear_bayes.kl_divergence()
    
    running_loss_bayes_t += loss_bayes_t.item()
    running_loss_bayes += loss_bayes.item()
    
    if epoch % 100 == 99:
        mu_t, sigma, df, weight = linear_bayes_t.return_params()
        mu, sigma, weight = linear_bayes.return_params()

        print("=====> Epoch %d " % (epoch+1))
        print("=====> Train loss T dist: %.2f " % (running_loss_bayes_t/100))
        print("=====> Train loss       : %.2f\n" % (running_loss_bayes/100))
        running_loss_bayes_t = 0.0
        running_loss_bayes = 0.0
    
    ELBO = loss_bayes + kl/N
    ELBO_t = loss_bayes_t + kl_t/N
    
    ELBO.backward()
    ELBO_t.backward()
    
    optimizer.step()
    optimizer_t.step()
    
    test_inputs = X_test
    test_targets = y_test.squeeze()
    
    test_outputs_bayes = linear_bayes(test_inputs)
    test_outputs_bayes_t = linear_bayes_t(test_inputs)
    
    test_loss_val_bayes = criterion(test_outputs_bayes, test_targets)
    test_loss_val_bayes_t = criterion(test_outputs_bayes_t, test_targets)
    
    train_loss += [loss_bayes.item()]
    test_loss += [test_loss_val_bayes.item()]
    
    train_loss_t += [loss_bayes_t.item()]
    test_loss_t += [test_loss_val_bayes_t.item()]
    
    updates += [epoch]

# Choose tolerance for weight concentration around zero
tol = 0.25

# Get weights close to zero and larger than 30
p = list(linear_bayes_t.parameters())
weights = p[1].data.numpy()
weights_t_zeros = weights[abs(weights) < tol]
weights_t_large = weights[weights >= 30]

p = list(linear_bayes.parameters())
weights = p[0].data.numpy()
weights_zeros = weights[abs(weights) < tol]
weights_large = weights[weights >= 30]


print("t dist      - total number of weights smaller than %.1e      = %d" % ( tol, weights_t_zeros.shape[0] ))
print("normal dist - total number of weights smaller than %.1e      = %d" % ( tol, weights_zeros.shape[0] ))

print("t dist      - total number of weights larger than 30    = %d" % ( weights_t_large.shape[0] ))
print("normal dist - total number of weights larger than 30    = %d" % ( weights_large.shape[0] ))    


"""
    Plotting
"""


# Plot histograms with theoretical T dist on top of each other
plt.hist(w_sample.squeeze(), alpha = 1.0, bins = 40, edgecolor='black', normed = True)
plt.hist(mu_t[0].squeeze(), alpha = 1.0, bins = 40, edgecolor='black', normed = True)
plt.hist(mu[0].squeeze(), alpha = 0.75, bins = 40, edgecolor='black', normed = True)

plt.xlabel("weights", fontsize = 14)
plt.ylabel('Normalized count', fontsize = 14)
plt.title("t distribution vs Normal distribution", fontsize = 14)
plt.legend(['True weights', 't dist trained weights', 'Normal dist trained weights'])
plt.show()