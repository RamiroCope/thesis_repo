# -*- coding: utf-8 -*-
"""
Created on Mon Jul 30 13:01:46 2018

@author: stud
"""

import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
#parentdir = os.path.dirname(parentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

from bayes_opt import BayesianOptimization
from Main.Optimizers.noisyKFAC_closure import noisyKFAC as noisyKFAC
from Main.Optimizers.noisyAdam_closure_custom import noisyAdam

import torch
import torch.nn.functional as F
import torch.nn as nn

import torch.nn.init as init
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.distributions import Normal


import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output
import matplotlib.mlab as mlab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from sklearn import preprocessing
from sklearn.model_selection import train_test_split




'REGRESSION UCI DATASETS'
from Main.Datasets.Regression.wine import wine as dataset

'''
    Minibatch Sensitivity in Noisy Adam and KFAC
'''


"""Our Model"""
class FNN(nn.Module):
    
    def __init__(self,num_features, H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=False)
        self.NN2 = nn.Linear(H1,1, bias=False)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x


def train_model(batch_size, model_name, epochs):
    
    assert (model_name == 'noisy_kfac') or (model_name == 'noisy_adam'), 'model name not supported. should be either noisy_kfac or noisy_adam'

    
    """Data Preparation"""

    batch_size=batch_size
    
    # train data
    trainset = dataset()
    y_train_mean, y_train_std = trainset.get_target_normalization_constants()
    trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)
    
    # test data
    testset = dataset(train = False)
    testLoader = DataLoader(testset, batch_size=batch_size, shuffle=False)
    
    N = trainset.train_data.shape[0]
    num_features = trainset.train_data.shape[1]
    
    """Hyper-parameters to Optimize"""
    
    model = FNN(num_features, 50)
    criterion = nn.MSELoss()
    
    if model_name == 'noisy_adam':
        optimizer = noisyAdam(model.parameters(), 
                              num_examples=N, 
                              gamma_ex=0.0,
                              var_init = 0.001,
                              kl_weight=1, #0.68,
                              lr=1e-3, 
                              #betas = (beta1, beta2), 
                              prior_var=1)
        
    elif model_name == 'noisy_kfac':
        optimizer = noisyKFAC(model.parameters(),
                          model.modules(),
                          num_examples=N,
                          t_inv= 20,
                          t_stats=10,
                          lr = 1e-6,
                          beta = 0.99,
                          prior_var = 1, #, 0.7,
                          kl_weight = 1) #0.125)
    
    
    
    
    for epoch in range(epochs):

        for i, data in enumerate(trainLoader, 0):

            inputs, labels = data

            def closure():
                optimizer.zero_grad()
                outputs = model(inputs) 
                loss = criterion(outputs, labels)
                loss.backward()
                return loss
            
            optimizer.step(closure)
    
    # Test on entire test set
    error=[]
    for data in testLoader:
        x, y = data
        #optimizer.sample(training = False)
        outputs = model(x)
        error += [(outputs.data - y.data).numpy()]
    #flattening list of lists
    error = np.array([x for ls in error for x in ls])
    normalized_RMSE  = np.mean(error**2)**0.5
    #unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5 
    
    return normalized_RMSE


###############################################################################


# construct the minibatch size grid space to evaluate
grid_points =  [i for i in range(1,30)] + [i for i in range(30,1000,20)]

print("Starting experiment...")

res_nadam = [train_model(batch_size=i, model_name='noisy_adam', epochs=50) for i in grid_points]
res_kfac = [train_model(batch_size=i, model_name='noisy_kfac', epochs=50) for i in grid_points]



# plot
import matplotlib
matplotlib.rcParams.update({'font.size':14})
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12,4))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(1,2,1)
plt.scatter(grid_points, res_nadam)
plt.xlabel('minibatch size')
plt.ylabel('RMSE')
plt.title('Minibatch Sensitivity in Noisy Adam')

plt.subplot(1,2,2)
plt.scatter(grid_points, res_kfac, label='noisy kfac')
plt.xlabel('minibatch size')
plt.ylabel('RMSE')
plt.title('Minibatch Sensitivity in Noisy KFAC')
plt.show()



