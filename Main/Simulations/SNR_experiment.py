"""
    This script performs a signal to noise ratio experiment on the setup
        y_true = f(X*w_true) + SNR*noise
    The experiment highlights a models ability to capture the signal in
    a noisy sample.
"""

"""
    Setup working environment
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

#Get cmd line args
if len(sys.argv) == 1:
    print("No activation function specified. Performing linear regression")
    act = None
if len(sys.argv) == 2:
    act = str(sys.argv[-1])
    if act != "sigmoid" and act != "tanh":
        print("Activation function not supported. Performing linear regression")
        act = None
if len(sys.argv) > 2:
    print("Too many input arguments received. Performing linear regression")
    act = None


"""
    Import libraries
"""


import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import torch.distributions as distributions
import math

from torch import optim
from Main.Optimizers.noisyAdam_custom import noisyAdam

"""
    Configure outer loop
"""

# Define necessary lists
snr_list = [1, 2, 4, 6, 8, 10]
epoch_list = [20000]*len(snr_list)
train_losses = []
test_losses = []
train_losses_bayes = []
test_losses_bayes = []

# Define mean and stdv for the true distribution of the weights
w_mu = 0.0
w_std = 0.25
# Choose number of weights and observations
num_weights = 100
num_obs = 128

# true distribution of the weights
p_w = distributions.Normal(w_mu, w_std)

# get a sample from the true distribution
w_sample = p_w.sample((1,num_weights))

# define parameters for constructing feature matrix
X_mu = 2.0
X_std = 2.0

# feature matrix distribution
q_w = distributions.Normal(X_mu, X_std)

# Construct feature matrix 
X_train = q_w.sample((num_obs, num_weights))
X_test = q_w.sample((num_obs, num_weights))

# define standard normal noise
eps_stdv = 1.0
d_eps = distributions.Normal(0.0, eps_stdv)

# sample noise
eps = d_eps.sample((num_obs,1))


"""
    Define model class
"""


class FNN(nn.Module):
    
    def __init__(self, in_features, act):
        super(FNN, self).__init__()
        self.act = act
        self.NN1 = nn.Linear(in_features, 1, bias = False)
        
    def forward(self,x):
        if self.act == "sigmoid":
            x = F.sigmoid(self.NN1(x))
        elif self.act == "tanh":
            x = F.tanh(self.NN1(x))
        else:
            x = self.NN1(x)

        return x
    
    
"""
    Configure objective and activation function
"""


#### Define objective ####
# For linear regression we just consider MSE loss
# Otherwise we consider the Gaussian MLE

objective = nn.MSELoss()
convergence_line = eps_stdv**2


"""
    Start experiment
"""


for j, snr in enumerate(snr_list):
    
    # Signal to Noise Ratio
    SNR = snr
    # Compute the SNR factor
    snr_factor_train = (F.linear(X_train, w_sample)).std()/(math.sqrt(SNR)*eps.std())
    snr_factor_test = (F.linear(X_test, w_sample)).std()/(math.sqrt(SNR)*eps.std())


    # define simulated target y    
    if act == "sigmoid":
        y_train = F.sigmoid(F.linear(X_train, w_sample)) + snr_factor_train*eps
        y_test = F.sigmoid(F.linear(X_test, w_sample)) + snr_factor_test*eps
    
    if act == "tanh":
        y_train = F.tanh(F.linear(X_train, w_sample)) + snr_factor_train*eps
        y_test = F.tanh(F.linear(X_test, w_sample)) + snr_factor_test*eps
    else:
        y_train = F.linear(X_train, w_sample) + snr_factor_train*eps
        y_test = F.linear(X_test, w_sample) + snr_factor_test*eps

    
    """
        Linear regression model with no bias
    """
 
      
    # Construct models
    linear = FNN(num_weights, act = act)
    linear_bayes = FNN(num_weights, act = act)
    
    # N as in pseudo code for noisy Adam is the total number of observations
    N = X_train.shape[0]
    
    # CHoose prior variance
    prior_var = 0.2
    
    # optimizer
    optimizer_bayes = noisyAdam(linear_bayes.parameters(), num_examples = N,
                              gamma_ex = 0,
                              lr = 1e-4,
                              prior_var = prior_var,
                              #df = df,
                              betas = (0.9, 0.999))
    
    optimizer = optim.Adam(linear.parameters(), lr = 1e-4)
    
    
    """
        Train linear regression
    """
    
    
    epochs = epoch_list[j]
    
    train_loss = []
    test_loss = []
    train_loss_bayes = []
    test_loss_bayes = []
    updates = []
    

    # Start training for an instance of SNR values    
    for epoch in range(epochs):
    
        running_loss = 0.0
        running_loss_bayes = 0.0
    
        mus = []
        sigmas = []
        fs = []
        weights = []
        
        inputs  = X_train
        targets = y_train
        
        params_dict = optimizer_bayes.sample(return_params=True)
        
        for i, p in enumerate(linear.parameters()):
            mu, sigma, f = params_dict["mu{0}".format(i)],\
                            params_dict["sigma{0}".format(i)],\
                            params_dict["f{0}".format(i)]
                        
            mus += [mu.view(-1).numpy()]
            sigmas += [sigma.view(-1).numpy()]
            fs += [f.view(-1).numpy()]
            weights += [p.data.view(-1).numpy()]
            
        
        
        outputs_bayes = linear_bayes(inputs)
        outputs = linear(inputs)
        
        loss_bayes = objective(outputs_bayes, targets)
        loss = objective(outputs, targets)
        
        optimizer.zero_grad()
        optimizer_bayes.zero_grad()
        
        loss.backward()
        loss_bayes.backward()
        
        optimizer_bayes.step()
        optimizer.step()
        
        running_loss += loss.item()
        running_loss_bayes += loss_bayes.item()
        
        test_inputs = X_test
        test_targets = y_test
        
        optimizer_bayes.sample(training = False)
        
        test_outputs = linear(test_inputs)
        test_outputs_bayes = linear_bayes(test_inputs)
        
        test_loss_val = objective(test_outputs, test_targets)
        test_loss_val_bayes = objective(test_outputs_bayes, test_targets)
        
        train_loss += [loss.item()]
        test_loss += [test_loss_val.item()]
        
        train_loss_bayes += [loss_bayes.item()]
        test_loss_bayes += [test_loss_val_bayes.item()]
        
        updates += [epoch]
        
    train_losses += [train_loss[-1]]
    test_losses += [test_loss[-1]]
    print("\n=====> Iteration %.d out of a total of %.d\n" % ( (j+1), len(snr_list) ))
    print("===========> Train loss: %.3f" % train_loss[-1])   
    print("===========> Test loss:  %.3f" % test_loss[-1])
    
    train_losses_bayes += [train_loss_bayes[-1]]
    test_losses_bayes += [test_loss_bayes[-1]]
    print("=====> Train loss Bayes: %.3f" % train_loss_bayes[-1])   
    print("=====> Test loss Bayes:  %.3f" % test_loss_bayes[-1])

"""
    Do plotting
"""    
    
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,2,1)

plt.hist([mus, w_sample.view(-1).numpy()], bins = 10, edgecolor='black', normed = True)
plt.xlabel("mu")
plt.title("mu histogram of variational posterior")
plt.legend(['Estimated means','true weights'])

plt.subplot(2,2,2)

plt.hist(sigmas, bins = 10, edgecolor='black', normed = True)
plt.xlabel("standard deviation")
plt.title("standard deviation histogram of variational posterior")
plt.legend(['Layer 1','Layer 2','Layer 3'])

plt.subplot(2,2,3)

plt.hist([weights, w_sample.view(-1).numpy()], bins = 10, edgecolor='black', normed = True)
plt.xlabel("weights")
plt.title("histogram of all weights in BNN")
plt.legend(['Trained weights', 'True weights'])

# update canvas immediately
plt.show()

fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(6,5))
fig.tight_layout(h_pad = 5, w_pad = 3,pad=4)      
plt.plot(snr_list[0:], train_losses[0:])
plt.plot(snr_list[0:], test_losses[0:])
plt.plot(snr_list[0:], train_losses_bayes[0:], '-.')
plt.plot(snr_list[0:], test_losses_bayes[0:], '-.')

plt.xlabel("SNR", fontsize = 14), plt.ylabel("MSE loss", fontsize = 14)
#plt.title("train/test loss for increasing Signal-to-Noise ratio", fontsize = 12)
plt.legend(["Train loss w. Adam", "Test loss w. Adam", "Train loss w. Noisy Adam", "Test loss w. Noisy Adam"])
plt.savefig('SNR_linreg_exp_1.eps')
plt.show()