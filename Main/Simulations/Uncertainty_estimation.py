

import os, sys, inspect
import argparse

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
#parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

from Main.Optimizers.noisyAdam_closure_custom import noisyAdam

import torch
import torch.nn.functional as F
import torch.nn as nn

import torch.nn.init as init
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.distributions import Normal


import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output
import matplotlib.mlab as mlab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from sklearn import preprocessing
from sklearn.model_selection import train_test_split



'''
_________________   EXPERIMENT : Prediction Uncertainty  _____________________
Uncertainty Estimation Showcasing with UCI Datasets
'''





"""Model"""
class FNN(nn.Module):
    
    def __init__(self,num_features, H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=False)
        self.NN2 = nn.Linear(H1,1, bias=False)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x
    
    
def train_model(model, trainset):
    """Data Preparation"""
    batch_size=32
    
    # train data
    y_train_mean, y_train_std = trainset.get_target_normalization_constants()
    trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)
    
    
    N = trainset.train_data.shape[0]
    
    
    """Optimizer"""
    #model = FNN(num_features, 50)
    criterion = nn.MSELoss()
    optimizer = noisyAdam(model.parameters(), 
                          num_examples=N, 
                          gamma_ex=0.0,
                          var_init = 0.001,
                          kl_weight=0.275,
                          lr=1e-3, 
                          prior_var=0.81)
    
    
    """Train"""
    for epoch in range(50):
    
        for i, data in enumerate(trainLoader, 0):
    
            inputs, labels = data
    
            def closure():
                optimizer.zero_grad()
                outputs = model(inputs) 
                loss = criterion(outputs, labels)
                loss.backward()
                return loss
            
            _, params = optimizer.step(closure, return_params=True)

    return model, params



def predict_with_qsamples(model, params, num_predictions, testset):
    
    # num test samples
    testLoader = DataLoader(testset, batch_size=160, shuffle=False)
    n = testLoader.dataset.test_data.size()[0]
    predictions = torch.ones([n,num_predictions], requires_grad=False)
    
    # store mu of variational posterior
    for i, p in enumerate(list(model.parameters())):
        params['mu{}'.format(i)] = p.data 
    
    
    for j in range(num_predictions):
        
        # 1. sample from variational posterior
        for i, p in enumerate(list(model.parameters())):
            q_dist = Normal(params['mu{}'.format(i)], params['sigma{}'.format(i)])
            p.data = q_dist.sample()
            
        # 2. forward pass with new weights
        list_of_outputs = []
        for data in testLoader:
            x, y = data
            outputs = model(x)
            list_of_outputs.append(outputs)
            
        # store results
        all_outputs = torch.cat(list_of_outputs, 0)
        predictions[:,j] = all_outputs.squeeze()
        
    return predictions.detach().numpy()




def main(iterations, samples, uci_dataset, plots):
    
    if uci_dataset == 'boston':
        from Main.Datasets.Regression.boston import boston as dataset
        print('boston')
    elif uci_dataset == 'wine':
        from Main.Datasets.Regression.wine import wine as dataset
        print('wine')
    
    """ Train / Test Data"""
    trainset = dataset()
    
    testset = dataset(train = False)
    testLoader = DataLoader(testset, batch_size=160, shuffle=False)
    targets = testset.test_labels.squeeze().numpy()
    num_features = testset.test_data.shape[1]
    
    
    iterations = iterations # number of times to perform the experiment
    samples = samples       # number of samples from var posterior
    average_MAP_RMSE = np.empty(iterations)
    average_QSAMP_RMSE = np.empty(iterations)
    
    for i in range(iterations):
        
        
        """ Train Models"""
        model   = FNN(num_features, 50)
        model, params = train_model(model, trainset)
        
        
        
        """Test on entire test set with MAP"""
        map_error=[]
        map_predictions = []
        
        for data in testLoader:
            x, y = data
            outputs = model(x)
            map_predictions.append(outputs.data.numpy())
            map_error.append((outputs.data - y.data).numpy())
            
        map_error = np.array(map_error[0])
        map_predictions = np.array(map_predictions[0]) #torch.cat(map_predictions, 0).squeeze().detach().numpy()
        
        
        normalized_RMSE_map  = np.mean(map_error**2)**0.5
        print('MAP RMSE: ', normalized_RMSE_map)
        average_MAP_RMSE[i] = normalized_RMSE_map
        #unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5
        
        
        
        """ Test on entire test set with QSAMP"""
        qsamp_predictions = predict_with_qsamples(model, params, samples, testset)
        qsamp_error = np.array((np.mean(qsamp_predictions, 1) - targets))
        qsamp_normalized_RMSE  = np.mean(qsamp_error**2)**0.5
        print('QSAMP RMSE: ', qsamp_normalized_RMSE)
        average_QSAMP_RMSE[i] = qsamp_normalized_RMSE
        #unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5 
    
    
    plt.boxplot(qsamp_predictions[0:30,:].T)
    plt.scatter(range(1, 31), targets[0:30].T , label='True Targets', s=100)
    #plt.scatter(range(1, 31), map_predictions[0:30,:].T , label='MAP')
    plt.legend()
    plt.show()    
    
    
    plt.boxplot([average_QSAMP_RMSE, average_MAP_RMSE])
    plt.show()
    
    
    

        




if __name__ == "__main__":
    
    print('Experiment can be run like this: ')
    print("\n python Uncertainty_wine2.py --iterations 1 --samples 10 --plots 'True' --uci_dataset 'wine'")
    
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--iterations", type = int, default = 10,  help = "Specify number of times to repeat the experiment")
    parser.add_argument("--plots", type = bool, default = True,  help = 'Specify True to plot boxplots, else False')
    parser.add_argument("--samples", type = int, default = 100, help = "Specify how many samples should be taken from variational posterior. The mean of the predictions of those samples will be used for comparisons with the MAP prediction")
    parser.add_argument("--uci_dataset", type = str, default = 'wine', choices = ['wine', 'boston'], help = "Specify which UCI dataset to run experiment on. Currently only wine and boston are included")
    
    
    args = parser.parse_args()
    iterations = args.iterations
    plots = args.plots
    samples = args.samples
    uci_dataset = args.uci_dataset
    print("Used parameters: \n\titerations: {0}\n\tplots: {1}\n\tsamples: {2}".format(iterations,plots,samples))
    # run main function
    main(iterations, samples, uci_dataset, plots)
    
    


import matplotlib
matplotlib.rcParams.update({'font.size':14})

from Main.Datasets.Regression.wine import wine as dataset

""" Train / Test Data"""
trainset = dataset()

testset = dataset(train = False)
testLoader = DataLoader(testset, batch_size=160, shuffle=False)
targets = testset.test_labels.squeeze().numpy()
num_features = testset.test_data.shape[1]


iterations = 1 # number of times to perform the experiment
samples = 100       # number of samples from var posterior
average_MAP_RMSE = np.empty(iterations)
average_QSAMP_RMSE = np.empty(iterations)

for i in range(iterations):
    
    
    """ Train Models"""
    model   = FNN(num_features, 50)
    model, params = train_model(model, trainset)
    
    
    
    """Test on entire test set with MAP"""
    map_error=[]
    map_predictions = []
    
    for data in testLoader:
        x, y = data
        outputs = model(x)
        map_predictions.append(outputs.data.numpy())
        map_error.append((outputs.data - y.data).numpy())
        
    map_error = np.array(map_error[0])
    map_predictions = np.array(map_predictions[0]) #torch.cat(map_predictions, 0).squeeze().detach().numpy()
    
    
    normalized_RMSE_map  = np.mean(map_error**2)**0.5
    print('MAP RMSE: ', normalized_RMSE_map)
    average_MAP_RMSE[i] = normalized_RMSE_map
    #unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5
    
    
    
    """ Test on entire test set with QSAMP"""
    qsamp_predictions = predict_with_qsamples(model, params, samples, testset)
    qsamp_error = np.array((np.mean(qsamp_predictions, 1) - targets))
    qsamp_normalized_RMSE  = np.mean(qsamp_error**2)**0.5
    print('QSAMP RMSE: ', qsamp_normalized_RMSE)
    average_QSAMP_RMSE[i] = qsamp_normalized_RMSE
    #unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5 

mean = np.mean(qsamp_predictions[0:30,:].T, axis=0)
std = np.std(qsamp_predictions[0:30,:].T, axis=0)

plt.figure(figsize=(12,4))
plt.boxplot(qsamp_predictions[0:30,:].T)
plt.scatter(range(1, 31), targets[0:30].T , label='True Targets', s=50)
#plt.scatter(range(1, 31), map_predictions[0:30,:].T , label='MAP')
#plt.errorbar(range(1, 31), map_predictions[0:30,:].T.squeeze(), std ,fmt = 'none')
#plt.fill_between(range(1, 31), map_predictions[0:30,:].T.squeeze() +std, map_predictions[0:30,:].T.squeeze() -std, alpha=0.4)

plt.xlabel('Predictions')
plt.ylabel('Normalized Wine Quality')
plt.title('Uncertainty in Wine Predictions')
plt.tight_layout()
plt.legend()
plt.savefig('wine_uncertainty_box.png')
plt.show()    







