"""
    Setup working environment
"""

import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from Main.Models.BBB_HS_test import BNN as BNN_HS
from Main.Models.BBB import BNN
from torch import optim




'''
PSEUDOCODE

create X matrix


for i in hidden_size:
    create normal_model(H=i)
    create bnn_model(H=i)
    
    for epoch in epochs:
        train models
        collect losses
        
plot
'''





"""
    ______________  Define FNN Model __________________
"""

#One Layer Network as in Noisy Natural Gradient Paper by Zhang et al (2018)
class FNN(nn.Module):
    
    def __init__(self,num_features, H1, H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,1)
        
    def forward(self,x):
        x = F.relu(self.NN1(x.view(-1,num_features)))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x    
    



"""
    ______________ Simulate X Data ____________________
"""
X_mu = 1
X_std = 1

# number of samples
num_obs = 200     # <---------- Define NxM data matrix dimensions
num_features = 200
print('Data Matrix dim: ', num_obs, 'x', num_features)

X_train = np.random.normal(X_mu, X_std, size = (num_obs, num_features)).astype(np.float32)
X_test  = np.random.normal(X_mu, X_std, size = (num_obs, num_features)).astype(np.float32)

# noise parameter         # <---------- Define noise level
eps = np.random.normal(0, 1.0, num_obs).reshape(num_obs, 1).astype(np.float32)
N = X_train.shape[0] #num observations


"""
    _____________  Simulate Targets Y  __________________
"""

# create Dummy FNN Model to simulate Y 
H1 = 200
H2 = 200
fnn_dummy  = FNN(num_features,H1=H1, H2 = H2) 
print('Hidden Units to simulate Y data: [%d, %d]'% (H1,H2))



### Assign Normally Distributed Weights to Dummy Model (to simulate Y)

# Sample Weights
weights_list=[]
#weights_plot=[]
for i,p in enumerate(list(fnn_dummy.parameters())):

    if len(p.size()) > 1:
        print('shape before', p.size())
        ps1 = p.size()[0] #output size
        ps2 = p.size()[1] #input size
#        w_zeros = torch.zeros(ps1,ps2-1)
        if i < 4:
            w_zeros = torch.zeros(ps1-1,ps2)
            w_vals = torch.Tensor(1,ps2).uniform_(4,6)
            print(w_zeros.shape)
            print(w_vals.shape)
            weights = torch.cat((w_zeros,w_vals),dim=0).t()

        else:
#            w_zeros = torch.zeros(ps1-1,ps2)
#            w_vals = torch.Tensor(1,ps2).uniform_(4,8)
            print(w_zeros.shape)
            print(w_vals.shape)
#            weights = torch.cat((w_zeros,w_vals),dim=0)
#            w_zeros = torch.zeros(ps1,ps2)
#            weights = w_zeros
            weights = torch.Tensor(ps1,ps2).uniform_(-0.1,0.1)

#        if i == 2:
#            w_vals = torch.Tensor(ps1,1).uniform_(-2,-1)*2
#        if i == 4:
#            w_vals = torch.Tensor(ps1,1).uniform_(-2,2)*2
#        weights = torch.cat((w_zeros.t(),w_vals.t())).t()
        print('shape after', weights.size())
#    w_vals = torch.Tensor(1,100).uniform_(1,2)*2
#    weights = torch.cat((w_zeros.t(),w_vals.t())).t()
#    weights = torch.Tensor(p.size()).normal_(0,0.1)
        weights_list.append(weights)
        print(weights_list)
    else:
        print('shape before', p.size())
        ps1 = p.size()[0]
        w_zeros = torch.zeros(ps1)
#        w_vals = torch.Tensor(1).uniform_(-2,2)*2
#        weights = torch.cat((w_zeros,w_vals))
        weights_list.append(w_zeros)
        print('shape after', w_zeros.size())
    #weights_plot.append(weights.view(-1).numpy())
    

# Assign Weights to FNN dummy
for i, p in enumerate(list(fnn_dummy.parameters())):
    p.data = weights_list[i]
    
    
# forward pass to simulate Y
y_train = fnn_dummy(torch.from_numpy(X_train)).detach().numpy() + eps
y_test  = fnn_dummy(torch.from_numpy(X_test)).detach().numpy()  + eps





"""
   ___________  Configure loops  ______________________
"""
hidden_size_list =  [200]#[2,5,10,25,50,75,100,150,200,250,300,500] #,750,1000] #,1250,1500,1750,2000] #,2250,2500,3000]
#epoch_list = [5000, 5000, 10000, 10000, 10000, 10000, 10000, 10000]
#epoch_list = [10000]*len(snr_list)
train_losses = []
test_losses = []
train_losses_bayes = []
test_losses_bayes = []

epochs = 30000
print('Epochs: ', epochs)


prior_prec = 10.0
prec_init = 50.0


for j, hidden_size in enumerate(hidden_size_list):
    
    print('Hidden Units: ', hidden_size)
    
    # Create Models
    fnn = BNN(input_size = num_features,
                    hidden_sizes = [hidden_size, hidden_size],
                    output_size = 1,
                    act_func = "Relu",
                    prior_prec = prior_prec,
                    prec_init = prec_init)
    
    fnn_bayes = BNN_HS(input_size = num_features,
                        hidden_sizes = [hidden_size, hidden_size],
                        output_size = 1,
                        act_func = "Relu",
                        prior_prec = prior_prec,
                        prec_init = prec_init)
    

    



    
    
    
    """
        Configure Optimizers
    """
    # loss
    criterion = nn.MSELoss()
    
    # optimizer
    optimizer_bayes = optim.Adam(fnn_bayes.parameters(), lr = 5e-3)

    optimizer = optim.Adam(fnn.parameters(), lr = 5e-3)
   
    
    
    """
        Train Models
    """    
    train_loss = []
    test_loss = []
    train_loss_bayes = []
    test_loss_bayes = []
    updates = []
        
    for epoch in range(epochs):
    
        running_loss = 0.0
        running_loss_bayes = 0.0
    
        mus = []
        sigmas = []
        fs = []
        weights = []
        
        inputs  = torch.from_numpy(X_train)
        targets = torch.from_numpy(y_train)
        
#        params_dict = optimizer_bayes.sample(return_params=True)
#        
#        for i, p in enumerate(fnn.parameters()):
#            mu, sigma, f = params_dict["mu{0}".format(i)],\
#                            params_dict["sigma{0}".format(i)],\
#                            params_dict["f{0}".format(i)]
#                        
#            mus += [mu.view(-1).numpy()]
#            sigmas += [sigma.view(-1).numpy()]
#            fs += [f.view(-1).numpy()]
#            weights += [p.data.view(-1).numpy()]
            
        
        # forward pass
        outputs_bayes = fnn_bayes(inputs)
        outputs       = fnn(inputs)
        
        # compute loss
        loss_bayes = criterion(outputs_bayes, targets)
        loss       = criterion(outputs, targets)
        
        optimizer.zero_grad()
        optimizer_bayes.zero_grad()
        
        # backpropagate
        loss.backward()
        loss_bayes.backward()
        
        # optimize/learn
        optimizer_bayes.step()
        optimizer.step()
        
        # collect losses
        running_loss += loss.item()
        running_loss_bayes += loss_bayes.item()
        
#        print('[%d] ====> train loss: %.3f' %
#                      (epoch + 1, running_loss))
        
        
        
        #___________ TEST/VAL _____________#
        
        test_inputs = torch.from_numpy(X_test)
        test_targets = torch.from_numpy(y_test)
        
#        optimizer_bayes.sample(training = False)
        
        # forward pass
        test_outputs = fnn(test_inputs)
        test_outputs_bayes = fnn_bayes(test_inputs, training = False)
        
        # compute loss
        test_loss_val = criterion(test_outputs, test_targets)
        test_loss_val_bayes = criterion(test_outputs_bayes, test_targets)
        
        # collect losses
        train_loss += [loss.item()]
        test_loss += [test_loss_val.item()]
        
        train_loss_bayes += [loss_bayes.item()]
        test_loss_bayes += [test_loss_val_bayes.item()]
        
        updates += [epoch]
        
        train_losses += [train_loss[-1]]
        test_losses += [test_loss[-1]]
        
        
        train_losses_bayes += [train_loss_bayes[epoch]]
        test_losses_bayes += [test_loss_bayes[epoch]]
        
        
        if epoch % 100 == 99:
            print('=====> Epoch %d out of %d' % ((epoch+1), epochs))
            print("=====> Train loss: %.3f" % train_loss[epoch])   
            print("=====> Test loss: %.3f" % test_loss[epoch])
            print("=====> Train loss Bayes: %.3f" % train_loss_bayes[epoch])   
            print("=====> Test loss Bayes: %.3f" % test_loss_bayes[epoch])


mu, sigma, z = fnn_bayes.return_params()
mu_gauss, sigma_gauss, weight_gauss = fnn.return_params()

#m_hs = mu[0].reshape(mu[0].shape[0]*mu[0].shape[1])
m_hs = mu[0]*z[0]
plt.figure(figsize=(10,6))
plt.boxplot(m_hs[:,:], vert=True)
plt.xlabel("Neuron #", fontsize=12)
plt.ylabel("weight value", fontsize=12)
plt.savefig('hs_neuron_selection_full_alt.eps')
plt.show()

m_hs = m_hs.reshape(m_hs.shape[0]*m_hs.shape[1])
m_gauss = mu_gauss[0].reshape(mu_gauss[0].shape[0]*mu_gauss[0].shape[1])

tol = 1e-2
print("# weights in HS below %.e = %d" % (tol, len(m_hs[abs(m_hs)<tol])))
      
#p_list = list(fnn.parameters())
#p = p_list[0]
#p = p.data.view(-1).numpy()

print("# weights in Gaussian model below %.e = %d" % (tol, len(m_gauss[abs(m_gauss)<tol])))

p_list_true = list(fnn_dummy.parameters())
p_true = p_list_true[0].data.t()
plt.boxplot(p_true[:,:], vert=True)
plt.show()
p_true = p_true.data.view(-1).numpy()

plt.hist([p_true,m_hs,m_gauss],bins=20, normed = False, edgecolor='black')
plt.xlabel('x')
plt.ylabel('count')
plt.legend(['true', 'HS', 'Gaussian'])
plt.show() 

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(9,4))
fig.tight_layout(h_pad = 5, w_pad = 6, pad=4)
plt.subplot(1,2,1)
plt.hist(m_gauss,bins=40,edgecolor='black')
plt.xlabel("weights", fontsize=12)
plt.ylabel("count", fontsize=12)
#plt.xscale("log")
plt.subplot(1,2,2)
plt.hist(m_hs[m_hs < 1],bins=40,edgecolor='black',alpha=1.0)
#plt.xscale("log")
plt.xlabel("weights", fontsize=12)
plt.ylabel("count", fontsize=12)
plt.savefig("hs_weight_exp_1.eps")
plt.show()

      
#print("Estimated weights: ", np.squeeze(mus))
#print("True weights: ", w_sample)


#plt.hist(mus, bins = 10,edgecolor='black')
#plt.hist(w_sample, bins = 10,edgecolor='black')
#plt.hist(fs, bins = 40,edgecolor='black')
#plt.show()

#    print('=============== PLOTTING ===============')
#
#    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,5))
#    fig.tight_layout(h_pad = 5, w_pad = 5)
#    
#    plt.subplot(2,2,1)     
#    plt.plot(updates, train_loss)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("SGD training loss")
#    
#    
#    plt.subplot(2,2,2)
#    plt.plot(updates, test_loss)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("SGD test loss")
#    
#    
#    plt.subplot(2,2,3)     
#    plt.plot(updates, train_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("Noisy Adam training loss")
#    
#    
#    plt.subplot(2,2,4)
#    plt.plot(updates, test_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("Noisy Adam test loss")
#    
#    plt.show()
    
    
#fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
#fig.tight_layout(h_pad = 5, w_pad = 5)
#
#plt.subplot(2,2,1)
##plt.plot(updates, train_loss) # plot something
#plt.hist([mus, weights_plot], bins = 10, edgecolor='black', normed = True)
#plt.xlabel("mu")
#plt.title("mu histogram of variational posterior")
#plt.legend(['Estimated means','true weights'])
#
#plt.subplot(2,2,2)
#
#plt.hist(sigmas, bins = 10, edgecolor='black', normed = True)
#plt.xlabel("standard deviation")
#plt.title("standard deviation histogram of variational posterior")
#plt.legend(['Layer 1','Layer 2','Layer 3'])
#
#plt.subplot(2,2,3)
#
#plt.hist([weights, weights_plot], bins = 10, edgecolor='black', normed = True)
##x = np.linspace(np.min(weights),np.max(weights),5000)
##pdf_t = stats.t.pdf(x, df=df)
##plt.plot(x,pdf_t)
#plt.xlabel("weights")
#plt.title("histogram of all weights in BNN")
#plt.legend(['Trained weights', 'True weights'])
#
## update canvas immediately
#plt.show()

#    
#plt.plot(hidden_size_list, train_losses)
#plt.plot(hidden_size_list, test_losses)
##plt.xlabel("Model Complexity"), plt.ylabel("loss")
##plt.title("Adam train/test loss across Model Complexity")
##plt.legend(["Train loss", "Test loss"])
##plt.show()
#
#plt.plot(hidden_size_list, train_losses_bayes, '-.')
#plt.plot(hidden_size_list, test_losses_bayes, '-.')
##plt.plot(snr_list[0:], [4]*len(snr_list[0:]), '--')
#plt.xlabel("Model Complexity"), plt.ylabel("loss")
#plt.title("Train/Test loss across Model Complexity")
#plt.legend(["Train loss", "Test loss", "BNN - Train loss", "BNN - Test loss"])
#plt.show()