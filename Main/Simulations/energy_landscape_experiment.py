# -*- coding: utf-8 -*-
"""
Created on Mon Jul 30 16:21:12 2018

@author: stud
"""

import os, sys, inspect
import argparse

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
#parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

import torch
import torch.nn as nn
import torch.nn.init as init
import torch.optim as optim
from torch.autograd import Variable
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output
import matplotlib.mlab as mlab
#from Optimizers.Vprop4 import Vprop as Vprop
#from Optimizers.noisyAdam import noisyAdam as noisyAdam
#from Optimizers.noisyKFAC3 import noisyKFAC as noisyKFAC
#from Testing.Ramiro.Optimizers.Newton import Newton as Newton

from Main.Optimizers.noisyAdam_closure_custom import noisyAdam
from Main.Optimizers.Vprop_closure import Vprop as Vprop
from Main.Optimizers.noisyKFAC_closure import noisyKFAC as noisyKFAC



#from Utilities.create_dataloaders import create_dataloaders 
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter


from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import numpy as np
import torch
from torch.utils.data import DataLoader
import torch.nn.functional as F



# FNN Linear Model 
class LinearRegressionModel(nn.Module):
    
    def __init__(self, in_dim, out_dim):
        super(LinearRegressionModel, self).__init__()
        self.Linear = nn.Linear(in_dim, out_dim, bias=False)
        
    def forward(self, x):
        return   F.sigmoid(self.Linear(x)) #self.Linear(x)
    



def plot_landscape_and_losses(w_mu, wdict, losses, labels, num_models):
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(7,3)) 
    fig.tight_layout()    
    clear_output(wait=True)
    
    # plot 1 Error Landscape
    alpha=0.5
    plt.subplot(1,2,1)
    CS = plt.contour(W1, W2, Z, 15)
    plt.clabel(CS, inline=1, fontsize=10)
    plt.scatter(w_mu[0], w_mu[1], s=50, color='k')
    plt.scatter(-7.5, -2.5, s=50, color='y')
    plt.scatter(wdict['vpp_w1'], wdict['vpp_w2'], label='Vprop', s=10, alpha=alpha)
    plt.scatter(wdict['nad_w1'], wdict['nad_w2'], label='Noisy Adam', s=10,alpha=alpha)
    plt.scatter(wdict['kfac_w1'], wdict['kfac_w2'], label='Noisy KFAC', s=10, marker='x',alpha=alpha)
    plt.scatter(wdict['sgd_w1'], wdict['sgd_w2'], label='SGD', s=10, marker= 'x', alpha=alpha)
    plt.scatter(wdict['ad_w1'], wdict['ad_w2'], label='Adam', s=10, marker='P',alpha=alpha)
    #plt.plot(wdict['lbfgs_w1'], wdict['lbfgs_w2'], label=labels[4])
    #plt.plot(wdict['ne_w1'], wdict['ne_w2'], label='Newton', s=10, alpha=alpha, marker='.', size=30)
    plt.legend(loc='upper right')
    plt.xlabel(r'$w_1$')
    plt.ylabel(r'$w_2$')
    
    # plot 2 losses
    plt.subplot(1,2,2)
    for i in range(num_models):
        plt.plot(losses[i] , label=labels[i])
    plt.legend(loc='upper right')
    plt.xlabel('Epochs')
    plt.ylabel('Train Loss')
    plt.show()
    





def train_deterministic(model, model_name, epochs, lr,  x_train, y_train, wdict):
    
    assert (model_name == 'adam') or (model_name == 'sgd') or (model_name == 'newton'), 'model name not supported. should be either adam, sgd or newton'

    """Optimizer"""
    criterion = nn.MSELoss()
    
    if model_name is 'sgd':
        optimizer = optim.SGD(model.parameters(), lr = lr)
        
        w1 = 'sgd_w1'
        w2 = 'sgd_w2'
    
    elif model_name is 'adam':
        optimizer = optim.Adam(model.parameters(), lr = lr)
        
        w1 = 'ad_w1'
        w2 = 'ad_w2'
        
    elif model_name is 'newton':
        optimizer = Newton(model.parameters(), lr = lr)
        
        w1 = 'nw_w1'
        w2 = 'nw_w2'
    
    
    
    """Train"""
    
    train_losses=[]

    for epoch in range(epochs):
        
        x  = torch.from_numpy(x_train)
        y  = torch.from_numpy(y_train)
        
        outputs = model(x)
        optimizer.zero_grad()
        loss = criterion(outputs, y)
        
        
        if model_name is 'newton':
            loss.backward(retain_graph=True)
            optimizer.step(loss, model)
        else:
            loss.backward()
            optimizer.step()
        
        train_losses.append(loss[0])

   
        # store optimization walk path
        for p in list(model.parameters()):
            wdict[str(w1)].append(p.data[0][0].item())
            wdict[str(w2)].append(p.data[0][1].item())
            
        print('epoch {}, train loss   {}'.format(epoch, loss[0]))
        

    return train_losses, wdict



def train_noisy(model, model_name, epochs, lr, x_train, y_train, wdict):
    
    assert (model_name == 'noisy_kfac') or (model_name == 'noisy_adam') or (model_name == 'vprop'), 'model name not supported. should be either noisy_kfac, vprop or noisy_adam'

    N = x_train.shape[0]
    criterion = nn.MSELoss()
    
    
    ''' Optimizers '''
    if model_name == 'noisy_adam':
        optimizer = noisyAdam(model.parameters(), 
                              num_examples=N, 
                              gamma_ex=0.0,
                              var_init = 0.001,
                              kl_weight=1, #0.68,
                              lr=lr, 
                              #betas = (beta1, beta2), 
                              prior_var=1)
        w1 = 'nad_w1'
        w2 = 'nad_w2'
        
    elif model_name == 'noisy_kfac':
        optimizer = noisyKFAC(model.parameters(),
                          model.modules(),
                          num_examples=N,
                          t_inv= 20,
                          t_stats=10,
                          lr = lr,
                          beta = 0.99,
                          prior_var = 1, #, 0.7,
                          kl_weight = 1) #0.125)
        w1 = 'kfac_w1'
        w2 = 'kfac_w2'
        
    elif model_name == 'vprop':
        optimizer = Vprop(Vprop_model.parameters(),
                          lr=lr,
                          num_examples=N)
        w1 = 'vpp_w1'
        w2 = 'vpp_w2'
    
    
    
    ''' Train '''
    train_losses=[]
    for epoch in range(epochs):

        x  = torch.from_numpy(x_train)
        y  = torch.from_numpy(y_train)

        def closure():
            optimizer.zero_grad()
            outputs = model(x) 
            loss = criterion(outputs, y)
            loss.backward()
            return loss
        
        loss = optimizer.step(closure)
        train_losses.append(loss[0])
        
        # store optimization walk path
        for p in list(model.parameters()):
            wdict[str(w1)] += [p.data[0][0].numpy()]
            wdict[str(w2)] += [p.data[0][1].numpy()]
            
        print('epoch {}, train loss   {}'.format(epoch, loss[0]))

    
    return train_losses, wdict






##############################################################################

'''1. Data '''

# Weights
w_mu  = np.array([[0.2 , 0.1]], dtype=np.float32).transpose()    # <======== Define W mu
print('w_mu shape (kx1): ', w_mu.shape)
print('w_mu: ', w_mu)


# X Data
X_mu  = np.array( [2. , 3.] )                             # <======== Define X mu
X_S   = np.matrix(' 1 0 ; 0 1 ')                          # <======== Define X Sigma
n     = 1000                                               # <======== Define number of data samples to be drawn

X = np.random.multivariate_normal( X_mu, X_S, n).astype(np.float32)
print('X shape (nxk):    ', X.shape)


# Noise
e_sigma = 0.1                          # <======== Define Noise Sigma
eps = np.random.normal(0, e_sigma, n).reshape(n,1).astype(dtype=np.float32)
print('Noise shape (nx1): ', eps.shape)


# Y Data (with some noise)
Y = np.add( np.matmul( X, w_mu), eps)
print('y shape (nx1): ', Y.shape)






'''2. Models '''
input_dim  = 2
output_dim = 1

SGD_model       = LinearRegressionModel(input_dim,output_dim)
Adam_model      = LinearRegressionModel(input_dim,output_dim)
LBFGS_model     = LinearRegressionModel(input_dim,output_dim)
Newton_model    = LinearRegressionModel(input_dim,output_dim)
Vprop_model     = LinearRegressionModel(input_dim,output_dim)
noisyAdam_model = LinearRegressionModel(input_dim,output_dim)
noisyKFAC_model = LinearRegressionModel(input_dim,output_dim)







'''3. Initialization Point'''

# W Initialization Point
#i0 = torch.Tensor([-7.5 , -2.5])  
i0 =  [-7.5 , -2.5]    # <========= Define Init Point
#i0.resize_(1,2)
for p in list(SGD_model.parameters()):
    p.data = torch.tensor(i0).resize_(1,2)
for p in list(Adam_model.parameters()):
    p.data = torch.tensor(i0).resize_(1,2)
for p in list(Newton_model.parameters()):
    p.data = torch.tensor(i0).resize_(1,2)
#for p in list(LBFGS_model.parameters()):
#    p.data = i0
for p in list(Vprop_model.parameters()):
    p.data = torch.tensor(i0).resize_(1,2) 
for p in list(noisyAdam_model.parameters()):
    p.data = torch.tensor(i0).resize_(1,2) 
for p in list(noisyKFAC_model.parameters()):
    p.data = torch.tensor(i0).resize_(1,2) 
    
# store walks in wdict
wdict = {}
wdict['sgd_w1']  = [-7.5]#[i0[0][0].numpy()]
wdict['sgd_w2']  = [-2.5] #[i0[0][1].numpy()]
wdict['ne_w1']  = [-7.5]#[i0[0][0].numpy()]
wdict['ne_w2']  = [-2.5] #[i0[0][1].numpy()]
wdict['ad_w1']  = [-7.5]#[i0[0][0].numpy()]
wdict['ad_w2']  = [-2.5] #[i0[0][1].numpy()]
wdict['vpp_w1']  = [-7.5] #[i0[0][0].numpy()]
wdict['vpp_w2']  = [-2.5] #[i0[0][1].numpy()]
wdict['nad_w1']  = [-7.5]#[i0[0][0].numpy()]
wdict['nad_w2']  = [-2.5] #[i0[0][1].numpy()]
wdict['kfac_w1'] = [-7.5]# [i0[0][0].numpy()]
wdict['kfac_w2'] = [-2.5] #[i0[0][1].numpy()]
    
    



'''4. Z Grid of Energy Landscape '''

ws = 10                                      # <============= define window scale factor (*resolution)
W1 = np.arange(w_mu[0]-ws, w_mu[0]+ws, 0.1)
W2 = np.arange(w_mu[1]-ws, w_mu[1]+ws, 0.1)
W1, W2 = np.meshgrid(W1, W2)

#eps = np.random.normal(0, 5, 2*n).reshape(n,2)
Z = np.zeros(shape=(len(W1), len(W1)))
for i in range(len(W1)):
    for j in range(len(W1)):
        Xw = np.matmul(X, np.row_stack((W1[0,i], W2[j,0])).astype(np.float32))
        Z[i,j] = np.mean((Y.transpose() - Xw)**2)



epochs=150
lr=1e-2

v_losses, wdict = train_noisy(Vprop_model, 'vprop', epochs=epochs, lr=1e-1, x_train=X, y_train=Y, wdict=wdict)
n_losses, wdict = train_noisy(noisyAdam_model, 'noisy_adam', epochs=epochs, lr=1e-1, x_train=X, y_train=Y, wdict=wdict)

k_losses1, wdict = train_noisy(noisyKFAC_model, 'noisy_kfac', epochs=1, lr=1e-6, x_train=X, y_train=Y, wdict=wdict)
k_losses2, wdict = train_noisy(noisyKFAC_model, 'noisy_kfac', epochs=149, lr=1e-2, x_train=X, y_train=Y, wdict=wdict)
k_losses = k_losses1 + k_losses2

s_losses, wdict = train_deterministic(SGD_model, 'sgd', epochs=epochs, lr=1e-2, x_train=X, y_train=Y, wdict=wdict)
a_losses, wdict = train_deterministic(Adam_model, 'adam', epochs=epochs, lr=1e-1, x_train=X, y_train=Y, wdict=wdict)
#n_losses, wdict = train_deterministic(Newton_model, 'newton', epochs=epochs, lr=1e-1, x_train=X, y_train=Y, wdict=wdict)



losses=[v_losses,n_losses,k_losses, s_losses, a_losses, n_losses]
labels=['Vprop','Noisy Adam','Noisy KFAC', 'SGD', 'Adam']

plot_landscape_and_losses(w_mu, wdict, losses, labels, num_models=5)

