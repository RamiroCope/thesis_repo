import os, sys, inspect
import argparse

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
#parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

from Datasets.Regression.data import data as data_gen
from Utilities.create_dataloaders import create_dataloaders
from Optimizers.noisyAdam_closure_custom import noisyAdam
from Optimizers.Vprop_closure import Vprop as Vprop
from Optimizers.noisyKFAC_closure import noisyKFAC as noisyKFAC

import torch
import torch.nn.functional as F
import torch.nn as nn

import torch.nn.init as init
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.distributions import Normal


import numpy as np
import matplotlib.pyplot as plt


"""Model"""
class FNN(nn.Module):
    
    def __init__(self,num_features, H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=True)
        self.NN2 = nn.Linear(H1,1, bias=True)
        self.NN3 = nn.Sigmoid()
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        x = self.NN3(x)
        return x

def train_noisy(model, x, y, epochs=500, lr=1e-3, gamma_ex=None, var_init=0.01, kl_weight=1, prior_var=0.1):
    
    
    """Data Preparation"""
    N = x.shape[0]
    
    x = torch.from_numpy(x.reshape(-1,1)).float()
    y = torch.from_numpy(y.reshape(-1,1)).float()

    
    
    """Optimizer"""
    criterion = nn.BCELoss()
    optimizer = noisyAdam(model.parameters(), 
                          num_examples=N, 
                          gamma_ex=gamma_ex, #0.1,
                          var_init=var_init, #0.001,
                          kl_weight=kl_weight, # 0.275,
                          lr=lr, 
                          prior_var=prior_var) #0.81)
    
#    optimizer = Vprop(model.parameters(),
#                          num_examples=N, 
#                          alpha=0.99,
#                          var_init = 0.01,
#                          lr=1e-3, 
#                          prior_var=prior_var)
    
#    optimizer = noisyKFAC(model.parameters(),
#                      model.modules(),
#                      num_examples=N,
#                      t_inv= 20,
#                      t_stats=1,
#                      lr = 1e-6,
#                      beta = 0.99,
#                      prior_var = prior_var,
#                      kl_weight = kl_weight)
    
    
    """Train"""
#    warm_up=2
        
    for epoch in range(epochs):
        
#        if epoch == warm_up:
#            for param_group in optimizer.param_groups:
#                param_group['lr'] = 1e-3
        
        def closure():
            optimizer.zero_grad()
            outputs = model(x) 
            loss = criterion(outputs, y)
            loss.backward()
            return loss
        
        loss, params = optimizer.step(closure, return_params=True)
        if epoch % 10 ==0:
            print(loss)

    return model, params




def predict_with_qsamples(model, params, num_predictions, testset):
        
    testset = torch.from_numpy(testset.reshape(-1,1)).float()
    n = testset.shape[0]
    predictions = torch.ones([n,num_predictions], requires_grad=False)
    
    # store mu of variational posterior
    for i, p in enumerate(list(model.parameters())):
        params['mu{}'.format(i)] = p.data 
    
    # get map predictions
    map_predictions = [model(x) for x in testset]
    map_predictions = torch.cat(map_predictions, 0).squeeze().detach().numpy()
    
    for j in range(num_predictions):
        
        # 1. sample from variational posterior
        for i, p in enumerate(list(model.parameters())):
            q_dist = Normal(params['mu{}'.format(i)], params['sigma{}'.format(i)])
            p.data = q_dist.sample()
            
        # 2. forward pass with new weights
#        qsamp_predictions = [model(x) for x in testset]
#        predictions[:,j] = torch.cat(qsamp_predictions, 0).squeeze().detach().numpy()
        list_of_outputs=[]
        for x in testset:
            outputs = model(x)
            list_of_outputs.append(outputs)
            
        #store results
        all_outputs = torch.cat(list_of_outputs, 0)
        predictions[:,j] = all_outputs.squeeze()
        
    return predictions.detach().numpy(), map_predictions



###############################################################################33
""" MAIN """    


# data
train_size = 2000
test_size = 10
class0 = np.random.normal(0.35, 0.07, int(train_size/2))
class1 = np.random.normal(0.65, 0.07, int(train_size/2))

x_train = np.hstack([class0,class1])
y_train = np.hstack([np.zeros(int(train_size/2)), np.ones(int(train_size/2))])
x_test = np.linspace(0, 1, test_size)



# train models
noisy_model = FNN(1,150)
noisy_model, params = train_noisy(noisy_model, x_train, y_train, lr=1e-2, epochs=3000, gamma_ex=0.01, kl_weight=0.4, var_init=0.01, prior_var=0.1)


# predict with noisy model
qsamp_predictions, map_predictions = predict_with_qsamples(noisy_model, params, num_predictions=100, testset=x_test)
   

# plot
import matplotlib
matplotlib.rcParams.update({'font.size':14})


fig = plt.figure(figsize=(12, 4), dpi=150, facecolor='w', edgecolor='w')
ax = fig.add_subplot(121)
ax.boxplot(qsamp_predictions.T, positions=[i/10 for i in range(1,11)], widths=[0.05]*10)
plt.xlim(0,1.1)
plt.xticks([0.0,0.2,0.4,0.6,0.8,1], [0.0,0.2,0.4,0.6,0.8,1])
plt.xlabel('x')
plt.ylabel(r'$\sigma(x)$')

plt.suptitle('Uncertainty in Binary Classification with BNN', fontsize=16)
ax2 = fig.add_subplot(122)
plt.hist(class0, bins=30, alpha=0.8)
plt.hist(class1, bins=30, alpha=0.8)
plt.xlabel('x')
plt.ylabel('Frequency')
plt.subplots_adjust(left=0.1, right=0.9, wspace=0.3, top=0.9, bottom=0.15)
plt.savefig('class_preds.eps')
plt.show()






  