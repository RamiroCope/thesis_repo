#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 17:20:44 2018

@author: ramiromata
"""
import os, sys, inspect
import argparse

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
#parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

from Datasets.Regression.data import data as data_gen
from Utilities.create_dataloaders import create_dataloaders
from Optimizers.noisyAdam_closure_custom import noisyAdam
from Optimizers.Vprop_closure import Vprop as Vprop
from Optimizers.noisyKFAC_closure import noisyKFAC as noisyKFAC

import torch
import torch.nn.functional as F
import torch.nn as nn

import torch.nn.init as init
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.distributions import Normal


import numpy as np
import math
import matplotlib.pyplot as plt


def f(x_min, x_max, num_samples):
    """
    Function to learn by Neural Network. As formulated in Blundell et al 2015
    Args: x_min        minimum range of training examples
          x_max        maximum range of training examples
          num_samples  number of observations desired in dataset
    
    Returns:
          x            numpy array with observations
          y            numpy array with targets
    """
    
    x = np.linspace(x_min, x_max, num_samples) 
    y = np.empty(x.shape)
    
    
    for i in range(num_samples):
        eps   = np.random.normal(0, 0.02)
        y[i] = x[i] + 0.3*np.sin(2*math.pi*(x[i] + eps)) + 0.3*np.sin(4*math.pi*(x[i] + eps)) + eps
        
    return x, y 




def add_noise(y, sigma):
    noise = np.random.normal(0,sigma, y.shape[0])
    return y + noise




"""Model"""
class FNN(nn.Module):
    
    def __init__(self,num_features, H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=True)
        self.NN2 = nn.Linear(H1,1, bias=True)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x



def train_adam(model, x, y, epochs, lr, return_predictions=False, x_test=None):
    
    
    """Data Preparation"""
    
    x = torch.from_numpy(x.reshape(-1,1)).float()
    y = torch.from_numpy(y.reshape(-1,1)).float()

    
    
    """Optimizer"""
    criterion = nn.MSELoss()

    optimizer = optim.Adam(model.parameters(), lr = lr)
    
    
    
    """Train"""
    for epoch in range(epochs):
        
        
        
        outputs = model(x)
        optimizer.zero_grad()
        loss = criterion(outputs, y)
        loss.backward()
        optimizer.step()

        if epoch % 10 ==0:
            print(loss)
            
    if return_predictions and x_test is not None:
        x_test = torch.from_numpy(x_test.reshape(-1,1)).float()
        predictions = [model(x) for x in x_test]
        predictions = torch.cat(predictions, 0).squeeze().detach().numpy()
        return model, predictions
    
    return model 


def train_noisy(model, x, y, epochs=500, lr=1e-3, gamma_ex=None, var_init=0.01, kl_weight=1, prior_var=0.1):
    
    
    """Data Preparation"""
    N = x.shape[0]
    
    x = torch.from_numpy(x.reshape(-1,1)).float()
    y = torch.from_numpy(y.reshape(-1,1)).float()

    
    
    """Optimizer"""
    criterion = nn.MSELoss()
    optimizer = noisyAdam(model.parameters(), 
                          num_examples=N, 
                          gamma_ex=gamma_ex, #0.1,
                          var_init=var_init, #0.001,
                          kl_weight=kl_weight, # 0.275,
                          lr=lr, 
                          prior_var=prior_var) #0.81)
    
#    optimizer = Vprop(model.parameters(),
#                          num_examples=N, 
#                          alpha=0.99,
#                          var_init = 0.01,
#                          lr=1e-3, 
#                          prior_var=prior_var)
    
#    optimizer = noisyKFAC(model.parameters(),
#                      model.modules(),
#                      num_examples=N,
#                      t_inv= 20,
#                      t_stats=1,
#                      lr = 1e-6,
#                      beta = 0.99,
#                      prior_var = prior_var,
#                      kl_weight = kl_weight)
    
    
    """Train"""
#    warm_up=2
        
    for epoch in range(epochs):
        
#        if epoch == warm_up:
#            for param_group in optimizer.param_groups:
#                param_group['lr'] = 1e-3
        
        def closure():
            optimizer.zero_grad()
            outputs = model(x) 
            loss = criterion(outputs, y)
            loss.backward()
            return loss
        
        loss, params = optimizer.step(closure, return_params=True)
        if epoch % 10 ==0:
            print(loss)

    return model, params




def predict_with_qsamples(model, params, num_predictions, testset):
        
    testset = torch.from_numpy(testset.reshape(-1,1)).float()
    n = testset.shape[0]
    predictions = torch.ones([n,num_predictions], requires_grad=False)
    
    # store mu of variational posterior
    for i, p in enumerate(list(model.parameters())):
        params['mu{}'.format(i)] = p.data 
    
    # get map predictions
    map_predictions = [model(x) for x in testset]
    map_predictions = torch.cat(map_predictions, 0).squeeze().detach().numpy()
    
    for j in range(num_predictions):
        
        # 1. sample from variational posterior
        for i, p in enumerate(list(model.parameters())):
            q_dist = Normal(params['mu{}'.format(i)], params['sigma{}'.format(i)])
            p.data = q_dist.sample()
            
        # 2. forward pass with new weights
#        qsamp_predictions = [model(x) for x in testset]
#        predictions[:,j] = torch.cat(qsamp_predictions, 0).squeeze().detach().numpy()
        list_of_outputs=[]
        for x in testset:
            outputs = model(x)
            list_of_outputs.append(outputs)
            
        #store results
        all_outputs = torch.cat(list_of_outputs, 0)
        predictions[:,j] = all_outputs.squeeze()
        
    return predictions.detach().numpy(), map_predictions





###############   MAIN   ################

# create train and test data
x_train, y_train = f(0, 0.5, 5000)
x_test, y_test   = f(-0.3, 1.2, 500)



# train models
noisy_model = FNN(1,150)
noisy_model, params = train_noisy(noisy_model, x_train, y_train, lr=1e-2, epochs=3000, gamma_ex=0.01, kl_weight=0.0000001, var_init=0.001, prior_var=0.01)


adam_model  = FNN(1,150)
adam_model, adam_predictions = train_adam(adam_model, x_train, y_train, 1000, 1e-2, True, x_test)


# predict with noisy model
qsamp_predictions, map_predictions = predict_with_qsamples(noisy_model, params, num_predictions=100, testset=x_test)
     

# predict with noisy model 2
noisy_model2 = FNN(1,150)
noisy_model2, params2 = train_noisy(noisy_model2, x_train, y_train, lr=1e-2, epochs=2000, gamma_ex=0.01, kl_weight=0.4, var_init=0.001, prior_var=1)
qsamp_predictions2, map_predictions2 = predict_with_qsamples(noisy_model2, params2, num_predictions=100, testset=x_test)


           
# Uncertainty intervals
mean   = np.mean(qsamp_predictions2, 1)
median = np.median(qsamp_predictions2, 1)
q99, q75, q25, q1 = np.percentile(qsamp_predictions2, [99.5, 75 ,25, 0.5], 1)
std = np.std(qsamp_predictions2, 1)

import matplotlib
matplotlib.rcParams.update({'font.size':14})
fig = plt.figure(figsize=(4, 4), dpi=150, facecolor='w', edgecolor='w')
plt.fill_between(x_test, q1, q99, alpha=0.2, color='blue')
plt.fill_between(x_test, q25, q75, alpha=0.4)
plt.scatter(x_test, y_test, s=1, color='g')
plt.scatter(x_train, y_train, s=1, color='k')
plt.scatter(x_test, median, s=1, color='r')
#plt.scatter(x_test, map_predictions2, s=1, label='noisy adam predictions')
#plt.scatter(x_test, adam_predictions, s=1, label='adam predictions')
plt.ylim(-0.75, 1.2)
plt.xlim(-0.3, 1.2)
plt.xlabel('x')
plt.ylabel('f(x)')
plt.tight_layout()
plt.savefig('blundells_wave.png')




# noisy: plot observations with predictions 
#plt.scatter(x_test, map_predictions, s=1, label='noisy adam predictions')
#plt.scatter(x_test, adam_predictions, s=1, label='adam predictions')
#plt.scatter(x_test, y_test, s=0.1, label='test points')
#plt.scatter(x_train, y_train, s=0.1, label='training points')
#plt.title('Noisy Adam -VS- Adam Predictions on Blundell Regression')
#plt.legend()
#plt.show()







