#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 29 14:32:41 2018

@author: ramiromata
"""

"""
NOTE: 
    CONCLUSION: THERE SEEMS TO BE NO STRONG TENDENCIES 
    ON PARAMETER SENSITIVITY BASED ON LOOKING AT THE BAYES OPT RESULTS.
    THEREFORE,
    THE PLOTS ARE NOT NECESSARY. 
"""


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Load WINE CSV Bayes opt files
parent_dir = '../HyperparamOpt/scripts/UCI/results/'
bbb_wine   = pd.read_csv(parent_dir + 'bayesopt_BBB_kl_divided_by_N_PROPER')
vprop_wine = pd.read_csv(parent_dir + 'bayesopt_WINE_Vprop_PROPER')
nadam_wine = pd.read_csv(parent_dir + 'bayesopt_WINE_noisyAdam_closure_custom_PROPER')
kfac_wine =  pd.read_csv(parent_dir + 'bayesopt_WINE_noisyKFAC_closure_PROPER')




# Prior Variance Sensitivity

# BBB WINE
plt.title('Parameter Sensitivity in BBB for Wine Dataset')
plt.scatter(bbb_wine['# prior_var'], bbb_wine[' target'])
plt.xlabel('prior variance'); plt.ylabel('performance')


# VPROP WINE 
plt.title('Parameter Sensitivity in Vprop for Wine Dataset')
plt.scatter(vprop_wine['# prior_var'], vprop_wine[' target'])
plt.xlabel('prior variance'); plt.ylabel('performance')


# NADAM Prior Var and KL results
cm = plt.cm.get_cmap('RdYlBu')
z = kfac_wine[' target']
sc = plt.scatter(nadam_wine['# prior_var'], nadam_wine[' kl_weight'], c=z, cmap=cm)
plt.colorbar(sc)
plt.show()