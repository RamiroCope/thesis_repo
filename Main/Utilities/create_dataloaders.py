from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import numpy as np
import torch
from torch.utils.data import DataLoader


"""
    Utility Functions
"""

def create_dataloaders(X, y, test_size, train_batch_size,train_shuffle, 
                       standardize=None, toarray=False, make_Y_binary=False):
    """
    Desc: This function creates a train and test pytorch DataLoader for
          regression and classification benchmark datasets.
          
    INPUT: X, y        : Numpy type X matrix and y targets
           test_size   : Float between 0 and 1
           shuffle     : Boolean True or False
           standardize : True to scale input features, None by default
           
    OUTPUT: Pytorch Train and Test DataLoader
    """
    assert(test_size <= 1 and test_size >= 0)
    
    if toarray:
        # For sparse matrices
        X = X.toarray()
    
    if standardize:
        # Standardize X matrix 
        X = preprocessing.scale(X)
    
    if make_Y_binary:
        # convert from (-1 and 1) to (0 and 1)
        y = (y > 0).astype(int)
    
    X, Xtest, y, ytest = train_test_split(X, y, test_size=test_size)
    
    # Merging
    train_data   = np.c_[ X, y ]
    test_data    = np.c_[ Xtest, ytest ]
    
    # Convert to Floats
    train_data   = torch.from_numpy(train_data).float()
    test_data    = torch.from_numpy(test_data).float()
    
    # Create Data Loaders
    if test_size == 0:
        return DataLoader(train_data, batch_size=train_batch_size, shuffle=train_shuffle)
    
    elif test_size ==1:
        return DataLoader(test_data, batch_size=len(Xtest))
    
    else: 
        trainLoader  = DataLoader(train_data, batch_size=train_batch_size, shuffle=train_shuffle)
        testLoader   = DataLoader(test_data, batch_size=len(Xtest))
        return trainLoader, testLoader
