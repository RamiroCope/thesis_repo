import torch
from torch.optim import Optimizer
from torch.distributions import Normal

class Vprop(Optimizer):
    """Implements Vprop algorithm.

    Proposed by Mohammad Emtiyaz Khan et al.
    `<http://bayesiandeeplearning.org/2017/papers/50.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-2)
        momentum (float, optional): momentum factor (default: 0)
        alpha (float, optional): smoothing constant (default: 0.99)
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        centered (bool, optional) : if True, compute the centered RMSProp,
            the gradient is normalized by an estimation of its variance
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        gamma (float, optional): prior precision (default: 0.1)

    """

    def __init__(self,
                 params,
                 lr=1e-2,
                 alpha=0.99,
                 eps=1e-8,
                 gamma=0.1,
                 weight_decay=0,
                 momentum=0,
                 centered=False):
        defaults = dict(lr=lr, momentum=momentum, alpha=alpha, eps=eps, centered=centered, weight_decay=weight_decay, gamma=gamma)
        super(Vprop, self).__init__(params, defaults)

    def __setstate__(self, state):
        super(Vprop, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('momentum', 0)
            group.setdefault('centered', False)

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """

        for group in self.param_groups:
            for p in group['params']:
                state = self.state[p]
                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    state['square_avg'] = p.data.new().resize_as_(p.data).zero_()
                    state['noise'] = p.data.new().resize_as_(p.data).zero_()
                    
                    if group['momentum'] > 0:
                        state['momentum_buffer'] = p.data.new().resize_as_(p.data).zero_()
                    if group['centered']:
                        state['grad_avg'] = p.data.new().resize_as_(p.data).zero_()

                normal_dist = Normal(state['noise'].new().resize_as_(state['noise']).zero_(),
                                     (p.data.new().resize_as_(p.data).zero_() + 1).div_(state['square_avg'].add(group['gamma']).sqrt_()))  # Normal distribution with zero mean and std=1
                
                state['noise'] = normal_dist.sample()
                p.data.add_(state['noise'])

        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                
                grad = p.grad.data

                state = self.state[p]

                # remove additive noise
                p.data.add_(-state['noise'])

                square_avg = state['square_avg']

                alpha = group['alpha']

                state['step'] += 1

                if group['weight_decay'] != 0:
                    grad = grad.add(group['weight_decay'], p.data)

                square_avg.mul_(alpha).addcmul_(1 - alpha, grad, grad)

                if group['centered']:
                    grad_avg = state['grad_avg']
                    grad_avg.mul_(alpha).add_(1 - alpha, grad)
                    avg = square_avg.addcmul(-1, grad_avg, grad_avg).sqrt().add_(group['eps'])
                else:
                    #avg = square_avg.sqrt().add_(group['eps']) # avg = square_avg.add_(group['eps']).sqrt()
                    avg = square_avg.add(group['gamma'])

                if group['momentum'] > 0:
                    buf = state['momentum_buffer']
                    buf.mul_(group['momentum']).addcdiv_(grad, avg)
                    p.data.add_(-group['lr'], buf)
                else:
                    p.data.addcdiv_(-group['lr'], grad.add_(p.data.mul(group['gamma'])), avg)

        return loss
