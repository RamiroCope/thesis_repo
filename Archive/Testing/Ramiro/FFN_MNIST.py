
"""
FFN on MNIST data using pyTorch
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
#from Vprop import Vprop
from Vprop1 import Vprop1
#from scheduler import ReduceLROnPlateau

"""
Load data
"""


# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=1, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=False)

classes = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')


def imshow(img):
    #img = img / 2 + 0.5
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    #plt.imshow(npimg)
    

# get some random training images
dataiter = iter(trainLoader)
images, labels = dataiter.next()

# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(1)))



"""
Model graph
"""


class FNN(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,10)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x
    
net = FNN(100,20)
"""
print(net)

#You can get the name of the parameters
for name, param in net.named_parameters():
    print(name)


for param in net.parameters():
    print(param.size())


params = list(net.parameters())
print(len(params))
print(params[1].size())
#Extract individual dims of param sizes
print(params[1].size()[0])
"""
# Loss and optimizer
precision = 1    
criterion = nn.CrossEntropyLoss()
#optimizer = optim.SGD(net.parameters(), lr = 0.001) 
#optimizer = optim.RMSprop(net.parameters(), lr = 0.001)
optimizer = Vprop1(net.parameters(), lr = 0.0001)
#scheduler = ReduceLROnPlateau(optimizer, factor=0.5, patience=4000)

print(optimizer)
print('Precision set to: ', precision)
"""
Feeding a single digit through the net
"""
"""
datiter = iter(trainLoader)
inputs, labels = datiter.next()

inputs, labels = Variable(inputs), Variable(labels)

optimizer.zero_grad()

outputs = net(inputs)

loss = criterion(outputs, labels)

loss.backward()

optimizer.step(precision)
"""

"""
Train the model
"""


for epoch in range(2):
    
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for i, data in enumerate(trainLoader, 0):
        
        inputs, labels = data
        
        inputs, labels = Variable(inputs), Variable(labels)
        
        optimizer.zero_grad()
        
        outputs = net(inputs)
    
        loss = criterion(outputs, labels)
        #Initialized parameters' gradients are None until you do
        #loss.backward() and start computing gradients
        loss.backward()
        optimizer.step(precision)
        #scheduler.step(loss, i)
        # Print statistics
        running_loss += loss.data[0]
        if i % 2000 == 1999:
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 2000))
            running_loss = 0.0

print('Finished training')



"""
Test the model
"""

# Initial testing on a few images
test_dataiter = iter(testLoader)


for _ in range(5):

    test_img, labels = test_dataiter.next()
    
    img = torchvision.utils.make_grid(test_img)
    npimg = img.numpy()
    npimg = np.transpose(npimg, (1,2,0))
    plt.imshow(npimg)
    #plt.imshow(npimg)
    
    # print images
    #imshow(test_img)
    print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(1)))
    
    
    test_output = net(Variable(test_img))
    
    _, predicted = torch.max(test_output.data, 1)
    
    print('Predicted: ', ' '.join('%5s' % classes[predicted[j]] for j in range(1)))


# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))
