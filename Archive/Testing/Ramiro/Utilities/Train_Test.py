import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output
from tensorboardX import SummaryWriter
import torch
from torch.autograd import Variable
from torch.distributions import Normal
import math

"""
    Train & Test Utility Class
"""

class Train_Test(object):
    def __init__(self, model, compute_loss, trainLoader, testLoader, optimizer, 
                 prior_mu=0, precision=1, sample_optimizer=False, Adam=False, 
                 KL=False, plot=False, test=False, TensorBoard=False):
        self.model             = model
        self.compute_loss      = compute_loss
        self.trainLoader       = trainLoader
        self.testLoader        = testLoader
        self.optimizer         = optimizer
        self.precision         = precision
        self.sample_optimizer  = sample_optimizer
        self.Adam              = Adam
        self.KL                = KL
        self.plot              = plot
        self.test              = test
        self.TensorBoard       = TensorBoard
        self.num_batches       = len(trainLoader)
        self.gradW_history     = []
        self.gradB_history     = []
        self.LL_loss_history   = []
        self.KL_loss_history   = []
        self.ELBO_history      = []
        self.x_axis            = []
        self.test_loss_history = []
        self.precision = precision
        self.p_dist = Normal(prior_mu, np.sqrt(1.0/self.precision))
        
        if plot:
            plt.figure()
        if TensorBoard:
            self.writer = SummaryWriter()
    
    
    def run(self, epoch):
        for batch_num, train_data in enumerate(self.trainLoader):
            
            # x-axis index
            self.time = batch_num + (epoch*self.num_batches)
            
            x, y = train_data[:,0:-1], train_data[:,-1]        
            if self.model.O > 1: 
                y =  torch.LongTensor(y.numpy()) # classification  y format
            x, y = Variable(x), Variable(y)
            
            # Sample Variational Posterior
            # < Vprop pseudocode line 1 >
            if self.sample_optimizer:
                params_dict = self.optimizer.sample()
            
            # forward pass 
            y_pred  = self.model(x)
            
            # extract diagnostics
            lqw, lpw     = 0.0,0.0
            gradW, gradB =  torch.FloatTensor([0,0]), torch.FloatTensor([0,0]) #[],[]
            for i,p in enumerate(self.model.parameters()):
                mu, sigma, grad = params_dict["mu{0}".format(i)],\
                                  params_dict["sigma{0}".format(i)],\
                                  params_dict["grad{0}".format(i)]
                if self.TensorBoard:
                    self.writer.add_histogram('train/mu{}'.format(i), mu.view(-1), self.time)
                    self.writer.add_histogram('train/sigma{}'.format(i), sigma.sqrt().view(-1), self.time)
                    self.writer.add_scalar('train/grad{}'.format(i), torch.norm(grad, p=2), self.time)
                    
                # Layer 1: for W matrix
                if i == 0:
                    self.muW = mu.view(-1)
                    self.sigmaW = sigma.sqrt().view(-1)
                    
                # Layer 1: for Bias vector
                if i == 1:
                    self.muB = mu.view(-1)
                    self.sigmaB = sigma.sqrt().view(-1)
                    
                # Gradient of W matrices & Bias vectors
                if i%2 == 0:          
                    gradW = torch.cat([gradW, grad.view(-1)]) 
                if i%2 == 1:
                    gradB = torch.cat([gradB, grad.view(-1)]) 
                    
                # KL Divergence terms    
                q_dist = Normal(mu, sigma.sqrt())
                lqw += q_dist.log_prob(p.data).sum()    
                lpw += self.p_dist.log_prob(p.data).sum()
            
            # collect gradient norm
            self.gradW_history += [torch.norm(gradW, p=2)]
            self.gradB_history += [torch.norm(gradB, p=2)]
            if self.TensorBoard:
                self.writer.add_scalar('train/gradW_net', torch.norm(gradW, p=2), self.time)
                self.writer.add_scalar('train/gradB_net', torch.norm(gradB, p=2), self.time)
                
            # compute loss, get new gradients
            if self.KL:
                # ELBO loss function
                LL_loss = self.compute_loss(y_pred, y)
                KL_loss = (lqw - lpw)/self.num_batches
                ELBO    = LL_loss + KL_loss
                self.optimizer.zero_grad()
                ELBO.backward()
                
                # collect plot variables
                self.LL_loss_history += [LL_loss.data[0]]
                self.KL_loss_history += [KL_loss]
                self.ELBO_history    += [ELBO.data[0]]
                if self.TensorBoard:
                    self.writer.add_scalar('train/LL',   LL_loss.data[0], self.time)
                    self.writer.add_scalar('train/KL',   KL_loss, self.time)
                    self.writer.add_scalar('train/ELBO', ELBO, self.time)
            else:
                # log likelihood loss function
                LL_loss = self.compute_loss(y_pred, y)
                self.optimizer.zero_grad()
                LL_loss.backward()
                
                # collect plot variables
                self.LL_loss_history += [LL_loss.data[0]]
                if self.TensorBoard:
                    self.writer.add_scalar('train/LL',   LL_loss.data[0], self.time)
                
                
            # update weights
            self.optimizer.step()
            
            # eval with test data
            Train_Test.test(self)
                        
            print('====> Epoch:  ', epoch, 'Batch: ', batch_num)
            print('====> LL loss: {:.4f}'.format(LL_loss.data[0]))            
            print('====> KL loss: {:.4f}'.format(KL_loss))            
            
            # plot
            if self.plot:
                self.x_axis += [batch_num + (epoch*self.num_batches)]
                Train_Test.plot(self)
            
    def test(self):
        """
        Test Function
        """
        x, y = self.testLoader.dataset[:,:-1], self.testLoader.dataset[:,-1]
        if self.model.O > 1: 
            y =  torch.LongTensor(y.numpy()) # classification  y format
        x, y = Variable(x), Variable(y)
        
        self.optimizer.sample(training = False)
        
        # forward pass
        y_pred    = self.model(x)
        test_loss = self.compute_loss(y_pred, y)
      
        #plot variables
        self.test_loss_history += [math.log(test_loss.data[0])]
        if self.TensorBoard:
            self.writer.add_scalar('test', math.log(test_loss.data[0]), self.time)

        print('====> Test set loss: {:.4f}'.format(test_loss.data[0]))
    

    def evaluate_model(self):
        """
        Evaluate Function after model has been trained
        """
        x, y = self.testLoader.dataset[:,:-1], self.testLoader.dataset[:,-1]
        if self.model.O > 1: 
            y =  torch.LongTensor(y.numpy()) # classification  y format
        x, y = Variable(x), Variable(y)
        
        self.optimizer.sample(training = False)
        
        # forward pass
        y_pred    = self.model(x)
        
        # regression RMSE
        if self.model.O == 1:
            test_loss = self.compute_loss(y_pred, y)
            print('====> Test set RMSE: {:.4f}'.format(math.sqrt(test_loss.data[0])))
            x_axis = [x for x in range(len(y.data))]
            plt.plot(x_axis, y.data.numpy(), 'bo',label='Observations')
            plt.plot(x_axis, y_pred.data.numpy(), 'ro', label='Predictions')
            plt.legend()
            plt.show()
        
        # classification accuracy
        if self.model.O > 1:    
            """
                To get Classification Accuracy once model has been trained
            """
            _, predicted = torch.max(y_pred.data, 1)
            total        = y.size(0)
            correct      = sum(predicted == y.data)
             
            print('====> Accuracy of the network on ', total, ' samples is: ',\
                                                     (correct/total)*100, '%')
            

    def plot(self):
        """
        Plot Function
        """
        clear_output(wait=True)
        fig, axes = plt.subplots(nrows=5, ncols=2, figsize=(10,8))
        fig.tight_layout() 
        
        plt.subplot(5,2,1)
        plt.title('LL Loss')     
        plt.plot(self.x_axis, self.LL_loss_history, color="blue", linestyle="-")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,2)
        plt.title('KL Loss')     
        plt.plot(self.x_axis, self.KL_loss_history, color="black", linestyle="-")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,3)
        plt.title('Total Loss/ELBO')     
        plt.plot(self.x_axis, self.ELBO_history, color="magenta", linestyle="-")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        if self.test:
            plt.subplot(5,2,4)
            plt.cla()
            plt.title('Test Log Loss')
            plt.plot(self.x_axis, self.test_loss_history, color="red", linestyle="-")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
        
        plt.subplot(5,2,5)
        plt.cla()
        plt.title('W Grad norm')
        plt.plot(self.x_axis, self.gradW_history, color="green", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,6)
        plt.cla()
        plt.title('Bias Grad norm')
        plt.plot(self.x_axis, self.gradB_history, color="green", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,7)
        plt.cla()
        plt.title('Mu W hist')
        plt.hist(self.muW, color="red", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,8)
        plt.cla()
        plt.title('Mu B hist')
        plt.hist(self.muB, color="red", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,9)
        plt.cla()
        plt.title('Sigma W hist')
        plt.hist(self.sigmaW, color="blue", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,10)
        plt.cla()
        plt.title('Sigma Bias hist')
        plt.hist(self.sigmaB, color="blue", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
         
        plt.show()



