import pandas as pd
import torch
import os, sys
#parentPath = os.path.abspath("../../../../thesis_repo/")
#if parentPath not in sys.path:
#    sys.path.insert(0, parentPath)

class year(object):  
         
    
    def __init__(self, train = True, test_size = 0.5):

        self.train = train
        
        data = pd.read_csv("../../Main/Datasets/Regression/YearPredictionMSD.txt", header = None)
        
        # we drop target col
        X = data.drop(0, axis=1)
        
        # we set year as the target
        y = data[0]
        
        # we take the 1st 463,715 rows as train
        # as specified by the UCI repository
        X_train = X.iloc[0:463715,:].as_matrix()
        X_test  = X.iloc[463715:,:].as_matrix()
        y_train = y.iloc[0:463715].as_matrix().astype(dtype='float32')
        y_test  = y.iloc[463715:].as_matrix().astype(dtype='float32')
        
        if train:
        
            # Convert to proper tensors
            self.train_data = torch.from_numpy(X_train).float()
            self.train_labels = torch.from_numpy(y_train.reshape(-1,1)).float()

        else:

            self.test_data = torch.from_numpy(X_test).float()
            self.test_labels = torch.from_numpy(y_test.reshape(-1,1)).float()
            
            
    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        if self.train:
            data, target = self.train_data[index], self.train_labels[index]
        else:
            data, target = self.test_data[index], self.test_labels[index]
            
        return data, target

            
    def __len__(self):
        if self.train:
            return len(self.train_data)
        else:
            return len(self.test_data)