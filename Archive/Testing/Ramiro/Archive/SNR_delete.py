#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  8 11:25:37 2018

@author: ramiromata
"""


"""

SIGNAL-TO-NOISE RATIO (SNR) EXPERIMENT:
    
    This experiment evaluates how different models behave under different
    doses of noise. It can answer questions, such as: 
        
        a) At what noise level do models break down?
        b) What models learn noise? What models filter noise and learn signal?

"""

import os, sys, inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

print('Parent Directory: ', parentdir)

import torch
import numpy as np
import math
import matplotlib.pyplot as plt

from Main.Optimizers.noisyAdam import noisyAdam
#from Main.Optimizers.vprop import Vprop
from Main.Optimizers.noisyKFAC import noisyKFAC
from Utilities.create_dataloaders import create_dataloaders 


def hierarchical_matrix(k, means, sigmas, dims, rows):
    """
    To generate data matrices (X) with hierarchical structure.
    For instance, it can be used to create a matrix where some
    columns share the same precision as others (i.e. hierarchical 
    prior on the precision). 
    
    INPUT:
          k      = number of factors, where each factor can share a precision
                   parameter for instance.
          means  = a list with k number of elements where each element corresponds
                   to a factor.
          sigmas = same as above but for the sigma of each factor
          dims   = list with number of columns for each factor
          rows   = total number of rows. Default is len(menas) + 1
          
    OUTPUT:
          Matrix with Hierarchical Structure
    """
    assert len(means) == len(sigmas) == k == len(dims)
    
    k_params = list(tuple(zip(means,sigmas,dims)))
    matrix=[]
    # iterate over factors
    for mean,sigma,dim in k_params:
        # iterate over columns for each factor
        for i in range(1,dim+1):
            current_col = np.random.normal(mean, sigma, rows)
            matrix.append(current_col)
    
    return np.matrix(matrix).T
            
    





"""
 1. Create Weights
"""


dim = 100      # <======== Define number of weights
mu = 0
sigma = 1
S = np.diag([sigma]*dim)

w = np.random.multivariate_normal([mu]*dim, S)
w = np.asarray(w, dtype=np.float32).reshape(-1,1)
print('W shape: ', w.shape)


"""
 2. Create Data 
 
     Q: Should we add noise on the data (X), the targets (Y), or both?
"""
sigma_x = 3
N = dim+1  # N = number of data samples

features = []
for i in range(N):
    features += [np.random.rand(dim)*sigma_x]
X = np.asarray(features, dtype=np.float32).reshape(N,dim)
#bias = 0
print('X design matrix shape: ', X.shape)

noise_levels = [sigma_of_noise for sigma_of_noise in range(0,9)]


"""
 3. Create Y (with differing levels of noise)

"""
eps = 0.5
y = np.add( np.matmul( X , w ), eps)
#y = [np.add(np.matmul( X , w ), eps) for eps in noise_levels]



""" One Layer Network as in Noisy Natural Gradient Paper by Zhang et al (2018)"""
class FNN(nn.Module):
    
    def __init__(self,H1):
        super(FNN2, self).__init__()
        self.NN1 = nn.Linear(num_features,H1)
        self.NN2 = nn.Linear(H1,1)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x    
    
    

# Data loaders
train_data, test_data = create_dataloaders(X, y, 
                                           test_size=0.2, 
                                           train_batch_size = N,
                                           train_shuffle = False)

num_features  = train_data.shape[1]
num_instances = train_data.shape[0]
print('Number of features in data: ', num_features)
print('Number of instances in data: ', num_instances)




sgd_model       = FNN(H1=10)
adam_model      = FNN(H1=10)
vprop_model     = FNN(H1=10)
nadam_model     = FNN(H1=10)

loss_function = nn.MSELoss()


# Define hyperparameters
l_rate    = 1e-2
precision = 1
N         = 5
gamma_ex  = 1e-2

# optimizers
SGD       = torch.optim.SGD(SGD_model.parameters(), lr=l_rate) #Stochastic Gradient Descent
Vprop     = Vprop(Vprop_model.parameters(), alpha=0.9999, precision=precision, lr=l_rate)
noisyAdam = noisyAdam(noisyAdam_model.parameters(), num_examples=N, gamma_ex=gamma_ex, lr=l_rate)
Adam      = torch.optim.Adam(adam_model.parameters(),lr=l_rate)








