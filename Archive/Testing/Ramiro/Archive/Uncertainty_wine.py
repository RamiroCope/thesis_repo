# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 15:21:20 2018

@author: stud
"""

import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
#parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 

from bayes_opt import BayesianOptimization
from Main.Optimizers.noisyAdam_closure_custom import noisyAdam

import torch
import torch.nn.functional as F
import torch.nn as nn

import torch.nn.init as init
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.distributions import Normal


import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output
import matplotlib.mlab as mlab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from sklearn import preprocessing
from sklearn.model_selection import train_test_split

'''
Uncertainty Estimation Showcasing
with Wine Dataset
'''

'''
___________________   EXPERIMENT 1: WINE ________________________________
'''

from Main.Datasets.Regression.wine import wine as dataset



"""Our Model"""
class FNN(nn.Module):
    
    def __init__(self,num_features, H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=False)
        self.NN2 = nn.Linear(H1,1, bias=False)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x
    
    

"""Data Preparation"""
batch_size=32

# train data
trainset = dataset()
y_train_mean, y_train_std = trainset.get_target_normalization_constants()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = dataset(train = False)
testLoader = DataLoader(testset, batch_size=160, shuffle=False)

N = trainset.train_data.shape[0]
num_features = trainset.train_data.shape[1]


"""Optimizer"""
model = FNN(num_features, 50)
criterion = nn.MSELoss()
optimizer = noisyAdam(model.parameters(), 
                      num_examples=N, 
                      gamma_ex=0.0,
                      var_init = 0.001,
                      kl_weight=0.275,
                      lr=1e-3, 
                      prior_var=0.81)


"""Train"""
for epoch in range(50):

    for i, data in enumerate(trainLoader, 0):

        inputs, labels = data

        def closure():
            optimizer.zero_grad()
            outputs = model(inputs) 
            loss = criterion(outputs, labels)
            loss.backward()
            return loss
        
        _, params = optimizer.step(closure, return_params=True)







def predict_with_qsamples(model, params, num_predictions):
    
    # num test samples
    n = testLoader.dataset.test_data.size()[0]
    predictions = torch.ones([n,num_predictions], requires_grad=False)
    
    for j in range(num_predictions):
        
        # 1. sample from variational posterior
        for i, p in enumerate(list(model.parameters())):
            mu = p.data
            sigma = params['sigma{}'.format(i)]
            q_dist = Normal(mu, sigma)
            p.data = q_dist.sample()
            
        # 2. forward pass with new weights
        list_of_outputs = []
        for data in testLoader:
            x, y = data
            outputs = model(x)
            list_of_outputs.append(outputs)
            
        # store results
        all_outputs = torch.cat(list_of_outputs, 0)
        predictions[:,j] = all_outputs.squeeze()
        
    return predictions.detach().numpy()

predictions = predict_with_qsamples(model, params, 2000)

#map_predictions = predictions
targets = testset.test_labels.squeeze().numpy()

plt.boxplot(predictions[0:30,:].T)
plt.scatter(range(1, 31), targets[0:30].T , label='True Targets', s=100)
#plt.scatter(range(1, 16), map_predictions[15:30,:].T , label='MAP')
plt.legend()
plt.show()
        



"""Test on entire test set with MAP"""
error=[]
outputs_tensor = []
for data in testLoader:
    print("i got called")
    x, y = data
    #optimizer.sample(training = False)
    output = model(x)
    #outputs_tensor shape (outputs X samples)
    #outputs_tensor.append(outputs)
    error += [(output.data - y.data).numpy()]
#flattening list of lists
error = np.array([x for ls in error for x in ls])
normalized_RMSE_map  = np.mean(error**2)**0.5
print('MAP RMSE: ', normalized_RMSE_map)
#unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5 

""" Test on entire test set with mean of q_Samples"""
errorq = np.array((np.mean(predictions, 1) - targets))
normalized_RMSE_qsamp  = np.mean(errorq**2)**0.5
print('Samples RMSE: ', normalized_RMSE_qsamp)
#unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5 




