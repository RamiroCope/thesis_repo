from sklearn.datasets import load_boston
from sklearn.datasets import load_svmlight_file
from urllib.request import urlopen
import subprocess
import torch.nn as nn
import torch.nn.functional as F
from Utilities.create_dataloaders import create_dataloaders
from Utilities.Train_Test import Train_Test
from Utilities.Vprop4 import VProp as Vprop
from Utilities.Vprop_C import Vprop as Vprop_C
from Utilities.Adam import Adam as Adam


"""
                            ___MAIN___
                This is an example on how to use the 
                Train_Test class with the Boston Dataset
"""

"""
    1. Creating Data Loader
"""
#X, y = load_boston(return_X_y = True)

australian_scale = urlopen("https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/australian_scale")
X, y = load_svmlight_file(australian_scale)

trainLoader, testLoader = create_dataloaders(X, y, test_size =0.5, 
                                            train_batch_size = 345,
                                            train_shuffle    = True,
                                            standardize      = True,
                                            toarray          = True,
                                            make_Y_binary    = True)



"""
    2. Defining MLP as in Vprop Paper (arxiv.org/pdf/1712.01038.pdf)
       Change O to 1 (for regression) or 2 (for classification) 
"""
class FNN(nn.Module):
    
    def __init__(self,I,H1,H2,O):
        super(FNN, self).__init__()
        self.I = I 
        self.O = O
        self.NN1 = nn.Linear(I,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,O)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x

I   = X.shape[1]
O   = 2
net = FNN(I,10,10,O)



"""
    3. Optimizer
"""
precision = 1   

# Choose a loss function
#compute_loss = nn.MSELoss(size_average=True, reduce=True)
compute_loss = nn.CrossEntropyLoss()

# Choose 1 optimzer
#optimizer = RMSprop(net.parameters(), lr = 0.001)
#optimizer = Vprop(net.parameters(), lr = 0.00001, precision=precision, alpha=0.99)
#optimizer = Vprop_C(net.parameters(), lr = 0.00001, precision=precision, alpha=0.99)
optimizer = Adam(net.parameters(), kl_weight=1, prior_var=1, 
                 num_examples=345, gamma_extrinsic=0, lr = 0.01)
#optimizer = noisyAdam(net.parameters(), lr = 0.01)


print(optimizer)
print('Precision set to: ', precision)


"""
    4. Train, Test, Plot
"""
# Define desired logdir and port for Tensorboard
#subprocess.Popen(["tensorboard","--logdir=runs", "--port=6011"])

train = Train_Test(net, compute_loss, trainLoader, testLoader, optimizer,
                                         prior_mu=0, precision=precision,
                                         sample_optimizer=True, Adam=True,
                                         KL=True, plot=False, test=True)

epochs = 100

for epoch in range(epochs): 
    train.run(epoch)
    train.evaluate_model()
    

    
    
    
    
    
    
    