#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 10:53:30 2018

@author: ramiromata
"""

import torch
from torch.autograd import Variable
from torch.optim import Optimizer


class Vprop1(Optimizer):
    """Implements RMSprop algorithm.

    Proposed by G. Hinton in his
    `course <http://www.cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf>`_.

    The centered version first appears in `Generating Sequences
    With Recurrent Neural Networks <https://arxiv.org/pdf/1308.0850v5.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-2)
        momentum (float, optional): momentum factor (default: 0)
        alpha (float, optional): smoothing constant (default: 0.99)
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        centered (bool, optional) : if ``True``, compute the centered RMSProp,
            the gradient is normalized by an estimation of its variance
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)

    """

    def __init__(self, params, lr=1e-2, alpha=0.99, eps=1e-8, weight_decay=0, momentum=0, centered=False, precision = 1):
        defaults = dict(lr=lr, momentum=momentum, alpha=alpha, eps=eps, centered=centered, weight_decay=weight_decay, precision=precision)
        super(Vprop1, self).__init__(params, defaults)
        self.posterior_params = {}
        self.precision = precision
        
            

    def __setstate__(self, state):
        super(Vprop1, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('momentum', 0)
            group.setdefault('centered', False)

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
            precison: inverse of variance
        """
        loss = None
        if closure is not None:
            loss = closure()
            
    
        for group in self.param_groups:
            for i,p in enumerate(group['params']):
                if p.grad is None:
                    continue
                #    PSEUDOCODE LINE 2    #
                #      SET GRADIENT       #
                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('RMSprop does not support sparse gradients')
                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    state['square_avg'] = torch.zeros_like(p.data)
                    #     STORE POLD IN STATE    #
                    """ 
                    where pold = mu_{t-1} = p.data of previous opt 
                    iteration - needed for pseudocode line 4 (mu update)
                    """
                    state['pold'] = p.data.clone()
                    if group['momentum'] > 0:
                        state['momentum_buffer'] = torch.zeros_like(p.data)
                    if group['centered']:
                        state['grad_avg'] = torch.zeros_like(p.data)
                square_avg = state['square_avg']; 
                alpha = group['alpha']
                
                #    RETRIEVE POLD     #
                pold = state['pold'];                      
                
                state['step'] += 1

                if group['weight_decay'] != 0:
                    grad = grad.add(group['weight_decay'], p.data)
                
                #    PSEUDOCODE LINE 3    #
                #        UPDATE S         #
                square_avg.mul_(alpha).addcmul_(1 - alpha, grad, grad)

                if group['centered']:
                    grad_avg = state['grad_avg']
                    grad_avg.mul_(alpha).add_(1 - alpha, grad)
                    avg = square_avg.addcmul(-1, grad_avg, grad_avg).sqrt().add_(group['eps'])
                else:
                    Precision = torch.add(torch.zeros_like(p.data), self.precision)
                    #  DENOM = avg = s + precision
                    avg = square_avg.add(Precision)
                    #  NOM = g + mu * precision
                    nom = grad.clone()
                    nom.addcmul_(pold,Precision)
                    #grad = grad + pold*Precision    
                    
                if group['momentum'] > 0:
                    buf = state['momentum_buffer']
                    buf.mul_(group['momentum']).addcdiv_(grad, avg)
                    p.data.add_(-group['lr'], buf)
                else:
                    #        PSEUDOCODE LINE 4     #
                    #          UPDATE MU           #
                    #p.data.addcdiv_(-group['lr'], grad, avg)
                    #p.data.mul_(0).addcdiv_(pold, -group['lr'], grad, avg)
                    p.data.mul_(0).add_(pold).addcdiv_(-group['lr'], nom, avg)#p.data = torch.addcdiv_(pold, -group['lr'], nom, avg) #***
                
                #        UPDATE POLD          #
                pold.mul_(0).add_(p.data)   ;                 
                
                #     PSEUDOCODE LINE 1       #
                #        MU + NOISE           #
                epsilon = torch.randn(p.data.size())
                p.data.addcdiv_(epsilon, torch.sqrt(avg));     
                
                self.posterior_params["mu{0}".format(i)] = p.data.clone()
                self.posterior_params["sigma{0}".format(i)] = 1/(square_avg+Precision)
                self.posterior_params["grad{0}".format(i)] = grad
        return self.posterior_params
    
   