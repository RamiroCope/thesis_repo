import torch
from torch.optim import Optimizer
from torch.distributions import Normal
from torch.autograd import Variable


class Vprop(Optimizer):
    """Implements Vprop algorithm.

    Proposed by Mohammad Emtiyaz Khan et al.
    `<http://bayesiandeeplearning.org/2017/papers/50.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-2)
        momentum (float, optional): momentum factor (default: 0)
        alpha (float, optional): smoothing constant (default: 0.99)
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        centered (bool, optional) : if True, compute the centered RMSProp,
            the gradient is normalized by an estimation of its variance
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        gamma (float, optional): prior precision (default: 0.1)

    """

    def __init__(self,
                 params,
                 lr=1e-2,
                 alpha=0.99,
                 eps=1e-8,
                 gamma=0.1,
                 weight_decay=0,
                 momentum=0,
                 centered=False,
                 precision=1):
        defaults = dict(lr=lr, momentum=momentum, alpha=alpha, eps=eps, centered=centered, weight_decay=weight_decay, gamma=gamma)
        super(Vprop, self).__init__(params, defaults)
        self.precision = precision
        self.posterior_params = {} 
        
    def __setstate__(self, state):
        super(Vprop, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('momentum', 0)
            group.setdefault('centered', False)
            
    def sample(self, training = True):
        for group in self.param_groups:
            for i,p in enumerate(group['params']):              
                if p.grad is None:
                    p.grad = Variable(torch.zeros(p.size()))
                grad = p.grad.data
                # State initialization
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    state['square_avg'] = torch.Tensor(p.size()).uniform_(0.00001*self.precision, 0.0001*self.precision)
                    state['mu'] = torch.Tensor(p.size()).uniform_(-0.1,0.2)
                square_avg = state['square_avg']
                mu = state['mu']
                
                
                if not training:   
                    """
                    After training we sample mu and predict with it 
                    """
                    p.data = mu                    
                    self.posterior_params["mu{0}".format(i)] = mu
                    self.posterior_params["sigma{0}".format(i)] = 1.0/(square_avg + self.precision)                    
                    return self.posterior_params
                
                #   PSEUDO CODE LINE 1   #
                eps = torch.randn(p.size())
                p.data = mu + (eps /(square_avg+self.precision).sqrt())
                
                self.posterior_params["mu{0}".format(i)] = mu
                self.posterior_params["sigma{0}".format(i)] = 1.0/(square_avg + self.precision)
                self.posterior_params["grad{0}".format(i)] = grad
                
        return self.posterior_params
    
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        normal_dist = Normal(torch.Tensor([0.0]), torch.Tensor([1.0])) #Normal distribution with zero mean and std=1

        for group in self.param_groups:
            for i,p in enumerate(group['params']):
                if p.grad is None:
                    continue
                grad = p.grad.data
                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    state['square_avg'] = grad.new().resize_as_(grad).zero_()
                    if group['momentum'] > 0:
                        state['momentum_buffer'] = grad.new().resize_as_(grad).zero_()
                    if group['centered']:
                        state['grad_avg'] = grad.new().resize_as_(grad).zero_()

                square_avg = state['square_avg']
                # update mu by a noise term
                ee = normal_dist.sample()   # Check speedup sampling for all parameters in one go and then just rescale afterwards
                grad.add_(ee.div_(torch.sqrt(square_avg.add(group['gamma']))))

                alpha = group['alpha']

                state['step'] += 1

                if group['weight_decay'] != 0:
                    grad = grad.add(group['weight_decay'], p.data)

                square_avg.mul_(alpha).addcmul_(1 - alpha, grad, grad)

                if group['centered']:
                    grad_avg = state['grad_avg']
                    grad_avg.mul_(alpha).add_(1 - alpha, grad)
                    avg = square_avg.addcmul(-1, grad_avg, grad_avg).sqrt().add_(group['eps'])
                else:
                    #avg = square_avg.sqrt().add_(group['eps']) # avg = square_avg.add_(group['eps']).sqrt()
                    avg = square_avg.add(group['gamma'])

                if group['momentum'] > 0:
                    buf = state['momentum_buffer']
                    buf.mul_(group['momentum']).addcdiv_(grad, avg)
                    p.data.add_(-group['lr'], buf)
                else:
                    p.data.addcdiv_(-group['lr'], grad.add_(p.data.mul(group['gamma'])), avg)
                    
                self.posterior_params["mu{0}".format(i)] = p.data.clone()
                self.posterior_params["sigma{0}".format(i)] = 1.0/(square_avg + self.precision)
                self.posterior_params["grad{0}".format(i)] = grad

        return loss
