
import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import torch
from torch.optim import Optimizer
import numpy as np
from scipy.linalg import sqrtm as mat_sqrt


"""
    noisy KFAC optimizer
"""




class noisyKFAC(Optimizer):

    """
        NOTES:
            - Add pi factor to code as given in eq.13
            - Move constant cov_fac out of loop and in to init? cov_fac is
                just the square root of self.gamma_in. So use this?
            - layer offset in pseudo code line 3 & 6 not accounted for.
    """
    
    
    def __init__(self,
                 params,
                 layers,
                 num_examples,
                 lr=1e-3,
                 beta=0.99,
                 eps=1e-8,
                 weight_decay=0,
                 kl_weight = 1,
                 prior_var = 1,
                 gamma_ex = 0,
                 t_inv = 1,
                 t_stats = 10):
        
        defaults = dict(lr=lr, beta=beta, eps=eps,
                        weight_decay=weight_decay, kl_weight = kl_weight, prior_var = prior_var,
                        gamma_ex = gamma_ex, num_examples = num_examples)
        
        super(noisyKFAC, self).__init__(params, defaults)
        
        self.posterior_params = {}
        # lambda in pseudo code - the KL weighting term
        self.kl_weight = kl_weight
        # total number of training examples (e.g. for MNIST, num_examples = 60.000)
        self.num_examples = num_examples
        # intrinsic damping term
        self.gamma_in = kl_weight/(num_examples*prior_var)
        # adds the two damping terms. gamma_ex may be zero.                
        self.gamma = self.gamma_in + gamma_ex
        # prior variance
        self.prior_var = prior_var
        
        # t_stats and t_inv for update scheme
        self.t_stats = t_stats
        self.t_inv = t_inv
        
        # step counter
        self.steps = 0
        
        # dicts for the a and g updates used in eq. 12
        self.aa_mappings = dict()
        self.gg_mappings = dict()
        
        # initialize hooks
        for i,layer in enumerate(layers):
            if i == 0:
                continue
            layer.register_forward_pre_hook(self.forward_hook_trigger)
            layer.register_backward_hook(self.backward_hook_trigger)
        

    def forward_hook_trigger(self, layer, i):
        # x is the network
        #print('Layer is: ',x)
        # i is the input data
        #print('Data contains: ',i)
        if self.steps % self.t_stats == 0:
            aa = torch.mm(i[0].data.t(), i[0].data) / i[0].size(1)
            for p in layer.parameters():
                self.aa_mappings[p] = aa
        return
    
    def backward_hook_trigger(self, layer, grad_in, grad_out):
        # x is the network
        #print('Layer is: ',x)
        # i is the input data
        #print('Data contains: ',i)
        if self.steps % self.t_stats == 0:
#            print('grad_out is: ', grad_out[0].data)
            gg = torch.mm(grad_out[0].data.t(), grad_out[0].data) / grad_out[0].size(1)
            for p in layer.parameters():
                self.gg_mappings[p] = gg
        return

    def sample(self, training = True, init_point=None):
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    # value initialization for mu parameter
                    init_val = 1.0/np.sqrt(p.size(0))
                    # mu - the mean of the variational posterior
                    # should be 100x784 for MNIST first layer
                    #state['mu'] = torch.Tensor(p.size(1), p.size(0)).uniform_(-init_val, init_val)
                    
                    # if weight matrix
                    if len(p.size()) == 2:
                        state['mu'] = torch.Tensor(p.size(1), p.size(0)).uniform_(-init_val, init_val)
                        # should be 784x784 for MNIST first layer
                        state['A'] = torch.ones(p.data.size(1), p.data.size(1))
                        # should be 100x100 for MNIST first layer
                        state['S'] = torch.ones(p.data.size(0), p.data.size(0))
                        state['A_inv'] = torch.ones(p.data.size(1), p.data.size(1))
                        state['S_inv'] = torch.ones(p.data.size(0), p.data.size(0))
                    # if bias vector
                    else:
                        continue
#                        state['A'] = torch.ones(p.data.size(0), p.data.size(0))
#                        state['S'] = torch.ones(1,1)
#                        state['A_inv'] = torch.ones(p.data.size(0), p.data.size(0))
#                        state['S_inv'] = torch.ones(1,1)
                
                if len(p.size()) == 1:
                    continue
                
                # mean parameter
                mu = state['mu']
                # factor multiplied on covariance matrices as in eq. 13
                cov_fac = np.sqrt(self.gamma_in)
                
                # if weight matrix
                if len(p.size()) == 2:
                    # A & S covariance matrices
                    self.A = state['A'] + cov_fac*torch.eye(p.size(1))
                    self.S = state['S'] + cov_fac*torch.eye(p.size(0))
                
                    # noise term
                    e = torch.randn(p.size(1),p.size(0))
                # if bias vector
#                else:
#                    # A & S covariance matrices
#                    A = state['A'] + cov_fac*torch.eye(p.size(0))
#                    S = state['S'] + cov_fac*torch.eye(1)
#                
#                    # noise term
#                    e = torch.randn(p.size())
#                    e = e.view(-1, 1)
#                if j == 4:
#                    print('S before: ', self.S)
                # compute the square root of A
                A_eigen_val, A_eigen_vec = self.A.symeig(eigenvectors = True)
                A_diag = torch.diag(A_eigen_val.sqrt())
                A_sqrt = torch.mm(A_eigen_vec, A_diag)
                A_sqrt = torch.mm(A_sqrt, A_eigen_vec.t())
                
                # inverse of square root of A^gamma_in
                A_sqrt_inv = A_sqrt.inverse()
                
                # compute the square root of S_inv
                S_eigen_val, S_eigen_vec = self.S.symeig(eigenvectors = True)
                S_diag = torch.diag(S_eigen_val.sqrt())
                S_sqrt = torch.mm(S_eigen_vec, S_diag)
                S_sqrt = torch.mm(S_sqrt, S_eigen_vec.t())
                
                # inverse of square root of S^gamma_in
                S_sqrt_inv = S_sqrt.inverse()
#                if j == 4:
#                    print('S after: ', self.S)
                # scalar multiplied on variance
                F_fact = np.sqrt(self.kl_weight/self.num_examples)

                # compute standard deviation
                eS = torch.mm(e, S_sqrt_inv)
                AeS = torch.mm(F_fact*A_sqrt_inv, eS)
                # do actual sampling
                p.data = (mu + AeS).t()
                
                 # to initialize at predefined starting point
                if (state['step'] == 0) and (init_point is not None):
                    p.data = init_point
        return

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                if p.grad is None:
                    continue

                # Line 1: Getting gradients
                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, please consider SparseAdam instead')

                if len(p.size()) == 1:
                    continue

                state = self.state[p]

                # update step
                state['step'] += 1
                # self.step is used for hooks
                self.steps = state['step']
              
                # get A, S, mu, beta and cov_fac as in pseudo code
                beta = group['beta']
                mu = state['mu']
                self.A = state['A']
                self.S = state['S']
#                if j == 4:
#                    print('S before: ', self.S)
                cov_fac = np.sqrt(self.gamma_in)

                # Pseduo code line 2
                # the beta value is reversed compared to pseudo code
                # to stick with the other implementations beta values
                if state['step'] % self.t_stats == 0:
                    self.A.mul_(beta).add_((1 - beta)*self.aa_mappings[p])
                    self.S.mul_(beta).add_((1 - beta)*self.gg_mappings[p])
                
#                if j == 4:
#                    print('S after: ', self.S)
#                    print('gg_mappings after: ', self.gg_mappings[p])
                
                # Pseudo code line 5
                if state['step'] % self.t_inv == 0:
                    state['A_inv'] = (self.A + cov_fac*torch.eye(state['A'].size(0))).inverse()
                    state['S_inv'] = (self.S + cov_fac*torch.eye(state['S'].size(0))).inverse()

                if group['weight_decay'] != 0:
                    grad = grad.add(group['weight_decay'], p.data)

                # modify gradient
                V = grad - self.gamma_in*p.data
                
                # compute step update
                AVS = torch.mm(torch.mm(state['A_inv'], V.t()), state['S_inv'])
                # update mu
                mu.add_(-group['lr']*AVS)

        return loss