import torch
from torch.optim import Optimizer
import numpy as np

class noisyAdam(Optimizer):

    def __init__(self,
                 params,
                 num_examples,
                 lr=1e-3,
                 beta1=0.9,
                 beta2=0.99999,
                 eps=1e-8,
                 weight_decay=0,
                 kl_weight = 1,
                 prior_var = 1,
                 gamma_ex = 0):
        
        defaults = dict(lr=lr, beta1=beta1, beta2=beta2, eps=eps,
                        weight_decay=weight_decay, kl_weight = kl_weight, prior_var = prior_var,
                        gamma_ex = gamma_ex, num_examples = num_examples)
        
        super(noisyAdam, self).__init__(params, defaults)
        
        self.posterior_params = {}
        # lambda in pseudo code - the KL weighting term
        self.kl_weight = kl_weight
        # total number of training examples (e.g. for MNIST, num_examples = 60.000)
        self.num_examples = num_examples
        # intrinsic damping term
        self.gamma_in = kl_weight/(num_examples*prior_var)
        # adds the two damping terms. gamma_ex may be zero.                
        self.gamma = self.gamma_in + gamma_ex
        

    def sample(self, training = True, init_point=None):
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    # m in pseudo code
                    state['exp_avg'] = torch.zeros_like(p.data)
                    # Exponential moving average of squared gradient values
                    # f in pseudo code
                    state['exp_avg_sq'] = torch.zeros_like(p.data)
                    # value initialization for mu parameter
                    init_val = 1.0/np.sqrt(p.size(0))
                    # mu - the mean of the variational posterior
                    state['mu'] = torch.Tensor(p.size()).uniform_(-init_val, init_val)
                    # sigma - the variance of the variational posterior
                    state['sigma'] = torch.zeros_like(p.data)
                
                # mean parameter of variational posterior
                mu = state['mu']
                
                # Fisher approximation, i.e. variance
                f = state['exp_avg_sq']
        
                # factor to be multiplied on variance
                f_fact = self.kl_weight/self.num_examples
        
                # noise term
                e = torch.randn(p.size())
                
                # damping variance with gamma intrinsic
                f_damped = 1.0/(f + self.gamma_in)
                
                # sigma of variational posterior
                sigma = (f_fact * f_damped).sqrt()
                
                # Pseudo code line 1
                # sample from variational posterior
                p.data = mu.add(e*(sigma))
                
                # to initialize at predefined starting point
                if (state['step'] == 0) and (init_point is not None):
                    p.data = init_point
                
                self.posterior_params["mu{0}".format(j)] = mu
                self.posterior_params["sigma{0}".format(j)] = sigma
                
        return self.posterior_params

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                # Line 1: Getting gradients
                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, please consider SparseAdam instead')

                state = self.state[p]

                # Pseduo code line 2
                # get modified gradient
                v = grad - (self.gamma_in*p.data)
                
                # get f, m, beta_1, beta_2 as in pseudo code
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1 = group['beta1']
                beta2 = group['beta2']
                mu = state['mu']

                # update step
                state['step'] += 1

                if group['weight_decay'] != 0:
                    grad = grad.add(group['weight_decay'], p.data)

                # Pseudo code line 3: Updated biased first moment estimate
                # Decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(1 - beta1, v)
                
                # Pseudo code line 4: Update biased second raw moment estimate
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)

                # Pseudo code line 5 + 6
                m_hat = exp_avg/((1 - beta1 ** state['step'])*(exp_avg_sq + self.gamma))
                
                # update parameter
                mu.add_(-group['lr']*m_hat)

        return loss
    
    
    