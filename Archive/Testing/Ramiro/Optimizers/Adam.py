import math
import torch
import torch
from torch.optim import Optimizer
from torch.autograd import Variable
from torch.distributions import Normal

class Adam(Optimizer):
    """Implements Adam algorithm.

    It has been proposed in `Adam: A Method for Stochastic Optimization`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-3)
        betas (Tuple[float, float], optional): coefficients used for computing
            running averages of gradient and its square (default: (0.9, 0.999))
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        amsgrad (boolean, optional): whether to use the AMSGrad variant of this
            algorithm from the paper `On the Convergence of Adam and Beyond`_

    .. _Adam\: A Method for Stochastic Optimization:
        https://arxiv.org/abs/1412.6980
    .. _On the Convergence of Adam and Beyond:
        https://openreview.net/forum?id=ryQu7f-RZ
    """

    def __init__(self, params, kl_weight, prior_var, num_examples, gamma_extrinsic,
                 lr=1e-3, betas=(0.9, 0.999), eps=1e-8, weight_decay=0, amsgrad=False):
        defaults = dict(lr=lr, betas=betas, eps=eps, weight_decay=weight_decay, amsgrad=amsgrad)
        super(Adam, self).__init__(params, defaults)
        self.posterior_params = {}
        self.kl_weight        = kl_weight
        self.prior_var        = prior_var
        self.num_examples     = num_examples
               
        """
        It can be advantageous to add external damping for numerical stability
        """
        # int damping term
        self.gamma_intrinsic = self.kl_weight / (self.prior_var * self.num_examples)
        # ext damping term
        self.gamma_total     = self.gamma_intrinsic + gamma_extrinsic
        
        
        
    def sample(self, training=True):
        for group in self.param_groups:
            for i,p in enumerate(group['params']):              
                if p.grad is None:
                    p.grad = Variable(torch.zeros(p.size()))
                grad = p.grad.data
                
                state = self.state[p]
                
                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    # m: Exponential moving average of gradient values
                    state['exp_avg'] = torch.zeros_like(p.data) 
                    # f: Exponential moving average of squared gradient values
                    state['exp_avg_sq'] = torch.zeros_like(p.data)
                    # mu
                    state['mu'] = torch.Tensor(p.size()).uniform_(-0.01,0.01)
                                
                exp_avg_sq = state['exp_avg_sq']
                mu = state['mu']
                
                if not training:   
                    """
                    After training we sample mu and predict with it 
                    """
                    p.data = mu                    
                    self.posterior_params["mu{0}".format(i)] = mu
                    q_var    = (self.kl_weight/self.num_examples) * (1.0/(exp_avg_sq + self.gamma_intrinsic))
                    self.posterior_params["sigma{0}".format(i)] = q_var                
                    return self.posterior_params
                
                #   PSEUDO CODE LINE 1   #
                #  sample Var Posterior  #
                q_var    = (self.kl_weight/self.num_examples) * (1.0/(exp_avg_sq + self.gamma_intrinsic))
                e        = torch.randn(p.size())
                p.data   = mu.add( e*torch.sqrt(q_var) )
                
                self.posterior_params["mu{0}".format(i)] = mu
                self.posterior_params["sigma{0}".format(i)] = q_var
                self.posterior_params["grad{0}".format(i)] = grad
                
        return self.posterior_params
    
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                #   PSCODE LINE 2      #
                grad      = p.grad.data.add_(-p.data * self.gamma_intrinsic)
    
                state = self.state[p]
                
                # exp_avg is m & exp_avg_sq is f in pseudo
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1, beta2 = group['betas']

                state['step'] += 1


                # Decay the first and second moment running average coefficient
                #   PSCODE LINE 3      #
                #     m update         #
                exp_avg.mul_(beta1).add_(1 - beta1, grad)

                
                #   PSCODE LINE 4      #
                #     f update         #
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)

                
                #  PSCODE LINE 5       #
                m_tilde = exp_avg / (1 - (beta1 ** state['step']))

                #   PSCODE LINE 6      #
                m_hat   = m_tilde / (exp_avg_sq + self.gamma_total)
                
       
                #   PSCODE LINE 7      #
                p.data.add_(-group['lr']*m_hat)

        return loss
    

import torch
from torch.optim import Optimizer
import numpy as np

class NoisyAdam(Optimizer):
    def __init__(self, params, gamma_extrinsic, num_examples, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,weight_decay=0,amsgrad=False,
                 prior_var=1, kl_weight=1):
        defaults = dict(lr=lr, betas=betas, eps=eps, weight_decay=weight_decay,amsgrad=amsgrad)
        super(NoisyAdam, self).__init__(params, defaults)
        self.posterior_params = {}
        self.num_examples = num_examples
        """
        It can be advantageous to add external damping for numerical stability
        """
        # int damping term
        self.gamma_intrinsic = kl_weight / (prior_var * num_examples)
        # ext damping term
        self.gamma_total     = self.gamma_intrinsic + gamma_extrinsic
        

    def sample(self, training = True):         

        
        for group in self.param_groups:
            for i,p in enumerate(group['params']):              
                if p.grad is None:
                    p.grad = Variable(torch.zeros(p.size()))
                grad = p.grad.data
                
                state = self.state[p]
                
                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    # m: Exponential moving average of gradient values
                    state['exp_avg'] = torch.zeros_like(p.data) 
                    # f: Exponential moving average of squared gradient values
                    state['exp_avg_sq'] = torch.zeros_like(p.data)
                    # mu
                    state['mu'] = torch.Tensor(p.size()).uniform_(-0.01,0.01)
                                
                exp_avg_sq = state['exp_avg_sq']
                mu = state['mu']
        
                if not training:   
                    """
                    After training we sample mu and predict with it 
                    """
                    p.data = mu                    
                    self.posterior_params["mu{0}".format(i)] = mu
                    q_var    = (self.kl_weight/self.num_examples) * (1.0/(exp_avg_sq + self.gamma_intrinsic))
                    self.posterior_params["sigma{0}".format(i)] = q_var                
                    return self.posterior_params
                
    
                #   PSEUDO CODE LINE 1   #
                #  sample Var Posterior  #
                q_var    = (self.kl_weight/self.num_examples) * (1.0/(exp_avg_sq + self.gamma_intrinsic))
                e        = torch.randn(p.size())
                p.data   = mu.add( e*torch.sqrt(q_var) )
                
                self.posterior_params["mu{0}".format(i)] = mu
                self.posterior_params["sigma{0}".format(i)] = q_var
                self.posterior_params["grad{0}".format(i)] = grad
                
        return

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                #   PSCODE LINE 2      #
                grad      = p.grad.data.add_(-p.data * self.gamma_intrinsic)
    
                state = self.state[p]
                
                # exp_avg is m & exp_avg_sq is f in pseudo
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1, beta2 = group['betas']

                state['step'] += 1


                # Decay the first and second moment running average coefficient
                #   PSCODE LINE 3      #
                #     m update         #
                exp_avg.mul_(beta1).add_(1 - beta1, grad)

                
                #   PSCODE LINE 4      #
                #     f update         #
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)

                
                #  PSCODE LINE 5       #
                m_tilde = exp_avg / (1 - (beta1 ** state['step']))

                #   PSCODE LINE 6      #
                m_hat   = m_tilde / (exp_avg_sq + self.gamma_total)
                
       
                #   PSCODE LINE 7      #
                p.data.add_(-group['lr']*m_hat)

        return loss
