import torch
import numpy as np
import torch.optim as optim
from torch.autograd import Variable
from torch.optim import Optimizer
from scipy.linalg import sqrtm as mat_sqrt


"""
    noisy KFAC optimizer
"""




class noisyKFAC(Optimizer):

    def __init__(self,
                 params,
                 num_examples,
                 t_stats,
                 t_inv,
                 lr   = 1e-3,
                 beta = 0.9,
                 eps  = 1e-8,
                 weight_decay = 0,
                 kl_weight = 1,
                 prior_var = 1,
                 gamma_ex  = 0,
                 pi = 1):
        
        defaults = dict(lr=lr, beta=beta, eps=eps, weight_decay=weight_decay, 
                        kl_weight=kl_weight, prior_var=prior_var, gamma_ex=gamma_ex,
                        num_examples=num_examples)
        
        super(noisyKFAC, self).__init__(params, defaults)
        
        self.posterior_params = {}
        self.kl_weight        = kl_weight
        self.num_examples     = num_examples
        self.gamma_in         = kl_weight/(num_examples*prior_var)
        self.gamma            = self.gamma_in + gamma_ex
        self.prior_var        = prior_var
        self.t_stats          = t_stats
        self.t_inv            = t_inv
        self.pi               = pi
        
        

    def sample(self, training = True):
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    
                    if len(p.size()) == 1:
                        continue
                    
                    state['A'] = torch.ones(p.data.size(0), p.data.size(0)) #torch.Tensor(p.data.size(0), p.data.size(0)).uniform_(-0.1, 0.1)
                    state['S'] = torch.ones(p.data.size(1), p.data.size(1)) #torch.Tensor(p.data.size(1), p.data.size(1)).uniform_(-0.1, 0.1)
                    
                    # value initialization for mu parameter
                    mu_init = 1.0/np.sqrt(p.size(0))
                    # mu - the mean of the variational posterior
                    state['mu'] = torch.Tensor(p.size()).uniform_(-mu_init, mu_init)
                    # sigma - the variance of the variational posterior
                
                # mean parameter
                mu = state['mu']
                
                # scalar multiplied on variance lambda/N
                F_fact = np.sqrt(self.kl_weight/self.num_examples)
                
                # covariance factor as given in paper eq. (13)
                # setting term in eq. (13) \pi_l = 1 at the moment
                cov_fact = np.sqrt(self.gamma_in)
                
                # identity matrix wrt. A
                I_A = torch.eye(p.data.size(0))
                
                # identity matrix wrt. S
                I_S = torch.eye(p.data.size(1))
                
                # A^gamma_in inverse
                A_inv = (state['A'] + cov_fact*I_A).inverse()
                print('A inverse is: ', A_inv)
                # compute the square root of A_inv
                A_inv_sqrt = torch.from_numpy(mat_sqrt (A_inv.numpy()).astype('float32'))
                
                # S^gamma_in inverse
                S_inv = (state['S'] + cov_fact*I_S).inverse()
                print('S inverse is: ', S_inv)
                # compute the square root of S_inv
                S_inv_sqrt = torch.from_numpy(mat_sqrt(S_inv.numpy()).astype('float32'))
                
                # noise term    
                e = torch.randn(p.size())

                # obtain covariance for sampling
                eS = torch.mm(e, S_inv_sqrt)
                AeS = torch.mm(F_fact*A_inv_sqrt, eS)
                
                # sample weights
                p.data = mu + AeS
        return

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                # Line 1: Getting gradients
                grad = p.grad.data - self.gamma_in*p.data
                
                state = self.state[p]
                
                mu = state['mu']
                A = state['A']
                S = state['S']
                
                state['step'] += 1
                beta = group['beta']
                
                if state['step'] % self.t_stats == 0:
                    # get a  here
                    # get ds here
                    
                    #Update A and S here as in eq. 12
                    A.mul_(1-beta).add_(beta*torch.mm(a.t(),a))
                    S.mul_(1-beta).add_(beta*torch.dot(ds.t(),ds))
                
                if state['step'] % self.t_inv == 0:
                    #Compute inverses of A and S as in eq. 13
                    A_inv = (A + self.pi*np.sqrt(self.gamma_in)*torch.eye(p.data.size(0))).inverse()
                    S_inv = (S + (self.pi**-1)*np.sqrt(self.gamma_in)*torch.eye(p.data.size(1))).inverse()
                    

                update_term = torch.mm(group['lr']*A_inv, grad)
                
                # update mu
                mu.add_(torch.mm(update_term, S_inv))

        return loss