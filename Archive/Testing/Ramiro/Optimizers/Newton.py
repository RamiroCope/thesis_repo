import torch
from torch.autograd import Variable
from torch.optim import Optimizer
from torch.autograd import grad

class Newton(Optimizer):
    r"""Implements newton method optimization 
    
    Example:
        >>> optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9)
        >>> optimizer.zero_grad()
        >>> loss_fn(model(input), target).backward()
        >>> optimizer.step()

    """

    def __init__(self, params, lr=1, momentum=0, dampening=0,
                 weight_decay=0, nesterov=False):
        defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
                        weight_decay=weight_decay, nesterov=nesterov)
        if nesterov and (momentum <= 0 or dampening != 0):
            raise ValueError("Nesterov momentum requires a momentum and zero dampening")
        super(Newton, self).__init__(params, defaults)

    def __setstate__(self, state):
        super(Newton, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('nesterov', False)

    def step(self, fx, model, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
                
            fx is the function to be optimized; i.e. the loss function, which
                is the ouput of def criterion(): 'loss'
                
            model is our network
        loss = None
        if closure is not None:
            loss = closure()
        """
            
        """ Build Hessian Matrix""" 
        # dx is the gradient of all params in one vector.
        #   setting create_graph=True allows the construction
        #   of a graph that in turn allows higher order derivs
        #   to be computed from it (e.g. the hessian)
        dx, = grad(fx, model.parameters(),  create_graph=True)  #, retain_graph=True, \
                   #only_inputs=True, allow_unused=False)
        #print('dx', dx)
        
        # dim is number of parameters
        dim = dx.size()[1]; #print(dim)
       
        
        """
        Filling up the Hessian:
            note that we feed a one-hot vector (a row of Heye: H[i,:])
            as grad_outputs to specify that we are only interested in 
            the 2nd-order gradients of the i'th element in dx. This 
            gives us the i'th row of the hessian matrix. 
            
            See for instance the links below:
                
                https://github.com/pytorch/pytorch/pull/1016#issuecomment-299919437
                https://discuss.pytorch.org/t/calculating-the-hessian-with-0-2/7233
                https://github.com/uber/pyro/issues/583
        """
        
        #for i in range(dim):
        #    if i == 0:    
        #        H[i,:],  = grad(dx, x, grad_outputs=H[i,:], retain_graph=True)
        #    H[i,:],  = grad(dx, x, grad_outputs=H[i,:])
     
        hess_row_list = [torch.autograd.grad(dx[0][i], model.parameters(), retain_graph=True)[0] for i in range(dim -1)]
        hess_row_list += [torch.autograd.grad(dx[0][dim -1], model.parameters(), retain_graph=False)[0]]

        #print('hess row', hess_row_list)
        H = torch.cat(hess_row_list, 0); #print('Hessian', H)
        H_inv = H.inverse(); #print('Hessian inv', H_inv)
        
        # modified gradient: g_new =  H^-1*g 
        g_new = torch.mm(H_inv, dx.transpose(0,1)); #print('g_new', g_new)


        """
        Subscripts:
            We use 'i' and 'j' to match the rows of the Hessian (each row corresponds
            to a single parameter) with the parameter groups as organized by Pytorch.
        """
        i=0 # beginning idx of subsect of g_new needed to update p.data
        j=0 # end idx of subsect of g_new needed to update p.data
        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                # find length of current p group
                j += p.grad.data.size()[1]
                
                # get subset of g_new pertaining to p.data group
                p.data.add_(-group['lr'], g_new.data[i:j]); #print('p.data', p.data)
                i = j
                

        return fx #aka loss function

