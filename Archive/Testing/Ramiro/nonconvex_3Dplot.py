import autograd.numpy as anp
import numpy as np

from autograd import grad, elementwise_grad, jacobian
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt


def f(w):
    return  w[0]*anp.sin(20*w[1]) + ((w[0]*anp.sin(20*w[0]))**2) * (anp.cosh(anp.sin(10*w[0])*w[0])) + ((w[0]*anp.cos(10*w[1]) - w[1]*anp.sin(10*w[1]))**2)*anp.cosh(anp.cos(20*w[1])*w[1])

def f2(w):
    return anp.power(w[0],2) + anp.power(w[1],2)

def vadam(f, mu, lr, N, l, b1, b2, t, m=None, s=None):
    
    if t == 1:
        m = 0.95
        s = 0.95
    
    sigma = 1/(np.sqrt(N*s + l))
    sigma = np.array([sigma, sigma]) #multivariate as w = [w1, w2]
    
    eps = np.random.randn(2)
    
    w = mu + sigma*eps; print('w', w)
    
    g = -grad(f)(w); print('g', g)
    
    m = b1*m + (1-b1)*(g + l*mu/N); print('m', m)
    
    s = b2*s + (1-b2)*(g*g); print('s', s)
    
    m_hat = m/(1-(np.power(b1,t))); print('m_hat', m_hat)
    
    s_hat = s/(1-(np.power(b2,t))); print('s_hat', s_hat)
    
    mu = mu + lr*m_hat/(np.sqrt(s_hat) + l/N); print('mu', mu)
    
    return mu, m, s
    

def sgd(f, mu, lr):
    g = -grad(f)(mu)
    mu = mu + lr*g
    return mu
    
def newton(f,mu,lr):
    pass



iter = 20
init = np.array([3.85,3.85])
sgd_path = np.empty([iter, 2])
vad_path = np.empty([iter, 2])

for t in range(1,iter+1):
    if t == 1:
        sgd_w =  sgd(f2,init,0.0001)
        sgd_path[t,:] = sgd_w
        
        vad_w, m, s = vadam(f2, init, 0.001, N=10,l=0.01, b1=0.95, b2=0.999, t=t, m=None, s=None)
        vad_path[t,:] = np.around(vad_w, 5)
        
    else:
         sgd_w =  sgd(f2,sgd_w,0.005)
         sgd_path[t,:] = sgd_w

         vad_w, m, s = vadam(f2, vad_w, 0.01, N=10,l=0.01, b1=0.95, b2=0.999, t=t, m=m, s=s)
         vad_path[t,:] = np.around(vad_w, 5)
    print('iter: ', t)
print(vad_path)




fig = plt.figure()
ax = fig.add_subplot(111) #, projection="3d")
X, Y = np.mgrid[-1:1:100j, -1:1:100j]
Z = X*np.sin(20*Y) + ((Y*np.sin(20*X))**2) * (np.cosh(np.sin(10*X)*X)) + ((X*np.cos(10*Y) - Y*np.sin(10*X))**2)*np.cosh(np.cos(20*Y)*Y)
#Z2 = X**2 + Y**2

#ax.plot_surface(X, Y, Z, cmap="viridis", lw=0.5, rstride=1, cstride=1, alpha=1)
ax.contour(X, Y, Z, 10, lw=3, cmap="viridis", linestyles="solid", offset=-1)
#ax.contour(X, Y, Z, 10, lw=3, colors="k", linestyles="solid")
ax.grid(False)
ax.scatter(sgd_path[:,0], sgd_path[:,1], color='k')
ax.plot(sgd_path[:,0], sgd_path[:,1], color='k')
plt.show()




