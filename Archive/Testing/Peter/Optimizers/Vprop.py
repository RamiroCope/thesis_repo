import torch
from torch.optim import Optimizer
import numpy as np

class Vprop(Optimizer):
    """Implements Vprop algorithm.

    Proposed by Mohammad Emtiyaz Khan et al.
    `<http://bayesiandeeplearning.org/2017/papers/50.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-2)
        momentum (float, optional): momentum factor (default: 0)
        alpha (float, optional): smoothing constant (default: 0.99)
        precision (float, optional): prior precision (default: 1)

    """
    def __init__(self,
                 params,
                 lr=1e-3,
                 alpha=0.99,
                 momentum=0,
                 precision = 1,
                 return_params = False):
        
        defaults = dict(lr=lr,
                        momentum=momentum,
                        alpha=alpha,
                        return_params)
        
        super(Vprop, self).__init__(params, defaults)
        
        self.precision = precision
        self.posterior_params = {}          

        
    def __setstate__(self, state):
        super(Vprop, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('momentum', 0)

            
    def sample(self, training = True):
        for group in self.param_groups:
            for i, p in enumerate(group['params']):              
                
                # State initialization
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0                    
                    # s as given in the pseudo code
                    state['square_avg'] = torch.zeros_like(p.data)
                    # value initialization for mu parameter
                    init_val = 1.0/np.sqrt(p.size(0))
                    # mu - the mean of the variational posterior
                    state['mu'] = torch.Tensor(p.size()).uniform_(-init_val, init_val)
                    if group['momentum'] > 0:
                        state['momentum_buffer'] = torch.zeros_like(p.data)
                    
                square_avg = state['square_avg']
                mu = state['mu']
                
                if not training:
                    p.data = mu
                    return
                
                # noise term
                e = torch.randn(p.size())
                # standard deviation of variational posterior
                sigma = 1.0/(square_avg + self.precision).sqrt()
                
                # Pseudo code line 1 - sample parameters
                p.data = mu.add(e*sigma)
                
                if group['return_params']:
                    
                    self.posterior_params["mu{0}".format(j)] = mu
                    self.posterior_params["sigma{0}".format(j)] = sigma
                    
                    return self.posterior_params
                
        return

                
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for i, p in enumerate(group['params']):
                if p.grad is None:
                    continue
                
                # Pseudo code line 2 - get gradient
                grad = p.grad.data
                
                # get state
                state = self.state[p]
                
                square_avg = state['square_avg']; 
                mu = state['mu']
                alpha = group['alpha']
                
                state['step'] += 1
                
                # Pseudo code line 3 - update s
                square_avg.mul_(alpha).addcmul_(1 - alpha, grad, grad)
                
                num = grad.add(self.precision, mu)
                denom = square_avg + self.precision
                
                if group['momentum'] > 0:
                    buf = state['momentum_buffer']
                    buf.mul_(group['momentum']).addcdiv_(num, denom)
                    mu.add_(-group['lr'], buf)
                else:    
                    # Pseudo code line 4
                    mu.addcdiv_(-group['lr'], num, denom)
        return