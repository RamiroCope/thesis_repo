import torch
from torch.optim import Optimizer
import numpy as np

class noisyAdam(Optimizer):
    """Implements Noisy Adam algorithm.

    Proposed by Guodong Zhang et al.
    `<https://arxiv.org/pdf/1712.02390.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        num_examples (integer): number of training examples
        lr (float, optional): learning rate (default: 1e-3)
        betas (float, optinal): decay rates for mean and Fisher updates
            (default: 0.9, 0.999)
        kl_weight (float, optional): weight term added to KL divergence (default: 1)
        df (float, optional): degrees of freedom of prior t distribution (default: 5)
        gamma_ex (float, optional): extrinsic damping term (default: 0)
        return_params (bool, optional): return posterior parameters? (default: False)
    """
    def __init__(self,
                 params,
                 num_examples,
                 lr=1e-3,
                 betas=(0.9, 0.999),
                 kl_weight = 1,
                 df = 5,
                 prior_var = 1,
                 gamma_ex = 0):
        
        defaults = dict(lr=lr, betas=betas, kl_weight = kl_weight,
                        df = df, gamma_ex = gamma_ex, prior_var = prior_var,
                        num_examples = num_examples)
        
        super(noisyAdam, self).__init__(params, defaults)
        
        # to store the posterior parameters, mu and sigma
        # used only for plotting KL divergence
        self.posterior_params = {}
        # lambda in pseudo code - the KL weighting term
        #self.kl_weight = kl_weight
        # total number of training examples (e.g. for MNIST, num_examples = 60.000)
        #self.num_examples = num_examples        

    def sample(self, training = True, return_params = False):
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                
                state = self.state[p]
                
                if p.grad is not None:
                    grad = p.grad.data
                
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    # m in pseudo code
                    state['exp_avg'] = torch.zeros_like(p.data)
                    # Exponential moving average of squared gradient values
                    # f in pseudo code
                    state['exp_avg_sq'] = torch.zeros_like(p.data)
                    # value initialization for mu parameter
                    init_val = 1.0/np.sqrt(p.size(0))
                    # mu - the mean of the variational posterior
                    #state['mu'] = torch.Tensor(p.size()).uniform_(-init_val, init_val)
                    state['mu'] = torch.Tensor(p.size()).normal_(0,0.1)
                    # sigma - the variance of the variational posterior
                    state['sigma'] = torch.zeros_like(p.data)
                
                # mean parameter of variational posterior
                mu = state['mu']
                
                # dimensionality of flattened tensor. Used in t-distribution calc.
                P = p.data.view(-1).size()[0]
                
                # degrees of freedom of t-dist
                df = group['df']
                
                # prior variance
                prior_var = group['prior_var']
                
                # w^T * w
                w_squared = p.data.norm(2)**2#torch.mm(p.data.view(1,-1), p.data.view(-1,1))[0]
                
                # when testing, weights are the mean of the variational posterior
                if not training:
                    p.data = mu
                    return
                
                # second order derivative of log(p(w))
#                dwdw_logp = (group['df'] + 1)*( group['df']*group['prior_var'] - p.data**2 ) /\
#                                ( p.data**2 + group['df']*group['prior_var'] )**2
                
                term1 = (df + P)*prior_var / (df + prior_var*w_squared)
                term2 = 2*(df + P)*prior_var**2 * p.data**2 / (df + prior_var*w_squared)**2
                
                dwdw_logp = term1 - term2
                
                # enforce positivity
                dwdw_logp = torch.log(1 + torch.exp(dwdw_logp)) + 1e-6

                # Fisher approximation, i.e. variance
                f = state['exp_avg_sq']
        
                # noise term
                e = torch.randn(p.size())
                
                # damped variance with gamma intrinsic added
                #f_damped = 1/(f + gamma_in)
                f_damped = 1/(group['num_examples']/group['kl_weight'] * f + dwdw_logp)
                
                #enforce positivity
                #f_damped = torch.log(1 + torch.exp(f_damped)) + 1e-7
                
                # pseudo code line 1
                # sample from variational posterior
                p.data = mu.add(e*f_damped.sqrt())                
                
#                if j == 0:
#                    print("================> SAMPLE <================")
#                    print("=> p.data ", p.data)
#                    print("=> p.data^2 ", p.data**2)
#                    print("===> dwdw_logp ", dwdw_logp)
#                    print("===> gamma_in ", gamma_in)
#                    print("===> f ", f)
#                    print("===> f_damped ", f_damped)
#                    print("======> stdv ", f_damped.sqrt())
#                    print("===> w_sq ", w_squared)
#                    print("===> mu ", mu)
#                    print("===> weights ", p.data)
#                    
#                    
#                    if np.isnan(dwdw_logp[0]):
#                        raise ValueError('NaN detected.')
                
                if return_params:
                    
                    # standard deviation of variational posterior
                    sigma = f_damped.sqrt()
                    
                    self.posterior_params["mu{0}".format(j)] = mu
                    self.posterior_params["sigma{0}".format(j)] = sigma
                    self.posterior_params["f{0}".format(j)] = f
                    if p.grad is None:
                        self.posterior_params["grad{0}".format(j)] = torch.zeros(p.size())
                    else:
                        self.posterior_params["grad{0}".format(j)] = grad
                    #self.posterior_params["t1{0}".format(j)] = term1
                    #self.posterior_params["t2{0}".format(j)] = term2
                    #self.posterior_params["w_sq{0}".format(j)] = w_squared
                    
        if return_params:
            return self.posterior_params
                
        return

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                if p.grad is None:
                    continue

                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, \
                                       please consider SparseAdam instead')

                state = self.state[p]

                # dimensionality of flattened tensor. Used in t-distribution calc.
                P = p.data.view(-1).size()[0]
                
                # w^T * w
                w_squared = p.data.norm(2)**2#torch.mm(p.data.view(1,-1), p.data.view(-1,1))[0]                
                # degrees of freedom of t-dist
                df = group['df']
                
                # prior variance
                prior_var = group['prior_var']
                
                # second order derivative of log(p(w))
#                dwdw_logp = (group['df'] + 1)*( group['df']*group['prior_var'] - p.data**2 ) /\
#                                ( p.data**2 + group['df']*group['prior_var'] )**2
                
                # enforce positivity
                #dwdw_logp = torch.log(1 + torch.exp(dwdw_logp))
                
                term1 = (df + P)*prior_var / (df + prior_var*w_squared)
                term2 = 2*(df + P)*prior_var**2 * p.data**2 / (df + prior_var*w_squared)**2
                
                dwdw_logp = term1 - term2
                
                # enforce positivity
                dwdw_logp = torch.log(1 + torch.exp(dwdw_logp)) + 1e-6
                
                # intrinsic damping term
                gamma_in = group['kl_weight']/group['num_examples'] * dwdw_logp
                
                gamma = group['gamma_ex'] + gamma_in

                # pseduo code line 2
                # get modified gradient
                #v = grad - (group['df'] + 1)*p.data/(p.data**2 + group['df']*group['prior_var'])
                #v *= group['kl_weight']/group['num_examples']
                
                # multivariate case
                
                dw_logp = (df + P) * prior_var * p.data / (df + prior_var * w_squared)
                                             
                v = grad - dw_logp
                v *= group['kl_weight']/group['num_examples']
                
                # get m, f, beta_1, beta_2 as in pseudo code
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1, beta2 = group['betas']
                mu = state['mu']

                # update step
                state['step'] += 1

                # pseudo code line 3: Updated biased first moment estimate
                # decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(1 - beta1, v)
                
                # pseudo code line 4: Update biased second raw moment estimate
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)

                # pseudo code line 5
                m_tilde = exp_avg/(1 - beta1 ** state['step'])
                # pseudo code line 6
                m_hat = m_tilde/(exp_avg_sq + gamma)
                #m_hat = m_tilde/torch.log(1 + torch.exp(exp_avg_sq + gamma)) + 1e-7
                
                # pseudo code line 7
                # update parameter
                mu.add_(-group['lr']*m_hat)
                
#                if j == 5:
#                    print("================> STEP <================")
#                    #print("=> p.data ", p.data)
#                    #print("=> p.data^2 ", p.data**2)
#                    print("===> dw_logp ", dw_logp)
#                    #print("===> dwdw_logp ", dwdw_logp)
#                    #print("===> gamma_in ", gamma_in)
#                    #print("======> stdv ", f_damped.sqrt())
#                    #print("===> w_sq ", w_squared)
#                    print("===> mu ", mu)
#                    
#                    
#                    if np.isnan(mu[0]):
#                        raise ValueError('NaN detected.')

        return loss
    
    
