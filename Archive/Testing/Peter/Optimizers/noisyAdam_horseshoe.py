import torch
from torch.optim import Optimizer
from torch.autograd import grad as gradient
from torch.autograd import Variable
import scipy.stats as stats
import numpy as np

class noisyAdam(Optimizer):
    """Implements Noisy Adam algorithm.

    Proposed by Guodong Zhang et al.
    `<https://arxiv.org/pdf/1712.02390.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        num_examples (integer): number of training examples
        lr (float, optional): learning rate (default: 1e-3)
        betas (float, optinal): decay rates for mean and Fisher updates
            (default: 0.9, 0.999)
        kl_weight (float, optional): weight term added to KL divergence (default: 1)
        prior_var (float, optional): variance of prior distribution (default: 1)
        gamma_ex (float, optional): extrinsic damping term (default: 0)
        return_params (bool, optional): return posterior parameters? (default: False)
    """
    
    def __init__(self,
                 params,
                 num_examples,
                 lr=1e-3,
                 betas=(0.9, 0.999),
                 kl_weight = 1,
                 local_var = 6,
                 global_var = 6,
                 gamma_ex = 0):
        
        defaults = dict(lr=lr, betas=betas, kl_weight=kl_weight,
                        local_var=local_var, global_var=global_var, gamma_ex=gamma_ex,
                        num_examples=num_examples)
        
        super(noisyAdam, self).__init__(params, defaults)
        
        # to store the posterior parameters, mu and sigma
        # used only for plotting KL divergence
        self.posterior_params = {}
        # lambda in pseudo code - the KL weighting term
        self.kl_weight = kl_weight
        # total number of training examples (e.g. for MNIST, num_examples = 60.000)
        self.num_examples = num_examples        

    def sample(self, training = True, return_params = False):
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                
                state = self.state[p]
                
                if p.grad is not None:
                    grad = p.grad.data
                
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    # m in pseudo code
                    state['exp_avg'] = torch.zeros_like(p.data)
                    # Exponential moving average of squared gradient values
                    # f in pseudo code
                    state['exp_avg_sq'] = torch.zeros_like(p.data)
                    # value initialization for mu parameter
                    init_val = 1.0/np.sqrt(p.size(0))
                    # mu - the mean of the variational posterior
                    state['mu'] = torch.Tensor(p.size()).uniform_(-init_val, init_val)
                    #state['mu'] = torch.Tensor(p.size()).uniform_(-0.1, 0.1)
                
                # mean parameter of variational posterior
                mu = state['mu']
                
                # when testing, weights are the mean of the variational posterior
                if not training:
                    p.data = mu
                    return

                # stats.gamma uses kappa, theta parameterization => beta = 1/theta
                state['lambda_0'] = stats.gamma.rvs(a = 1, scale = 1/group['local_var']**2, size = p.size())
                state['lambda_G'] = stats.gamma.rvs(a = 1, scale = 1/group['global_var']**2, size = 1)
                
                # stats.invgamma uses alpha, beta parameterization
                state['local_scale'] = np.float32(stats.invgamma.rvs(a=0.5, scale = state['lambda_0'], size = p.size()))
                state['local_scale'] = Variable(torch.from_numpy(state['local_scale']))
                
                state['global_scale'] = np.float32(stats.invgamma.rvs(a=0.5, scale = state['lambda_G'], size = 1))
                state['global_scale'] = Variable(torch.from_numpy(state['global_scale']))
                
                local_scale = state['local_scale']
                global_scale = state['global_scale']
                
                # log p(gamma_sample)
                #log_pgamma = ((group['alpha'] - 1)*torch.log(gamma_sample) - group['beta']*gamma_sample).sum()

                # log p(w), p ~ N(0, local^-1*global^-1)
                log_pw = (-0.5*p**2/(local_scale**2*global_scale**2)).sum()

                log_prior = log_pw # + log_pgamma

#                print("=====> SAMPLE =====> log_prior is: ",log_prior)
                
                dw_log_pw = gradient(log_prior, p, create_graph = True)[0]
                dwdw_log_pw = gradient(dw_log_pw.sum(), p, create_graph = True)[0]
                
                # Fisher approximation, i.e. variance
                f = state['exp_avg_sq']
        
                # noise term
                e = torch.randn(p.size())
                
                # damped precision                
                f_damped = 1/(group['num_examples']/group['kl_weight']*f - dwdw_log_pw.data)
                
                # pseudo code line 1
                # sample from variational posterior
                p.data = mu.add(e*f_damped.sqrt())
                
                if return_params:
                    
                    # standard deviation of variational posterior
                    self.posterior_params["mu{0}".format(j)] = mu
                    self.posterior_params["sigma{0}".format(j)] = f_damped.sqrt()
                    self.posterior_params["f{0}".format(j)] = f
                    if p.grad is None:
                        self.posterior_params["grad{0}".format(j)] = torch.zeros(p.size())
                    else:
                        self.posterior_params["grad{0}".format(j)] = grad
                
        if return_params:
            return self.posterior_params
        
        return

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue

                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, \
                                       please consider SparseAdam instead')

                state = self.state[p]
                #gamma_sample = state['gamma_sample']
                
                local_scale = state['local_scale']
                global_scale = state['global_scale']
                
                # log p(gamma_sample)
                #log_pgamma = ((group['alpha'] - 1)*torch.log(gamma_sample) - group['beta']*gamma_sample).sum()
                
                # log p(w), p ~ N(0, gamma^-1)
                log_pw = (-0.5*p**2/(local_scale**2*global_scale**2)).sum()
                
                # total log prior
                log_prior = log_pw #+ log_pgamma
                
#                print("=====> STEP =====> log_prior is: ",log_prior)
                
                # get first and second order gradients of log prior
                dw_log_pw = gradient(log_prior, p, create_graph = True)[0]
                dwdw_log_pw = gradient(dw_log_pw.sum(), p, create_graph = True)[0]

                # pseduo code line 2
                # get modified gradient
                v = grad + dw_log_pw.data * group['kl_weight']/group['num_examples']
                
                # get m, f, beta_1, beta_2 as in pseudo code
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1, beta2 = group['betas']
                mu = state['mu']

                # update step
                state['step'] += 1

                # pseudo code line 3: Updated biased first moment estimate
                # decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(1 - beta1, v)
                
                # pseudo code line 4: Update biased second raw moment estimate
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)

                # pseudo code line 5
                m_tilde = exp_avg/(1 - beta1 ** state['step'])
                
                # pseudo code line 6
                m_hat = m_tilde/(exp_avg_sq + group['gamma_ex'] +\
                                     group['kl_weight']/group['num_examples'] * dwdw_log_pw.data)
                
                # pseudo code line 7
                # update parameter
                mu.add_(-group['lr']*m_hat)

        return loss
    
