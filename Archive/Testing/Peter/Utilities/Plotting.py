from IPython.display import clear_output
from tensorboardX import SummaryWriter
import torch
from torch.autograd import Variable
from torch.distributions import Normal
import math
from numpy import random
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D



def plot_diagnositc(ll, kl, test, epochs):
    num_batches =  len(ll)
    batch_per_epoch = num_batches // epochs
    data_passes  = [x for x in range(1,epochs+1)]
    batch_passes = [x for x in range(1,num_batches+1)]
    f, axarr = plt.subplots(4, sharex=True, sharey=False, figsize=(10,8))
    
    plt.xticks(np.arange(min(batch_passes), max(batch_passes)+1, batch_per_epoch), data_passes)
    
    # y labels
    axarr[0].set(ylabel='NLL')
    axarr[1].set(ylabel='KL Div')
    axarr[2].set(ylabel='ELBO')
    axarr[3].set(ylabel='Test Loss')

    #log likelihoods
    l1 =axarr[0].plot(batch_passes, ll)

    #kl divergence
    axarr[1].plot(batch_passes, kl)

    #elbo
    axarr[2].plot(batch_passes, [x+y for x,y in zip(ll,kl)])


    #test loss
    axarr[3].plot(batch_passes, test)


    # Bring subplots close to each other.
    f.subplots_adjust(hspace=0)

    # Hide x labels and tick labels for all but bottom plot.
    for ax in axarr:
        ax.label_outer()

    
    plt.show()