# -*- coding: utf-8 -*-
"""
Created on Thu Jul 12 14:13:18 2018

@author: snook
"""

"""
    Set working directory
"""
import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.distributions import Normal
import scipy.stats as stats
from torch import optim
from Peter.Pytorch04.Models.BBB_t import BNN as BNN_t
from Peter.Pytorch04.Models.BBB import BNN
import math

#import matplotlib
#matplotlib.use('TkAgg')
#from torch.nn.parameter import Parameter
#from torch.optim import Optimizer
#from torch.distributions import Normal
#from IPython.display import Image, display, clear_output
#import numpy as np

def avneg_loglik_gaussian(mu, y, tau):
    logliks = 0.5 * (- math.log(2 * math.pi * tau) - (y - mu)**2/tau)
    avneg_loglik = - torch.mean(logliks)
    return avneg_loglik

def objective(mu, y):
    return avneg_loglik_gaussian(mu, y, tau = 0.1)


"""
    Generate data
"""

num_obs = 500

#np.random.seed(seed)
noise_var = 0.1

x = np.linspace(-4, 4, num_obs)
y = 1*np.sin(x) + np.sqrt(noise_var)*np.random.randn(num_obs)
#y = y.reshape((y.size()[0], 1))

train_count = int (0.2 * num_obs)
idx = np.random.permutation(range(num_obs))
x_train = x[idx[:train_count], np.newaxis ]
x_test = x[ idx[train_count:], np.newaxis ]
y_train = y[ idx[:train_count] ]
y_test = y[ idx[train_count:] ]

mu = np.mean(x_train, 0)
std = np.std(x_train, 0)
x_train = (x_train - mu) / std
x_test = (x_test - mu) / std
mu = np.mean(y_train, 0)
std = np.std(y_train, 0)
y_train = (y_train - mu) / std
train_stats = dict()
train_stats['mu'] = mu
train_stats['sigma'] = std

x_train = torch.from_numpy(x_train.astype(np.float32))
x_test = torch.from_numpy(x_test.astype(np.float32))
y_train = torch.from_numpy(y_train.astype(np.float32))
y_test = torch.from_numpy(y_test.astype(np.float32))


"""
    Define the neural net
"""

# Hierarchical setup collapses to studen t
# df = 2*alpha, scale = sqrt(alpha * beta)

prior_prec = 100
prec_init = 1000

df_prior = 1.0
df_init = 5.0

# initialize network    
net_t = BNN_t(input_size = 1,
              hidden_sizes = [20],
              output_size = 1,
              act_func = "Relu",
              df_prior = df_prior,
              df_init = df_init,
              prior_prec = prior_prec,
              prec_init = prec_init)

net = BNN_t(input_size = 1,
              hidden_sizes = [20],
              output_size = 1,
              act_func = "Relu",
              prior_prec = prior_prec,
              prec_init = prec_init)

"""
    Define loss and optimizer
"""

# loss
criterion = nn.MSELoss()#nn.CrossEntropyLoss()

# optimizer
optimizer_t = optim.Adam(net_t.parameters(),
                          lr = 0.005,
                          betas = (0.9, 0.999))

optimizer = optim.Adam(net.parameters(),
                          lr = 0.005,
                          betas = (0.9, 0.999))

# setup adaptive learning rate
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[9], gamma=0.1)
#scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.85)


"""
    Training the network
"""

max_epochs = 5000

# indices for accessing weight matrices of the different layers
indices = [0,2,4]

train_loss_t = []
test_loss_t = []
KL_div_t = []
ELBO_updates_t = []

train_loss = []
test_loss = []
KL_div = []
ELBO_updates = []


updates = []
lr_plt = []


for epoch in range(max_epochs):
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss_t = 0.0
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs

    inputs, labels = x_train.squeeze(), y_train
        
    optimizer_t.zero_grad()
    optimizer.zero_grad()
    outputs_t = net_t(inputs)   
    outputs = net(inputs)
    NLL_t = objective(outputs_t.squeeze(), labels)#criterion(outputs.squeeze(), labels)
    NLL = objective(outputs.squeeze(), labels)#criterion(outputs.squeeze(), labels)
#    pi = 2**(num_batches - batch + 1) / (2**(num_batches) - 1)
    kl_t = net_t.kl_divergence()#/num_batches
    kl = net.kl_divergence()#/num_batches
    ELBO_t = NLL_t + kl_t
    ELBO = NLL + kl
    ELBO_t.backward()
    ELBO.backward()
    
    optimizer_t.step()
    optimizer.step()
    
    # Print statistics
    running_loss_t += NLL_t.item()
    running_loss += NLL.item()
    
    mus = []
    dfs = []
    sigmas = []
    weights = []
        
#            mu, sigma, df, weight = net.return_params()
#            
#            mus += [mu]
#            dfs += [df]
#            sigmas += [sigma]
#            weights += [weight]
        
    print('[%d] ====> train loss student\'s T: %.3f' %
          (epoch + 1, running_loss_t))
    print('[%d] ====> train loss: %.3f' %
          (epoch + 1, running_loss))
    KL_div_t += [kl_t]
    KL_div += [kl]
    ELBO_updates_t += [ELBO_t.item()]
    ELBO_updates += [ELBO.item()]
    updates += [epoch]
    train_loss_t += [running_loss_t]
    train_loss += [running_loss]
    
    
    net.train(False)
    images, labels = x_test.squeeze(), y_test
    outputs_t = net_t(images, training = False)
    outputs = net(images, training = False)
    test_loss_val_t = objective(outputs_t.squeeze(), labels)#criterion(outputs.squeeze(), labels)
    test_loss_val = objective(outputs.squeeze(), labels)#criterion(outputs.squeeze(), labels)
    test_loss_t += [test_loss_val_t.item()]
    test_loss += [test_loss_val.item()]
#                
#            print('[%d, %5d] =======> test loss: %.3f' %
#                  (epoch + 1, batch + 1, test_loss_val.item()))
    
    print("[%d] =====> KL divergence student\'s T: %.3f" %
          (epoch + 1, kl_t))
    print("[%d] =====> KL divergence: %.3f" %
          (epoch + 1, kl))
        
    running_loss_t = 0.0
    running_loss = 0.0
#            mus_flat = [item for sublist in mus for item in sublist]
#            dfs_flat = [item for sublist in dfs for item in sublist]
#            sigmas_flat = [item for sublist in sigmas for item in sublist]
#            weights_flat = [item for sublist in weights for item in sublist]
#            
#            
#            fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
#            fig.tight_layout(h_pad = 5, w_pad = 5)
#            
#            plt.subplot(2,2,1)
#            plt.hist(mus_flat, bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("mu")
#            plt.title("mu histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.subplot(2,2,2)
#            plt.hist(dfs_flat, bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("df")
#            plt.title("df histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.subplot(2,2,3)
#            plt.hist(sigmas_flat, bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("sigma")
#            plt.title("sigma histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.subplot(2,2,4)
#            plt.hist(weights_flat, bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("weights")
#            plt.title("weight histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.show()
#            
#            print("=======================================================")
            
#    lr_plt += [scheduler.get_lr()[0]]        
#    scheduler.step()
#    print("=======> lr is: %.10f" % scheduler.get_lr()[0])
    
print('Finished training')    


"""
    Plotting
"""


fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,8))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,2,1)
 
plt.plot(updates, train_loss_t)
plt.plot(updates, train_loss)
plt.xlabel("Updates"), plt.ylabel("train loss")
plt.title("Noisy Adam training loss")


plt.subplot(2,2,2)
plt.plot(updates, test_loss_t)
plt.plot(updates, test_loss)
plt.xlabel("Updates"), plt.ylabel("test loss")
plt.title("Noisy Adam test loss")


plt.subplot(2,2,3)
plt.plot(updates, KL_div_t)
plt.plot(updates, KL_div)
plt.xlabel("Updates"), plt.ylabel("KL Divergence")
plt.title("Noisy Adam KL Divergence")
#
#
plt.subplot(2,2,4)
plt.plot(updates, ELBO_updates_t)
plt.plot(updates, ELBO_updates)
plt.xlabel("Updates"), plt.ylabel("ELBO")
plt.title("Noisy Adam ELBO")
plt.show()


"""
    Plot function
"""

ins = torch.from_numpy(x.astype(np.float32))
preds = net_t(ins, training = False)

plt.plot(ins.numpy(),np.sin(ins.numpy()))
plt.plot(ins.numpy(), preds.numpy())
plt.show()


mu = np.mean(x, 0)
std = np.std(x, 0)
x_norm = (x - mu) / std

ins_norm = torch.from_numpy(x_norm.astype(np.float32))
preds = net_t(ins_norm, training = False)

preds_unnorm = preds*std+mu

plt.plot(ins.numpy(),np.sin(ins.numpy()))
#plt.plot(ins.numpy(), preds.numpy())
plt.plot(ins.numpy(), preds_unnorm.numpy())
plt.show()


## Test on entire test set
#correct = 0
#total = 0
#
#for data in testLoader:
#    net.train(False)
#    images, labels = data
#    outputs = net(images, training = False)
#    _, predicted = torch.max(outputs, 1)
#    total += labels.size(0)
#    correct += (predicted == labels).sum()
#
#print('Accuracy of the network on the 10000 test images: %.3f %%' % (
#    100 * correct.item() / total))