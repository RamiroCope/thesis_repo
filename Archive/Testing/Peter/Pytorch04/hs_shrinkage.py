import numpy as np
import matplotlib.pyplot as plt
import math
import scipy.stats as stats

quantiles_beta = np.linspace(stats.beta.ppf(0.05, 0.5,0.5), stats.beta.ppf(0.95, 0.5,0.5), 5000)
pdf_beta = stats.beta.pdf(quantiles_beta, 0.5,0.5)

#plt.figure(figsize = (800/96,600/96), dpi = 96)
plt.plot(quantiles_beta,pdf_beta, lw = 2)
plt.xlabel('z',fontsize=12)
plt.ylabel('p(z)', fontsize=12)
plt.show()