"""
    Set working directory
"""
import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.distributions import Normal
import scipy.stats as stats
from torch import optim
from Peter.Pytorch04.Models.BBB_gaussian_mix import BNN

#import matplotlib
#matplotlib.use('TkAgg')
#from torch.nn.parameter import Parameter
#from torch.optim import Optimizer
#from torch.distributions import Normal
#from IPython.display import Image, display, clear_output
#import numpy as np


"""
    Load data
"""

batch_size = 64

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)


"""
    Define the neural net
"""

prior_prec = 100
prec_init = 200

# initialize network    
net = BNN(input_size = 784,
              hidden_sizes = [100],
              output_size = 10,
              act_func = "Relu",
              prior_prec = prior_prec,
              prec_init = prec_init)

"""
    Define loss and optimizer
"""

# loss
criterion = nn.CrossEntropyLoss()

# N as in pseudo code for noisy Adam is the total number of observations
N = trainset.train_data.shape[0]

# optimizer
optimizer = optim.Adam(net.parameters(), lr = 1e-3)

# setup adaptive learning rate
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[1,29], gamma=0.1)
#scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.90)


"""
    Training the network
"""

max_epochs = 50

num_batches = N/batch_size

# indices for accessing weight matrices of the different layers
indices = [0,2,4]

train_loss = []
test_loss = []
KL_div = []
ELBO_updates = []
updates = []
lr_plt = []

for epoch in range(max_epochs):
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for batch, data in enumerate(trainLoader, 0):
        net.train(True)
        inputs, labels = data
        
        optimizer.zero_grad()
        outputs = net(inputs)   
        NLL = criterion(outputs, labels)
        ELBO = NLL + net.kl_divergence()/num_batches
        ELBO.backward()
        
        optimizer.step()
        
        # Print statistics
        running_loss += NLL.item()
        
        if batch % 100 == 99:
            print('[%d, %5d] ====> train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss / 100))
            KL_div += [(net.kl_divergence()).item()/num_batches]
            ELBO_updates += [ELBO.item()]
            updates += [batch + (epoch*num_batches)]
            train_loss += [running_loss/100]
            
            for data in testLoader:
                net.train(False)
                images, labels = data
                outputs = net(images, training = False)
                test_loss_val = criterion(outputs, labels)
                test_loss += [test_loss_val.item()]
                
            print('[%d, %5d] ====> test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val.item()))
            
            running_loss = 0.0
#    lr_plt += [scheduler.get_lr()[0]]        
#    scheduler.step()
#    print("=======> lr is: %.10f" % scheduler.get_lr()[0])
    
print('Finished training')    


"""
    Plotting
"""


fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,8))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,2,1)
 
plt.plot(updates, train_loss)
plt.xlabel("Updates"), plt.ylabel("train loss")
plt.title("Noisy Adam training loss")


plt.subplot(2,2,2)
plt.plot(updates, test_loss)
plt.xlabel("Updates"), plt.ylabel("test loss")
plt.title("Noisy Adam test loss")


plt.subplot(2,2,3)
plt.plot(updates, KL_div)
plt.xlabel("Updates"), plt.ylabel("KL Divergence")
plt.title("Noisy Adam KL Divergence")
#
#
plt.subplot(2,2,4)
plt.plot(updates, ELBO_updates)
plt.xlabel("Updates"), plt.ylabel("ELBO")
plt.title("Noisy Adam ELBO")
plt.show()


"""
    Accuracy of fitted model
"""



# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    net.train(False)
    images, labels = data
    outputs = net(images, training = False)
    _, predicted = torch.max(outputs, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))