
import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir) 


#from Main.Optimizers.noisyKFAC_closure import noisyKFAC as noisyAdam
from Optimizers.noisyKFAC_closure import noisyKFAC as noisyAdam

import torch
import torch.nn.functional as F
import torch.nn as nn


from torch.utils.data import DataLoader



import numpy as np
import matplotlib.pyplot as plt


'REGRESSION UCI DATASETS'
from Main.Datasets.Regression.boston import boston as dataset



"""Our Model"""
class FNN(nn.Module):
    
    def __init__(self,num_features, H1, H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1,bias=False)
        self.NN2 = nn.Linear(H1,H2, bias=False)
        self.NN3 = nn.Linear(H2,1,bias = False)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x



"""Data Preparation"""

batch_size=10

# train data
trainset = dataset()
y_train_mean, y_train_std = trainset.get_target_normalization_constants()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = dataset(train = False)
testLoader = DataLoader(testset, batch_size=batch_size, shuffle=False)

N = trainset.train_data.shape[0]
num_features = trainset.train_data.shape[1]

"""Hyper-parameters to Optimize"""

net = FNN(num_features, 15, 15)

criterion = nn.MSELoss()
optimizer = noisyAdam(net.parameters(),
                      net.modules(),
                      num_examples=N,
                      t_inv= 10,
                      t_stats=1,
                      lr = 1e-3,
                      beta = 0.99,
                      prior_var = 0.8,
                      kl_weight = 1.0)



for epoch in range(100):    

    for j, data in enumerate(trainLoader, 0):

        inputs, labels = data
        #inputs, labels = Variable(inputs), Variable(labels)

        mus = []
        As_inv = []
        Ss_inv = []
        weights = []
        Covs = []

        def closure():
            optimizer.zero_grad()
            outputs = net(inputs) 
            loss = criterion(outputs, labels)
            loss.backward()
            return loss
        
        LL_loss, params = optimizer.step(closure, return_params = True)

        for i, p in enumerate(net.parameters()):
            sample, A_inv, S_inv = params["sample{0}".format(i)],\
                        params["A_inv{0}".format(i)],\
                        params["S_inv{0}".format(i)]
                        
            mus += [p.data.cpu().view(-1).detach().numpy()]
            if i == 0:
                As_inv = A_inv
                Ss_inv = S_inv
            weights += [sample.cpu().view(-1).detach().numpy()]  
    print("=========> Epoch %d\n" % epoch)

        
# Test on entire test set
error=[]
for data in testLoader:
    x, y = data
    #optimizer.sample(training = False)
    outputs = net(x)
    error += [(outputs.data - y.data).numpy()]
#flattening list of lists
error = np.array([x for ls in error for x in ls])
normalized_RMSE  = np.mean(error**2)**0.5
#unormalized_RMSE = np.mean((error*y_train_std + y_train_mean)**2)**0.5 
print(normalized_RMSE)


#As_inv = (As_inv - As_inv.mean())/As_inv.std()
#Ss_inv = (Ss_inv - Ss_inv.mean())/Ss_inv.std()
Ss_inv = F.tanh(Ss_inv)
As_inv = F.tanh(As_inv)
cov_mat = np.kron(Ss_inv, As_inv)
#cov_mean = np.mean(cov_mat)
#cov_std = np.std(cov_mat)
#cov_mat_normalized = (cov_mat - cov_mean)/cov_std
#D = 1.0/np.sqrt(np.diag(cov_mat))
#D_inv = np.diag(D)
#corr_mat = np.matmul(D_inv, np.matmul(cov_mat, D_inv))


fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(cov_mat, interpolation='nearest')
fig.colorbar(cax)
plt.show()