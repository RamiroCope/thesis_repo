# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 19:44:34 2018

@author: stud
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 09:34:19 2018

@author: stud
"""

"""
    Setup working environment
"""

import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable
from Peter.Pytorch04.Optimizers.noisyAdam import noisyAdam
from torch.distributions import Normal
import scipy.stats as stats
from torch import optim




'''
PSEUDOCODE

create X matrix


for i in hidden_size:
    create normal_model(H=i)
    create bnn_model(H=i)
    
    for epoch in epochs:
        train models
        collect losses
        
plot
'''





"""
    ______________  Define FNN Model __________________
"""

#One Layer Network as in Noisy Natural Gradient Paper by Zhang et al (2018)
class FNN(nn.Module):
    
    def __init__(self,num_features, H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(num_features,H1)
        self.NN2 = nn.Linear(H1,1)
        
    def forward(self,x):
        x = F.sigmoid(self.NN1(x.view(-1,num_features)))
        x = self.NN2(x)
        return x    
    




"""
    ______________ Simulate X Data ____________________
"""
X_mu = 2
X_std = 1

# number of samples
num_obs = 20000           # <---------- Define NxM data matrix dimensions
num_features = 20

X_train = np.random.normal(X_mu, X_std, size = (num_obs, num_features)).astype(np.float32)
X_test  = np.random.normal(X_mu, X_std, size = (num_obs, num_features)).astype(np.float32)

# noise parameter         # <---------- Define noise level
eps = np.random.normal(0, 2, num_obs).reshape(num_obs, 1).astype(np.float32)
N = X_train.shape[0] #num observations



"""
    _____________  Simulate Targets Y  __________________
"""

# create Dummy FNN Model to simulate Y 
fnn_dummy  = FNN(num_features,H1=10) 



### Assign Normally Distributed Weights to Dummy Model (to simulate Y)

# Sample Weights
weights_list=[]
#weights_plot=[]
for p in list(fnn_dummy.parameters()):
    weights = torch.Tensor(p.size()).normal_(0,1)
    weights_list.append(weights)
    #weights_plot.append(weights.view(-1).numpy())
    

# Assign Weights to FNN dummy
for i, p in enumerate(list(fnn_dummy.parameters())):
    p.data = weights_list[i]
    
    
# forward pass to simulate Y
y_train = fnn_dummy(torch.from_numpy(X_train)).detach().numpy() + eps
y_test  = fnn_dummy(torch.from_numpy(X_test)).detach().numpy()  + eps





"""
   ___________  Configure outer loop  ______________________
"""
hidden_size_list =  [10] #[2,5,10,25,50,75,100,150,200,250,300,500,750,1000] #,1250,1500,1750,2000] #,2250,2500,3000]
#epoch_list = [5000, 5000, 10000, 10000, 10000, 10000, 10000, 10000]
#epoch_list = [10000]*len(snr_list)
train_losses = []
test_losses = []
train_losses_bayes = []
test_losses_bayes = []




for j, hidden_size in enumerate(hidden_size_list):
    
    
    # Create Models
    fnn       = FNN(num_features,hidden_size)
    fnn_bayes = FNN(num_features,hidden_size)
    
    
    
    """
        Configure Optimizers
    """
    # loss
    criterion = nn.MSELoss()    
    prior_var = 0.5
    
    # optimizer
    optimizer_bayes = noisyAdam(fnn_bayes.parameters(), num_examples = N,
                              gamma_ex = 0.1,
                              lr = 1e-3,
                              prior_var = prior_var,
                              betas = (0.9, 0.999))

    optimizer = optim.Adam(fnn.parameters(), lr = 1e-3)
   
    
    
    """
        Train Models
    """
    epochs = 4000
    
    train_loss = []
    test_loss = []
    train_loss_bayes = []
    test_loss_bayes = []
    updates = []
        
    for epoch in range(epochs):
    
        running_loss = 0.0
        running_loss_bayes = 0.0
    
        mus = []
        sigmas = []
        fs = []
        weights = []
        
        inputs  = torch.from_numpy(X_train)
        targets = torch.from_numpy(y_train)
        
        params_dict = optimizer_bayes.sample(return_params=True)
        
        for i, p in enumerate(fnn.parameters()):
            mu, sigma, f = params_dict["mu{0}".format(i)],\
                            params_dict["sigma{0}".format(i)],\
                            params_dict["f{0}".format(i)]
                        
            mus += [mu.view(-1).numpy()]
            sigmas += [sigma.view(-1).numpy()]
            fs += [f.view(-1).numpy()]
            weights += [p.data.view(-1).numpy()]
            
        
        # forward pass
        outputs_bayes = fnn_bayes(inputs)
        outputs       = fnn(inputs)
        
        # compute loss
        loss_bayes = criterion(outputs_bayes, targets)
        loss       = criterion(outputs, targets)
        
        optimizer.zero_grad()
        optimizer_bayes.zero_grad()
        
        # backpropagate
        loss.backward()
        loss_bayes.backward()
        
        # optimize/learn
        optimizer_bayes.step()
        optimizer.step()
        
        # collect losses
        running_loss += loss.item()
        running_loss_bayes += loss_bayes.item()
        
#        print('[%d] ====> train loss: %.3f' %
#                      (epoch + 1, running_loss))
        
        
        
        #___________ TEST/VAL _____________#
        
        test_inputs = torch.from_numpy(X_test)
        test_targets = torch.from_numpy(y_test)
        
        optimizer_bayes.sample(training = False)
        
        # forward pass
        test_outputs = fnn(test_inputs)
        test_outputs_bayes = fnn_bayes(test_inputs)
        
        # compute loss
        test_loss_val = criterion(test_outputs, test_targets)
        test_loss_val_bayes = criterion(test_outputs_bayes, test_targets)
        
        # collect losses
        train_loss += [loss.item()]
        test_loss += [test_loss_val.item()]
        
        train_loss_bayes += [loss_bayes.item()]
        test_loss_bayes += [test_loss_val_bayes.item()]
        
        updates += [epoch]
        
    train_losses += [train_loss[-1]]
    test_losses += [test_loss[-1]]
    print("=====> Train loss: ", train_loss[-1])   
    print("=====> Test loss: ", test_loss[-1])
    
    train_losses_bayes += [train_loss_bayes[-1]]
    test_losses_bayes += [test_loss_bayes[-1]]
    print("=====> Train loss Bayes: ", train_loss_bayes[-1])   
    print("=====> Test loss Bayes: ", test_loss_bayes[-1])

 
#print("Estimated weights: ", np.squeeze(mus))
#print("True weights: ", w_sample)


#plt.hist(mus, bins = 10,edgecolor='black')
#plt.hist(w_sample, bins = 10,edgecolor='black')
#plt.hist(fs, bins = 40,edgecolor='black')
#plt.show()

#    print('=============== PLOTTING ===============')
#
#    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,5))
#    fig.tight_layout(h_pad = 5, w_pad = 5)
#    
#    plt.subplot(2,2,1)     
#    plt.plot(updates, train_loss)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("SGD training loss")
#    
#    
#    plt.subplot(2,2,2)
#    plt.plot(updates, test_loss)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("SGD test loss")
#    
#    
#    plt.subplot(2,2,3)     
#    plt.plot(updates, train_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("Noisy Adam training loss")
#    
#    
#    plt.subplot(2,2,4)
#    plt.plot(updates, test_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("Noisy Adam test loss")
#    
#    plt.show()
    
    
#fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
#fig.tight_layout(h_pad = 5, w_pad = 5)
#
#plt.subplot(2,2,1)
##plt.plot(updates, train_loss) # plot something
#plt.hist([mus, weights_plot], bins = 10, edgecolor='black', normed = True)
#plt.xlabel("mu")
#plt.title("mu histogram of variational posterior")
#plt.legend(['Estimated means','true weights'])
#
#plt.subplot(2,2,2)
#
#plt.hist(sigmas, bins = 10, edgecolor='black', normed = True)
#plt.xlabel("standard deviation")
#plt.title("standard deviation histogram of variational posterior")
#plt.legend(['Layer 1','Layer 2','Layer 3'])
#
#plt.subplot(2,2,3)
#
#plt.hist([weights, weights_plot], bins = 10, edgecolor='black', normed = True)
##x = np.linspace(np.min(weights),np.max(weights),5000)
##pdf_t = stats.t.pdf(x, df=df)
##plt.plot(x,pdf_t)
#plt.xlabel("weights")
#plt.title("histogram of all weights in BNN")
#plt.legend(['Trained weights', 'True weights'])
#
## update canvas immediately
#plt.show()

    
plt.plot(hidden_size_list, train_losses)
plt.plot(hidden_size_list, test_losses)
#plt.xlabel("Model Complexity"), plt.ylabel("loss")
#plt.title("Adam train/test loss across Model Complexity")
#plt.legend(["Train loss", "Test loss"])
#plt.show()

plt.plot(hidden_size_list, train_losses_bayes, '-.')
plt.plot(hidden_size_list, test_losses_bayes, '-.')
#plt.plot(snr_list[0:], [4]*len(snr_list[0:]), '--')
plt.xlabel("Model Complexity"), plt.ylabel("loss")
plt.title("Train/Test loss across Model Complexity")
plt.legend(["Train loss", "Test loss", "BNN - Train loss", "BNN - Test loss"])
plt.show()