import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import math
import scipy.stats as stats


x = np.linspace(-4,4,5000)


def HS_density(x):
    return np.log(1+2/x**2)

def cauchy(x,loc,scale):
    return 1/(np.pi*scale*(1+((x-loc)/scale)**2))

y = HS_density(x)
y_norm = 1/(1+np.exp(-y))
y_c = cauchy(x,0,1)

k = 8*math.log(2/64+1)+2*math.sqrt(2)*math.atan(8/math.sqrt(2))

plt.subplot(1,2,1)
plt.plot(x,y_c)
plt.xlabel("x", fontsize = 12)
plt.ylabel("p(x)", fontsize = 12)
#plt.ylim(0,1)
plt.legend(['x ~ C(0,1)'])
plt.title("Density of the Cauchy distribution", fontsize = 14)

x = np.linspace(0,4,5000)

y_c = cauchy(x,0,0.11)

plt.subplot(1,2,2)
plt.plot(x,y_c)
plt.xlim(xmin=-4)
plt.axvline(x=0, ymin=0.0, linewidth=2, linestyle = ':', color='#1f77b4')
plt.xlabel("x", fontsize = 12)
plt.ylabel("p(x)", fontsize = 12)
plt.legend(['x ~ C+(0,1)'])
plt.title("Density of the Half-Cauchy distribution", fontsize = 14)

plt.show()


scale = 1.0

quantiles_t = np.linspace(stats.t.ppf(0.01, df = 1.0, loc = 0.0, scale=scale), stats.t.ppf(0.99, df = 1.0, loc = 0.0, scale=scale), 5000)
pdf_t = stats.t.pdf(quantiles_t, df=1.0, scale = scale, loc = 0.0)


x = np.linspace(-4,4,5000)

plt.plot(x,y/10)
plt.plot(quantiles_t,pdf_t, ':', lw = 2)
plt.xlabel("x", fontsize = 12)
plt.ylabel("p(x)", fontsize = 12)
#plt.ylim(0,1)
#plt.title("Density of the Horseshoe distribution", fontsize = 14)
plt.legend(['Horseshoe', 'student\'s t, df = 1'])

plt.show()