"""
    Set working directory
"""
import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from Optimizers.noisyAdam import noisyAdam
from Optimizers.vadam import Vadam
from torch.distributions import Normal
import scipy.stats as stats
from torch import optim

#import matplotlib
#matplotlib.use('TkAgg')
#from torch.nn.parameter import Parameter
#from torch.optim import Optimizer
#from torch.distributions import Normal
#from IPython.display import Image, display, clear_output
#import numpy as np


"""
    Load data
"""

batch_size = 128

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)


"""
    Define the neural net
"""

class FNN(nn.Module):
    
    def __init__(self,H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1)
        self.NN2 = nn.Linear(H1,10)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x

# initialize network    
net_noisy = FNN(100)
net_vadam = FNN(100)
net = FNN(100)

"""
    Define loss and optimizer
"""


# loss
criterion = nn.CrossEntropyLoss()

# N as in pseudo code for noisy Adam is the total number of observations
N = trainset.train_data.shape[0]

# prior variance for noisyAdam
prior_var = 0.1
# prior precision and variational posterior precision init value for Vadam
prior_prec = 10
prec_init = 10

# optimizer for Noisy Adam
optimizer_noisy = noisyAdam(net_noisy.parameters(), num_examples = 60000,
                          gamma_ex = 0,
                          lr = 1e-3,
                          prior_var = prior_var,
                          betas = (0.9, 0.999))

# optimizer for Vadam
optimizer_vadam = Vadam(net_vadam.parameters(), train_set_size = 60000,
                          lr = 1e-3,
                          prior_prec = prior_prec,
                          prec_init = prec_init,
                          betas = (0.9, 0.999))

# optimizer for Adam
optimizer = optim.Adam(net.parameters())

# setup adaptive learning rate
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[30,40], gamma=0.1)
#scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.85)


"""
    Training the network
"""

max_epochs = 15

num_batches = trainset.train_data.shape[0]/batch_size

p_dist = Normal(0, 1.0/np.sqrt(prior_var))
#p_dist = torch.distributions

# indices for accessing weight matrices of the different layers
indices = [0,2,4]

train_loss_noisy = []
test_loss_noisy = []

train_loss = []
test_loss = []

train_loss_vadam = []
test_loss_vadam = []

KL_div = []
ELBO = []
updates = []
lr_plt = []

for epoch in range(max_epochs):
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss_noisy = 0.0
    running_loss_vadam = 0.0
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for batch, data in enumerate(trainLoader, 0):
        
        mus = []
        sigmas = []
        weights_noisy = []
        weights_vadam = []
        weights = []
        fs = []
        
        inputs, labels = data
        #inputs, labels = Variable(inputs), Variable(labels)
        
        params_dict = optimizer_noisy.sample(return_params=True)
        
        outputs_noisy = net_noisy(inputs)
        outputs = net(inputs)
        
        # define closure for Vadam
        def closure():
            optimizer_vadam.zero_grad()
            outputs_vadam = net_vadam(inputs)   
            loss_vadam = criterion(outputs_vadam, labels)
            loss_vadam.backward()
            return loss_vadam
        loss_vadam = optimizer_vadam.step(closure)        
        
        # extract diagnostics
        lqw, lpw = 0.0, 0.0

        for i, p in enumerate(net_noisy.parameters()):
            mu, sigma, f = params_dict["mu{0}".format(i)],\
                            params_dict["sigma{0}".format(i)],\
                            params_dict["f{0}".format(i)]
                        
            mus += [mu.view(-1).detach().numpy()]
            sigmas += [sigma.view(-1).detach().numpy()]
            fs += [f.view(-1).detach().numpy()]
            weights_noisy += [p.view(-1).detach().numpy()]            
        
            # KL Divergence terms    
            q_dist = Normal(mu, sigma)
            lqw += q_dist.log_prob(p).sum()
            lpw += p_dist.log_prob(p).sum()
            
        for i, p in enumerate(net.parameters()):
            weights += [p.view(-1).detach().numpy()]            
        
        KL_loss = (lqw - lpw)/num_batches
        
        LL_loss_noisy = criterion(outputs_noisy, labels)
        LL_loss = criterion(outputs, labels)
    
        loss_noisy = LL_loss_noisy #+ KL_loss
        loss = LL_loss
        
        optimizer_noisy.zero_grad()
        optimizer.zero_grad()
        loss_noisy.backward()
        loss.backward()
        optimizer_noisy.step()
        optimizer.step()
        
        # Print statistics
        running_loss_noisy += LL_loss_noisy.item()
        running_loss_vadam += loss_vadam.item()
        running_loss += LL_loss.item()
        if batch % 100 == 99:
            print('[%d, %5d] ====> Noisy train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_noisy / 100))
            print('[%d, %5d] ====> Vadam train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_vadam / 100))
            print('[%d, %5d] ====> train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss / 100))
            updates += [batch + (epoch*num_batches)]
            train_loss_noisy += [running_loss_noisy/100]
            train_loss_vadam += [running_loss_vadam/100]
            train_loss += [running_loss/100]
            KL_div += [KL_loss]
            ELBO += [loss_noisy.item()]
            
            for data in testLoader:
                images, labels = data
                optimizer_noisy.sample(training = False)
                outputs_noisy = net_noisy(images)
                outputs_vadam = net_vadam(images)
                outputs = net(images)
                test_loss_val_noisy = criterion(outputs_noisy, labels)
                test_loss_val_vadam = criterion(outputs_vadam, labels)
                test_loss_val = criterion(outputs, labels)
                test_loss_noisy += [test_loss_val_noisy.item()]
                test_loss_vadam += [test_loss_val_vadam.item()]
                test_loss += [test_loss_val.item()]
                
            print('[%d, %5d] ====> Noisy test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_noisy.item()))
            print('[%d, %5d] ====> Vadam test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_vadam.item()))
            print('[%d, %5d] ====> test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val.item()))
            
#            fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
#            fig.tight_layout(h_pad = 5, w_pad = 5)
#
#            plt.subplot(2,2,1)
#            #plt.plot(updates, train_loss) # plot something
#            plt.hist([mus[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("mu")
#            plt.title("mu histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.subplot(2,2,2)
#            
#            plt.hist([sigmas[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("standard deviation")
#            plt.title("standard deviation histogram of variational posterior")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#            
#            plt.subplot(2,2,3)
#            
#            plt.hist([weights[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            #x = np.linspace(np.min(weights),np.max(weights),5000)
#            #pdf_t = stats.t.pdf(x, df=df)
#            #plt.plot(x,pdf_t)
#            plt.xlabel("weights")
#            plt.title("histogram of all weights in BNN")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#    
#            plt.subplot(2,2,4)
#            
#            plt.hist([fs[i] for i in indices], bins = 30, edgecolor='black', normed = True)
#            plt.xlabel("fisher approximation")
#            plt.title("histogram of fisher approximation")
#            plt.legend(['Layer 1','Layer 2','Layer 3'])
#    
#            # update canvas immediately
#            plt.show()
            running_loss_noisy = 0.0
            running_loss_vadam = 0.0
            running_loss = 0.0
#    lr_plt += [scheduler.get_lr()[0]]        
#    scheduler.step()
#    print("=======> lr is: %.10f" % scheduler.get_lr()[0])
    
print('Finished training')    


"""
    Plotting
"""


fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,8))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,2,1)
 
plt.plot(updates, train_loss_noisy)
plt.plot(updates, train_loss_vadam)
plt.plot(updates, train_loss)
plt.xlabel("Updates"), plt.ylabel("train loss")
plt.title("Training loss")
plt.legend(['Noisy Adam','Vadam','Adam'])

plt.subplot(2,2,2)
plt.plot(updates, test_loss_noisy)
plt.plot(updates, test_loss_vadam)
plt.plot(updates, test_loss)
plt.xlabel("Updates"), plt.ylabel("test loss")
plt.title("Test loss")
plt.legend(['Noisy Adam','Vadam','Adam'])

plt.subplot(2,2,3)
plt.plot(updates, KL_div)
plt.xlabel("Updates"), plt.ylabel("KL Divergence")
plt.title("Noisy Adam KL Divergence")

plt.subplot(2,2,4)
plt.plot(updates, ELBO)
plt.xlabel("Updates"), plt.ylabel("ELBO")
plt.title("Noisy Adam ELBO")
plt.show()


plt.hist([weights[i] for i in [0,2]],normed=True,bins=30,edgecolor='black')
plt.show()


#x = np.linspace(np.min(weights[0]),np.max(weights[0]),5000)
#pdf = stats.norm.pdf(x, np.mean(weights[0]), np.std(weights[0]))
#pdf_t = stats.t.pdf(x, 5)
##pdf = stats.norm.pdf(x, np.mean(mus), np.mean(sigmas))
#plt.hist(weights[0],normed=True, bins = 50,edgecolor='black')
#plt.plot(x,pdf)
#plt.plot(x,pdf_t)
#plt.show()
#
#
#plt.hist(fs,normed=True, bins = 50,edgecolor='black')
#
#
#for j, p in enumerate(net.parameters()):
#    #print(j)
#    
#    if j % 2 == 0:
#        plt.subplot(2,1,1)
#        plt.hist(p.data.view(-1).numpy(), bins = 40, edgecolor='black', alpha = 0.5, normed = True)
#        plt.legend(['L1_W','L2_W','L3_W'])
#    else:
#        plt.subplot(2,1,2)
#        plt.hist(p.data.view(-1).numpy(), bins = 40, edgecolor='black', alpha = 0.5, normed = True)
#        plt.legend(['L1_b','L2_b','L3_b'])
#    
#
#plt.show()
#
#plist = list(net.parameters())
#
#plt.hist(plist[0:2])


"""
    Accuracy of fitted model
"""



# Test on entire test set
correct_noisy = 0
correct_vadam = 0
correct = 0
total = 0

for data in testLoader:
    images, labels = data
    optimizer_noisy.sample(training = False)
    outputs_noisy = net_noisy(images)
    outputs_vadam = net_vadam(images)
    outputs = net(images)
    _, predicted_noisy = torch.max(outputs_noisy, 1)
    _, predicted_vadam = torch.max(outputs_vadam, 1)
    _, predicted = torch.max(outputs, 1)
    total += labels.size(0)
    correct_noisy += (predicted_noisy == labels).sum()
    correct_vadam += (predicted_vadam == labels).sum()
    correct += (predicted == labels).sum()

print('Accuracy of the noisy network on the 10000 test images: %d %%' % (
    100 * correct_noisy / total))
print('Accuracy of the Vadam network on the 10000 test images: %d %%' % (
    100 * correct_vadam / total))
print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))