"""
    Setup working environment
"""

import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable
from Peter.Pytorch04.Optimizers.noisyKFAC_closure import noisyKFAC
from torch.distributions import Normal
import scipy.stats as stats
from torch import optim
import torch.distributions as distributions

"""
    Configure outer loop
"""


num_obs_list = [100, 200, 300, 400, 500, 800, 1000, 2000, 4000]
#epoch_list = [5000, 5000, 10000, 10000, 10000, 10000, 10000, 10000]
epoch_list = [10000]*len(num_obs_list)
train_losses = []
test_losses = []
train_losses_bayes = []
test_losses_bayes = []

# Define mean and stdv for the true distribution of the weights
w_mu = 0.0
w_std = 1.0
num_weights = 100

p_w = distributions.Normal(w_mu, w_std)

w_sample = p_w.sample((1,num_weights))

X_mu = 2.0
X_std = 1.0

q_w = distributions.Normal(X_mu, X_std)

eps_stdv = 1.0
d_eps = distributions.Normal(0.0, eps_stdv)

# Sample num_weights weights from true distribution

#w_sample = np.random.normal(w_mu, w_std, num_weights).reshape(1,100).astype(np.float32)

class FNN(nn.Module):
    
    def __init__(self, in_features):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(in_features, 1, bias = False)
        
    def forward(self,x):
#        x = F.sigmoid(self.NN1(x))
        x = self.NN1(x)
        return x

for j, obs in enumerate(num_obs_list):
    
    """
        Simulate feature matrix
    """
    

    
    # number of samples
    num_obs = obs
    
#    X_train = np.random.normal(X_mu, X_std, size = (num_obs, num_weights)).astype(np.float32)
#    X_test = np.random.normal(X_mu, X_std, size = (num_obs, num_weights)).astype(np.float32)
    X_train = q_w.sample((num_obs, num_weights))
    X_test = q_w.sample((num_obs, num_weights))
    
    # noise parameter
    eps = d_eps.sample((num_obs,1))#np.random.normal(0.0, 2.0, num_obs).astype(np.float32)
    
    # define simulated target y
    y_train = F.linear(X_train, w_sample) + eps
    y_test = F.linear(X_test, w_sample) + eps

#    y_train = 1.0/(1.0 + np.exp(-np.matmul(w_sample,X_train.transpose())))
#    y_test = 1.0/(1.0 + np.exp(-np.matmul(w_sample,X_test.transpose())))
    
#    y_train = np.add( y_train, eps ).astype(np.float32)
#    y_test = np.add( y_test, eps ).astype(np.float32)
    
#    y_train = np.add( np.matmul(X_train, w_sample), eps ).astype(np.float32)
#    y_test = np.add( np.matmul(X_test, w_sample), eps ).astype(np.float32)
    
    
    """
        Linear regression model with no bias
    """
       
#    linear = nn.Linear(num_weights, 1, bias = False)
#    linear_bayes = nn.Linear(num_weights, 1, bias = False)
    

    
    linear = FNN(num_weights)
    linear_bayes = FNN(num_weights)
#    for p in linear.parameters():
#        p.data = w_sample
    
    # loss
    criterion = nn.MSELoss()
#    criterion = nn.BCELoss()
    
    # N as in pseudo code for noisy Adam is the total number of observations
    N = X_train.shape[0]
    
    prior_var = 0.1
    
    # optimizer
    optimizer_bayes = noisyKFAC(linear_bayes.parameters(),
                                      linear_bayes.modules(),
                                      num_examples=N,
                                      t_inv= 100,
                                      t_stats=1,
                                      lr = 1e-7,
                                      beta = 0.99,
                                      prior_var = prior_var,
                                      kl_weight = 1)

    #optimizer = optim.SGD(linear.parameters(), 1e-2)    
    optimizer = optim.Adam(linear.parameters(), lr = 1e-3)
    """
        Train linear regression
    """
    
    epochs = epoch_list[j]
    
    train_loss = []
    test_loss = []
    train_loss_bayes = []
    test_loss_bayes = []
    updates = []
        
    for epoch in range(epochs):
    
        if epoch == 5:
            for param_group in optimizer.param_groups:
                param_group['lr'] = 1e-3
        
        running_loss = 0.0
        running_loss_bayes = 0.0
    
        mus = []
        As_inv = []
        Ss_inv = []
        weights = []
        
        inputs  = X_train
        targets = y_train
        
        def closure():
            optimizer_bayes.zero_grad()
            outputs_bayes = linear_bayes(inputs) 
            loss_bayes = criterion(outputs_bayes, targets)
            loss_bayes.backward()
            return loss_bayes
        
        loss_bayes, params_dict = optimizer_bayes.step(closure, return_params = True)
        
        for i, p in enumerate(linear.parameters()):
            sample, A_inv, S_inv = params_dict["sample{0}".format(i)],\
                            params_dict["A_inv{0}".format(i)],\
                            params_dict["S_inv{0}".format(i)]
                        
            mus += [p.data.cpu().view(-1).detach().numpy()]
            As_inv += [A_inv.cpu().contiguous().view(-1).detach().numpy()]
            Ss_inv += [S_inv.cpu().contiguous().view(-1).detach().numpy()]
            weights += [sample.cpu().view(-1).detach().numpy()]
            
        
        
        outputs = linear(inputs)
        
        loss = criterion(outputs, targets)
        
        optimizer.zero_grad()
        
        loss.backward()
        
        optimizer.step()
        
        running_loss += loss.item()
        running_loss_bayes += loss_bayes.item()
        
#        print('[%d] ====> train loss: %.3f' %
#                      (epoch + 1, running_loss))
        
        test_inputs =X_test
        test_targets = y_test
        
        test_outputs = linear(test_inputs)
        test_outputs_bayes = linear_bayes(test_inputs)
        
        test_loss_val = criterion(test_outputs, test_targets)
        test_loss_val_bayes = criterion(test_outputs_bayes, test_targets)
        
        train_loss += [loss.item()]
        test_loss += [test_loss_val.item()]
        
        train_loss_bayes += [loss_bayes.item()]
        test_loss_bayes += [test_loss_val_bayes.item()]
        
        updates += [epoch]
        
    train_losses += [train_loss[-1]]
    test_losses += [test_loss[-1]]
    print("=====> Train loss: ", train_loss[-1])   
    print("=====> Test loss: ", test_loss[-1])
    
    train_losses_bayes += [train_loss_bayes[-1]]
    test_losses_bayes += [test_loss_bayes[-1]]
    print("=====> Train loss Bayes: ", train_loss_bayes[-1])   
    print("=====> Test loss Bayes: ", test_loss_bayes[-1])

 
#print("Estimated weights: ", np.squeeze(mus))
#print("True weights: ", w_sample)


#plt.hist(mus, bins = 10,edgecolor='black')
#plt.hist(w_sample, bins = 10,edgecolor='black')
#plt.hist(fs, bins = 40,edgecolor='black')
#plt.show()

#    print('=============== PLOTTING ===============')
#
#    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,5))
#    fig.tight_layout(h_pad = 5, w_pad = 5)
#    
#    plt.subplot(2,2,1)     
#    plt.plot(updates, train_loss)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("SGD training loss")
#    
#    
#    plt.subplot(2,2,2)
#    plt.plot(updates, test_loss)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("SGD test loss")
#    
#    
#    plt.subplot(2,2,3)     
#    plt.plot(updates, train_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("Noisy Adam training loss")
#    
#    
#    plt.subplot(2,2,4)
#    plt.plot(updates, test_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("Noisy Adam test loss")
#    
#    plt.show()
    
    
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,2,1)
#plt.plot(updates, train_loss) # plot something
plt.hist([mus, w_sample.view(-1).numpy()], bins = 10, edgecolor='black', normed = True)
plt.xlabel("mu")
plt.title("mu histogram of variational posterior")
plt.legend(['Estimated means','true weights'])

plt.subplot(2,2,2)

plt.hist(As_inv, bins = 10, edgecolor='black', normed = True)
plt.xlabel("standard deviation")
plt.title("standard deviation histogram of variational posterior")
plt.legend(['Layer 1','Layer 2','Layer 3'])

plt.subplot(2,2,3)

plt.hist([weights, w_sample.view(-1).numpy()], bins = 10, edgecolor='black', normed = True)
#x = np.linspace(np.min(weights),np.max(weights),5000)
#pdf_t = stats.t.pdf(x, df=df)
#plt.plot(x,pdf_t)
plt.xlabel("weights")
plt.title("histogram of all weights in BNN")
plt.legend(['Trained weights', 'True weights'])

# update canvas immediately
plt.show()

    
plt.plot(num_obs_list[0:], train_losses[0:])
plt.plot(num_obs_list[0:], test_losses[0:])
plt.plot(num_obs_list[0:], train_losses_bayes[0:], '-.')
plt.plot(num_obs_list[0:], test_losses_bayes[0:], '-.')
plt.plot(num_obs_list[0:], [eps_stdv**2]*len(num_obs_list[0:]), '--')

plt.xlabel("Number of observations"), plt.ylabel("loss")
plt.title("Noisy Adam train/test loss for increasing number of observations")
plt.legend(["Train loss", "Test loss", "BNN - Train loss", "BNN - Test loss", "Noise level"])
plt.show()