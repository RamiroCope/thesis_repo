"""
    Set working directory
"""


import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""


import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from Optimizers.noisyKFAC_closure import noisyKFAC
from Optimizers.noisyAdam_closure_custom import noisyAdam
from Optimizers.Vprop_closure import Vprop
import torch.optim as optim
from Models.BBB import BNN
import numpy as np


"""
Load data
"""


batch_size = 100

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)
    
N = trainset.train_data.shape[0]

"""
    Define neural networks
"""


class FNN(nn.Module):
    
    def __init__(self,H1,bias = True):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1, bias = bias)
        self.NN2 = nn.Linear(H1,10, bias = bias)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x

# Initialize neural nets
net_KFAC = FNN(100, bias = False)
net_NoisyAdam = FNN(100, bias = False)
net_Vprop = FNN(100, bias = False)
net_Adam = FNN(100, bias = False)

# For black box
# initialize network    
net_BBB = BNN(input_size = 784,
                  hidden_sizes = [100],
                  output_size = 10,
                  act_func = "Relu",
                  prior_prec = 10,
                  prec_init = 100)


"""
    Loss and optimizers
"""


criterion = nn.CrossEntropyLoss()

# Note that KFAC typically needs a warmup time with a very low learning rate
# for a number of epochs, before turning up the learning rate.
optimizer_KFAC = noisyKFAC(net_KFAC.parameters(),
                      net_KFAC.modules(),
                      num_examples = N,
                      t_inv = 100,
                      t_stats = 10,
                      lr = 1e-6,
                      beta = 0.99,
                      prior_var = 1,
                      kl_weight = 1)

prior_var = 0.1
var_init = 0.1

optimizer_NoisyAdam = noisyAdam(net_NoisyAdam.parameters(), num_examples = N,
                          gamma_ex = 0,
                          lr = 1e-3,
                          prior_var = prior_var,
                          var_init = var_init,
                          kl_weight = 1,
                          betas = (0.9, 0.999))

optimizer_Vprop = Vprop(net_Vprop.parameters(), num_examples = N,
                          lr = 1e-3,
                          alpha = 0.99,
                          prior_var = prior_var,                          
                          var_init = var_init)

optimizer_Adam = optim.Adam(net_Adam.parameters(), lr = 1e-3)

optimizer_BBB = optim.Adam(net_BBB.parameters(), lr = 1e-3)

"""
    Training the network
"""

# choose how many epochs the noisy KFAC network should run warm_up
warm_up = 1
max_epochs = 25

num_batches = N/batch_size

# indices for accessing weight matrices of the different layers
indices = [0,1,2]

train_loss_KFAC = []
test_loss_KFAC = []
train_loss_NoisyAdam = []
test_loss_NoisyAdam = []
train_loss_Vprop = []
test_loss_Vprop = []
train_loss_Adam = []
test_loss_Adam = []
train_loss_BBB = []
test_loss_BBB = []


KL_div = []
ELBO = []
updates = []
lr_plt = []

for epoch in range(max_epochs):
    
    if epoch == warm_up:
        for param_group in optimizer_KFAC.param_groups:
            param_group['lr'] = 1e-3

    running_loss_KFAC      = 0.0
    running_loss_NoisyAdam = 0.0
    running_loss_Vprop     = 0.0
    running_loss_Adam      = 0.0
    running_loss_BBB       = 0.0

    for batch, data in enumerate(trainLoader, 0):
        
        inputs, labels = data
#        inputs = inputs.cuda()
#        labels = labels.cuda()
        
        # For KFAC
        def closure():
            optimizer_KFAC.zero_grad()
            outputs = net_KFAC(inputs) 
            loss = criterion(outputs, labels)
            loss.backward()
            return loss
        
        LL_loss_KFAC, _ = optimizer_KFAC.step(closure, return_params = False)
        
        # For noisyAdam
        def closure():
            optimizer_NoisyAdam.zero_grad()
            outputs = net_NoisyAdam(inputs) 
            loss = criterion(outputs, labels)
            loss.backward()
            return loss
        
        LL_loss_NoisyAdam, _ = optimizer_NoisyAdam.step(closure, return_params = False)
        
        # For Vprop
        def closure():
            optimizer_Vprop.zero_grad()
            outputs = net_Vprop(inputs) 
            loss = criterion(outputs, labels)
            loss.backward()
            return loss
        
        LL_loss_Vprop, _ = optimizer_Vprop.step(closure, return_params = False)
        
        # For Adam
        optimizer_Adam.zero_grad()
        output_Adam = net_Adam(inputs)
        loss_Adam = criterion(output_Adam, labels)
        loss_Adam.backward()
        optimizer_Adam.step()
        
        # For BBB
        optimizer_BBB.zero_grad()
        output_BBB = net_BBB(inputs)
        LL_loss_BBB = criterion(output_BBB, labels)
        KL_loss = net_BBB.kl_divergence()/N#num_batches
        loss_BBB = LL_loss_BBB + KL_loss
        loss_BBB.backward()
        optimizer_BBB.step()
        
        # Print statistics
        running_loss_KFAC      += LL_loss_KFAC.item()
        running_loss_NoisyAdam += LL_loss_NoisyAdam.item()
        running_loss_Vprop     += LL_loss_Vprop.item()
        running_loss_Adam      += loss_Adam.item()
        running_loss_BBB       += LL_loss_BBB.item()#loss_BBB.item()
        
        if batch % 100 == 99:
            
            print('[%d, %5d] =====> NoisyKFAC train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_KFAC / 100))
            
            print('[%d, %5d] ========> NoisyAdam train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_NoisyAdam / 100))
            
            print('[%d, %5d] ===============> Vprop train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_Vprop / 100))
            
            print('[%d, %5d] ===================> Adam train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss_Adam / 100))
            
            print('[%d, %5d] =======================> BBB train loss: %.3f \n' %
                  (epoch + 1, batch + 1, running_loss_BBB / 100))
            
            
            updates += [batch + (epoch*num_batches)]
            
            train_loss_KFAC += [running_loss_KFAC/100]
            train_loss_NoisyAdam += [running_loss_NoisyAdam/100]
            train_loss_Vprop += [running_loss_Vprop/100]
            train_loss_Adam += [running_loss_Adam/100]
            train_loss_BBB += [running_loss_BBB/100]
            
            for data in testLoader:
                images, labels = data
#                images = images.cuda()
#                labels = labels.cuda()
                outputs_KFAC = net_KFAC(images)
                outputs_NoisyAdam = net_NoisyAdam(images)
                outputs_Vprop = net_Vprop(images)
                outputs_Adam = net_Adam(images)
                outputs_BBB = net_BBB(images)
                
                test_loss_val_KFAC = criterion(outputs_KFAC, labels)
                test_loss_val_NoisyAdam = criterion(outputs_NoisyAdam, labels)
                test_loss_val_Vprop = criterion(outputs_Vprop, labels)
                test_loss_val_Adam = criterion(outputs_Adam, labels)
                test_loss_val_BBB = criterion(outputs_BBB, labels)
                
                test_loss_KFAC += [test_loss_val_KFAC.item()]
                test_loss_NoisyAdam += [test_loss_val_NoisyAdam.item()]
                test_loss_Vprop += [test_loss_val_Vprop.item()]
                test_loss_Adam += [test_loss_val_Adam.item()]
                test_loss_BBB += [test_loss_val_BBB.item()]
                
            print('[%d, %5d] =====> NoisyKFAC test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_KFAC.item()))
            print('[%d, %5d] ========> NoisyAdam test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_NoisyAdam.item()))
            print('[%d, %5d] ===============> Vprop test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_Vprop.item()))
            print('[%d, %5d] ===================> Adam test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val_Adam.item()))
            print('[%d, %5d] =======================> BBB test loss: %.3f \n' %
                  (epoch + 1, batch + 1, test_loss_val_BBB.item()))
    
            running_loss_KFAC = 0.0
            running_loss_NoisyAdam = 0.0
            running_loss_Vprop = 0.0
            running_loss_Adam = 0.0
            running_loss_BBB = 0.0
    
print('Finished training')    


"""
    Plotting
"""


fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10,8))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,1,1)
 
kfac,  = plt.plot(updates[2:], train_loss_KFAC[2:], '-.', label = 'Noisy KFAC')
nadam, = plt.plot(updates, train_loss_NoisyAdam, '--', label = 'Noisy Adam')
vprop, = plt.plot(updates, train_loss_Vprop, ':', label = 'Vprop')
adam,  = plt.plot(updates, train_loss_Adam, label = 'Adam')
bbb,   = plt.plot(updates, train_loss_BBB, '-.', label = 'BBB')

plt.legend(handles=[kfac, nadam, vprop, adam, bbb])
plt.xlabel("Updates"), plt.ylabel("train loss")
plt.title("Training loss")


plt.subplot(2,1,2)

kfac_test,  = plt.plot(updates, test_loss_KFAC, '-.', label = 'Noisy KFAC')
nadam_test, = plt.plot(updates, test_loss_NoisyAdam, '--', label = 'Noisy Adam')
vprop_test, = plt.plot(updates, test_loss_Vprop, ':', label = 'Vprop')
adam_test,  = plt.plot(updates, test_loss_Adam, label = 'Adam')
bbb_test,   = plt.plot(updates, test_loss_BBB, '-.', label = 'BBB')

plt.legend(handles = [kfac_test, nadam_test, vprop_test, adam_test, bbb_test])
plt.xlabel("Updates"), plt.ylabel("test loss")
plt.title("Test loss")

#
#plt.subplot(2,2,3)
#plt.plot(updates, KL_div)
#plt.xlabel("Updates"), plt.ylabel("KL Divergence")
#plt.title("Noisy Adam KL Divergence")
#
#
#plt.subplot(2,2,4)
#plt.plot(updates, ELBO)
#plt.xlabel("Updates"), plt.ylabel("ELBO")
#plt.title("Noisy Adam ELBO")
plt.show()


"""
    Accuracy of fitted model
"""


# Test on entire test set
correct_KFAC = 0
correct_NoisyAdam = 0
correct_Vprop = 0
correct_Adam = 0
correct_BBB = 0
total = 0

for data in testLoader:
    images, labels = data
#    images = images.cuda()
#    labels = labels.cuda()
    outputs_KFAC = net_KFAC(images)
    outputs_NoisyAdam = net_NoisyAdam(images)
    outputs_Vprop = net_Vprop(images)
    outputs_Adam = net_Adam(images)
    outputs_BBB = net_BBB(images)
    
    _, predicted_KFAC = torch.max(outputs_KFAC, 1)
    _, predicted_NoisyAdam = torch.max(outputs_NoisyAdam, 1)
    _, predicted_Vprop = torch.max(outputs_Vprop, 1)
    _, predicted_Adam = torch.max(outputs_Adam, 1)
    _, predicted_BBB = torch.max(outputs_BBB, 1)
    
    total += labels.size(0)
    
    correct_KFAC += (predicted_KFAC == labels).sum()
    correct_NoisyAdam += (predicted_NoisyAdam == labels).sum()
    correct_Vprop += (predicted_Vprop == labels).sum()
    correct_Adam += (predicted_Adam == labels).sum()
    correct_BBB += (predicted_BBB == labels).sum()

print('Accuracy with Noisy KFAC on the 10000 test images: %.3f %%' % (
    100 * correct_KFAC.item() / total))
print('Accuracy with Noisy Adam on the 10000 test images: %.3f %%' % (
    100 * correct_NoisyAdam.item() / total))
print('Accuracy with Vprop on the 10000 test images:      %.3f %%' % (
    100 * correct_Vprop.item() / total))
print('Accuracy with Adam on the 10000 test images:       %.3f %%' % (
    100 * correct_Adam.item() / total))
print('Accuracy with BBB on the 10000 test images:        %.3f %%' % (
    100 * correct_BBB.item() / total))