import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from Optimizers.noisyKFAC_closure import noisyKFAC

"""
    Set working directory
"""
import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
Load data
"""


batch_size = 128

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)
    
N = trainset.train_data.shape[0]
    
"""
    Define neural net
"""


class FNN(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1, bias = False)
        self.NN2 = nn.Linear(H1,H2, bias = False)
        self.NN3 = nn.Linear(H2,10, bias = False)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x
    
net = FNN(200,100)

net.cuda()


"""
    Loss and optimizer
"""

criterion = nn.CrossEntropyLoss()

# Note that KFAC typically needs a warmup time with a very low learning rate
# for a number of epochs, before turning up the learning rate.
optimizer = noisyKFAC(net.parameters(),
                      net.modules(),
                      num_examples=N,
                      t_inv= 100,
                      t_stats=10,
                      lr = 1e-6,
                      beta = 0.99,
                      prior_var = 1,
                      kl_weight = 1)



"""
    Training the network
"""

warm_up = 5
max_epochs = 25

num_batches = N/batch_size

#p_dist = torch.distributions

# indices for accessing weight matrices of the different layers
indices = [0,1,2]

train_loss = []
test_loss = []
KL_div = []
ELBO = []
updates = []
lr_plt = []

for epoch in range(max_epochs):
    
    if epoch == warm_up:
        for param_group in optimizer.param_groups:
            param_group['lr'] = 1e-3
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for batch, data in enumerate(trainLoader, 0):        
        
        mus = []
        As_inv = []
        Ss_inv = []
        weights = []
        Covs = []
        
        inputs, labels = data
        inputs = inputs.cuda()
        labels = labels.cuda()
        
        def closure():
            optimizer.zero_grad()
            outputs = net(inputs) 
            loss = criterion(outputs, labels)
            loss.backward()
            return loss
        
        LL_loss, params = optimizer.step(closure, return_params = True)
        
#        params_dict = optimizer.sample(return_params = True)
        
#        outputs = net(inputs)        
        
        # extract diagnostics
#        lqw, lpw = 0.0, 0.0
#
        for i, p in enumerate(net.parameters()):
            sample, A_inv, S_inv = params["sample{0}".format(i)],\
                        params["A_inv{0}".format(i)],\
                        params["S_inv{0}".format(i)]
                        
            mus += [p.data.cpu().view(-1).detach().numpy()]
            As_inv += [A_inv.cpu().view(-1).detach().numpy()]
            Ss_inv += [S_inv.cpu().view(-1).detach().numpy()]
            weights += [sample.cpu().view(-1).detach().numpy()]            
#        
#            # KL Divergence terms    
#            q_dist = Normal(mu, sigma)
#            lqw += q_dist.log_prob(p).sum()
#            lpw += p_dist.log_prob(p).sum()
    
#        mus = np.concatenate(mus)
#        sigmas = np.concatenate(sigmas)
#        weights = np.concatenate(weights)
#        fs = np.concatenate(fs)
    
#        KL_loss = (lqw - lpw)/num_batches
        
#        LL_loss = criterion(outputs, labels)
    
        loss = LL_loss #+ KL_loss
        
        # Print statistics
        running_loss += LL_loss.item()
        if batch % 100 == 99:
            print('[%d, %5d] ====> train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss / 100))
            updates += [batch + (epoch*num_batches)]
            train_loss += [running_loss/100]
#            KL_div += [KL_loss]
#            ELBO += [loss.item()]
            
            for data in testLoader:
                images, labels = data
                images = images.cuda()
                labels = labels.cuda()
                outputs = net(images)
                test_loss_val = criterion(outputs, labels)
                test_loss += [test_loss_val.item()]
                
            print('[%d, %5d] ====> test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val.item()))
            
            fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
            fig.tight_layout(h_pad = 5, w_pad = 5)

            plt.subplot(2,2,1)
            #plt.plot(updates, train_loss) # plot something
            plt.hist([mus[i] for i in indices], bins = 30, edgecolor='black', normed = True)
            plt.xlabel("mu")
            plt.title("mu histogram of variational posterior")
            plt.legend(['Layer 1','Layer 2','Layer 3'])
            
            plt.subplot(2,2,2)
            
            plt.hist([As_inv[i] for i in indices], bins = 30, edgecolor='black', normed = True)
            plt.xlabel("Covariance matrix A")
            plt.title("A matrix histogram")
            plt.legend(['Layer 1','Layer 2','Layer 3'])
            
            plt.subplot(2,2,3)
            
            plt.hist([weights[i] for i in indices], bins = 30, edgecolor='black', normed = True)
            plt.xlabel("weights")
            plt.title("histogram of all weights in BNN")
            plt.legend(['Layer 1','Layer 2','Layer 3'])
    
            plt.subplot(2,2,4)
            
            plt.hist([Ss_inv[i] for i in indices], bins = 30, edgecolor='black', normed = True)
            plt.xlabel("Covariance matrix S")
            plt.title("S matrix histogram")
            plt.legend(['Layer 1','Layer 2','Layer 3'])
    
            # update canvas immediately
            plt.show()
            running_loss = 0.0
#    lr_plt += [scheduler.get_lr()[0]]        
#    scheduler.step()
#    print("=======> lr is: %.10f" % scheduler.get_lr()[0])
    
print('Finished training')    


"""
    Plotting
"""


fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,8))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,2,1)
 
plt.plot(updates[2:], train_loss[2:])
plt.xlabel("Updates"), plt.ylabel("train loss")
plt.title("Noisy Adam training loss")


plt.subplot(2,2,2)
plt.plot(updates, test_loss)
plt.xlabel("Updates"), plt.ylabel("test loss")
plt.title("Noisy Adam test loss")

#
#plt.subplot(2,2,3)
#plt.plot(updates, KL_div)
#plt.xlabel("Updates"), plt.ylabel("KL Divergence")
#plt.title("Noisy Adam KL Divergence")
#
#
#plt.subplot(2,2,4)
#plt.plot(updates, ELBO)
#plt.xlabel("Updates"), plt.ylabel("ELBO")
#plt.title("Noisy Adam ELBO")
plt.show()





#x = np.linspace(np.min(weights[0]),np.max(weights[0]),5000)
#pdf = stats.norm.pdf(x, np.mean(weights[0]), np.std(weights[0]))
#pdf_t = stats.t.pdf(x, 5)
##pdf = stats.norm.pdf(x, np.mean(mus), np.mean(sigmas))
#plt.hist(weights[0],normed=True, bins = 50,edgecolor='black')
#plt.plot(x,pdf)
#plt.plot(x,pdf_t)
#plt.show()
#
#
#plt.hist(fs,normed=True, bins = 50,edgecolor='black')
#
#
#for j, p in enumerate(net.parameters()):
#    #print(j)
#    
#    if j % 2 == 0:
#        plt.subplot(2,1,1)
#        plt.hist(p.data.view(-1).numpy(), bins = 40, edgecolor='black', alpha = 0.5, normed = True)
#        plt.legend(['L1_W','L2_W','L3_W'])
#    else:
#        plt.subplot(2,1,2)
#        plt.hist(p.data.view(-1).numpy(), bins = 40, edgecolor='black', alpha = 0.5, normed = True)
#        plt.legend(['L1_b','L2_b','L3_b'])
#    
#
#plt.show()
#
#plist = list(net.parameters())
#
#plt.hist(plist[0:2])


"""
    Accuracy of fitted model
"""



# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
    images = images.cuda()
    labels = labels.cuda()
    outputs = net(images)
    _, predicted = torch.max(outputs, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))