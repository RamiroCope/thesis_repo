"""
    Setup working environment
"""

import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable
from Optimizers.noisyAdam import noisyAdam
from Models.BBB_t_linear import BNN as BNN_t
from Models.BBB_linear import BNN
from torch.distributions import Normal
import scipy.stats as stats
from torch import optim
import torch.distributions as distributions


"""
    Simulate targets
"""

### Design matrix

# number of observations
num_obs = 1000
# number of weights
num_weights = 1000

X_mu = 2.0
X_std = 1.0

X_dist = distributions.Normal(X_mu, X_std)

X_train = X_dist.sample((num_obs, num_weights))
X_test = X_dist.sample((num_obs, num_weights))


# Noise
eps_stdv = 1.0
d_eps = distributions.Normal(0.0, eps_stdv)
eps = d_eps.sample((num_obs,1))

# Weights
w_mu = 0.0
w_std = 2

alpha = 1.0
beta = 1.0

#p_lambda = distributions.Gamma(alpha, beta)
#lambdas = p_lambda.sample((1,num_weights))
#p_w = distributions.Normal(w_mu, 1.0/lambdas)
df_true = 1.5
scale_true = 1.0

p_w = distributions.StudentT(df = df_true, scale = scale_true)


#w_sample = p_w.sample((1, num_weights))
w_zeros = torch.zeros((1,990))
w_vals = torch.Tensor(1,10).uniform_(1,2)*30
w_sample = torch.cat((w_zeros.t(),w_vals.t())).t()

# Generate targets
y_train = F.linear(X_train, w_sample) + eps
y_test = F.linear(X_test, w_sample) + eps


"""
    Linear regression in a neural network style class
"""

linear_bayes_t = BNN_t(input_size = num_weights, hidden_sizes = [],
                       prior_prec = 1.0, prec_init=500, df_prior = 3.0, df_init = 3.0)

linear_bayes   = BNN(input_size = num_weights, hidden_sizes = [],
                       prior_prec = 1.0, prec_init=500)


# loss
criterion = nn.MSELoss()
#    criterion = nn.BCELoss()

# N as in pseudo code for noisy Adam is the total number of observations
N = X_train.shape[0]

## optimizer
#optimizer_bayes = noisyAdam(linear_bayes.parameters(), num_examples = N,
#                          gamma_ex = 0,
#                          lr = 1e-3,
#                          prior_var = prior_var,
#                          #df = df,
#                          betas = (0.9, 0.999))

#optimizer = optim.SGD(linear.parameters(), 1e-2)    
optimizer = optim.Adam(linear_bayes.parameters(), lr = 1e-3)
optimizer_t = optim.Adam(linear_bayes_t.parameters(), lr = 1e-2)
"""
    Train linear regression
"""

epochs = 20000

train_loss = []
train_loss_t = []
test_loss = []
test_loss_t = []
updates = []
    
for epoch in range(epochs):

    running_loss_bayes_t = 0.0
    running_loss_bayes = 0.0

    mus = []
    sigmas = []
    fs = []
    weights = []
    
    inputs  = X_train
    targets = y_train.squeeze()
    
#    for i, p in enumerate(linear.parameters()):
#        mu, sigma, f = params_dict["mu{0}".format(i)],\
#                        params_dict["sigma{0}".format(i)],\
#                        params_dict["f{0}".format(i)]
#                    
#        mus += [mu.view(-1).numpy()]
#        sigmas += [sigma.view(-1).numpy()]
#        fs += [f.view(-1).numpy()]
#        weights += [p.data.view(-1).numpy()]
        
    
    
    outputs_bayes = linear_bayes(inputs)
    outputs_bayes_t = linear_bayes_t(inputs)
#    outputs = linear(inputs)
    
    optimizer.zero_grad()
    optimizer_t.zero_grad()
#    optimizer_bayes.zero_grad()
    
    loss_bayes = criterion(outputs_bayes, targets)
    loss_bayes_t = criterion(outputs_bayes_t, targets)
#    loss = criterion(outputs, targets)
    
    kl_t = linear_bayes_t.kl_divergence()
    kl = linear_bayes.kl_divergence()
    running_loss_bayes_t += loss_bayes_t.item()
    running_loss_bayes += loss_bayes.item()
    
    if epoch % 100 == 99:
        mu_t, sigma, df, weight = linear_bayes_t.return_params()
        mu, sigma, weight = linear_bayes.return_params()
#        plt.hist([mu_t[0].squeeze(), mu[0].squeeze(), w_sample.squeeze().numpy()], edgecolor='black', normed = True)
#        plt.legend(['T dist MAP', 'MAP', 'True weights'])
#        plt.show()
        print("=====> Epoch %d " % (epoch+1))
        print("=====> Train loss T dist: %.2f " % (running_loss_bayes_t/100))
        print("=====> Train loss       : %.2f " % (running_loss_bayes/100))
        running_loss_bayes_t = 0.0
        running_loss_bayes = 0.0
#        print("=====> Test loss: %.2f" % test_loss_bayes[epoch])
        
        
#    mus += [mu]
#    dfs += [df]
#    sigmas += [sigma]
    
#    weights += [weight]
#    weights_flat = [item for sublist in weights for item in sublist]

    
    
#    loss.backward()
    ELBO = loss_bayes + kl/N
    ELBO_t = loss_bayes_t + kl_t/N
    
    ELBO.backward()
    ELBO_t.backward()
    
#    optimizer_bayes.step()
    optimizer.step()
    optimizer_t.step()
    
#    running_loss += loss.item()
    
    
#        print('[%d] ====> train loss: %.3f' %
#                      (epoch + 1, running_loss))
    
    test_inputs = X_test
    test_targets = y_test.squeeze()
    
#    test_outputs = linear(test_inputs)
    test_outputs_bayes = linear_bayes(test_inputs)
    test_outputs_bayes_t = linear_bayes_t(test_inputs)
    
#    test_loss_val = criterion(test_outputs, test_targets)
    test_loss_val_bayes = criterion(test_outputs_bayes, test_targets)
    test_loss_val_bayes_t = criterion(test_outputs_bayes_t, test_targets)
    
#    train_loss += [loss.item()]
#    test_loss += [test_loss_val.item()]
    
    train_loss += [loss_bayes.item()]
    test_loss += [test_loss_val_bayes.item()]
    
    train_loss_t += [loss_bayes_t.item()]
    test_loss_t += [test_loss_val_bayes_t.item()]
    
    updates += [epoch]

tol = 0.25

p = list(linear_bayes_t.parameters())
weights = p[1].data.numpy()
weights_t_zeros = weights[abs(weights) < tol]
weights_t_large = weights[weights >= 30]


p = list(linear_bayes.parameters())
weights = p[0].data.numpy()
weights_zeros = weights[abs(weights) < tol]
weights_large = weights[weights >= 30]


print("t dist      - total number of weights smaller than %.1e      = %d" % ( tol, weights_t_zeros.shape[0] ))
print("normal dist - total number of weights smaller than %.1e      = %d" % ( tol, weights_zeros.shape[0] ))

print("t dist      - total number of weights larger than 30    = %d" % ( weights_t_large.shape[0] ))
print("normal dist - total number of weights larger than 30    = %d" % ( weights_large.shape[0] ))    

# Plot histograms of true weights, and MAP weights for T dist and normal dist
#
#plt.hist([mu_t[0], mu[0], w_sample.squeeze().numpy()], alpha = 0.5, bins = 30, edgecolor='black', normed = True)
#x = np.linspace(np.min(mu_t[0]),np.max(mu_t[0]),5000)
#quantiles_t = np.linspace(stats.t.ppf(0.0001, df = df_true, loc = 0.0, scale=scale_true), stats.t.ppf(0.9999, df = df_true, loc = 0.0, scale=scale_true), 5000)
#quantiles_norm = np.linspace(stats.norm.ppf(0.0001, loc = 0.0, scale=scale_true), stats.norm.ppf(0.9999, loc = 0.0, scale=scale_true), 5000)
##quantiles_inv = np.linspace(stats.invgamma.ppf(0.01, 0.5, 1), stats.invgamma.ppf(0.99, 0.5, 1), 5000)
#pdf_t = stats.t.pdf(quantiles_t, df=df_true, scale = scale_true, loc = 0.0)
#pdf_norm = stats.norm.pdf(quantiles_norm, scale = scale_true, loc = 0.0)
##pdf_t = stats.t.pdf(x, df=df)
##plt.plot(x,pdf_t)
#plt.plot(quantiles_t,pdf_t, '--', lw = 2)
#plt.plot(quantiles_norm,pdf_norm, '-.', lw = 2)
#plt.xlabel("weights")
#plt.title("histogram of all weights in BNN")
#plt.legend(['True T dist', 'Standard Normal', 'T dist trained weights', 'Gaussian dist trained weights', 'True weights'])    
#plt.show()

fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(5,5))
fig.tight_layout(h_pad = 5, w_pad = 5,pad=4)

# Plot histograms with theoretical T dist on top of each other
plt.hist(w_sample.squeeze()[w_sample.squeeze() > 10], alpha = 1.0, bins = 10, edgecolor='black', normed = True)
plt.hist(mu_t[0].squeeze()[mu_t[0].squeeze() > 10], alpha = 1.0, bins = 10, edgecolor='black', normed = True)
#quantiles_t = np.linspace(stats.t.ppf(0.001, df = df_true, loc = 0.0, scale=scale_true), stats.t.ppf(0.999, df = df_true, loc = 0.0, scale=scale_true), 5000)
#pdf_t = stats.t.pdf(quantiles_t, df=df_true, scale = scale_true, loc = 0.0)
#plt.plot(quantiles_t,pdf_t, '--', lw = 2)
plt.xlabel("weights", fontsize = 14)
plt.ylabel('Normalized count', fontsize = 14)
plt.title("student's t prior -VS- Gaussian prior", fontsize = 14)

plt.hist(mu[0].squeeze()[mu[0].squeeze() > 10], alpha = 0.5, bins = 2, edgecolor='black', normed = True)
#quantiles_t = np.linspace(stats.t.ppf(0.0001, df = df_true, loc = 0.0, scale=scale_true), stats.t.ppf(0.9999, df = df_true, loc = 0.0, scale=scale_true), 5000)
#pdf_t = stats.t.pdf(quantiles_t, df=df_true, scale = scale_true, loc = 0.0)
#quantiles_norm = np.linspace(stats.norm.ppf(0.0001, loc = 0.0, scale=scale_true), stats.norm.ppf(0.9999, loc = 0.0, scale=scale_true), 5000)
#pdf_norm = stats.norm.pdf(quantiles_norm, scale = scale_true, loc = 0.0)
#plt.plot(quantiles_norm,pdf_norm, '-.', lw = 2)
#plt.xlabel("weights")
#plt.title("histogram of all weights in BNN")
#plt.legend(['True T dist', 'Normal dist', 'T dist trained weights', 'Gaussian dist trained weights'])
plt.legend(['True weights', 't dist trained weights', 'Normal dist trained weights'])
plt.savefig('t_dist_exp_large_w.png')
plt.show()


# Plot only theoretical pdfs against each other

scale = 1.0

quantiles_t = np.linspace(stats.t.ppf(0.01, df = 1.0, loc = 0.0, scale=scale), stats.t.ppf(0.99, df = 1.0, loc = 0.0, scale=scale), 5000)
pdf_t = stats.t.pdf(quantiles_t, df=1.0, scale = scale, loc = 0.0)

quantiles_t_df5 = np.linspace(stats.t.ppf(0.01, df = 3.0, loc = 0.0, scale=scale), stats.t.ppf(0.99, df = 3.0, loc = 0.0, scale=scale), 5000)
pdf_t_df5 = stats.t.pdf(quantiles_t_df5, df=3.0, scale = scale, loc = 0.0)

quantiles_t_df10 = np.linspace(stats.t.ppf(0.001, df = 10.0, loc = 0.0, scale=scale), stats.t.ppf(0.999, df = 10.0, loc = 0.0, scale=scale), 5000)
pdf_t_df10 = stats.t.pdf(quantiles_t_df10, df=10.0, scale = scale, loc = 0.0)

quantiles_norm = np.linspace(stats.norm.ppf(0.0001, loc = 0.0, scale=scale), stats.norm.ppf(0.9999, loc = 0.0, scale=scale), 5000)
pdf_norm = stats.norm.pdf(quantiles_norm, scale = scale, loc = 0.0)

plt.figure(figsize = (800/96,600/96), dpi = 96)
plt.plot(quantiles_t,pdf_t, ':', lw = 2)
plt.plot(quantiles_t_df5, pdf_t_df5, '-.', lw = 2)
plt.plot(quantiles_t_df10, pdf_t_df10, '--', lw = 2)
plt.plot(quantiles_norm,pdf_norm, '-', lw = 2)
plt.legend(['t dist, df = 1', 't dist, df = 3', 't dist, df = 10', 'Standard Normal dist'])
plt.title("t distribution vs. standard normal distribution", fontsize = 14)
plt.xlabel("x", fontsize = 14)
plt.ylabel("P(x)", fontsize = 14)
#plt.savefig("fig_test.png", dpi = 96)
plt.show()



#    
#train_losses += [train_loss[-1]]
#test_losses += [test_loss[-1]]
#print("=====> Train loss: ", train_loss[-1])   
#print("=====> Test loss: ", test_loss[-1])
#
#train_losses_bayes += [train_loss_bayes[-1]]
#test_losses_bayes += [test_loss_bayes[-1]]
#print("=====> Train loss Bayes: ", train_loss_bayes[-1])   
#print("=====> Test loss Bayes: ", test_loss_bayes[-1])

 
#print("Estimated weights: ", np.squeeze(mus))
#print("True weights: ", w_sample)


#plt.hist(mus, bins = 10,edgecolor='black')
#plt.hist(w_sample, bins = 10,edgecolor='black')
#plt.hist(fs, bins = 40,edgecolor='black')
#plt.show()

#    print('=============== PLOTTING ===============')
#
#    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,5))
#    fig.tight_layout(h_pad = 5, w_pad = 5)
#    
#    plt.subplot(2,2,1)     
#    plt.plot(updates, train_loss)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("SGD training loss")
#    
#    
#    plt.subplot(2,2,2)
#    plt.plot(updates, test_loss)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("SGD test loss")
#    
#    
#    plt.subplot(2,2,3)     
#    plt.plot(updates, train_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("Noisy Adam training loss")
#    
#    
#    plt.subplot(2,2,4)
#    plt.plot(updates, test_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("Noisy Adam test loss")
#    
#    plt.show()
    
#    
#fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
#fig.tight_layout(h_pad = 5, w_pad = 5)
#
#plt.subplot(2,2,1)
##plt.plot(updates, train_loss) # plot something
#plt.hist([mus, w_sample.view(-1).numpy()], bins = 10, edgecolor='black', normed = True)
#plt.xlabel("mu")
#plt.title("mu histogram of variational posterior")
#plt.legend(['Estimated means','true weights'])
#
#plt.subplot(2,2,2)
#
#plt.hist(sigmas, bins = 10, edgecolor='black', normed = True)
#plt.xlabel("standard deviation")
#plt.title("standard deviation histogram of variational posterior")
#plt.legend(['Layer 1','Layer 2','Layer 3'])
#
#plt.subplot(2,2,3)
#

#
## update canvas immediately
#plt.show()
#
##    
##plt.plot(num_obs_list[0:], train_losses[0:])
##plt.plot(num_obs_list[0:], test_losses[0:])
##plt.plot(num_obs_list[0:], train_losses_bayes[0:], '-.')
##plt.plot(num_obs_list[0:], test_losses_bayes[0:], '-.')
##plt.plot(num_obs_list[0:], [eps_stdv**2]*len(num_obs_list[0:]), '--')
#
#plt.xlabel("Number of observations"), plt.ylabel("loss")
#plt.title("Noisy Adam train/test loss for increasing number of observations")
#plt.legend(["Train loss", "Test loss", "BNN - Train loss", "BNN - Test loss", "Noise level"])
#plt.show()