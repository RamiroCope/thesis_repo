import torch
from torch.optim import Optimizer

class Vprop(Optimizer):
    """Implements Vprop algorithm.

    Proposed by Mohammad Emtiyaz Khan et al.
    `<https://arxiv.org/pdf/1806.04854.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        num_examples (int): total size of the data set
        lr (float, optional): learning rate (default: 1e-2)
        alpha (float, optional): smoothing constant (default: 0.99)
        prior_var (float, optional): prior variance (default: 1)
        var_init (float, optional): initialization value for variance of
            variational posterior

    """
    def __init__(self,
                 params,
                 num_examples,
                 lr=1e-3,
                 alpha=0.99,
                 prior_var = 1,
                 var_init = 1):
        
        defaults = dict(lr=lr, prior_var=prior_var,
                        alpha=alpha, var_init=var_init)
        
        super(Vprop, self).__init__(params, defaults)
        
        self.precision = 1.0/prior_var
        self.posterior_params = {}
        self.num_examples = num_examples

    def step(self, closure=None, return_params = False):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
            return_params (bool, optional): if True, returns the parameters of
                the variational posterior
        """    
        
        pid = 0
        original_values = {}
        
        for group in self.param_groups:
            for j, p in enumerate(group['params']):              
                
                # store original weights
                original_values.setdefault(pid, p.detach().clone())
                
                # State initialization
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0                    
                    # s as given in the pseudo code
                    state['square_avg'] = torch.ones_like(p.data) * (1.0/group['var_init'] - self.precision) / self.num_examples
                    
                square_avg = state['square_avg']
                                    
                # noise term
                e = torch.normal(mean=torch.zeros_like(p.data), std=1.0)
                
                # Pseudo code line 1 - sample parameters
                p.data.addcdiv_(1.0, e, (self.num_examples*square_avg + self.precision).sqrt())
                
                if return_params:
                    sigma = 1.0/(self.num_examples*square_avg + self.precision).sqrt()
                    self.posterior_params["sample{0}".format(j)] = p.data
                    self.posterior_params["sigma{0}".format(j)] = sigma
                    
                pid = pid + 1
        
        loss = None
        if closure is not None:
            loss = closure()
            
        pid = 0
        
        for group in self.param_groups:
            for i, p in enumerate(group['params']):
                
                if p.grad is None:
                    continue
                
                # Restore original weights
                p.data = original_values[pid]
                
                # Pseudo code line 2 - get gradient
                grad = p.grad.data
                
                # get state
                state = self.state[p]
                
                square_avg = state['square_avg']
                alpha = group['alpha']
                
                state['step'] += 1
                
                # Pseudo code line 3 - update s
                square_avg.mul_(alpha).addcmul_(1 - alpha, grad, grad)
                
                num = grad + self.precision/self.num_examples*p.data
                denom = square_avg.sqrt() + self.precision/self.num_examples
                
                # Pseudo code line 4
                p.data.addcdiv_(-group['lr'], num, denom)
                
                pid = pid + 1
                
        return loss, self.posterior_params