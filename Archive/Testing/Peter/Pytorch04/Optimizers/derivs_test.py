import torch
import torch.distributions as distributions

data = torch.rand(10,10, requires_grad=True)

# normal dist
normal_dist = distributions.normal.Normal(0,1)

grad = torch.autograd.grad(normal_dist.log_prob(data).sum(), data, create_graph = True)[0]

grad2 = torch.autograd.grad(grad.sum(), data, create_graph = True)[0]

grad3 = torch.autograd.grad(grad2.sum(),data, create_graph = True)[0]

print("Data is ", data)
print("gradient through grad is ", grad)
print("second order gradient through grad is ", grad2)
print("third order gradient through grad is ", grad3)