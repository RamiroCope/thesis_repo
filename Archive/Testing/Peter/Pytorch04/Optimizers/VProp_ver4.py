import torch
from torch.autograd import Variable
from torch.optim import Optimizer

class VProp(Optimizer):
    def __init__(self, params,
                         lr=1e-2,
                         alpha=0.99,
                         eps=1e-8,
                         weight_decay=0,
                         momentum=0,
                         centered=False,
                         precision = 1):
        
        defaults = dict(lr=lr,
                        momentum=momentum,
                        alpha=alpha,
                        eps=eps,
                        centered=centered,
                        weight_decay=weight_decay)
        
        super(VProp, self).__init__(params, defaults)
        
        self.precision = precision
        self.posterior_params = {}          
        
    def __setstate__(self, state):
        super(VProp, self).__setstate__(state)
        
        for group in self.param_groups:
            group.setdefault('momentum', 0)
            group.setdefault('centered', False)
            
    def sample(self, training = True):
        for group in self.param_groups:
            for i, p in enumerate(group['params']):              
                
                if p.grad is None:
                    p.grad = Variable(torch.zeros(p.size()))
                
                grad = p.grad.data
                
                # State initialization
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    state['square_avg'] = torch.Tensor(p.size()).uniform_(0.25*self.precision, 0.5*self.precision)#torch.zeros_like(p.data)
                    state['mu'] = torch.Tensor(p.size()).uniform_(-0.01,0.01)
                
                square_avg = state['square_avg']
                mu = state['mu']
                
                if not training:
                    
                    p.data = mu
                    
                    self.posterior_params["mu{0}".format(i)] = mu
                    self.posterior_params["sigma{0}".format(i)] = 1.0/(square_avg + self.precision)
                    self.posterior_params["grad{0}".format(i)] = grad
                    
                    return self.posterior_params
                
                #   PSEUDO CODE LINE 1   #
                #Sample weights = mu + eps/(sqrt(square_avg + precision))
                eps = torch.randn(p.size())
                p.data = mu + (eps /(square_avg+self.precision).sqrt())
                
                self.posterior_params["mu{0}".format(i)] = mu
                self.posterior_params["sigma{0}".format(i)] = 1.0/(square_avg + self.precision)
                self.posterior_params["grad{0}".format(i)] = grad
                
        return self.posterior_params
                
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for i,p in enumerate(group['params']):
                if p.grad is None:
                    continue
                #    PSEUDOCODE LINE 2    #
                #      SET GRADIENT       #
                grad = p.grad.data
                state = self.state[p]

                # State initialization
                square_avg = state['square_avg']; 
                mu = state['mu']
                alpha = group['alpha']
                
                state['step'] += 1
                
                #    PSEUDOCODE LINE 3    #
                #        UPDATE S         #
                square_avg.mul_(alpha).addcmul_(1 - alpha, grad, grad)
                
                num = grad.add(self.precision*mu)
                denom = square_avg + self.precision
                
                #   PSEUDOCODE LINE 4     #
                mu.addcdiv_(-group['lr'], num, denom)
                

#                self.posterior_params["mu{0}".format(i)] = mu
#                self.posterior_params["sigma{0}".format(i)] = 1.0/(square_avg+self.precision)
#                self.posterior_params["grad{0}".format(i)] = grad

        return self.posterior_params