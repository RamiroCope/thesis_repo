import torch
from torch.optim import Optimizer
import numpy as np
import torch.distributions as distributions
import torch.autograd.grad as gradient

class noisyAdam(Optimizer):
    """Implements Noisy Adam algorithm.

    Proposed by Guodong Zhang et al.
    `<https://arxiv.org/pdf/1712.02390.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        num_examples (integer): number of training examples
        lr (float, optional): learning rate (default: 1e-3)
        betas (float, optinal): decay rates for mean and Fisher updates
            (default: 0.9, 0.999)
        kl_weight (float, optional): weight term added to KL divergence (default: 1)
        prior_var (float, optional): variance of prior distribution (default: 1)
        gamma_ex (float, optional): extrinsic damping term (default: 0)
        return_params (bool, optional): return posterior parameters? (default: False)
    """
    
    def __init__(self,
                 params,
                 num_examples,
                 lr=1e-3,
                 betas=(0.9, 0.999),
                 kl_weight = 1,
                 prior_alpha = 5,
                 prior_beta = 5,
                 gamma_ex = 0):
        
        defaults = dict(lr=lr, betas=betas, kl_weight = kl_weight,
                        gamma_ex = gamma_ex, prior_alpha = prior_alpha,
                        prior_beta = prior_beta, num_examples = num_examples)
        
        super(noisyAdam, self).__init__(params, defaults)
        
        # to store the posterior parameters, mu and sigma
        # used only for plotting KL divergence
        self.posterior_params = {}
        # lambda in pseudo code - the KL weighting term
        self.kl_weight = kl_weight
        # total number of training examples (e.g. for MNIST, num_examples = 60.000)
        self.num_examples = num_examples
        

    def sample(self, training = True, return_params = False):
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    # m in pseudo code
                    state['exp_avg'] = torch.zeros_like(p)
                    # Exponential moving average of squared gradient values
                    # f in pseudo code
                    state['exp_avg_sq'] = torch.zeros_like(p)
                    # value initialization for mu parameter
                    init_val = 1.0/np.sqrt(p.size(0))
                    # mu - the mean of the variational posterior
                    state['mu'] = torch.Tensor(p.size()).uniform_(-init_val, init_val)
                    #state['mu'] = torch.Tensor(p.size()).uniform_(-0.1, 0.1)
                    
                    state['alpha'] = torch.ones(p.size())
                    state['beta'] = torch.ones(p.size())
                
                # mean parameter of variational posterior
                mu = state['mu']
                
                alpha = state['alpha']
                beta  = state['beta']
                
                
                
                
                gamma_prior = distributions.gamma.Gamma(group['prior_alpha'], group['prior_beta'])
                gaussian_prior = distributions.normal.Normal(0, gamma_prior.sample(p.size()))
                
                gamma_posterior = distributions.gamma.Gamma(alpha,beta)                
                gamma_posterior = gamma_posterior.rsample()
                gaussian_posterior = distributions.normal.Normal(0, gamma_posterior)
                
                
                
                # when testing, weights are the mean of the variational posterior
                if not training:
                    p.data = mu
                    return
                
                # Fisher approximation, i.e. variance
                f = state['exp_avg_sq']
        
                # noise term
                e = torch.randn(p.size())
                
                # compute the sum of the log probs
                log_pw = gaussian_prior.log_prob(p.data).sum()
                
                #compute first and second order derivatives
                dw_log_pw = gradient(log_pw, p, create_graph = True)[0]
                dwdw_log_pw = gradient(dw_log_pw.sum(), p, create_graph = True)[0]
                
                # damped precision                
                f_damped = 1/(group['num_examples']/group['kl_weight']*f - dwdw_log_pw.data)
                
                # pseudo code line 1
                # sample from variational posterior
                p.data = mu.add(e*f_damped.sqrt())
                
                if return_params:
                    
                    # standard deviation of variational posterior
                    self.posterior_params["mu{0}".format(j)] = mu
                    self.posterior_params["sigma{0}".format(j)] = f_damped.sqrt()
                    self.posterior_params["f{0}".format(j)] = f
                
        if return_params:
            return self.posterior_params
        
        return

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue

                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, \
                                       please consider SparseAdam instead')

                state = self.state[p]

                # pseduo code line 2
                # get modified gradient
                v = grad - self.gamma_in*p.data
                
                # get m, f, beta_1, beta_2 as in pseudo code
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1, beta2 = group['betas']
                mu = state['mu']

                # update step
                state['step'] += 1

                # pseudo code line 3: Updated biased first moment estimate
                # decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(1 - beta1, v)
                
                # pseudo code line 4: Update biased second raw moment estimate
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)

                # pseudo code line 5
                m_tilde = exp_avg/(1 - beta1 ** state['step'])
                # pseudo code line 6
                m_hat = m_tilde/(exp_avg_sq + self.gamma)
                
                # pseudo code line 7
                # update parameter
                mu.add_(-group['lr']*m_hat)

        return loss
    
    
