import torch
from torch.optim import Optimizer

class noisyAdam(Optimizer):
    
    """Implements Noisy Adam algorithm.

    Proposed by Guodong Zhang et al.
    `<https://arxiv.org/pdf/1712.02390.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        num_examples (integer): total size of data set being trained
        lr (float, optional): learning rate (default: 1e-3)
        betas (float, optinal): decay rates for mean and Fisher updates
            (default: 0.9, 0.999)
        kl_weight (float, optional): weight term added to KL divergence (default: 1)
        prior_var (float, optional): variance of prior distribution (default: 1)
        gamma_ex (float, optional): extrinsic damping term (default: 0)
        var_init (float, optional): initialization value for posterior variance (default: 1)
    """
    
    def __init__(self,
                 params,
                 num_examples,
                 lr=1e-3,
                 betas=(0.9, 0.999),
                 kl_weight = 1,
                 prior_var = 1,
                 gamma_ex = 0,
                 var_init = 1):
        
        defaults = dict(lr=lr, betas=betas, kl_weight = kl_weight,
                        prior_var = prior_var, gamma_ex = gamma_ex,
                        num_examples = num_examples, var_init=var_init)
        
        super(noisyAdam, self).__init__(params, defaults)
        
        # to store the posterior parameters, mu and sigma
        # used only for plotting KL divergence
        self.posterior_params = {}
        # lambda in pseudo code - the KL weighting term
        self.kl_weight = kl_weight
        # total number of training examples (e.g. for MNIST, num_examples = 60.000)
        self.num_examples = num_examples
        # intrinsic damping term
        self.gamma_in = kl_weight/(num_examples*prior_var)
        # adds the two damping terms. gamma_ex may be zero.                
        self.gamma = self.gamma_in + gamma_ex
        

    def step(self, closure=None, return_params = False):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        
        pid = 0
        original_values = {}
        
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                
                # store original weights
                original_values.setdefault(pid, p.detach().clone())
                
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    # m in pseudo code
                    state['exp_avg'] = torch.zeros_like(p)
                    # Exponential moving average of squared gradient values
                    # f in pseudo code
                    state['exp_avg_sq'] = torch.ones_like(p.data) * (1.0/group['var_init'] - 1.0/group['prior_var']) / self.num_examples
                
                # noise term
                e = torch.normal(mean=torch.zeros_like(p.data), std=1.0)
                
                # Fisher approximation, i.e. variance
                f = state['exp_avg_sq']

                # damped precision                
                f_damped = 1.0/(group['num_examples']/group['kl_weight']*f + 1.0/group['prior_var'])
                
                # pseudo code line 1
                # sample from variational posterior
                p.data.add_(e*f_damped.sqrt())
                
                pid += 1
                
                if return_params:
                    
                    # standard deviation of variational posterior
                    self.posterior_params["sample{0}".format(j)] = p.data
                    self.posterior_params["sigma{0}".format(j)] = f_damped.sqrt()
        
        loss = None
        if closure is not None:
            loss = closure()

        pid = 0

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue

                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, \
                                       please consider SparseAdam instead')
                p.data = original_values[pid]

                state = self.state[p]

                # pseduo code line 2 - Get modified gradient
                # Note the difference in sign from pseudo code.
                # This is because we minimize instead of maximize ELBO.
                v = grad + self.gamma_in*p.data
                
                # get m, f, beta_1, beta_2 as in pseudo code
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                beta1, beta2 = group['betas']

                # update step
                state['step'] += 1

                # pseudo code line 3: Updated biased first moment estimate
                # decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(1 - beta1, v)
                
                # pseudo code line 4: Update biased second raw moment estimate
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)

                # pseudo code line 5 & 6
                bias_correction1 = 1 - beta1 ** state['step']
                bias_correction2 = 1 - beta2 ** state['step']
                
                m_hat = exp_avg.div(bias_correction1)
                f_hat = exp_avg_sq.div(bias_correction2)

                # pseudo code line 7
                # update parameter
                p.data.addcdiv_(-group['lr'], m_hat, f_hat.sqrt() + self.gamma)
                
                pid += 1

        return loss, self.posterior_params