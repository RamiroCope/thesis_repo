import torch
import numpy as np
from torch.optim import Optimizer

"""
    noisy KFAC optimizer
"""

class noisyKFAC(Optimizer):

    """Implements Noisy KFAC algorithm.

    Proposed by Guodong Zhang et al.
    `<https://arxiv.org/pdf/1712.02390.pdf>`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        layers (iterable): iterable of layers in the neural network (net.module())
        num_examples (integer): number of training examples
        lr (float, optional): learning rate (default: 1e-3)
        beta (float, optinal): decay rate for fisher approximation update
            (default: 0.99)
        kl_weight (float, optional): weight term added to KL divergence (default: 1)
        prior_var (float, optional): variance of prior distribution (default: 1)
        t_inv (int, optional): how often inverse of fisher approximation is calculated
        t_stats (int, optional): how often the fisher approximation is updated.
    """
    
    
    def __init__(self,
                 params,
                 layers,
                 num_examples,
                 lr=1e-5,
                 beta=0.99,
                 kl_weight = 1,
                 prior_var = 1,
                 t_inv = 1,
                 t_stats = 10):
        
        defaults = dict(lr=lr, beta=beta, kl_weight = kl_weight,
                        prior_var = prior_var, num_examples = num_examples)
        
        super(noisyKFAC, self).__init__(params, defaults)
        
        self.posterior_params = {}
        # lambda in pseudo code - the KL weighting term
        self.kl_weight = kl_weight
        # total number of training examples (e.g. for MNIST, num_examples = 60.000)
        self.num_examples = num_examples
        # intrinsic damping term
        self.gamma_in = kl_weight/(num_examples*prior_var)
        # prior variance
        self.prior_var = prior_var
        # t_stats and t_inv for update schemes
        self.t_stats = t_stats
        self.t_inv = t_inv
        
        # step counter
        self.steps = 0
        
        # dicts for the a and g updates used in eq. (12)
        self.aa_mappings = {}
        self.gg_mappings = {}
        
        # initialize hooks
        for i,layer in enumerate(layers):
            # i == 0 contains the parent class FNN
            if i == 0:
                continue
            layer.register_forward_pre_hook(self.forward_hook_trigger)
            layer.register_backward_hook(self.backward_hook_trigger)
        
    # Get a^T * a as in pseudo code eq. (12) and eq. (3)
    def forward_hook_trigger(self, layer, i):
        if self.steps % self.t_stats == 0:
            aa = torch.mm(i[0].data.t(), i[0].data) / i[0].size(1)
            for p in layer.parameters():
                self.aa_mappings[p] = aa
        return
    
    # Get g^T * g as in pseudo code eq. (12) and eq. (3)
    def backward_hook_trigger(self, layer, grad_in, grad_out):
        if self.steps % self.t_stats == 0:
            gg = torch.mm(grad_out[0].data.t(), grad_out[0].data) / grad_out[0].size(1)
            for p in layer.parameters():
                self.gg_mappings[p] = gg
        return

    def sample(self, training = True, return_params = False):
        """ Samples from the variational posterior
            
        Arguments:
            training (bool, optional): when testing, set to false to return the MAP
            return_params (bool, optional): if True, returns posterior parameters
        """
        
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    
                    # only weight matrices are supported at the moment.
                    if len(p.size()) == 2:
                        stdv = 1.0/np.sqrt(p.size(1))
                        state['mu'] = torch.Tensor(p.size()).uniform_(-stdv, stdv)

                        state['A'] = torch.ones(p.data.size(1), p.data.size(1))
                        state['S'] = torch.ones(p.data.size(0), p.data.size(0))
                        
                        state['A_inv'] = torch.ones(p.data.size(1), p.data.size(1))
                        state['S_inv'] = torch.ones(p.data.size(0), p.data.size(0))
                        
                        state['A_inv_sqrt'] = torch.ones(p.data.size(1), p.data.size(1))
                        state['S_inv_sqrt'] = torch.ones(p.data.size(0), p.data.size(0))
                    else:
                        raise Exception("Bias not yet supported with KFAC.")
                
                # mean parameter
                mu = state['mu']
                
                if not training:
                    p.data = mu
                    continue

                # Update covariance matrices
                if state['step'] % self.t_stats == 0:
                    
                    A_inv = state['A_inv']
                    S_inv = state['S_inv']
                    
                    # compute the square root of A
                    A_eigen_val, A_eigen_vec = A_inv.symeig(eigenvectors = True)
                    
                    A_diag = torch.diag(A_eigen_val.sign()*A_eigen_val.abs().sqrt())
                    
                    A_sqrt = torch.mm(A_eigen_vec, A_diag)                    
                    A_sqrt = torch.mm(A_sqrt, A_eigen_vec.t())

                    # compute the square root of S_inv
                    S_eigen_val, S_eigen_vec = S_inv.symeig(eigenvectors = True)
                    
                    S_diag = torch.diag(S_eigen_val.sign()*S_eigen_val.abs().sqrt())
                    
                    S_sqrt = torch.mm(S_eigen_vec, S_diag)
                    S_sqrt = torch.mm(S_sqrt, S_eigen_vec.t())
                    
                    state['A_inv_sqrt'] = A_sqrt#_inv
                    state['S_inv_sqrt'] = S_sqrt#_inv
    
                # scalar multiplied on variance
                F_fact = np.sqrt(self.kl_weight/self.num_examples)
                
                # noise term
                e = torch.randn(p.size())
                
                # compute standard deviation
                Se = torch.mm(state['S_inv_sqrt'], e)
                SeA = torch.mm(Se, F_fact*state['A_inv_sqrt'])
                
                # do actual sampling
                p.data = mu + SeA
                
                if return_params:
                    
                    self.posterior_params["mu{0}".format(j)] = mu
                    self.posterior_params["A_inv_sqrt{0}".format(j)] = state['A_inv_sqrt']
                    self.posterior_params["S_inv_sqrt{0}".format(j)] = state['S_inv_sqrt']
                    
            if return_params:
                return self.posterior_params
        return

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                if p.grad is None:
                    continue
                grad = p.grad.data

                state = self.state[p]

                # update step
                state['step'] += 1
                # self.step is used for hooks
                self.steps = state['step']
                
                # get A, S, mu, beta and cov_fac as in pseudo code
                mu = state['mu']
                A = state['A']
                S = state['S']
                beta = group['beta']
                cov_fac = np.sqrt(self.gamma_in)

                # the beta value is reversed compared to pseudo code
                # to stick with the other implementations beta values
                if state['step'] % self.t_stats == 0:
                    A.mul_(beta).add_((1 - beta)*self.aa_mappings[p])
                    S.mul_(beta).add_((1 - beta)*self.gg_mappings[p])                
                
                # Compute inverses of covariance matrices
                if state['step'] % self.t_inv == 0:
                    state['A_inv'] = (A + cov_fac*torch.eye(state['A'].size(0))).inverse()
                    state['S_inv'] = (S + cov_fac*torch.eye(state['S'].size(0))).inverse()

                # modify gradient
                V = grad + self.gamma_in*state['mu']#p.data
                
                # compute step update
                SVA = torch.mm(torch.mm(state['S_inv'], V), state['A_inv'])
                # update mu
                mu.add_(-group['lr']*SVA)

        return loss