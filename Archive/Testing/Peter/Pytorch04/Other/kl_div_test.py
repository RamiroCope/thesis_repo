import torch
import torch.distributions as dist
import math

def kl_gaussian(p_mu, p_sigma, q_mu, q_sigma):
    var_ratio = (p_sigma / q_sigma)**2
    t1 = ((p_mu - q_mu) / q_sigma)**2
    return 0.5 * var_ratio + t1 - 1 - math.log(var_ratio)

def kl_gaussian_univ(p_mu, p_sigma, q_mu, q_sigma):
    var_ratio = (q_sigma/p_sigma)**2
    log_var_ratio = math.log((p_sigma/q_sigma)**2)
    return 0.5 * ( log_var_ratio + var_ratio + ((q_mu-p_mu)/p_sigma)**2 - 1 )

sample = torch.randn(10)

p_mu = torch.tensor([[0.0,0.0],[0.0,0.0]])
p_sigma = torch.tensor([[1.0,1.0],[1.0,1.0]])

q_mu = torch.tensor([[0.6, 0.5],[0.6, 0.5]])
q_sigma = torch.tensor([[0.2, 0.3],[0.2, 0.3]])

p = dist.Gamma(p_mu, p_sigma)
q = dist.Gamma(q_mu, q_sigma)

# manual KL computation

KL_man = kl_gaussian(q_mu, q_sigma, p_mu, p_sigma)
KL_man_univ = kl_gaussian_univ(p_mu, p_sigma, q_mu, q_sigma)

KL_def = dist.kl_divergence(q, p)

print("KL_man_univ = ", KL_man_univ)
print("KL_man = ", KL_man)
print("KL_def = ", KL_def.item())
print("Difference between two methods is: %.3f" % (KL_man_univ - KL_def.item()) )