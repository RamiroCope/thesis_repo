import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-1,0.5,1000)

def relu(x):
    y = x.copy()
    y[y < 0] = 0
    return y


def prelu(x,slope):
    y = x.copy()
    y[y < 0] = slope*y[y < 0]
    return y


y = relu(x)
yp1 = prelu(x,0.1)
yp2 = prelu(x,0.25)
yp3 = prelu(x,0.4)


plt.plot(x,y)
plt.plot(x,yp1, linestyle = ':')
plt.plot(x,yp2, linestyle = '--')
plt.plot(x,yp3, linestyle = '-.')
plt.xlabel("x")
plt.ylabel("f(x)")
plt.legend(["Relu", "PRelu a = 0.1", "PRelu a = 0.25", "PRelu a = 0.4"])

plt.show()