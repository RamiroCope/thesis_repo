import numpy as np
import matplotlib.pyplot as plt
import random
random.seed(2018)

true_preds = np.array([0.1,0.2,0.3,0.4,0.5,0.6,0.7])

true_ys = np.array([32,32,16,8,4,2,1])

idxs = np.argsort(true_preds)

np.random.shuffle(true_preds)

np.random.shuffle(true_ys)

preds = np.sort(true_preds)
preds_rev = preds[::-1]

y = np.array([32,32,16,8,4,2,1])

idx = np.argsort(preds)

y_sort = y[idx]

plt.plot(preds,np.cumsum(y_sort)[::-1])