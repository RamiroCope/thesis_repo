import os, inspect, sys
script_directory = os.path.dirname(os.path.abspath("test_location.py"))
print("this script is located at ", script_directory)

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

print("the parent directory is ", parentdir)