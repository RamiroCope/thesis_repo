from sklearn.datasets import load_boston        
import torch
from sklearn.model_selection import train_test_split
import numpy as np

class boston(object):  
     
    def __init__(self, train = True, test_size = 0.5):

        self.train = train
        
        X, y = load_boston(return_X_y = True)
        print('Number of features: ', X.shape[1])
        print('Number of instances: ', X.shape[0])
        
        # Standardize data
        X -= np.mean(X, axis=0)
        X /= np.std(X, axis=0)
        
        
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = test_size, random_state = 0)
        
        if train:
        
            # Convert to proper tensors
            self.train_data = torch.from_numpy(X_train).float()
            self.train_labels = torch.from_numpy(y_train).float()

        else:

            self.test_data = torch.from_numpy(X_test).float()
            self.test_labels = torch.from_numpy(y_test).float()
            
            
    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        if self.train:
            data, target = self.train_data[index], self.train_labels[index]
        else:
            data, target = self.test_data[index], self.test_labels[index]
            
        return data, target

            
    def __len__(self):
        if self.train:
            return len(self.train_data)
        else:
            return len(self.test_data)