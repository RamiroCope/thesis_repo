import torch

# generate some data
data = torch.autograd.Variable(torch.Tensor(2,2).uniform_(-0.1,0.1), requires_grad=True)

# define density - assume zero mean
prior_std = 1

log_gauss = -data.norm(2)**2/(2*prior_std**2)

# compute gradients
grad = torch.autograd.grad(log_gauss,data, create_graph = True)[0]

grad2 = torch.autograd.grad(grad.sum(),data, create_graph = True)[0]

grad3 = torch.autograd.grad(grad2.sum(),data, create_graph = True)[0]

print("Data is ", data)
print("gradient through grad is ", grad)
print("second order gradient through grad is ", grad2)
print("third order gradient through grad is ", grad3)