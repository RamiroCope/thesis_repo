import torch
from scipy.linalg import sqrtm as mat_sqrt
import matplotlib.pyplot as plt


x = torch.ones(100,100)
fact = 1/100
x += fact*torch.eye(x.size(0))

x_inv = x.inverse()

x_eval, x_evec = x.symeig(eigenvectors = True)

#diag = torch.diag(x_eval.sign()*(x_eval + 1e-7).abs().sqrt())
diag = torch.diag(x_eval.sqrt())

x_sqrt = torch.mm(x_evec, diag)

x_sqrt = torch.mm(x_sqrt, x_evec.t())

x_sqrt_inv = x_sqrt.inverse()

x_inv_eval, x_inv_evec = x_sqrt_inv.symeig(eigenvectors = True)



x_sqrt_alt = mat_sqrt(x_inv)

x_sqrt_alt = torch.from_numpy(x_sqrt_alt.astype('float32'))

plt.hist(x_sqrt.view(-1))
plt.hist(x_sqrt_alt.view(-1))

torch.mean(x_sqrt)
torch.mean(x_sqrt_alt)



x_eval, x_evec = x_inv.symeig(eigenvectors = True)

D = torch.diag(x_eval.sqrt())

D_inv = D.inverse()


x_sqrt_v3 = torch.mm( x_evec, D_inv )
x_sqrt_v3 = torch.mm( D_inv, x_evec.t())



x_sqrt_v3 = mat_sqrt(x)
x_sqrt_v3 = torch.from_numpy(x_sqrt_v3.astype('float32'))

x_sqrt_v3 = x_sqrt_v3.inverse()
