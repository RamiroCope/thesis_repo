"""
    Setup working environment
"""

import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import scipy.stats as stats

from torch.autograd import Variable
from torch.distributions import Normal
from torch.utils.data import DataLoader
from torch import optim

# optimizer
from Testing.Peter.Optimizers.noisyAdam_mixture_normals import noisyAdam

# dataset
from Main.Datasets.Regression.wine import wine as wine_data

#import matplotlib
#matplotlib.use('TkAgg')
#from torch.nn.parameter import Parameter
#from torch.optim import Optimizer
#from torch.distributions import Normal
#from IPython.display import Image, display, clear_output
#import numpy as np


"""
    Load data
"""

# train data
trainset = wine_data()

batch_size = 10

y_train_mean, y_train_std = trainset.get_target_normalization_constants() 
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = wine_data(train = False)

# do batching
test_batch_size = testset.test_data.shape[0]

testLoader = DataLoader(testset, batch_size=test_batch_size, shuffle=False)

# number of features in dataset
num_features = trainset.train_data.shape[1]

"""
    Define the neural net
"""

""" Two Layer Network """
class FNN1(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN1, self).__init__()
        self.NN1 = nn.Linear(num_features,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,1)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x

""" One Layer Network as in Noisy Natural Gradient Paper by Zhang et al (2018)"""
class FNN2(nn.Module):
    
    def __init__(self,H1):
        super(FNN2, self).__init__()
        self.NN1 = nn.Linear(num_features,H1)
        self.NN2 = nn.Linear(H1,1)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x    
    
# initialize network    
net = FNN2(50)

rmses = []

for k in range(1):

    net = FNN2(50)
    
    
    """
        Define loss and optimizer
    """
    
    
    # loss
    criterion = nn.MSELoss()
    
    # N as in pseudo code for noisy Adam is the total number of observations
    N = trainset.train_data.shape[0]
    
    # 0 < prior_var < 1   => smaller sample variance
    # 1 < prior_var < inf => larger sample variance
    prior_var = 10
    
    # optimizer
    optimizer = noisyAdam(net.parameters(), num_examples = N,
                              gamma_ex = 1e0,
                              lr = 1e-1,
                              alpha=6,
                              beta=6,
                              betas = (0.9, 0.999))
    
    # setup adaptive learning rate
    #scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[5,15], gamma=0.1)
    scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.9)
    
    
    """
        Training the network
    """
    
    max_epochs = 200
    
    num_batches = trainset.train_data.shape[0]/batch_size
    
    p_dist = Normal(0, 1.0/np.sqrt(prior_var))
    #p_dist = torch.distributions
    
    # indices for accessing weight matrices of the different layers
    indices = [0,2]#[0,2,4]
    
    train_loss = []
    test_loss = []
    KL_div = []
    ELBO = []
    updates = []
    
    
    for epoch in range(max_epochs):
        #print('============> Learning rate is: ',scheduler.get_lr())
        running_loss = 0.0
        #enumerate basically indexes the iterations, so i keeps track
        #of the index. The ",0" part is just ensuring we start in zero every
        #time the for loop runs
        for batch, data in enumerate(trainLoader, 0):
            
            mus = []
            sigmas = []
            weights = []
            fs = []
            
            inputs, labels = data
            inputs, labels = Variable(inputs), Variable(labels)
            
            optimizer.zero_grad()
            
            params_dict = optimizer.sample(return_params=True)
            
            outputs = net(inputs)
            
            
            # extract diagnostics
            lqw, lpw = 0.0, 0.0
    
            for i, p in enumerate(net.parameters()):
                mu, sigma, f = params_dict["mu{0}".format(i)],\
                                params_dict["sigma{0}".format(i)],\
                                params_dict["f{0}".format(i)]
                            
                mus += [mu.view(-1).numpy()]
                sigmas += [sigma.view(-1).numpy()]
                fs += [f.view(-1).numpy()]
                weights += [p.data.view(-1).numpy()]            
            
                # KL Divergence terms    
                q_dist = Normal(mu, sigma)
                lqw += q_dist.log_prob(p.data).sum()
                lpw += p_dist.log_prob(p.data).sum()
        
    #        mus = np.concatenate(mus)
    #        sigmas = np.concatenate(sigmas)
    #        weights = np.concatenate(weights)
    #        fs = np.concatenate(fs)
        
            KL_loss = (lqw - lpw)/num_batches
            
            LL_loss = criterion(outputs, labels)
        
            loss = LL_loss + KL_loss
            
            
            #Initialized parameters' gradients are None until you do
            #loss.backward() and start computing gradients
            loss.backward()
            optimizer.step()
            
            # Print statistics
            running_loss += LL_loss.data[0]
            if batch % 20 == 19:
                print('[%d, %5d] ====> train loss: %.3f' %
                      (epoch + 1, batch + 1, running_loss/20))
                updates += [batch + (epoch*num_batches)]
                train_loss += [running_loss/20]
                KL_div += [KL_loss]
                ELBO += [loss.data[0]]
                
                for data in testLoader:
                    inputs, labels = data
                    optimizer.sample(training = False)
                    outputs = net(Variable(inputs))
                    test_loss_val = criterion(outputs, Variable(labels))
                    test_loss += [test_loss_val.data[0]]
                    
                print('[%d, %5d] ====> test loss: %.3f' %
                      (epoch + 1, batch + 1, test_loss_val.data[0]))            
                
                fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
                fig.tight_layout(h_pad = 5, w_pad = 5)
        
                plt.subplot(2,2,1)
                #plt.plot(updates, train_loss) # plot something
                plt.hist([mus[i] for i in indices], bins = 30, edgecolor='black', normed = True)
                plt.xlabel("mu")
                plt.title("mu histogram of variational posterior")
                plt.legend(['Layer 1','Layer 2','Layer 3'])
                
                plt.subplot(2,2,2)
                
                plt.hist([sigmas[i] for i in indices], bins = 30, edgecolor='black', normed = True)
                plt.xlabel("standard deviation")
                plt.title("standard deviation histogram of variational posterior")
                plt.legend(['Layer 1','Layer 2','Layer 3'])
                
                plt.subplot(2,2,3)
                
                plt.hist([weights[i] for i in indices], bins = 30, edgecolor='black', normed = True)
                #x = np.linspace(np.min(weights),np.max(weights),5000)
                #pdf_t = stats.t.pdf(x, df=df)
                #plt.plot(x,pdf_t)
                plt.xlabel("weights")
                plt.title("histogram of all weights in BNN")
                plt.legend(['Layer 1','Layer 2','Layer 3'])
        
                plt.subplot(2,2,4)
                
                plt.hist([fs[i] for i in indices], bins = 30, edgecolor='black', normed = True)
                plt.xlabel("fisher approximation")
                plt.title("histogram of fisher approximation")
                plt.legend(['Layer 1','Layer 2','Layer 3'])
        
                # update canvas immediately
                plt.show()
                running_loss = 0.0
        
                
        scheduler.step()
        print("=======> lr is: %.10f" % scheduler.get_lr()[0])
        
    print('Finished training')    
    
    
    """
        Plotting
    """
    
    
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,8))
    fig.tight_layout(h_pad = 5, w_pad = 5)
    
    plt.subplot(2,2,1)
     
    plt.plot(updates[1:], train_loss[1:])
    plt.xlabel("Updates"), plt.ylabel("train loss")
    plt.title("Noisy Adam training loss")
    
    
    plt.subplot(2,2,2)
    plt.plot(updates[2:], test_loss[2:])
    plt.xlabel("Updates"), plt.ylabel("test loss")
    plt.title("Noisy Adam test loss")
    
    
    plt.subplot(2,2,3)
    plt.plot(updates, KL_div)
    plt.xlabel("Updates"), plt.ylabel("KL Divergence")
    plt.title("Noisy Adam KL Divergence")
    
    
    plt.subplot(2,2,4)
    plt.plot(updates, ELBO)
    plt.xlabel("Updates"), plt.ylabel("ELBO")
    plt.title("Noisy Adam ELBO")
    plt.show()
    
    
    
    
    
    #x = np.linspace(np.min(weights[0]),np.max(weights[0]),5000)
    #pdf = stats.norm.pdf(x, np.mean(weights[0]), np.std(weights[0]))
    #pdf_t = stats.t.pdf(x, 5)
    ##pdf = stats.norm.pdf(x, np.mean(mus), np.mean(sigmas))
    #plt.hist(weights[0],normed=True, bins = 50,edgecolor='black')
    #plt.plot(x,pdf)
    #plt.plot(x,pdf_t)
    #plt.show()
    #
    #
    #plt.hist(fs,normed=True, bins = 50,edgecolor='black')
    #
    #
    #for j, p in enumerate(net.parameters()):
    #    #print(j)
    #    
    #    if j % 2 == 0:
    #        plt.subplot(2,1,1)
    #        plt.hist(p.data.view(-1).numpy(), bins = 40, edgecolor='black', alpha = 0.5, normed = True)
    #        plt.legend(['L1_W','L2_W','L3_W'])
    #    else:
    #        plt.subplot(2,1,2)
    #        plt.hist(p.data.view(-1).numpy(), bins = 40, edgecolor='black', alpha = 0.5, normed = True)
    #        plt.legend(['L1_b','L2_b','L3_b'])
    #    
    #
    #plt.show()
    #
    #plist = list(net.parameters())
    #
    #plt.hist(plist[0:2])
    
    #
    #"""
    #    Accuracy of fitted model
    #"""
    #
    #
    #
    ## Test on entire test set
    #correct = 0
    #total = 0
    #
    #for data in testLoader:
    #    inputs, labels = data
    #    optimizer.sample(training = False)
    #    outputs = net(Variable(inputs))
    #    _, predicted = torch.max(outputs.data, 1)
    #    total += labels.size(0)
    #    correct += (predicted == labels).sum()
    #
    #print('Accuracy of the network on the 10000 test images: %d %%' % (
    #    100 * correct / total))
    
    
    # Test on entire test set
    
    total_normalized_rmse=[]
    total_unormalized_rmse=[]
    for data in testLoader:
        x, y = data
        x, y = Variable(x), Variable(torch.FloatTensor(y.numpy()))
        outputs = net(x)
        error = (outputs.data - y.data).numpy()
        rmse_norm  = [np.mean(error**2)**0.5]  #RMSE
        rmse_unorm = [np.mean((error*y_train_std + y_train_std)**2)**0.5]
        print('Normalized RMSE on batch: ', rmse_norm)
        print('Unnormalized RMSE on batch: ', rmse_unorm)
        total_normalized_rmse += rmse_norm
        total_unormalized_rmse += rmse_unorm
        
    print('Mean Normalized RMSE on entire test set: ',  np.mean(total_normalized_rmse))
    print('Mean Unnormalized RMSE on entire test set: ',  np.mean(total_unormalized_rmse))
    
    rmses += [np.mean(total_unormalized_rmse)]
    
    
    
    
    