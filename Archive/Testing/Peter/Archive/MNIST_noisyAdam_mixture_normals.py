"""
    Setup working environment
"""

import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

"""
    Import libraries
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable
from Optimizers.noisyAdam_mixture_normals import noisyAdam
from torch.distributions import Normal
import scipy.stats as stats
from torch import optim

#import matplotlib
#matplotlib.use('TkAgg')
#from torch.nn.parameter import Parameter
#from torch.optim import Optimizer
#from torch.distributions import Normal
#from IPython.display import Image, display, clear_output
#import numpy as np


"""
    Load data
"""

batch_size = 100

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=10000, shuffle=False)


"""
    Define the neural net
"""

class FNN(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,10)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x

class FNN2(nn.Module):
    
    def __init__(self,H1):
        super(FNN2, self).__init__()
        self.NN1 = nn.Linear(784,H1)
        self.NN2 = nn.Linear(H1,10)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x


# a 100,20 NN with gamma_ex = 1e-2 and lr = 1e-3 achieves 90% after 100 epochs
# a 100,100 NN with gamma_ex = 1e-2 and lr = 1e-3 and prior var = 0.1 achieve 95% after 10        

# initialize network    
net = FNN(100,100)
net = FNN2(100)


"""
    Define loss and optimizer
"""


# loss
criterion = nn.CrossEntropyLoss()

# N as in pseudo code for noisy Adam is the total number of observations
N = trainset.train_data.shape[0]

alpha = 6
beta = 6

# optimizer
optimizer = noisyAdam(net.parameters(), num_examples = 60000,
                          gamma_ex = 1e-1,#1e-5
                          lr = 1e-2,
                          alpha = alpha,
                          beta = beta,
                          betas = (0.9, 0.999))

# setup adaptive learning rate
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[2, 10, 20], gamma=0.1)
#scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.95)


"""
    Training the network
"""

max_epochs = 20

indices = [0,2]

num_batches = trainset.train_data.shape[0]/batch_size

p_dist = Normal(0, 1)
#p_dist = torch.distributions

train_loss = []
test_loss = []
KL_div = []
ELBO = []
updates = []
term1 = []
term2 = []
w_squared = []


for epoch in range(max_epochs):
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for batch, data in enumerate(trainLoader, 0):
        
        mus = []
        sigmas = []
        weights = []
        fs = []
        gradients = []
        
        inputs, labels = data
        inputs, labels = Variable(inputs), Variable(labels)
        
#        optimizer.zero_grad()
        
        params_dict = optimizer.sample(return_params=True)
        
        outputs = net(inputs)
        
        
        # extract diagnostics
        lqw, lpw = 0.0, 0.0

        for i, p in enumerate(net.parameters()):
            mu, sigma, f, grad = params_dict["mu{0}".format(i)],\
                                        params_dict["sigma{0}".format(i)],\
                                        params_dict["f{0}".format(i)],\
                                        params_dict["grad{0}".format(i)]
                                        #params_dict["t1{0}".format(i)], \
                                        #params_dict["t2{0}".format(i)], \
                                        #params_dict["w_sq{0}".format(i)]
                        
            mus += [mu.view(-1).numpy()]
            sigmas += [sigma.view(-1).numpy()]
            fs += [f.view(-1).numpy()]
            weights += [p.data.view(-1).numpy()]
            gradients += [grad.view(-1).numpy()]
            #term1 += [t1]
            #term2 += [t2.view(-1).numpy()]
            #w_squared += [w_sq]
            
        
            # KL Divergence terms    
            q_dist = Normal(mu, sigma)
            lqw += q_dist.log_prob(p.data).sum()
            lpw += p_dist.log_prob(p.data).sum()
    
#        mus = np.concatenate(mus)
#        sigmas = np.concatenate(sigmas)
#        weights = np.concatenate(weights)
#        fs = np.concatenate(fs)
    
        KL_loss = (lqw - lpw)/num_batches
        
        LL_loss = criterion(outputs, labels)
    
        loss = LL_loss + KL_loss
        
        
        #Initialized parameters' gradients are None until you do
        #loss.backward() and start computing gradients
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        # Print statistics
        running_loss += LL_loss.data[0]
        if batch % 100 == 99:
            print('[%d, %5d] ====> train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss / 100))
            updates += [batch + (epoch*num_batches)]
            train_loss += [running_loss/100]
            KL_div += [KL_loss]
            ELBO += [loss.data[0]]
            
            for data in testLoader:
                images, labels = data
                optimizer.sample(training = False)
                outputs = net(Variable(images))
                test_loss_val = criterion(outputs, Variable(labels))
                test_loss += [test_loss_val.data[0]]
                
            print('[%d, %5d] ====> test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val.data[0]))
            
            fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
            fig.tight_layout(h_pad = 5, w_pad = 5)

            plt.subplot(2,2,1)
            #plt.plot(updates, train_loss) # plot something
            plt.hist([mus[i] for i in indices], bins = 30, edgecolor='black', normed = True)
            plt.xlabel("mu")
            plt.title("mu histogram of variational posterior")
            plt.legend(['Layer 1','Layer 2','Layer 3'])
            
            plt.subplot(2,2,2)
            
            plt.hist([sigmas[i] for i in indices], bins = 30, edgecolor='black', normed = True)
            plt.xlabel("standard deviation")
            plt.title("standard deviation histogram of variational posterior")
            plt.legend(['Layer 1','Layer 2','Layer 3'])
            
            plt.subplot(2,2,3)
            
            plt.hist([weights[i] for i in indices], bins = 30, edgecolor='black', normed = True)
            #x = np.linspace(np.min(weights),np.max(weights),5000)
            #pdf_t = stats.t.pdf(x, df=df)
            #plt.plot(x,pdf_t)
            plt.xlabel("weights")
            plt.title("histogram of all weights in BNN")
            plt.legend(['Layer 1','Layer 2','Layer 3'])
    
            plt.subplot(2,2,4)
            
            plt.hist([gradients[i] for i in indices], bins = 30, edgecolor='black', normed = True)
            #plt.hist([fs[i] for i in indices], bins = 30, edgecolor='black', normed = True)
            plt.xlabel("Gradients")
            plt.title("histogram of gradients")
            plt.legend(['Layer 1','Layer 2','Layer 3'])
    
            # update canvas immediately
            plt.show()
            running_loss = 0.0
            
    #scheduler.step()
    #print("=======> lr is: %.10f" % scheduler.get_lr()[0])
    
print('Finished training')    


"""
    Plotting
"""


fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,8))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,2,1)
 
plt.plot(updates[2:], train_loss[2:])
plt.xlabel("Updates"), plt.ylabel("train loss")
plt.title("Noisy Adam training loss")


plt.subplot(2,2,2)
plt.plot(updates, test_loss)
plt.xlabel("Updates"), plt.ylabel("test loss")
plt.title("Noisy Adam test loss")


plt.subplot(2,2,3)
plt.plot(updates, KL_div)
plt.xlabel("Updates"), plt.ylabel("KL Divergence")
plt.title("Noisy Adam KL Divergence")


plt.subplot(2,2,4)
plt.plot(updates, ELBO)
plt.xlabel("Updates"), plt.ylabel("ELBO")
plt.title("Noisy Adam ELBO")
plt.show()

x = np.linspace(np.min(weights[0]),np.max(weights[0]),5000)
pdf_t = stats.t.pdf(x, df = 2*alpha)
#pdf = stats.norm.pdf(x, np.mean(mus), np.mean(sigmas))
plt.hist(weights[0],normed=True, bins = 50,edgecolor='black')
#plt.plot(x,pdf)
plt.plot(x,pdf_t)
plt.title("Layer 1 weights distribution")
plt.legend(["t-dist df = 2*alpha", "Layer 1 weights"])
plt.show()



x = np.linspace(np.min(sigmas[0]),np.max(sigmas[0]),5000)
pdf = stats.gamma.pdf(x, a = 6, scale = 6)
#pdf = stats.norm.pdf(x, np.mean(mus), np.mean(sigmas))
plt.hist(weights[0],normed=True, bins = 50,edgecolor='black')
#plt.plot(x,pdf)
plt.plot(x,1/(1+pdf))
plt.title("Layer 1 weights distribution")
plt.legend(["t-dist df = 2*alpha", "Layer 1 weights"])
plt.show()

x = np.linspace(stats.gamma.ppf(0.0001, a=6,scale=1/6), stats.gamma.ppf(0.9999, a=6,scale=1/6), 1000)
y = np.linspace(stats.invgamma.ppf(0.0001, a=6,scale=1/6), stats.invgamma.ppf(0.9999, a=6,scale=1/6), 1000)
plt.plot(x, stats.gamma.pdf(x,a=6,scale=1/6))
plt.plot(y, 1/(1+stats.invgamma.pdf(y,a=6,scale=1/6)))
plt.show()
#
#
plt.hist([fs[i] for i in indices],normed=True, bins = 40,edgecolor='black')
#
#
#for j, p in enumerate(net.parameters()):
#    #print(j)
#    
#    if j % 2 == 0:
#        plt.subplot(2,1,1)
#        plt.hist(p.data.view(-1).numpy(), bins = 40, edgecolor='black', alpha = 0.5, normed = True)
#        plt.legend(['L1_W','L2_W','L3_W'])
#    else:
#        plt.subplot(2,1,2)
#        plt.hist(p.data.view(-1).numpy(), bins = 40, edgecolor='black', alpha = 0.5, normed = True)
#        plt.legend(['L1_b','L2_b','L3_b'])
#    
#
#plt.show()
#
#
#
#plist = list(net.parameters())
#
#plt.hist(plist[0:2])



# Plotting term1
#plt.plot([term1[i] for i in range(0, len(term1), 6)])
#plt.plot([term1[i] for i in range(2, len(term1), 6)])
#plt.plot([term1[i] for i in range(4, len(term1), 6)])
#plt.title('First term of second order derivative of prior')
#plt.xlabel("updates")
#plt.legend(['Layer 1','Layer 2','Layer 3'])
#plt.show()
#
## Plotting w^T*w
#plt.plot([w_squared[i] for i in range(0, len(w_squared), 6)])
#plt.plot([w_squared[i] for i in range(2, len(w_squared), 6)])
#plt.plot([w_squared[i] for i in range(4, len(w_squared), 6)])
#plt.xlabel("updates")
#plt.title('w^T * w')
#plt.legend(['Layer 1','Layer 2','Layer 3'])
#plt.show()


"""
    Accuracy of fitted model
"""



# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
    optimizer.sample(training = False)
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))