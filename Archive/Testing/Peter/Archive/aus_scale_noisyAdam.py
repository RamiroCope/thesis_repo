import torch
import matplotlib.pyplot as plt
from IPython.display import Image, display, clear_output
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.nn.parameter import Parameter
from torch.optim import Optimizer
from torch.utils.data import DataLoader
from Optimizers.noisyAdam import noisyAdam
from data.aus_scale import ausScale



"""
    Load data
"""

batch_size = 35

# train data
trainset = ausScale()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = ausScale(train = False)
testLoader = DataLoader(testset, batch_size=batch_size, shuffle=False)


"""
    Define the neural net
"""



class FNN(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(14,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,2)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x

# a 100,20 NN with gamma_ex = 1e-2 and lr = 1e-3 achieves 90% after 100 epochs

# initialize network    
net = FNN(10,10)



"""
    Define loss and optimizer
"""


# loss
criterion = nn.CrossEntropyLoss()

# N as in pseudo code for noisy Adam
# is the total number of observations
N = trainset.train_data.shape[0]

# optimizer
optimizer = noisyAdam(net.parameters(), num_examples = N, gamma_ex = 0, lr = 1e-3)

# setup adaptive learning rate
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[10,30], gamma=0.1)
#scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.45)



"""
    Training the network
"""



for epoch in range(100):
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss = 0.0
    for i, data in enumerate(trainLoader, 0):
        
        inputs, labels = data
        inputs, labels = Variable(inputs), Variable(labels)
        
        optimizer.zero_grad()
        
        optimizer.sample()
        
        outputs = net(inputs)
    
        loss = criterion(outputs, labels)
        #Initialized parameters' gradients are None until you do
        #loss.backward() and start computing gradients
        loss.backward()
        optimizer.step()
        
        # Print statistics
        running_loss += loss.data[0]
        if i % 2 == 1:
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 2))
            running_loss = 0.0
            
    #scheduler.step()
    

print('Finished training')





# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))

