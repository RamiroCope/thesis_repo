"""
FFN on MNIST data using pyTorch
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
from IPython.display import Image, display, clear_output
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.nn.parameter import Parameter
from torch.optim import Optimizer
from torch.distributions import Normal
import Adam

"""
Load data
"""


batch_size = 200


# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False)


"""
    VProp Optimizer
"""


class VProp(Optimizer):
    def __init__(self, params, lr=1e-2, alpha=0.99, eps=1e-8, weight_decay=0, momentum=0, centered=False, precision = 1):
        defaults = dict(lr=lr, momentum=momentum, alpha=alpha, eps=eps, centered=centered, weight_decay=weight_decay)
        super(VProp, self).__init__(params, defaults)
        self.precision = precision
        self.posterior_params = {}          
        
    def __setstate__(self, state):
        super(VProp, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('momentum', 0)
            group.setdefault('centered', False)
            
            
    def sample(self, training = True, init = False):
        for group in self.param_groups:
            for i,p in enumerate(group['params']):
                state = self.state[p]
                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    std = 1.0/np.sqrt(self.precision)
                    #state['sigma'] = torch.Tensor(p.size()).uniform_(std/4,std/2)
                    state['square_avg'] = torch.Tensor(p.size()).uniform_(self.precision, 3*self.precision)
                    #state['square_avg'] = torch.Tensor(p.size()).uniform_(self.precision*2 + 10000, 4*self.precision + 10000)
                    #state['sigma'] = torch.Tensor(p.size()).uniform_(1/(4*self.precision), 1/(2*self.precision))
                    state['mu'] = torch.Tensor(p.size()).uniform_(-0.01,0.01)
                
                #sigma = state['sigma']
                mu = state['mu']
                #sigma = state['sigma']
                square_avg = state['square_avg']
                if not training:
                    p.data = mu
                    return

                eps = torch.randn(p.size())                
                if init:
                    p.data = mu + eps.mul(square_avg.sqrt())                    
                    self.posterior_params["sigma{0}".format(i)] = square_avg
                else:
                    p.data = mu + eps.div((square_avg+self.precision).sqrt())
                    self.posterior_params["sigma{0}".format(i)] = 1.0/(square_avg + self.precision)
                #p.data = mu + eps.mul(sigma.sqrt())
                
                self.posterior_params["mu{0}".format(i)] = mu
                
                
        return self.posterior_params
            
            
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for i,p in enumerate(group['params']):
                grad = p.grad.data
                state = self.state[p]
                #square_avg = state['square_avg']
                #sigma = state['sigma']
                square_avg = state['square_avg']
                mu = state['mu']
                alpha = group['alpha']                
                state['step'] += 1
                
                #    PSEUDOCODE LINE 3    #
                #        UPDATE S         #
                square_avg.mul_(alpha).addcmul_(1 - alpha, grad, grad)
                #square_avg = alpha*(sigma**2 - self.precision) + (1 - alpha)*grad*grad
                
                num = grad + self.precision*mu
                denom = square_avg + self.precision
                
                #sigma = 1.0/(square_avg + self.precision)
                mu.addcdiv_(-group["lr"], num, denom)
                
                #update sigma
                #state['sigma'] = 1.0/(square_avg + self.precision)
        return



"""
Model graph
"""

class FNN(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,10)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x
    
net = FNN(200,200)

#Conversion from log standard deviation
precision = (1.0/np.exp(-3)**2)

# Loss and optimizer    
criterion = nn.CrossEntropyLoss(reduce=True, size_average=False)

#Note alpha = 1-beta from Vprop pseudocode
#Note lr = alpha in Vprop pseudocode
#optimizer = VProp(params = net.parameters(),lr=0.01, precision = precision, alpha = 0.99)
optimizer = Adam(params = net.parameters(),lr=0.01)



"""
Train the model
"""

n_mc_samples = 2
n_batches = trainset.train_data.shape[0]/batch_size
KL_loss = 0.0
lpw = 0.0
lqw = 0.0
p_dist = Normal(0, np.sqrt(1.0/precision))

updates = []
KL_div = []
nll_loss = []
total_loss = []

plt.figure()

init = True

for epoch in range(50):
    
    running_loss = 0.0
    for i, data in enumerate(trainLoader, 0):
        
        optimizer.zero_grad()
        
        inputs, labels = data
        
        inputs, labels = Variable(inputs), Variable(labels)

        KL_loss, lpw, lqw, nll = 0.0, 0.0, 0.0, 0.0
        for _ in range(0, n_mc_samples):
            
            posterior_parameters = optimizer.sample()
            init = False
            
            #Forward pass
            outputs = net(inputs)    
            
            for j, p in enumerate(net.parameters()):            
                mu, sigma = posterior_parameters["mu{0}".format(j)], posterior_parameters["sigma{0}".format(j)]
                q_dist = Normal(mu, sigma.sqrt())
        
                lqw += q_dist.log_prob(p.data).sum()    
                lpw += p_dist.log_prob(p.data).sum()
        
                #For plotting
                if j == 0:
                    #mu_plot1 = mu.view(-1)
                    mu_plot1 = p.data.view(-1)
                    #weights = p.data.view(-1)
                if j == 1:
                    mu_plot2 = mu.view(-1)
            
            nll += criterion(outputs,labels)
                
                
        #nll = criterion(outputs,labels)
        #nll = F.log_softmax(outputs, dim = 1)
        #nll = F.nll_loss(nll,labels)
        
        KL_loss = (lqw/n_mc_samples - lpw/n_mc_samples)/n_batches
        
        nll = nll/n_mc_samples
        nll = nll/batch_size
        
        loss = nll + KL_loss
        #Backpropagate
        loss.backward()
        
        optimizer.step()


        #print(sigma)
        # Print statistics
        running_loss += loss.data[0]
        if i % 2 == 1:
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 2))
            print('=====> KL_loss = ', KL_loss)
            print('=====> Total loss = ', loss.data[0])
            print('=====> NLL loss = ', nll.data[0])
            print(i)
            running_loss = 0.0
            
            updates += [i + epoch*(trainset.train_data.shape[0]/batch_size)]
            KL_div += [KL_loss]
            nll_loss += [nll.data[0]]
            total_loss += [nll.data[0] + KL_loss]


            plt.subplot(2,2,1)
            plt.title('KL Loss')
            plt.legend(['KL'])
            plt.xlabel('Updates'), plt.ylabel('Error [nats]')
            plt.plot(updates, KL_div, color="black", linestyle="--")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
            
            
            plt.subplot(2,2,2)
            plt.cla()
            plt.title('NLL')
            plt.legend(['NLL'])
            plt.xlabel('Updates'), plt.ylabel('Error [nats]')
            plt.plot(updates, nll_loss, color="red", linestyle="--")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
            
            plt.subplot(2,2,3)
            plt.cla()
            plt.title('Total loss')
            plt.legend(['Total loss'])
            plt.xlabel('Updates'), plt.ylabel('Error [nats]')
            plt.plot(updates, total_loss, color="blue", linestyle="--")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
            
            plt.subplot(2,2,4)
            plt.cla()
            plt.title('Mu histogram')
            plt.legend(['Mu'])
            #plt.xlabel('Updates'), plt.ylabel('Error [nats]')
            plt.hist(mu_plot1, color="blue", bins = 50)
            #plt.hist(mu_plot2, color="blue", linestyle="--")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
            
#            plt.subplot(2,2,4)
#            plt.cla()
#            plt.title('weights')
#            plt.legend(['weights'])
#            #plt.xlabel('Updates'), plt.ylabel('Error [nats]')
#            plt.hist(weights, color="blue", linestyle="--")
#            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
#            plt.grid('on')
            
            
            plt.savefig("out53.png")
            display(Image(filename="out53.png"))
            clear_output(wait=True)


print('Finished training')


"""
Test the model
"""

## Initial testing on a few images
#test_dataiter = iter(testLoader)
#
#
#for _ in range(5):
#
#    test_img, labels = test_dataiter.next()
#    
#    img = torchvision.utils.make_grid(test_img)
#    npimg = img.numpy()
#    npimg = np.transpose(npimg, (1,2,0))
#    plt.imshow(npimg)
#    #plt.imshow(npimg)
#    
#    # print images
#    #imshow(test_img)
#    print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(1)))
#    
#    
#    test_output = net(Variable(test_img))
#    
#    _, predicted = torch.max(test_output.data, 1)
#    
#    print('Predicted: ', ' '.join('%5s' % classes[predicted[j]] for j in range(1)))


# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    optimizer.sample(training = False)
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))




















x = optimizer.sample()

h1 = x['mu0'].view(-1).numpy()
h2 = x['mu1'].view(-1).numpy()

plt.hist([h1,h2], bins = 20, alpha=0.7, label=['h1', 'h2'], color = ['g','r'])
plt.show()

plt.hist(x['mu0'].view(-1), bins = 20, alpha = 0.5)
plt.hist(x['mu1'].view(-1), bins = 20, color = "green", alpha = 0.5)
plt.show()


