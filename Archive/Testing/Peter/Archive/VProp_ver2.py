"""
FFN on MNIST data using pyTorch
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
from IPython.display import Image, display, clear_output
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.nn.parameter import Parameter
from torch.optim import Optimizer
from torch.distributions import Normal


"""
Load data
"""


# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=25, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=False)

classes = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')


def imshow(img):
    #img = img / 2 + 0.5
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    #plt.imshow(npimg)
    

# get some random training images
dataiter = iter(trainLoader)
images, labels = dataiter.next()

# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(1)))


"""
    VProp Optimizer
"""


class VProp(Optimizer):
    def __init__(self, params, lr=1e-2, alpha=0.99, eps=1e-8, weight_decay=0, momentum=0, centered=False, precision = 1):
        defaults = dict(lr=lr, momentum=momentum, alpha=alpha, eps=eps, centered=centered, weight_decay=weight_decay)
        super(VProp, self).__init__(params, defaults)
        self.precision = precision
        self.posterior_params = {}
        i = 0        
        
        for group in self.param_groups:
            for p in group['params']:
                #What should we initialize especially the variance to?
                self.posterior_params["mu{0}".format(i)] = Variable(torch.Tensor(p.size()).uniform_(-0.01,0.01))
                self.posterior_params["sigma{0}".format(i)] = Variable(torch.Tensor(p.size()).uniform_(self.precision/4 + 1e-5,self.precision/2  + 1e-5))
                i += 1
            
        
    def __setstate__(self, state):
        super(VProp, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('momentum', 0)
            group.setdefault('centered', False)
            
            
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        j = 0

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                
                lambdas = self.precision*torch.ones(p.size())
                grad = p.grad.data
                    
                #At initialization, state is empty
                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    #state['square_avg'] = torch.zeros_like(p.data)
                    state['square_avg'] = 1.0/self.posterior_params["sigma{0}".format(j)].data - lambdas

                square_avg = state['square_avg']

                #Do sampling
                eps = torch.randn(p.size())
                p.data = self.posterior_params["mu{0}".format(j)].data + eps.div((square_avg+lambdas).sqrt())

                #alpha = 1-beta in Vprop pseudocode
                alpha = group['alpha']

                state['step'] += 1

                square_avg.mul_(alpha).addcmul_(1 - alpha, grad, grad)
                
                self.posterior_params["sigma{0}".format(j)].data = 1.0/(square_avg + lambdas)

                #avg = square_avg.sqrt().add_(group['eps'])
                var = state['square_avg'] + lambdas
                avg = grad + lambdas*self.posterior_params["mu{0}".format(j)].data

                self.posterior_params["mu{0}".format(j)].data.addcdiv_(-group['lr'], avg, var)
                
                
                j += 1

        return self.posterior_params



"""
Model graph
"""

class FNN(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,10)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x
    
net = FNN(60,40)

precision = 1000

# Loss and optimizer    
criterion = nn.CrossEntropyLoss()

#Note alpha = 1-beta from Vprop pseudocode
#Note lr = alpha in Vprop pseudocode
optimizer = VProp(params = net.parameters(),lr=0.01, precision = precision, alpha = 0.85)



"""
Train the model
"""


batch_size = 25
n_batches = trainset.train_data.shape[0]/batch_size
KL_loss = 0.0
lpw = 0.0
lqw = 0.0
p_dist = Normal(0, np.sqrt(1.0/precision))
init = False

updates = []
KL_div = []
nll_loss = []
total_loss = []

plt.figure()


for epoch in range(5):
    
    running_loss = 0.0
    for i, data in enumerate(trainLoader, 0):
        
        inputs, labels = data
        
        inputs, labels = Variable(inputs), Variable(labels)
        
        outputs = net(inputs)

        posterior_parameters = optimizer.step()

        j = 0

        lqw, lpw = 0.0, 0.0
        for p in net.parameters():
            mu, sigma = posterior_parameters["mu{0}".format(j)].data,\
                            posterior_parameters["sigma{0}".format(j)].data
            q_dist = Normal(mu, sigma.sqrt())
            
            lqw += q_dist.log_prob(p.data).sum()    
            lpw += p_dist.log_prob(p.data).sum()
            
            if j == 1:
                mu_plot = sigma
            
            j += 1
            
            



        KL_loss = (lqw - lpw)/n_batches
        nll = criterion(outputs, labels)
        #nll = F.nll_loss(outputs,labels)

        loss = nll + KL_loss
        optimizer.zero_grad()
        loss.backward()
        #print(sigma)
        # Print statistics
        running_loss += loss.data[0]
        if i % 80 == 79:
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 80))
            print('KL_loss = ', KL_loss)
            print('NLL = ', nll.data)
            print(i)
            running_loss = 0.0
            
            updates += [i + epoch*(trainset.train_data.shape[0]/batch_size)]
            KL_div += [KL_loss]
            nll_loss += [nll.data]
            total_loss += [loss.data]
            
            # Plotting
            #plt.ion()
            #plt.draw()
            plt.subplot(2,2,1)
            plt.title('KL Loss')
            plt.legend(['KL'])
            plt.xlabel('Updates'), plt.ylabel('Error [nats]')
            plt.plot(updates, KL_div, color="black", linestyle="--")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
            
            
            plt.subplot(2,2,2)
            plt.cla()
            plt.title('NLL')
            plt.legend(['NLL'])
            plt.xlabel('Updates'), plt.ylabel('Error [nats]')
            plt.plot(updates, nll_loss, color="red", linestyle="--")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
            
            plt.subplot(2,2,3)
            plt.cla()
            plt.title('Total loss')
            plt.legend(['Total loss'])
            plt.xlabel('Updates'), plt.ylabel('Error [nats]')
            plt.plot(updates, total_loss, color="blue", linestyle="--")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
            
            plt.subplot(2,2,4)
            plt.cla()
            plt.title('Mu histogram')
            plt.legend(['Mu'])
            #plt.xlabel('Updates'), plt.ylabel('Error [nats]')
            plt.hist(mu_plot, color="blue", linestyle="--")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
            
            
            plt.savefig("out53.png")
            display(Image(filename="out53.png"))
            clear_output(wait=True)


print('Finished training')


"""
Feeding a single digit through the net
"""

batch_size = 50
n_batches = trainset.train_data.shape[0]/batch_size

datiter = iter(trainLoader)
inputs, labels = datiter.next()

inputs, labels = Variable(inputs), Variable(labels)

optimizer.zero_grad()

outputs = net(inputs)

lpw, lqw = net.get_lpw_lqw()



posterior_parameters = optimizer.step()


p_dist = Normal(0, 1/precision)

KL_loss = 0.0
lpw = 0.0
lqw = 0.0
i = 0

for p in net.parameters():
    mu, sigma = posterior_parameters["mu{0}".format(i)].data, posterior_parameters["sigma{0}".format(i)].data
    q_dist = Normal(mu, sigma)
    
    lqw += q_dist.log_prob(p.data).sum()    
    lpw += p_dist.log_prob(p.data).sum()
    
    KL_loss += (lqw - lpw)/n_batches
    
    i += 1

nll = criterion(outputs, labels)

loss = nll + KL_loss

loss.backward()






"""
Test the model
"""

# Initial testing on a few images
test_dataiter = iter(testLoader)


for _ in range(5):

    test_img, labels = test_dataiter.next()
    
    img = torchvision.utils.make_grid(test_img)
    npimg = img.numpy()
    npimg = np.transpose(npimg, (1,2,0))
    plt.imshow(npimg)
    #plt.imshow(npimg)
    
    # print images
    #imshow(test_img)
    print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(1)))
    
    
    test_output = net(Variable(test_img))
    
    _, predicted = torch.max(test_output.data, 1)
    
    print('Predicted: ', ' '.join('%5s' % classes[predicted[j]] for j in range(1)))


# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))

