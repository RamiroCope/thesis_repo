from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_svmlight_file
from IPython.display import clear_output
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader
from torch.autograd import Variable
from urllib.request import urlopen
#from VProp_ver4 import VProp as Vprop
#from Optimizers.VProp_ver4 import VProp as Vprop
#from Vprop1 import Vprop1 as Vprop

import subprocess
import torch

import numpy as np
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions import Normal

def create_dataloaders(X, y, test_size, train_batch_size = 1,
                               train_shuffle = True, standardize=None):
    """
    Desc: This function creates a train and test pytorch DataLoader for
          regression datasets.
          
    INPUT: X, y        : Numpy type X matrix and y targets
           test_size   : Float between 0 and 1
           shuffle     : Boolean True or False
           standardize : True to scale input features, None by default
           
    OUTPUT: Pytorch Train and Test DataLoader
    """
    assert(test_size <= 1 and test_size >= 0)
    
    if standardize:
        # Standardize X matrix 
        X = preprocessing.scale(X)
    
    X, Xtest, y, ytest = train_test_split(X, y, test_size=test_size)
    
    # Merging
    train_data = np.c_[ X, y ]
    test_data  = np.c_[ Xtest, ytest ]
    
    # Convert to Floats
    train_data = torch.from_numpy(train_data).float()
    test_data  = torch.from_numpy(test_data).float()
    
    # Create Data Loaders
    trainLoader = DataLoader(train_data, batch_size=train_batch_size, shuffle=train_shuffle)
    testLoader  = DataLoader(test_data, batch_size=len(Xtest))
    
    return trainLoader, testLoader

australian_scale = urlopen("https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/australian_scale")

X, y = load_svmlight_file(australian_scale)

for i in range(0,len(y)):
    if y[i] < 0:
        y[i] = 0

X = X.toarray()

trainLoader, testLoader = create_dataloaders(X, y, test_size=0.5, 
                                             train_batch_size = 100,
                                             standardize = False)


class FNN(nn.Module):
    
    def __init__(self,I,H1,H2,O):
        super(FNN, self).__init__()
        self.I = I 
        self.O = O
        self.NN1 = nn.Linear(I,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,O)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x

#I   = X.shape[1]
I = X.shape[1]
O   = 2
net = FNN(I,10,10,O)

criterion = nn.CrossEntropyLoss()
optimizer = optim.RMSprop(net.parameters(),lr = 0.01, alpha = 0.9)

for epoch in range(20):
    
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for i, data in enumerate(trainLoader, 0):
        x, y = data[:,0:-1], data[:,-1]
        x, y = Variable(x), Variable(torch.LongTensor(y.numpy()))
        
        optimizer.zero_grad()
        
        outputs = net(x)
    
        loss = criterion(outputs, y)
        #Initialized parameters' gradients are None until you do
        #loss.backward() and start computing gradients
        loss.backward()
        optimizer.step()
        
        # Print statistics
        running_loss += loss.data[0]
        if i % 2 == 1:
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 2))
            running_loss = 0.0

print('Finished training')


# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    x, y = testLoader.dataset[:,:-1], testLoader.dataset[:,-1]
    x, y = Variable(x), Variable(torch.LongTensor(y.numpy()))
    outputs = net(x)
    _, predicted = torch.max(outputs.data, 1)
    total += y.size(0)
    correct += (predicted == y.data).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))
