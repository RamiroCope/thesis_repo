import math
import torch
import torch
from torch.autograd import Variables
from torch.optim import Optimizer
from torch.autograd import Variable
from torch.distributions import Normal

class Adam(Optimizer):
    """Implements Adam algorithm.

    It has been proposed in `Adam: A Method for Stochastic Optimization`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-3)
        betas (Tuple[float, float], optional): coefficients used for computing
            running averages of gradient and its square (default: (0.9, 0.999))
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        amsgrad (boolean, optional): whether to use the AMSGrad variant of this
            algorithm from the paper `On the Convergence of Adam and Beyond`_

    .. _Adam\: A Method for Stochastic Optimization:
        https://arxiv.org/abs/1412.6980
    .. _On the Convergence of Adam and Beyond:
        https://openreview.net/forum?id=ryQu7f-RZ
    """

    def __init__(self, params, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0, amsgrad=False):
        if not 0.0 <= betas[0] < 1.0:
            raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
        if not 0.0 <= betas[1] < 1.0:
            raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))
        defaults = dict(lr=lr, betas=betas, eps=eps,
                        weight_decay=weight_decay, amsgrad=amsgrad)
        super(Adam, self).__init__(params, defaults)
    
    def sample(self, training = True, batch_size, kl_weight, prior_var, gamma_extrinsic=None):
        
        # int damping term
        self.gamma_intrinsic = kl_weight / (prior_var + batch_size)
        if gamma_extrinsic:
            """
            It can be advantageous to add external damping for numerical stability
            """
            # ext damping term
            self.gamma_extrinsic = gamma_extrinsic
            self.gamma_total     = self.gamma_intrinsic + self.gamma_extrinsic
        else:
            self.gamma_total     = self.gamma_intrinsic
        
        
        for group in self.param_groups:
            for i,p in enumerate(group['params']):              
                if p.grad is None:
                    p.grad = Variable(torch.zeros(p.size()))
                grad = p.grad.data
                # State initialization
                state = self.state[p]
                
                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    # Mk: Exponential moving average of gradient values
                    state['exp_avg'] = torch.zeros_like(p.data) #torch.Tensor(p.size()).uniform_(0.00001*self.precision, 0.0001*self.precision)
                    # Fk: Exponential moving average of squared gradient values
                    state['exp_avg_sq'] = torch.zeros_like(p.data)
                    state['mu'] = torch.Tensor(p.size()).uniform_(-0.1,0.2)
                                
                exp_avg = state['exp_avg']
                exp_avg_sq = state['exp_avg_sq']
                mu = state['mu']
                
                if not training:   
                    """
                    After training we sample mu and predict with it 
                    """
                    p.data = mu                    
                    self.posterior_params["mu{0}".format(i)] = mu
                    damped_f = torch.inverse(exp_avg_sq + self.gamma_intrinsic)
                    q_var    = (kl_weight/batch_size) * damped_f
                    self.posterior_params["sigma{0}".format(i)] = q_var                
                    return self.posterior_params
                
                #   PSEUDO CODE LINE 1   #
                damped_f = torch.inverse(exp_avg_sq + self.gamma_intrinsic)
                q_var    = (kl_weight/batch_size) * damped_f
                q_dist   = Normal(mu, torch.sqrt(q_var))
                self.w   = q_dist.sample()
                
                self.posterior_params["mu{0}".format(i)] = mu
                self.posterior_params["sigma{0}".format(i)] = q_var
                self.posterior_params["grad{0}".format(i)] = grad
                
        return self.posterior_params
    
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                #   PSCODE LINE 2      #
                grad      = p.grad.data - (self.gamma_intrinsic * self.w)
                if grad.is_sparse:
                    raise RuntimeError('Adam does not support sparse gradients, please consider SparseAdam instead')
                amsgrad = group['amsgrad']

                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    state['exp_avg'] = torch.zeros_like(p.data)
                    # Exponential moving average of squared gradient values
                    state['exp_avg_sq'] = torch.zeros_like(p.data)
                    if amsgrad:
                        # Maintains max of all exp. moving avg. of sq. grad. values
                        state['max_exp_avg_sq'] = torch.zeros_like(p.data)
                
                # exp_avg is mk, and exp_avg_sq is fk in pseudo
                exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                if amsgrad:
                    max_exp_avg_sq = state['max_exp_avg_sq']
                beta1, beta2 = group['betas']

                state['step'] += 1

                if group['weight_decay'] != 0:
                    grad = grad.add(group['weight_decay'], p.data)

                # Decay the first and second moment running average coefficient
                #   PSCODE LINE 3      #
                exp_avg.mul_(beta1).add_(1 - beta1, grad)
                
                #   PSCODE LINE 4      #
                exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)
                if amsgrad:
                    # Maintains the maximum of all 2nd moment running avg. till now
                    torch.max(max_exp_avg_sq, exp_avg_sq, out=max_exp_avg_sq)
                    # Use the max. for normalizing running avg. of gradient
                    denom = max_exp_avg_sq.sqrt().add_(group['eps'])
                else:
                    #   PSCODE LINE 6: denom    #
                    denom = exp_avg_sq.add_(self.gamma_total)
                
                # PSCODE LINES 5,6 & 7 are replaced by the following (see Adam paper) # 
                bias_correction1 = 1 - beta1 ** state['step']
                bias_correction2 = 1 - beta2 ** state['step']
                step_size = group['lr'] * math.sqrt(bias_correction2) / bias_correction1

                #   PSCODE LINE 7      #
                p.data.addcdiv_(-step_size, exp_avg, denom)

        return loss
