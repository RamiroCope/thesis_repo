
import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import torch
from torch.optim import Optimizer
import numpy as np
from scipy.linalg import sqrtm as mat_sqrt



"""
Load data
"""


# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=100, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=100, shuffle=False)



"""
    noisy KFAC optimizer
"""




class noisyKFAC(Optimizer):

    """
        NOTES:
            - Add pi factor to code as given in eq.13
            - Move constant cov_fac out of loop and in to init? cov_fac is
                just the square root of self.gamma_in. So use this?
            - layer offset in pseudo code line 3 & 6 not accounted for.
            - add twiddle factor as the largest eigenvalue to A and S
            - setting t_stats too high causes gg_mappings to generate nans
                or very high numbers, causing the code to terminate. Why?
            -
    """
    
    
    def __init__(self,
                 params,
                 layers,
                 num_examples,
                 lr=1e-3,
                 beta=0.99,
                 eps=1e-8,
                 weight_decay=0,
                 kl_weight = 1,
                 prior_var = 1,
                 gamma_ex = 0,
                 t_inv = 1,
                 t_stats = 10):
        
        defaults = dict(lr=lr, beta=beta, eps=eps,
                        weight_decay=weight_decay, kl_weight = kl_weight, prior_var = prior_var,
                        gamma_ex = gamma_ex, num_examples = num_examples)
        
        super(noisyKFAC, self).__init__(params, defaults)
        
        self.posterior_params = {}
        # lambda in pseudo code - the KL weighting term
        self.kl_weight = kl_weight
        # total number of training examples (e.g. for MNIST, num_examples = 60.000)
        self.num_examples = num_examples
        # intrinsic damping term
        self.gamma_in = kl_weight/(num_examples*prior_var)
        # adds the two damping terms. gamma_ex may be zero.                
        self.gamma = self.gamma_in + gamma_ex
        # prior variance
        self.prior_var = prior_var
        
        # t_stats and t_inv for update scheme
        self.t_stats = t_stats
        self.t_inv = t_inv
        
        # step counter
        self.steps = 0
        
        # dicts for the a and g updates used in eq. 12
        self.aa_mappings = dict()
        self.gg_mappings = dict()
        
        # initialize hooks
        for i,layer in enumerate(layers):
            if i == 0:
                continue
            layer.register_forward_pre_hook(self.forward_hook_trigger)
            layer.register_backward_hook(self.backward_hook_trigger)
        

    def forward_hook_trigger(self, layer, i):
        # x is the network
        #print('Layer is: ',x)
        # i is the input data
        #print('Data contains: ',i)
        if self.steps % self.t_stats == 0:
            aa = torch.mm(i[0].data.t(), i[0].data) / i[0].size(1)
            for p in layer.parameters():
                self.aa_mappings[p] = aa
        return
    
    def backward_hook_trigger(self, layer, grad_in, grad_out):
        # x is the network
        #print('Layer is: ',x)
        # i is the input data
        #print('Data contains: ',i)
        if self.steps % self.t_stats == 0:
#            print('grad_out is: ', grad_out[0].data)
            gg = torch.mm(grad_out[0].data.t(), grad_out[0].data) / grad_out[0].size(1)
            for p in layer.parameters():
                self.gg_mappings[p] = gg
        return

    def sample(self, training = True):
        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0
                    # value initialization for mu parameter
                    init_val = 1.0/np.sqrt(p.size(0))
                    # mu - the mean of the variational posterior
                    # should be 100x784 for MNIST first layer
                    #state['mu'] = torch.Tensor(p.size(1), p.size(0)).uniform_(-init_val, init_val)
                    
                    # if weight matrix
                    if len(p.size()) == 2:
                        state['mu'] = torch.Tensor(p.size()).uniform_(-init_val, init_val)
                        # should be 784x784 for MNIST first layer
                        state['A'] = torch.ones(p.data.size(1), p.data.size(1))
                        # should be 100x100 for MNIST first layer
                        state['S'] = torch.ones(p.data.size(0), p.data.size(0))
                        state['A_inv'] = torch.ones(p.data.size(1), p.data.size(1))
                        state['S_inv'] = torch.ones(p.data.size(0), p.data.size(0))
                    # if bias vector
                    else:
                        p.data = torch.zeros(p.size())
                        continue
#                        state['A'] = torch.ones(p.data.size(0), p.data.size(0))
#                        state['S'] = torch.ones(1,1)
#                        state['A_inv'] = torch.ones(p.data.size(0), p.data.size(0))
#                        state['S_inv'] = torch.ones(1,1)
                
                if len(p.size()) == 1:
                    continue
                
                # mean parameter
                mu = state['mu']
                
                if not training:
                    p.data = mu
                    continue
                
                # factor multiplied on covariance matrices as in eq. 13
                cov_fac = np.sqrt(self.gamma_in)
                
                # if weight matrix
                if len(p.size()) == 2:
                    # A & S covariance matrices
                    self.A = state['A'].t() + cov_fac*torch.eye(p.size(1)) + 1e-7
                    self.S = state['S'].t() + cov_fac*torch.eye(p.size(0)) + 1e-7
                
                    # noise term
                    e = torch.randn(p.size())
                # if bias vector
#                else:
#                    # A & S covariance matrices
#                    A = state['A'] + cov_fac*torch.eye(p.size(0))
#                    S = state['S'] + cov_fac*torch.eye(1)
#                
#                    # noise term
#                    e = torch.randn(p.size())
#                    e = e.view(-1, 1)
#                if j == 4:
#                    print('S before: ', self.S)
                # compute the square root of A
                if state['step'] % self.t_stats == 0:
                    A_eigen_val, A_eigen_vec = self.A.symeig(eigenvectors = True)
                    A_diag = torch.diag(A_eigen_val.sqrt())
                    A_sqrt = torch.mm(A_eigen_vec, A_diag)
                    A_sqrt = torch.mm(A_sqrt, A_eigen_vec.t())
                    
                    # inverse of square root of A^gamma_in
                    A_sqrt_inv = A_sqrt.inverse()
                    
                    # compute the square root of S_inv
                    S_eigen_val, S_eigen_vec = self.S.symeig(eigenvectors = True)
                    S_diag = torch.diag(S_eigen_val.sqrt())
                    S_sqrt = torch.mm(S_eigen_vec, S_diag)
                    S_sqrt = torch.mm(S_sqrt, S_eigen_vec.t())
                    
                    # inverse of square root of S^gamma_in
                    S_sqrt_inv = S_sqrt.inverse()
                    
                    state['A_inv'] = A_sqrt_inv
                    state['S_inv'] = S_sqrt_inv
                    
#                if j == 4:
#                    print('S after: ', self.S)
                # scalar multiplied on variance
                F_fact = np.sqrt(self.kl_weight/self.num_examples)

                # compute standard deviation
                Se = torch.mm(state['S_inv'], e)
                SeA = torch.mm(Se, F_fact*state['A_inv'])
                # do actual sampling
                p.data = mu + SeA
        return

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for j, p in enumerate(group['params']):
                if p.grad is None:
                    continue

                # Line 1: Getting gradients
                grad = p.grad.data

                if len(p.size()) == 1:
                    continue

                state = self.state[p]

                # update step
                state['step'] += 1
                # self.step is used for hooks
                self.steps = state['step']
              
                # get A, S, mu, beta and cov_fac as in pseudo code
                beta = group['beta']
                mu = state['mu']
                self.A = state['A'].t()
                self.S = state['S'].t()
                A_inv = state['A_inv']
                S_inv = state['S_inv']
#                if j == 4:
#                    print('S before: ', self.S)
                cov_fac = np.sqrt(self.gamma_in)

                # Pseduo code line 2
                # the beta value is reversed compared to pseudo code
                # to stick with the other implementations beta values
                if state['step'] % self.t_stats == 0:
                    self.A.mul_(beta).add_((1 - beta)*self.aa_mappings[p])
                    self.S.mul_(beta).add_((1 - beta)*self.gg_mappings[p])
                
#                if j == 4:
#                    print('S after: ', self.S)
#                    print('gg_mappings after: ', self.gg_mappings[p])
                
                # Pseudo code line 5
                if state['step'] % self.t_inv == 0:
                    A_inv = (self.A + cov_fac*torch.eye(state['A'].size(0)) + 1e-8).inverse()
                    S_inv = (self.S + cov_fac*torch.eye(state['S'].size(0)) + 1e-8).inverse()

                if group['weight_decay'] != 0:
                    grad = grad.add(group['weight_decay'], p.data)

                # modify gradient
                V = grad - self.gamma_in*p.data
                
                # compute step update
                SVA = torch.mm(torch.mm(S_inv, V), A_inv)
                # update mu
                mu.add_(-group['lr']*SVA)

        return loss
    
    
    
"""
    Define neural net
"""





class FNN(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,10)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x
    
#class FNN(nn.Module):
#    def __init__(self):
#        super(FNN,self).__init__()
#        
#        self.model = nn.Sequential(
#                
#                nn.Linear(784,100),
#                nn.ReLU(),
#                nn.Linear(100,100),
#                nn.ReLU(),
#                nn.Linear(100,10)
#                
#                )
#        
#    def forward(self, x):
#        x = x.view(-1,784)
#        return self.model(x)
#    
#    def hooktest(self, x, i):
#        # x is the network
#        print('Layer is: ',x)
#        # i is the input data
#        print('Data contains: ',i)
#        return
#    
#    
#net = FNN()
net = FNN(100,100)




"""
    Loss and optimizer
"""

criterion = nn.CrossEntropyLoss()
#optimizer = noisyKFAC(net.parameters(),
#                      net.modules(),
#                      num_examples=60000,
#                      t_inv= 100,
#                      t_stats=10,
#                      lr = 1e-3,
#                      beta = 0.99,
#                      prior_var = 0.4,
#                      kl_weight = 1)

# nice performance
optimizer = noisyKFAC(net.parameters(),
                      net.modules(),
                      num_examples=1000,
                      t_inv= 10,
                      t_stats=10,
                      lr = 1e-3,
                      beta = 0.99,
                      prior_var = 1,
                      kl_weight = 1)



"""
    Training the network
"""



for epoch in range(50):
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for i, data in enumerate(trainLoader, 0):

        inputs, labels = data
        inputs, labels = Variable(inputs), Variable(labels)
        
        optimizer.zero_grad()
        
        optimizer.sample()
        
        outputs = net(inputs)
    
        loss = criterion(outputs, labels)
        #Initialized parameters' gradients are None until you do
        #loss.backward() and start computing gradients
        loss.backward()
        optimizer.step()
        
        # Print statistics
        running_loss += loss.data[0]
        if i % 20 == 19:
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 20))
            running_loss = 0.0
            
    #scheduler.step()
    

print('Finished training')





# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
    optimizer.sample(training = False)
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))









for name, p in net.named_parameters():
    print('name', name)
    print('p', p)
    














"""
    Train network
"""

# forward pass
datiter = iter(trainLoader)

inputs, labels = datiter.next()
inputs, labels = Variable(inputs), Variable(labels)

outputs = net(inputs)

# Test sampling function
optimizer.sample()

loss = criterion(outputs, labels)
#Initialized parameters' gradients are None until you do
#loss.backward() and start computing gradients
loss.backward()
optimizer.step()

# print weights
params = list(net.parameters())

params[0]

plt.hist(params[0].data.view(-1), bins = 50)

inputs.view(-1,784)
