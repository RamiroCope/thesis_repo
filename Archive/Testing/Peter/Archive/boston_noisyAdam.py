import torch
from torch.utils.data import DataLoader
from data.boston import boston


"""
    Load data
"""

batch_size = 35

# train data
trainset = boston()
trainLoader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = boston(train = False)
testLoader = DataLoader(testset, batch_size=batch_size, shuffle=False)


# test data loader
datiter = iter(trainLoader)

inputs, labels = datiter.next()
