import torch
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
from torch.autograd import Variable
from torch.autograd import grad as gradient

p = Variable(torch.Tensor(100,100).uniform_(-0.1,0.1),requires_grad=True)

local_var = 1
global_var = 1

# sample gamma(1/2, 1/local_var**2) using shape scale parameterization
lambda_0 = np.float32(np.random.gamma(5, local_var**2, size=p.size()))

lambda_G = np.float32(np.random.gamma(5, global_var**2, 1))
#lambda_G = Variable(torch.from_numpy(lambda_G))

local_scale = np.float32(np.random.gamma(5, 1/lambda_0, p.size()))
local_scale = Variable(torch.from_numpy(local_scale))

global_scale = np.float32(np.random.gamma(5, 1/lambda_G, 1))
global_scale = Variable(torch.from_numpy(global_scale))

local_scale = 1/local_scale
global_scale = 1/global_scale

# log p(gamma_sample)
#log_pgamma = ((group['alpha'] - 1)*torch.log(gamma_sample) - group['beta']*gamma_sample).sum()

# log p(w), p ~ N(0, local^-1*global^-1)
log_pw = (-0.5*p**2/(local_scale**2*global_scale**2)).sum()

log_prior = log_pw # + log_pgamma

dw_log_pw = gradient(log_prior, p, create_graph = True)[0]
dwdw_log_pw = gradient(dw_log_pw.sum(), p, create_graph = True)[0]

print("local scale ", local_scale**2)
#print("first order gradient is ", dw_log_pw)
#print("second order gradient is ", dwdw_log_pw)

plt.hist(1/((lambda_0+1e-6).flatten()),bins=20,edgecolor='black',normed=True)
plt.show()
plt.hist(local_scale.data.view(-1).numpy(),edgecolor='black',bins=20,normed=True)
plt.show()

inv_gam = stats.invgamma.rvs(a = 1.1, scale = 1, size=10000)
plt.hist(inv_gam,edgecolor='black',bins=20,normed=True)

#x = np.linspace(np.min(p.data.view(-1).numpy()),np.max(p.data.view(-1).numpy()),5000)
#x = np.linspace(np.min(lambda_0.flatten()),np.max(lambda_0.flatten()),5000)
#pdf = stats.gamma.pdf(x, a=0.5, scale=2)
##pdf = stats.norm.pdf(x, np.mean(mus), np.mean(sigmas))
#plt.hist(lambda_0.flatten(),bins=20,edgecolor='black',normed=True)
##plt.plot(x,pdf)
#plt.plot(x,pdf)
#plt.show()

