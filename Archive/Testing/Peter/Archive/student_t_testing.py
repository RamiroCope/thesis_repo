import numpy as np
import torch
from torch.autograd import Variable
import matplotlib.pyplot as plt
import scipy.stats as stats
from torch.autograd import grad as gradient
from torch.distributions import Normal

p = Variable(torch.Tensor(100,100).uniform_(-1,1), requires_grad=True)

a = 2
b = 1

local_scale = np.float32(stats.invgamma.rvs(a = a, scale = b, size = p.size()))

kappa = 1/(1+1/local_scale**2)

#x = np.linspace(stats.beta.ppf(0.01, 0.5, 0.5), stats.beta.ppf(0.99, 0.5, 0.5), 5000)
#pdf = stats.beta.pdf(x, a=0.5, b=0.5)

plt.hist(kappa.flatten(),bins = 40, edgecolor='black',normed=True)
#plt.plot(x, pdf)
plt.show()

local_scale = Variable(torch.from_numpy(local_scale))

# normal dist
gauss = Normal(mean=0, std = local_scale)

log_pw = (-0.5 * p**2 / local_scale**2).sum()
log_gauss = gauss.log_prob(p)

dw_log_pw = gradient(log_pw, p, create_graph = True)[0]
dw_log_gauss = gradient(log_gauss.sum(), p, create_graph = True)[0]
dwdw_log_pw = gradient(dw_log_pw.sum(), p, create_graph = True)[0]

print("log_pw is ", log_pw)
print("log_gauss is ", log_gauss.sum())
print("dw_log_pw is ", dw_log_pw.sum())
print("dwdw_log_pw is ", dwdw_log_pw.sum())

points = local_scale.data.view(-1).numpy()[np.where(local_scale.data.view(-1).numpy() < 1000)]

#plt.hist(lambda0.flatten(),bins = 40, edgecolor='black',normed=True)
#plt.show()
#
#print(np.mean(lambda0.flatten()))
#print(np.var(lambda0.flatten()))
#
#quantiles = np.linspace(stats.gamma.ppf(0.01, a=0.5, scale=1), stats.gamma.ppf(0.99, a = 0.5, scale=1), 5000)
#quantiles_inv = np.linspace(stats.invgamma.ppf(0.01, 0.5, 1), stats.invgamma.ppf(0.99, 0.5, 1), 5000)
#
#gam = stats.gamma.pdf(quantiles, a = 0.5, scale = 1)
#inv_gam = stats.invgamma.pdf(quantiles_inv, a = 0.5, scale = 1)
#
#plt.plot(quantiles,gam)
#plt.show()
#plt.hist(lambda0.flatten(),normed=True,bins=50,edgecolor='black')
#plt.plot(quantiles_inv,inv_gam)
#plt.show()