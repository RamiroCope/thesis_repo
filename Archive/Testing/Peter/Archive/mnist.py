from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_svmlight_file
from IPython.display import clear_output
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader
from torch.autograd import Variable
from urllib.request import urlopen
#from VProp_ver4 import VProp as Vprop
from Optimizers.VProp_ver4 import VProp as Vprop
#from Vprop1 import Vprop1 as Vprop
import torchvision
import torchvision.transforms as transforms

import subprocess
import torch

import numpy as np
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions import Normal


"""
    Utility Functions
"""

def create_dataloaders(X, y, test_size, train_batch_size = 1,
                               train_shuffle = True, standardize=None):
    """
    Desc: This function creates a train and test pytorch DataLoader for
          regression datasets.
          
    INPUT: X, y        : Numpy type X matrix and y targets
           test_size   : Float between 0 and 1
           shuffle     : Boolean True or False
           standardize : True to scale input features, None by default
           
    OUTPUT: Pytorch Train and Test DataLoader
    """
    assert(test_size <= 1 and test_size >= 0)
    
    if standardize:
        # Standardize X matrix 
        X = preprocessing.scale(X)
    
    X, Xtest, y, ytest = train_test_split(X, y, test_size=test_size)
    
    # Merging
    train_data = np.c_[ X, y ]
    test_data  = np.c_[ Xtest, ytest ]
    
    # Convert to Floats
    train_data = torch.from_numpy(train_data).float()
    test_data  = torch.from_numpy(test_data).float()
    
    # Create Data Loaders
    trainLoader = DataLoader(train_data, batch_size=train_batch_size, shuffle=train_shuffle)
    testLoader  = DataLoader(test_data, batch_size=len(Xtest))
    
    return trainLoader, testLoader



"""
    Train & Test Utility Class
"""

class Train_Test(object):
    def __init__(self, model, compute_loss, trainLoader, testLoader, optimizer, 
                                          precision, Vprop=False, plot=False, test=False):
        self.model             = model
        self.compute_loss      = compute_loss
        self.trainLoader       = trainLoader
        self.testLoader        = testLoader
        self.optimizer         = optimizer
        self.precision         = precision
        self.Vprop             = Vprop
        self.plot              = plot
        self.test              = test
        self.num_batches       = len(trainLoader)
        self.writer            = SummaryWriter()
        self.gradW_history     = []
        self.gradB_history     = []
        self.LL_loss_history   = []
        self.KL_loss_history   = []
        self.ELBO_history      = []
        self.x_axis            = []
        self.test_loss_history = []
        self.precision = precision
        self.p_dist = Normal(0, np.sqrt(1.0/self.precision))
        
        if plot:
            plt.figure()
    
    
    def run(self, epoch):
        for batch_num, train_data in enumerate(self.trainLoader):
            
            # x-axis index
            self.time = batch_num + (epoch*self.num_batches)
            
            #x, y = train_data[:,0:-1], train_data[:,-1]
            #x, y = Variable(x), Variable(torch.LongTensor(y.numpy()))
            x, y = train_data
            x, y = Variable(x), Variable(y)
            
            # Sample Variational Posterior
            if self.Vprop:
                params_dict = self.optimizer.sample()
            
            # forward pass
            y_pred  = self.model(x)
#            self.optimizer.zero_grad()
            
            # extract diagnostics
            lqw, lpw = 0.0,0.0
            for i,p in enumerate(self.model.parameters()):
                mu, sigma, grad = params_dict["mu{0}".format(i)],\
                                  params_dict["sigma{0}".format(i)],\
                                  params_dict["grad{0}".format(i)]
                # Tensorboard          
                self.writer.add_histogram('train/mu{}'.format(i), mu.view(-1), self.time)
                self.writer.add_histogram('train/sigma{}'.format(i), sigma.sqrt().view(-1), self.time)
                self.writer.add_scalar('train/grad{}'.format(i), torch.norm(grad, p=2), self.time)
                
                # Layer 1: for W matrix
                if i == 0:
                    self.muW = mu.view(-1)
                    self.sigmaW = sigma.sqrt().view(-1)
                    self.gradW_history += [torch.norm(grad, p=2)]
                # Layer 1: for Bias vector
                if i == 1:
                    self.muB = mu.view(-1)
                    self.sigmaB = sigma.sqrt().view(-1)
                    self.gradB_history += [torch.norm(grad, p=2)]
                    
                # KL Divergence terms    
                q_dist = Normal(mu, sigma.sqrt())
                lqw += q_dist.log_prob(p.data).sum()    
                lpw += self.p_dist.log_prob(p.data).sum()
            
            # compute loss, get gradients
            if self.Vprop:
                # ELBO loss function
                LL_loss = self.compute_loss(y_pred, y)
                KL_loss = (lqw - lpw)/self.num_batches
                ELBO    = LL_loss + KL_loss
                self.optimizer.zero_grad()
                ELBO.backward()
                
                # plot variables
                self.writer.add_scalar('train/LL',   LL_loss.data[0], self.time)
                self.writer.add_scalar('train/KL',   KL_loss, self.time)
                self.writer.add_scalar('train/ELBO', ELBO, self.time)
                self.LL_loss_history += [LL_loss.data[0]]
                self.KL_loss_history += [KL_loss]
                self.ELBO_history    += [ELBO]
            else:
                # log likelihood loss function
                LL_loss = self.compute_loss(y_pred, y)
                LL_loss.backward()
                
                #plot variables
                self.writer.add_scalar('train/LL',   LL_loss.data[0], self.time)
                self.LL_loss_history += [LL_loss.data[0]]
                
                
            # update weights
            self.optimizer.step()
            
            # eval with test data
            Train_Test.test(self)
            
            # plot
            self.x_axis += [batch_num + (epoch*self.num_batches)]
            Train_Test.plot(self)
            
            print('====> Epoch: ', epoch)
            print('====> LL loss: {:.4f}'.format(LL_loss.data[0]))            
            print('====> KL loss: {:.4f}'.format(KL_loss))            
         
    
    def test(self):
        """
        Test Function
        """
        #x, y = self.testLoader.dataset[:,:-1], self.testLoader.dataset[:,-1]
        x, y = self.testLoader
        x, y = Variable(x), Variable(torch.LongTensor(y.numpy()))
        
        self.optimizer.sample(training = False)
        
        # forward pass
        y_pred = self.model(x)
        test_loss = self.compute_loss(y_pred, y)
        
        #plot variables
        self.writer.add_scalar('test', test_loss.data[0], self.time)
        self.test_loss_history += [test_loss.data[0]]
    
        print('====> Test set loss: {:.4f}'.format(test_loss.data[0]))
        
    
    def plot(self):
        """
        Plot Function
        """
        clear_output(wait=True)
        fig, axes = plt.subplots(nrows=5, ncols=2)
        fig.tight_layout() 
        
        plt.subplot(5,2,1)
        plt.title('LL Loss')     
        plt.plot(self.x_axis, self.LL_loss_history, color="blue", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,2)
        plt.title('KL Loss')     
        plt.plot(self.x_axis, self.KL_loss_history, color="black", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        """
        plt.subplot(5,2,3)
        plt.title('Total Loss/ELBO')     
        plt.plot(self.x_axis, self.ELBO_history, color="blue", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        """
        if self.test:
            plt.subplot(5,2,4)
            plt.cla()
            plt.title('Test')
            plt.plot(self.x_axis, self.test_loss_history, color="red", linestyle="--")
            plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
            plt.grid('on')
        
        plt.subplot(5,2,5)
        plt.cla()
        plt.title('W Grad norm')
        plt.plot(self.x_axis, self.gradW_history, color="green", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,6)
        plt.cla()
        plt.title('Bias Grad norm')
        plt.plot(self.x_axis, self.gradB_history, color="green", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,7)
        plt.cla()
        plt.title('Mu W hist')
        plt.hist(self.muW, color="red", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,8)
        plt.cla()
        plt.title('Mu B hist')
        plt.hist(self.muB, color="red", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,9)
        plt.cla()
        plt.title('Sigma W hist')
        plt.hist(self.sigmaW, color="blue", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
        
        plt.subplot(5,2,10)
        plt.cla()
        plt.title('Sigma Bias hist')
        plt.hist(self.sigmaB, color="blue", linestyle="--")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.grid('on')
         
        plt.show()


"""
                            ___MAIN___
"""

"""
    1. Creating Data Loader
"""

transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
X = trainset.train_data
y = trainset.train_labels
#trainLoader = torch.utils.data.DataLoader(trainset, batch_size=25, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
#testLoader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=False)

trainLoader, testLoader = create_dataloaders(X, y, test_size=0.5, 
                                             train_batch_size = 10,
                                             standardize = True)


"""
    2. Defining MLP as in Vprop Paper (arxiv.org/pdf/1712.01038.pdf)
"""
class FNN(nn.Module):
    
    def __init__(self,I,H1,H2,O):
        super(FNN, self).__init__()
        self.I = I 
        self.O = O
        self.NN1 = nn.Linear(I,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,O)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x

#I   = X.shape[1]
I = 784        
O   = 10
net = FNN(I,100,100,O)



"""
    3. Optimizer
"""
precision = 1
compute_loss = nn.CrossEntropyLoss()

# Choose 1 optimzer
#optimizer = optim.RMSprop(net.parameters(), lr = 0.001)
optimizer = Vprop(net.parameters(), lr = 0.0001, precision=precision, alpha = 0.99)

print(optimizer)
print('Precision set to: ', precision)


"""
    4. Train, Test, Plot
"""
# Define desired logdir and port for Tensorboard
p = subprocess.Popen(["tensorboard","--logdir=runs", "--port=6011"])

train = Train_Test(net, compute_loss, trainLoader, testLoader, optimizer = optimizer,
                                         Vprop = True, precision = precision, plot=True, test=True)

epochs = 20

for epoch in range(epochs): 
    train.run(epoch)
    

p.terminate()
    
    
    
    
    
    