"""
FFN on MNIST data using pyTorch
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.nn.parameter import Parameter
from torch.optim import Optimizer
from torch.distributions import Normal




"""
Load data
"""


# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=50, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=False)

classes = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')


def imshow(img):
    #img = img / 2 + 0.5
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    #plt.imshow(npimg)
    

# get some random training images
dataiter = iter(trainLoader)
images, labels = dataiter.next()

# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(1)))


"""
    VProp Optimizer
"""


class VProp(Optimizer):
    def __init__(self, params, lr=1e-2, alpha=0.99, eps=1e-8, weight_decay=0, momentum=0, centered=False, precision = 1):
        defaults = dict(lr=lr, momentum=momentum, alpha=alpha, eps=eps, centered=centered, weight_decay=weight_decay)
        super(VProp, self).__init__(params, defaults)
        self.precision = precision
        
    def __setstate__(self, state):
        super(VProp, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('momentum', 0)
            group.setdefault('centered', False)
            
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for i in range(0,len(group['params'])-2,3):
                p = group['params'][i]
                mu = group['params'][i+1]
                sigma = group['params'][i+2]
                lambdas = torch.ones(sigma.size())*self.precision
                
                if p.grad is None:
                    continue
                
                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('RMSprop does not support sparse gradients')
                    
                #At initialization, state is empty
                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    state['square_avg'] = torch.zeros_like(p.data)

                #alpha = 1-beta in Vprop pseudocode
                alpha = group['alpha']

                state['step'] += 1

                sigma.data.mul_(alpha).addcmul_(1 - alpha, grad, grad)

                #avg = square_avg.sqrt().add_(group['eps'])
                var = sigma.data + lambdas
                avg = grad + lambdas*mu.data

                mu.data.addcdiv_(-group['lr'], avg, var)

        return loss



"""
Model graph
"""


class get_layer(nn.Module):
    """
        get_variable creates a random variable of desired size
    """
    def __init__(self, num_out, num_in, precision):
        super(get_layer, self).__init__()
        #Number of inputs
        self.num_in = num_in
        #Number of outputs
        self.num_out = num_out
        #Precision parameter
        self.precision = precision
        #Prior distribution
        self.p_dist = Normal(0, 1/self.precision)
        #Distribution probabilites
        self.lpw, self.lqw = 0.0, 0.0
        
        #Initialize W and b for mean and variance
        self.W = Parameter(torch.Tensor(num_out,num_in).uniform_(-0.01,0.01))
        self.W_mu , self.W_sigma = Parameter(torch.Tensor(num_out,num_in).uniform_(-0.01,0.01)),\
                                    Parameter(torch.Tensor(num_out,num_in).uniform_(0.99,1.01))

        self.b = Parameter(torch.Tensor(num_out).uniform_(-0.01,0.01)) 
        self.b_mu , self.b_sigma = Parameter(torch.Tensor(num_out).uniform_(-0.01,0.01)),\
                                    Parameter(torch.Tensor(num_out).uniform_(0.99,1.01))
    
    def sample_from_posterior(self, mu, sigma, num_in, num_out):
        if num_in == 1:
            eps= torch.Tensor(num_out).normal_()
            lambdas = torch.ones(num_out)*self.precision
            sample = mu.data + eps/torch.sqrt(sigma.data+lambdas)
        else:
            eps= torch.Tensor(num_out,num_in).normal_()
            lambdas = torch.ones(num_out,num_in)*self.precision
            sample = mu.data + eps/torch.sqrt(sigma.data+lambdas)
        return sample
    
    def forward(self, input, training = True):
        if not training:
            return F.linear(input, self.W, self.b)
        
        self.W.data = self.sample_from_posterior(self.W_mu, self.W_sigma, self.num_in, self.num_out)
        self.b.data = self.sample_from_posterior(self.b_mu, self.b_sigma, 1, self.num_out)
        
        #log p(w|0,1/precision)
        self.lpw = torch.sum(self.p_dist.log_prob(self.W.data)) +\
                        torch.sum(self.p_dist.log_prob(self.b.data))
                        
        #Define posterior distribution for the weights               
        #Note that the sigma tensors are defined as sigma = var - precision
        #So to define the posterior distribution as q ~ N(mu, sigma^2)
        #We have to add the precision
        self.q_W_dist = Normal(self.W_mu.data, self.W_sigma.data+self.precision)
        self.q_b_dist = Normal(self.b_mu.data, self.b_sigma.data+self.precision)
        
        #log q(w|mu, sigma)
        self.lqw = torch.sum(self.q_W_dist.log_prob(self.W.data)) +\
                        torch.sum(self.q_b_dist.log_prob(self.b.data))
        
        output = F.linear(input,self.W,self.b)
        W_mu_sigma, b_mu_sigma = self.get_posterior_params()
        return output, W_mu_sigma, b_mu_sigma
        
    def get_posterior_params(self):
        W_mu_sigma = (self.W_mu,self.W_sigma)
        b_mu_sigma = (self.b_sigma, self.b_mu)
        return W_mu_sigma, b_mu_sigma
    

class FNN(nn.Module):
    
    def __init__(self,H1,H2, precision):
        super(FNN, self).__init__()
        self.H1 = H1
        self.H2 = H2
        self.precision = precision
        #Normal distribution with mean 0 and variance = 1/precision, since precision = 1/sigma^2
        self.dist = Normal(0, 1/precision)
        
        #Layers
        self.l1 = get_layer(H1, 784, self.precision)
        self.l2 = get_layer(H2, H1, self.precision)
        self.l3 = get_layer(10, H2, self.precision)
        
    def forward(self,x, training = True):
        x = x.view(-1,784)
        if not training:
            x = self.l1(x, training)
            x = self.l2(x, training)
            x = self.l3(x, training)
            return x
        else:
            x, l1_W, l1_b = self.l1(x, training)
            x = F.relu(x)
            x, l2_W, l2_b = self.l2(x, training)
            x = F.relu(x)
            x, l3_W, l3_b = self.l3(x, training)
            params = [l1_W, l1_b, l2_W, l2_b, l3_W, l3_b]
            return x, params
        
    def get_lpw_lqw(self):
        lpw = self.l1.lpw + self.l2.lpw + self.l3.lpw
        lqw = self.l1.lqw + self.l2.lqw + self.l3.lqw
        return lpw, lqw

precision = 1
net = FNN(10,10,precision)


print(net)

net.parameters()

for name, param in net.named_parameters():
    print(name)


# Loss and optimizer    
nll = nn.CrossEntropyLoss()
#Note alpha = 1-beta from Vprop pseudocode
#Note lr = alpha in Vprop pseudocode
optimizer = VProp(net.parameters(),lr=0.0001, precision = precision, alpha = 0.8)


"""
Feeding a single digit through the net
"""

batch_size = 50
n_batches = trainset.train_data.shape[0]/batch_size

datiter = iter(trainLoader)
inputs, labels = datiter.next()

inputs, labels = Variable(inputs), Variable(labels)

optimizer.zero_grad()

outputs, params = net(inputs)

lpw, lqw = net.get_lpw_lqw()

loss = nll(outputs, labels) + (lqw - lpw)/n_batches

loss.backward()

optimizer.step()



"""
Train the model
"""

batch_size = 50
n_batches = trainset.train_data.shape[0]/batch_size


for epoch in range(40):
    
    running_loss = 0.0
    for i, data in enumerate(trainLoader, 0):
        
        inputs, labels = data
        
        inputs, labels = Variable(inputs), Variable(labels)
        
        optimizer.zero_grad()
        
        outputs, params = net(inputs)
        lpw, lqw = net.get_lpw_lqw()
        loss = nll(outputs, labels) + (lqw - lpw)/n_batches
        loss.backward()
        optimizer.step()
        
        # Print statistics
        running_loss += loss.data[0]
        if i % 40 == 39:
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 40))
            plt.hist(net.l1.W_sigma.data[0])
            running_loss = 0.0

print('Finished training')



"""
Test the model
"""

# Initial testing on a few images
test_dataiter = iter(testLoader)


for _ in range(5):

    test_img, labels = test_dataiter.next()
    
    img = torchvision.utils.make_grid(test_img)
    npimg = img.numpy()
    npimg = np.transpose(npimg, (1,2,0))
    plt.imshow(npimg)
    #plt.imshow(npimg)
    
    # print images
    #imshow(test_img)
    print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(1)))
    
    
    test_output = net(Variable(test_img), training = False)
    
    _, predicted = torch.max(test_output.data, 1)
    
    print('Predicted: ', ' '.join('%5s' % classes[predicted[j]] for j in range(1)))


# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
    outputs = net(Variable(images), training = False)
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))


