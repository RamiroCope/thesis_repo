"""
    Setup working environment
"""

import os, inspect, sys

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)


"""
    Import libraries
"""

import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import numpy as np
from Peter.Pytorch04.Optimizers.noisyAdam import noisyAdam
from torch import optim


"""
    Configure outer loop
"""


snr_list = [1, 2, 4, 6, 8, 10]
epoch_list = [10000]*len(snr_list)
train_losses = []
test_losses = []
train_losses_bayes = []
test_losses_bayes = []

# Define mean and stdv for the true distribution of the weights
w_mu = 0.0
w_std = 1

# Sample num_weights weights from true distribution
num_weights = 100
w_sample = np.random.normal(w_mu, w_std, num_weights).reshape(num_weights, 1).astype(np.float32)

"""
    Simulate feature matrix
"""

X_mu = 2
X_std = 1

# number of samples
num_obs = 128

X_train = np.random.normal(X_mu, X_std, size = (num_obs, num_weights)).astype(np.float32)
X_test = np.random.normal(X_mu, X_std, size = (num_obs, num_weights)).astype(np.float32)

# noise parameter
eps = np.random.normal(0, 1, num_obs).reshape(num_obs, 1).astype(np.float32)


for j, snr in enumerate(snr_list):
    
    # Signal to Noise Ratio
    SNR = snr
    # Generate targets based on SNR
    snr_factor_train = (np.matmul(X_train, w_sample)).std()/(np.sqrt(SNR)*eps.std())
    snr_factor_test = (np.matmul(X_test, w_sample)).std()/(np.sqrt(SNR)*eps.std())
    
    y_train = np.add( np.matmul(X_train, w_sample), snr_factor_train*eps ).astype(np.float32)
    y_test = np.add( np.matmul(X_test, w_sample), snr_factor_test*eps ).astype(np.float32)

    
    """
        Linear regression model with no bias
    """
       
    linear = nn.Linear(num_weights, 1, bias = False)
    linear_bayes = nn.Linear(num_weights, 1, bias = False)
    
    
    # loss
    criterion = nn.MSELoss()
    
    # N as in pseudo code for noisy Adam is the total number of observations
    N = X_train.shape[0]
    
    prior_var = 1
    
    # optimizer
    optimizer_bayes = noisyAdam(linear_bayes.parameters(), num_examples = N,
                              gamma_ex = 0,
                              lr = 1e-3,
                              prior_var = prior_var,
                              #df = df,
                              betas = (0.9, 0.999))

    #optimizer = optim.SGD(linear.parameters(), 1e-2)    
    optimizer = optim.Adam(linear.parameters(), lr = 1e-3)
    """
        Train linear regression
    """
    
    epochs = epoch_list[j]
    
    train_loss = []
    test_loss = []
    train_loss_bayes = []
    test_loss_bayes = []
    updates = []
        
    for epoch in range(epochs):
    
        running_loss = 0.0
        running_loss_bayes = 0.0
    
        mus = []
        sigmas = []
        fs = []
        weights = []
        
        inputs  = torch.from_numpy(X_train)
        targets = torch.from_numpy(y_train)
        
        params_dict = optimizer_bayes.sample(return_params=True)
        
        for i, p in enumerate(linear.parameters()):
            mu, sigma, f = params_dict["mu{0}".format(i)],\
                            params_dict["sigma{0}".format(i)],\
                            params_dict["f{0}".format(i)]
                        
            mus += [mu.view(-1).numpy()]
            sigmas += [sigma.view(-1).numpy()]
            fs += [f.view(-1).numpy()]
            weights += [p.data.view(-1).numpy()]
            
        
        
        outputs_bayes = linear_bayes(inputs)
        outputs = linear(inputs)
        
        loss_bayes = criterion(outputs_bayes, targets)
        loss = criterion(outputs, targets)
        
        optimizer.zero_grad()
        optimizer_bayes.zero_grad()
        
        loss.backward()
        loss_bayes.backward()
        
        optimizer_bayes.step()
        optimizer.step()
        
        running_loss += loss.item()
        running_loss_bayes += loss_bayes.item()
        
#        print('[%d] ====> train loss: %.3f' %
#                      (epoch + 1, running_loss))
        
        test_inputs = torch.from_numpy(X_test)
        test_targets = torch.from_numpy(y_test)
        
        optimizer_bayes.sample(training = False)
        
        test_outputs = linear(test_inputs)
        test_outputs_bayes = linear_bayes(test_inputs)
        
        test_loss_val = criterion(test_outputs, test_targets)
        test_loss_val_bayes = criterion(test_outputs_bayes, test_targets)
        
        train_loss += [loss.item()]
        test_loss += [test_loss_val.item()]
        
        train_loss_bayes += [loss_bayes.item()]
        test_loss_bayes += [test_loss_val_bayes.item()]
        
        updates += [epoch]
        
    train_losses += [train_loss[-1]]
    test_losses += [test_loss[-1]]
    print("=====> Train loss: ", train_loss[-1])   
    print("=====> Test loss: ", test_loss[-1])
    
    train_losses_bayes += [train_loss_bayes[-1]]
    test_losses_bayes += [test_loss_bayes[-1]]
    print("=====> Train loss Bayes: ", train_loss_bayes[-1])   
    print("=====> Test loss Bayes: ", test_loss_bayes[-1])

 
#print("Estimated weights: ", np.squeeze(mus))
#print("True weights: ", w_sample)


#plt.hist(mus, bins = 10,edgecolor='black')
#plt.hist(w_sample, bins = 10,edgecolor='black')
#plt.hist(fs, bins = 40,edgecolor='black')
#plt.show()

#    print('=============== PLOTTING ===============')
#
#    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,5))
#    fig.tight_layout(h_pad = 5, w_pad = 5)
#    
#    plt.subplot(2,2,1)     
#    plt.plot(updates, train_loss)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("SGD training loss")
#    
#    
#    plt.subplot(2,2,2)
#    plt.plot(updates, test_loss)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("SGD test loss")
#    
#    
#    plt.subplot(2,2,3)     
#    plt.plot(updates, train_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("train loss")
#    plt.title("Noisy Adam training loss")
#    
#    
#    plt.subplot(2,2,4)
#    plt.plot(updates, test_loss_bayes)
#    plt.xlabel("Epochs"), plt.ylabel("test loss")
#    plt.title("Noisy Adam test loss")
#    
#    plt.show()
    
    
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,7))
fig.tight_layout(h_pad = 5, w_pad = 5)

plt.subplot(2,2,1)
#plt.plot(updates, train_loss) # plot something
plt.hist([mus, w_sample], bins = 10, edgecolor='black', normed = True)
plt.xlabel("mu")
plt.title("mu histogram of variational posterior")
plt.legend(['Estimated means','true weights'])

plt.subplot(2,2,2)

plt.hist(sigmas, bins = 10, edgecolor='black', normed = True)
plt.xlabel("standard deviation")
plt.title("standard deviation histogram of variational posterior")
plt.legend(['Layer 1','Layer 2','Layer 3'])

plt.subplot(2,2,3)

plt.hist([weights, w_sample], bins = 10, edgecolor='black', normed = True)
#x = np.linspace(np.min(weights),np.max(weights),5000)
#pdf_t = stats.t.pdf(x, df=df)
#plt.plot(x,pdf_t)
plt.xlabel("weights")
plt.title("histogram of all weights in BNN")
plt.legend(['Trained weights', 'True weights'])

# update canvas immediately
plt.show()

    
plt.plot(snr_list[0:], train_losses[0:])
plt.plot(snr_list[0:], test_losses[0:])
plt.plot(snr_list[0:], train_losses_bayes[0:], '-.')
plt.plot(snr_list[0:], test_losses_bayes[0:], '-.')
#plt.plot(snr_list[0:], [4]*len(snr_list[0:]), '--')

plt.xlabel("SNR"), plt.ylabel("loss")
plt.title("Noisy Adam train/test loss for increasing number of observations")
plt.legend(["Train loss", "Test loss", "BNN - Train loss", "BNN - Test loss"])
plt.show()