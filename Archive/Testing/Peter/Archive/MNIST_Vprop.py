"""
    Set working directory
"""
import os
# Alipes PC
os.chdir("C:\\repos\\thesis_repo\\Testing\\Peter")
# Home PC
os.chdir("D:\\DTU MMC\\github repos\\thesis_repo\\Testing\\Peter")


"""
    Import libraries
"""

import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
from IPython.display import Image, display, clear_output
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from Optimizers.Vprop import Vprop



"""
    Load data
"""



batch_size = 100

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = torchvision.datasets.MNIST(root='./data', train=True, download=False, transform=transform)
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)

# test data
testset = torchvision.datasets.MNIST(root='./data', train=False, download=False, transform=transform)
testLoader = torch.utils.data.DataLoader(testset, batch_size=100, shuffle=False)



"""
    Define the neural net
"""



class FNN(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(784,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,10)
        
    def forward(self,x):
        x = x.view(-1,784)
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x

# a 100,20 NN with gamma_ex = 1e-2 and lr = 1e-3 achieves 90% after 100 epochs

# initialize network    
net = FNN(100,100)



"""
    Define loss and optimizer
"""


# loss
criterion = nn.CrossEntropyLoss()

# prior precision = 1/sigma^2

precision = 10
# optimizer
optimizer = Vprop(net.parameters(),
                      alpha = 0.9,
                      precision = precision,
                      lr = 1e-3,
                      momentum = 0)

# setup adaptive learning rate
#scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.9)
# setup adaptive learning rate
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[1,3,5,7,9], gamma=0.1)


"""
    Training the network
"""

max_epochs = 10

num_batches = trainset.train_data.shape[0]/batch_size


train_loss = []
test_loss = []
updates = []

for epoch in range(max_epochs):
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for batch, data in enumerate(trainLoader, 0):
        
        inputs, labels = data
        inputs, labels = Variable(inputs), Variable(labels)
        
        optimizer.zero_grad()
        
        optimizer.sample()
        
        outputs = net(inputs)
    
        loss = criterion(outputs, labels)
        #Initialized parameters' gradients are None until you do
        #loss.backward() and start computing gradients
        loss.backward()
        optimizer.step()
        
        # Print statistics
        running_loss += loss.data[0]

        if batch % 100 == 99:
            print('[%d, %5d] ====> train loss: %.3f' %
                  (epoch + 1, batch + 1, running_loss / 100))
            updates += [batch + (epoch*num_batches)]
            train_loss += [running_loss/100]
            plt.plot(updates, train_loss) # plot something
    
            # update canvas immediately
            plt.show()
            running_loss = 0.0
    
            for data in testLoader:
                images, labels = data
                optimizer.sample(training = False)
                outputs = net(Variable(images))
                test_loss_val = criterion(outputs, Variable(labels))
                test_loss += [test_loss_val.data[0]]
                
            print('[%d, %5d] ====> test loss: %.3f' %
                  (epoch + 1, batch + 1, test_loss_val.data[0]))
            
    #scheduler.step()
    

print('Finished training')





# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    images, labels = data
    outputs = net(Variable(images))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))

