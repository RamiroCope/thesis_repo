import torchvision
import torchvision.transforms as transforms
import torch
import matplotlib.pyplot as plt
from IPython.display import Image, display, clear_output
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.nn.parameter import Parameter
from torch.optim import Optimizer
from torch.distributions import Normal
import torch
from data.a1a import a1a



"""
    Load data
"""



train_batch_size = 16#1605
test_batch_size = 1#30956

# transformer used to transform images to tensors
transform = transforms.Compose([transforms.ToTensor()])

# train data
trainset = a1a()
trainLoader = torch.utils.data.DataLoader(trainset, batch_size=train_batch_size, shuffle=True)

# test data
testset = a1a(train = False)
testLoader = torch.utils.data.DataLoader(testset, batch_size=test_batch_size, shuffle=False)



"""
    The optimizer
"""



class Vprop(Optimizer):
    def __init__(self,
                 params,
                 lr=1e-3,
                 alpha=0.99,
                 eps=1e-8,
                 weight_decay=0,
                 momentum=0,
                 centered=False,
                 precision = 1):
        
        defaults = dict(lr=lr,
                        momentum=momentum,
                        alpha=alpha,
                        eps=eps,
                        centered=centered,
                        weight_decay=weight_decay)
        
        super(Vprop, self).__init__(params, defaults)
        
        self.precision = precision
        self.posterior_params = {}          

        
    def __setstate__(self, state):
        super(Vprop, self).__setstate__(state)
        
        for group in self.param_groups:
            group.setdefault('momentum', 0)
            group.setdefault('centered', False)

            
    def sample(self, training = True):
        for group in self.param_groups:
            for i, p in enumerate(group['params']):              
                
                # State initialization
                state = self.state[p]
                
                if len(state) == 0:
                    state['step'] = 0                    
                    # s as given in the pseudo code
                    state['square_avg'] = torch.Tensor(p.size()).uniform_(0.25*self.precision, 0.5*self.precision)#torch.zeros_like(p.data)
                    # value initialization for mu parameter
                    init_val = 1.0/np.sqrt(p.size(0))
                    # mu - the mean of the variational posterior
                    state['mu'] = torch.Tensor(p.size()).uniform_(-init_val, init_val)
                    
                square_avg = state['square_avg']
                mu = state['mu']
                
                if not training:
                    p.data = mu
                    return
                
                # noise term
                e = torch.randn(p.size())
                # standard deviation of variational posterior
                sigma = 1.0/((square_avg + self.precision).sqrt())
                
                # Pseudo code line 1 - sample parameters
                p.data = mu.add(e*sigma)
                
        return

                
    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for i,p in enumerate(group['params']):
                
                if p.grad is None:
                    continue
                
                # Pseudo code line 2 - get gradient
                grad = p.grad.data
                
                # get state
                state = self.state[p]
                
                square_avg = state['square_avg']; 
                mu = state['mu']
                alpha = group['alpha']
                
                state['step'] += 1
                
                # Pseudo code line 3 - update s
                square_avg.mul_(alpha).addcmul_(1 - alpha, grad, grad)
                
                num = grad.add(self.precision*mu)
                denom = square_avg + self.precision
                
                # Pseudo code line 4
                mu.addcdiv_(-group['lr'], num, denom)

        return




"""
    Define the neural net
"""



class FNN(nn.Module):
    
    def __init__(self,H1,H2):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(123,H1)
        self.NN2 = nn.Linear(H1,H2)
        self.NN3 = nn.Linear(H2,2)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = F.relu(self.NN2(x))
        x = self.NN3(x)
        return x

# a 100,20 NN with gamma_ex = 1e-2 and lr = 1e-3 achieves 90% after 100 epochs

# initialize network    
net = FNN(10,10)



"""
    Alternate single layer neural net
"""


class FNN(nn.Module):
    
    def __init__(self,H1):
        super(FNN, self).__init__()
        self.NN1 = nn.Linear(123,H1)
        self.NN2 = nn.Linear(H1,2)
        
    def forward(self,x):
        x = F.relu(self.NN1(x))
        x = self.NN2(x)
        return x

net = FNN(10)



"""
    Define loss and optimizer
"""


# loss
criterion = nn.CrossEntropyLoss()


# prior precision = 1/sigma^2
precision = 10

# optimizer
optimizer = Vprop(net.parameters(), alpha = 0.9999, precision = precision, lr = 1e-4)

# setup adaptive learning rate
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[10,30], gamma=0.1)
scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.9)



"""
    Training the network
"""



for epoch in range(100):
    #print('============> Learning rate is: ',scheduler.get_lr())
    running_loss = 0.0
    #enumerate basically indexes the iterations, so i keeps track
    #of the index. The ",0" part is just ensuring we start in zero every
    #time the for loop runs
    for i, data in enumerate(trainLoader, 0):
        
        inputs, labels = data
        inputs, labels = Variable(inputs), Variable(labels)
        
        optimizer.zero_grad()
        
        optimizer.sample()
        
        outputs = net(inputs)
    
        loss = criterion(outputs, labels)
        #Initialized parameters' gradients are None until you do
        #loss.backward() and start computing gradients
        loss.backward()
        optimizer.step()
        
        # Print statistics
        running_loss += loss.data[0]
        if i % 6 == 5:
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 6))
            running_loss = 0.0
            
    #scheduler.step()
    

print('Finished training')





# Test on entire test set
correct = 0
total = 0

for data in testLoader:
    inputs, labels = data
    optimizer.sample(training = False)
    outputs = net(Variable(inputs))
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network: %d %%' % (
    100 * correct / total))

